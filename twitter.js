/* SPDX-FileCopyrightText: © 2024 promising future digital media llc. All rights reserved. <admin@parleystar.com> */
/* SPDX-License-Identifier: Mozilla Public License 1.1 (MPL-1.1) */
console.log('loading twitter', scbt);

scbt.f.chat_clean_twitter = function(obj, elem){
  var elemArr = [];
  var str = null;
  var special = false;
  var stampArr = new Date().toLocaleTimeString().replace(/ /g, '').split(':'); // = 11:34:03AM  ['12', '11', '42PM']
  var timestamp = stampArr[0] + ':' + stampArr[1] + stampArr[2].slice(2);

  // HOST EVENT
  var elemArr = elem.getElementsByClassName('arena-host');
  if (elemArr[0]) {
    special = true;
    var elem2Arr = elem.getElementsByClassName('arena-host');
    obj.username = 'anonymous';
    obj.message = 'just hosted with viewers';
    if (elem2Arr[0] && elem2Arr[0].textContent) { 
      obj.username = elem2Arr[0].textContent;
    }
    var elemArr3 = elem.getElementsByClassName('arena-host');
    if (elemArr3[0] && elemArr3[0].textContent) { 
      obj.message = elemArr3[0].textContent;
    }
    obj.timestamp = timestamp;
    obj.anevent = 1;
  }

  // SUB EVENT
  var elemArr = elem.getElementsByClassName('arena-sub');
  if (elemArr[0]) {
    special = true;
    obj.username = 'anonymous';
    obj.message = 'just subscribed for 1 month';
    var elem2Arr = elem.getElementsByClassName('arena-host');
    if (elem2Arr[0] && elem2Arr[0].textContent) { 
      obj.username = elem2Arr[0].textContent;
    }
    var elem3Arr = elem.getElementsByClassName('arena-host');
    if (elem3Arr[0] && elem3Arr[0].textContent) { 
      obj.message = elem3Arr[0].textContent;
    }
    obj.timestamp = timestamp;
    obj.sub = 1;
    obj.newSub = 1;
    obj.anevent = 1;
  }

  // GIFT EVENT
  var elemArr = elem.getElementsByClassName('arena-host');
  if (elemArr[0]) {
    special = true;
    obj.username = 'anonymous';
    obj.message = 'has gifted 1 subscription to the community';
    var elem2Arr = elem.getElementsByClassName('arena-host');
    if (elem2Arr[0] && elem2Arr[0].textContent) { 
      obj.username = elem2Arr[0].textContent;
    }
    var elem3Arr = elem.getElementsByClassName('arena-host');
    if (elem3Arr[0] && elem3Arr[0].textContent) { 
      obj.message = elem3Arr[0].textContent;
    }
    obj.timestamp = timestamp;
    obj.sub = 1;
    obj.gifter = 1;
    obj.newSub = 1;
    obj.anevent = 1;
  }

  // CLIP EVENT
  var elemArr = elem.getElementsByClassName('arena-clip');
  if (elemArr[0]) {
    special = true;
    obj.username = 'anonymous';
    obj.message = 'made a nice clip.';

    var elem2Arr = elemArr[0].getElementsByClassName('arena-clip');

    if (elem2Arr[0] && elem2Arr[0].textContent) { 
      obj.username = elem2Arr[0].textContent;
    }
    var elem3Arr = elemArr[0].getElementsByClassName('arena-clip');
    if (elem3Arr[0] && elem3Arr[0].textContent) { 
      obj.message = elem3Arr[0].textContent;
    }
    var imgElemArr = elemArr[0].getElementsByTagName('img');
    if (imgElemArr[0]) {
      obj.message = obj.message + ' img ' + imgElemArr[0].src;
    }
    obj.timestamp = timestamp;
    obj.anevent = 1;
  }

  // regular chat
  if (special === false) {
    var elemArr = elem.querySelectorAll('.arena-chat');
    if (elemArr[0] && elemArr[0].textContent) { 
      obj.timestamp = elemArr[0].textContent;
    }

    var elemArr = elem.getElementsByClassName('chat-entry-username');
    if (elemArr[0]) { } else {
      var elemArr = elem.getElementsByClassName('chat-message-identity');
    }
    
    obj.username = null;
    if (elemArr[0] && elemArr[0].textContent) { 
      obj.username = elemArr[0].textContent;
      obj = scbt.f.get_obj_cleaned_username_from_obj(obj);
    }

    var elemArr = elem.getElementsByClassName('chat-entry-content');
    obj.message = null;
    if (elemArr[0] && elemArr[0].textContent) {
      obj.message = elemArr[0].textContent;
      obj = scbt.f.get_obj_cleaned_message_from_obj(obj);
      if (obj.message.indexOf('@') > -1) {
        obj.mention = 1;
      }
      if (obj.message.indexOf('#') > -1) {
        obj.hashtag = 1;
      }

      if (obj.message == null || obj.message.trim() === '') { } else { 
        obj.message = obj.message.replace(/(?:\r\n|\r|\n)/g, '');
        obj.message = obj.message.split("\t").join("");
        obj.message = obj.message.split("\"").join("");
        obj.message = obj.message.split("\'").join("");
      }
    }

    if (elem.classList.contains('twitters') ) {
      obj.donation = 1;
      obj.anevent = 1;
    }

    obj.sub = scbt.f.get_binary_role_from_chat_message(elem, 'sub'); // 0 or 1
    obj.moderator = scbt.f.get_binary_role_from_chat_message(elem, 'moderator');
    obj.founder = scbt.f.get_binary_role_from_chat_message(elem, 'founder');
    obj.verified = scbt.f.get_binary_role_from_chat_message(elem, 'verified');
    obj.og = scbt.f.get_binary_role_from_chat_message(elem, 'og');
    obj.owner = scbt.f.get_binary_role_from_chat_message(elem, 'owner');
    obj.gifter = scbt.f.get_binary_role_from_chat_message(elem, 'gifter');
    obj.staff = scbt.f.get_binary_role_from_chat_message(elem, 'staff');
  }

  if (obj.username && obj.message) {
    if (special === true) {
      obj.itemid = Date.now();
    } else {
      if (elem.dataset.chatEntry) {
        obj.itemid = elem.dataset.chatEntry;
      } else {
        var um = obj.message;
        obj.itemid = obj.username + um.substring(0, 6);  
      }
    }
  }

  elem = elemArr = elem2Arr = elemArr3 = imgElemArr = str = special = stampArr = timestamp = um = null; return obj;
}


scbt.f.chat_make_decisions_twitter = function(obj, elem, settings){
  
  if (settings.sz1b1twitter === true) {
    var elemArr = elem.getElementsByTagName('img');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    var elemArr = elem.getElementsByTagName('i');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    var elemArr = elem.getElementsByTagName('svg');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    var elemArr = elem.getElementsByTagName('canvas');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    elemArr = elm2 = null;
  }

  // Highlight sub messages in chat in this hex colour h1
  if (settings.sz2c1twitter != '#000000') {
    if (obj.sub === 1) {
      obj.isHighlighted = settings.sz2c1twitter;
    }
  }
  // Highlight gifter messages in chat in this hex colour h2
  if (settings.sz2c9twitter != '#000000') {
    if (obj.gifter === 1) {
      obj.isHighlighted = settings.sz2c9twitter;
    }
  }
  // Highlight VIP messages in chat in this hex colour h3 
  if (settings.sz2c5twitter != '#000000') {
    if (elem.classList.contains('vip')) {
      obj.isHighlighted = settings.sz2c5twitter;
    }
  }
  // Highlight founder messages in chat in this hex colour  
  if (settings.sz2c6twitter != '#000000') {
    if (obj.founder === 1) {
      obj.isHighlighted = settings.sz2c6twitter;
    }
  }
  // Highlight OG messages in chat in this hex colour  
  if (settings.sz2c7twitter != '#000000') {
    if (obj.og === 1) {
      obj.isHighlighted = settings.sz2c7twitter;
    }
  }
  // Highlight owner messages in chat in this hex colour  
  if (settings.sz2c8twitter != '#000000') {
    if (obj.owner === 1) {
      obj.isHighlighted = settings.sz2c8twitter;
    }
  }
  // Highlight moderator messages in chat in this hex colour  
  if (settings.sz2c2twitter != '#000000') {
      if (obj.moderator === 1) {
        obj.isHighlighted = settings.sz2c2twitter;
      }
  }
  // Mute owner/streamer messages in chat in dim text  
  if (settings.sz3b2 === true) {
      if (obj.owner === 1) {
        obj.isMuted = 1;
      }
  }
  // Hide owner/streamer messages in chat  
  if (settings.sz4b2 === true) {
      if (obj.owner === 1) {
        obj.isHidden = 1;
      }
  }
  if ( (obj.owner === 1) || (obj.moderator === 1) || (obj.sub === 1) || (obj.newSub === 1) || (obj.verified === 1) ) { } else {
    // scbtmuted4 Mute non moderator/sub messages in chat  
    if (settings.sz3b1 === true) {
      obj.isMuted = 1;
    }
    // scbthidden4 Hide non moderator/sub messages in chat 
    if (settings.sz4b1 === true) {
      obj.isHidden = 1;
    }
  }
  // scbtmuted3 Mute bot messages  
  if (settings.sz3b4 === true) {
    if (obj.username && obj.message) {
      if (obj.isBot === 1) {
        obj.isMuted = 1;
      }
    }
  }
  // scbthidden3 Hide bot  messages 
  if (settings.sz4b4 === true) {
    if (obj.username && obj.message) {
      if (obj.isBot === 1) {
        obj.isHidden = 1;
      }
    }
  }
  // Highlight mention messages in chat in this hex colour 
  if (settings.sz2c3twitter != '#000000') {
    if (obj.mention === 1) {
      obj.isHighlighted = settings.sz2c3twitter;
    }
  }
  // Highlight hashtag messages in chat in this hex colour  
  if (settings.sz2c4twitter != '#000000') {
    if (obj.hashtag === 1) {
      obj.isHighlighted = settings.sz2c4twitter;
    }
  }
  // Mute @ mention messages in chat in dim text  
  if (settings.sz3b3 === true) {
    if (obj.mention === 1) {
      obj.isMuted = 1;
    }
  }
  // Hide @ mention messages in chat 
  if (settings.sz4b3 === true) {
    if (obj.mention === 1) {
      obj.isHidden = 1;
    }
  }
  elem = settings = null;
  return obj;
}


scbt.f.style_with_obj_of_changes_twitter = function(obj){
  var css = '';
  console.log('style_with_obj_of_changes_twitter', obj);

  // Background of chat in this hex colour.
  if (obj.sz1c1twitter) {
    if (obj.sz1c1twitter == '#000000' || obj.sz1c1twitter == '') {
      css = css + ' body.scbt-twitter #chatroom { background-color: asdf !important; } ';
    }
    if (obj.sz1c1twitter == '#ffffff') {
      css = css + ' body.scbt-twitter #chatroom { background-color: transparent !important; } ';
    }
    if ( (obj.sz1c1twitter != '#ffffff') && (obj.sz1c1twitter != '#000000') && (obj.sz1c1twitter != '') ) {
      css = css + ' body.scbt-twitter #chatroom { background-color: ' + obj.sz1c1twitter + ' !important; } ';
    }
  }

  // User names in chat in this hex colour
  if (obj.sz1c2twitter) {
    if (obj.sz1c2twitter == '#000000' || obj.sz1c2twitter == '') {
      css = css + ' body.scbt-twitter .chat-entry-username { color: asdf !important; text-shadow: 0px; } ';
    }
    if (obj.sz1c2twitter == '#ffffff') {
      css = css + ' body.scbt-twitter .chat-entry-username { color: transparent !important; text-shadow: 0.3px 0.3px dimgray; } ';
    }
    if ( (obj.sz1c2twitter != '#ffffff') && (obj.sz1c2twitter != '#000000') && (obj.sz1c2twitter != '') ) {
      css = css + ' body.scbt-twitter .chat-entry-username { color:' + obj.sz1c2twitter + ' !important; text-shadow: 0.5px 0.5px darkslategrey; } ';
    }
  }

  // Highlights of chat in this colour
  if (obj.sz1c3twitter) {
    if (obj.sz1c3twitter != '#000000') {
      scbt.c.scbtBorderColor = obj.sz1c3twitter;
    } else {
      scbt.c.scbtBorderColor = '#ff0000';
    }
  }

  // Chat messages in this colour
  if (obj.sz1c4twitter) {
    if (obj.sz1c4twitter == '#000000' || obj.sz1c4twitter == '') {
      css = css + ' body.scbt-twitter .chat-entry-content { color: asdf !important; text-shadow: 0px; } ';
    }
    if (obj.sz1c4twitter == '#ffffff') {
      css = css + ' body.scbt-twitter .chat-entry-content { color: transparent !important; text-shadow: 0.3px 0.3px dimgray; } ';
    }  
    if ( (obj.sz1c4twitter != '#ffffff') && (obj.sz1c4twitter != '#000000') && (obj.sz1c4twitter != '') ) {
      css = css + ' body.scbt-twitter .chat-entry-content { color: ' + obj.sz1c4twitter + ' !important; text-shadow: 0.5px 0.5px darkslategrey; } ';
    }
  }

  // Chat font sizes
  if (obj.sz1n1twitter) {
    if ( Number(obj.sz1n1twitter) > 0) {
      var str = Number(obj.sz1n1twitter) + 'rem';
      css = css + ' body.scbt-twitter .chat-entry-content { font-size: ' + str + '; line-height: ' + Number(obj.sz1n1twitter)  + '; } ';
    } else {
      css = css + ' body.scbt-twitter .chat-entry-content { font-size: initial; line-height: unset; } ';
    }
  }

  // text only chat
  if (obj.sz1b1twitter === true) {
    css = css + ' body.scbt-twitter .text-xs.font-semibold.text-gray-400, body.scbt-twitter .chat-entry img, body.scbt-twitter .chat-entry svg, body.scbt-twitter .chat-entry i, body.scbt-twitter .chat-entry canvas { visibility: hidden !important; } ';
  }
  if (obj.sz1b1twitter === false) {
    css = css + ' body.scbt-twitter .text-xs.font-semibold.text-gray-400, body.scbt-twitter .chat-entry img, body.scbt-twitter .chat-entry svg, body.scbt-twitter .chat-entry i, body.scbt-twitter .chat-entry canvas { visibility: visible !important; } ';      
  }

  // Left handed screen. Flip screen so video is on the right and chat is on the left.
  if (obj.sz5b16 === true) {
    css = css + ' body.scbt-twitter #placeholder { flex-direction: row-reverse; } ';
  }
  if (obj.sz5b16 === false) {
    css = css + ' body.scbt-twitter #placeholder .relative.flex.h-full.w-full { flex-direction: initial; } ';
  }

  // Upside down chat screen. Newest chat is on the top, oldest chat is on the bottom 
  if (obj.sz5b17 === true) {
     css = css + ' body.scbt-twitter #placeholder { display: flex; flex-direction: column-reverse; } ';
  }
  if (obj.sz5b17 === false) {
    css = css + ' body.scbt-twitter #placeholder { flex-direction: initial; display: block; } ';
  }

  // mouseover hover enlarge
  if (obj.sz5b18 === true) {
    css = css + ' body.scbt-twitter #placeholder:hover { font-size: 250% !important; } body.scbt-twitter .chat-emote-container { height: 250%; width: 250%; } ';
  }
  if (obj.sz5b18 === false) {
    css = css + ' body.scbt-twitter #placeholder:hover { font-size: 100% !important; } body.scbt-twitter .chat-emote-container { height: asdf; width: asdf; } ';
  }

  if (css) {
    var head = document.getElementsByTagName('head')[0];
    var style = document.createElement('style');
    head.appendChild(style);
    style.type = 'text/css';
    if (style.styleSheet){
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }
  }
  obj = t = head = style = css = null; return false;
}


