/* SPDX-FileCopyrightText: © 2024 promising future digital media llc. All rights reserved. <admin@parleystar.com> */
/* SPDX-License-Identifier: Mozilla Public License 1.1 (MPL-1.1) */
console.log('loading rumble', scbt);

scbt.f.chat_clean_rumble = function(obj, elem){
  var elemArr = [];
  var str = null;
  var special = false;
  var stampArr = new Date().toLocaleTimeString().replace(/ /g, '').split(':'); // = 11:34:03AM  ['12', '11', '42PM']
  var timestamp = stampArr[0] + ':' + stampArr[1] + stampArr[2].slice(2);
  
  var classArr = elem.classList;
  if (classArr.contains('user-notice-line') || classArr.contains('donation') || classArr.contains('anevent') ) {
    obj.anevent = 1;
  }
  if (classArr.contains('sub') ) {
    obj.sub = 1;
  }
  if (classArr.contains('moderator') ) {
    obj.moderator = 1;
  }
  if (classArr.contains('founder') ) {
    obj.founder = 1;
  }
  if ( classArr.contains('vip') || classArr.contains('verified') ) {
    obj.verified = 1;
  }
  if (classArr.contains('owner') ) {
    obj.owner = 1;
  }
  if (classArr.contains('staff') ) {
    obj.staff = 1;
  }
  if (classArr.contains('gifter') ) {
    obj.gifter = 1;
  }

  // username
  elemArr = elem.getElementsByClassName('chat-history--username');
  if (elemArr[0] && elemArr[0].textContent) {
    str = elemArr[0].textContent;
  }
  
  if (!str) {
    elemArr = elem.getElementsByClassName('chat-history--rant-username');
    if (elemArr[0] && elemArr[0].textContent) { 
      str = elemArr[0].textContent;
    }
    elemArr = elem.getElementsByClassName('chat-history--notification-username');
    if (elemArr[0] && elemArr[0].textContent) { 
      str = elemArr[0].textContent;
    }
  }

  obj.username = null;
  if (str) {
    obj.username = str;
    obj = scbt.f.get_obj_cleaned_username_from_obj(obj);
  }

  str = null;
  // chat message
  var elemArr = scbt.f.get_arr_message_elems_from_parent_element(elem);
  if (elemArr[0] && elemArr[0].textContent) {
    str = elemArr[0].textContent;
  }

  if (!str) {
    elemArr = elem.getElementsByClassName('chat-history--rant-text');
    if (elemArr[0] && elemArr[0].textContent) { 
      obj.donation = 1;
      obj.anevent = 1;
      var elem2Arr = elem.getElementsByClassName('chat-history--rant-price');
      if (elem2Arr[0] && elem2Arr[0].textContent) { 
        str = elem2Arr[0].textContent + ' ' + str;
      }
      str = str + ' ' + elemArr[0].textContent;
    }
    var elem3Arr = elem.getElementsByClassName('chat-history--notification-text');
    if (elem3Arr[0] && elem3Arr[0].textContent) { 
      str = obj.username + ' ' + elem3Arr[0].textContent;
      obj.newSub = 1;
      obj.anevent = 1;
    }
  }

  obj.message = null;
  if (str) {
    obj.message = str;
    obj = scbt.f.get_obj_cleaned_message_from_obj(obj);
    if (obj.message.indexOf('@') > -1) {
      obj.mention = 1;
    }
    if (obj.message.indexOf('#') > -1) {
      obj.hashtag = 1;
    }

  if (obj.message == null || obj.message.trim() === '') { } else { 
    obj.message = obj.message.replace(/(?:\r\n|\r|\n)/g, '');
    obj.message = obj.message.split("\t").join("");
    obj.message = obj.message.split("\"").join("");
    obj.message = obj.message.split("\'").join("");
  }

  var elemArr = elem.getElementsByTagName('img');
  [].forEach.call(elemArr, function(imgElm) {
    var alt = imgElm.getAttribute('alt');
    if (alt) {
      var a = alt.toLowerCase().trim();
      if ( (a.indexOf('broadcaster') > -1) || (a.indexOf('owner') > -1) || (a.indexOf('admin') > -1) ) {
        obj.owner = 1;
      }
      if ( (a.indexOf('supporter') > -1) || (a.indexOf('supporter+') > -1) || (a.indexOf('sub') > -1) ) {
        obj.sub = 1;
      }
      if (a.indexOf('moderator') > -1) {
        obj.moderator = 1;
      }
    }
  });

  if (elem.classList.contains('vip') ) {
    obj.verified = 1;
  }
}

  if (obj.username && obj.message) {
    var um = obj.message;
    var itemId = elem.getAttribute('data-message-id'); // timeout here
    if (itemId) {
      obj.itemid = itemId;
    } else {
      obj.itemid = obj.username + um.substring(0, 6) + Math.floor(Math.random() * 100);  
    }
  }
  
	special = stampArr = str = elemArr = elemArr2 = elemArr3 = timestamp = imgs = alt = imgElm = imgElemArr = itemId = a = elem = um = null; return obj;
}


scbt.f.chat_make_decisions_rumble = function(obj, elem, settings){
  
  if (settings.sz1b1rumble === true) {
    var elemArr = elem.getElementsByTagName('img');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    var elemArr = elem.getElementsByTagName('i');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    var elemArr = elem.getElementsByTagName('svg');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    var elemArr = elem.getElementsByTagName('canvas');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    elemArr = elm2 = null;
  }

  // Highlight sub messages in chat in this hex colour h1
  if (settings.sz2c1rumble != '#000000') {
    if (obj.sub === 1) {
      obj.isHighlighted = settings.sz2c1rumble;
    }
  }
  // Highlight gifter messages in chat in this hex colour h2
  if (settings.sz2c9rumble != '#000000') {
    if (obj.gifter === 1) {
      obj.isHighlighted = settings.sz2c9rumble;
    }
  }
  // Highlight VIP messages in chat in this hex colour h3 
  if (settings.sz2c5rumble != '#000000') {
    if (elem.classList.contains('vip')) {
      obj.isHighlighted = settings.sz2c5rumble;
    }
  }
  // Highlight founder messages in chat in this hex colour  
  if (settings.sz2c6rumble != '#000000') {
    if (obj.founder === 1) {
      obj.isHighlighted = settings.sz2c6rumble;
    }
  }
  // Highlight OG messages in chat in this hex colour  
  if (settings.sz2c7rumble != '#000000') {
    if (obj.og === 1) {
      obj.isHighlighted = settings.sz2c7rumble;
    }
  }
  // Highlight owner messages in chat in this hex colour  
  if (settings.sz2c8rumble != '#000000') {
    if (obj.owner === 1) {
      obj.isHighlighted = settings.sz2c8rumble;
    }
  }
  // Highlight moderator messages in chat in this hex colour  
  if (settings.sz2c2rumble != '#000000') {
      if (obj.moderator === 1) {
        obj.isHighlighted = settings.sz2c2rumble;
      }
  }
  // Mute owner/streamer messages in chat in dim text  
  if (settings.sz3b2 === true) {
      if (obj.owner === 1) {
        obj.isMuted = 1;
      }
  }
  // Hide owner/streamer messages in chat  
  if (settings.sz4b2 === true) {
      if (obj.owner === 1) {
        obj.isHidden = 1;
      }
  }
  if ( (obj.owner === 1) || (obj.moderator === 1) || (obj.sub === 1) || (obj.newSub === 1) || (obj.verified === 1) ) { } else {
    // scbtmuted4 Mute non moderator/sub messages in chat  
    if (settings.sz3b1 === true) {
      obj.isMuted = 1;
    }
    // scbthidden4 Hide non moderator/sub messages in chat 
    if (settings.sz4b1 === true) {
      obj.isHidden = 1;
    }
  }
  // scbtmuted3 Mute bot messages  
  if (settings.sz3b4 === true) {
    if (obj.username && obj.message) {
      if (obj.isBot === 1) {
        obj.isMuted = 1;
      }
    }
  }
  // scbthidden3 Hide bot  messages 
  if (settings.sz4b4 === true) {
    if (obj.username && obj.message) {
      if (obj.isBot === 1) {
        obj.isHidden = 1;
      }
    }
  }
  // Highlight mention messages in chat in this hex colour 
  if (settings.sz2c3rumble != '#000000') {
    if (obj.mention === 1) {
      obj.isHighlighted = settings.sz2c3rumble;
    }
  }
  // Highlight hashtag messages in chat in this hex colour  
  if (settings.sz2c4rumble != '#000000') {
    if (obj.hashtag === 1) {
      obj.isHighlighted = settings.sz2c4rumble;
    }
  }
  // Mute @ mention messages in chat in dim text  
  if (settings.sz3b3 === true) {
    if (obj.mention === 1) {
      obj.isMuted = 1;
    }
  }
  // Hide @ mention messages in chat 
  if (settings.sz4b3 === true) {
    if (obj.mention === 1) {
      obj.isHidden = 1;
    }
  }
  elem = settings = null;
  return obj;
}


scbt.f.style_with_obj_of_changes_rumble = function(obj){
  var css = '';
  console.log('style_with_obj_of_changes_rumble obj', obj);

  // Background of chat in this hex colour.
  if (obj.s1c1rumble) {
    if (obj.s1c1rumble == '#000000' || obj.s1c1rumble == '') {
      css = css + ' body.scbt-rumble #chat-history-list { background-color: asdf !important; } ';
    }
    if (obj.s1c1rumble == '#ffffff') {
      css = css + ' body.scbt-rumble #chat-history-list { background-color: transparent !important; } ';
    }
    if ( (obj.s1c1rumble != '#ffffff') && (obj.s1c1rumble != '#000000') && (obj.s1c1rumble != '') ) {
      css = css + ' body.scbt-rumble #chat-history-list { background-color: ' + obj.s1c1rumble + ' !important; } ';
    }
  }

  // User names in chat in this hex colour
  if (obj.s1c2rumble) {
    if (obj.s1c2rumble == '#000000' || obj.s1c2rumble == '') {
      css = css + ' body.scbt-rumble .chat-history--username a { color: asdf !important; text-shadow: 0px; } ';
    }
    if (obj.s1c2rumble == '#ffffff') {
      css = css + ' body.scbt-rumble .chat-history--username a { color: transparent !important; text-shadow: 0.3px 0.3px dimgray; } ';
    }
    if ( (obj.s1c2rumble != '#ffffff') && (obj.s1c2rumble != '#000000') && (obj.s1c2rumble != '') ) {
      css = css + ' body.scbt-rumble .chat-history--username a { color:' + obj.s1c2rumble + ' !important; text-shadow: 0.5px 0.5px darkslategrey; } ';
    }
  }

  // Highlights of chat in this colour
  if (obj.s1c3rumble) {
    if (obj.s1c3rumble != '#000000') {
      scbt.c.scbtBorderColor = obj.s1c3rumble;
    } else {
      scbt.c.scbtBorderColor = '#ff0000';
    }
  }

  // Chat messages in this colour
  if (obj.s1c4rumble) {
    if (obj.s1c4rumble == '#000000' || obj.s1c4rumble == '') {
      css = css + ' body.scbt-rumble .chat-history--message { color: asdf !important; text-shadow: 0px; } ';
    }
    if (obj.s1c4rumble == '#ffffff') {
      css = css + ' body.scbt-rumble .chat-history--message { color: transparent !important; text-shadow: 0.3px 0.3px dimgray; } ';
    }  
    if ( (obj.s1c4rumble != '#ffffff') && (obj.s1c4rumble != '#000000') && (obj.s1c4rumble != '') ) {
      css = css + ' body.scbt-rumble .chat-history--message { color: ' + obj.s1c4rumble + ' !important; text-shadow: 0.5px 0.5px darkslategrey; } ';
    }
  }

  // Chat font sizes
  if (obj.s1n1rumble) {
    if ( Number(obj.s1n1rumble) > 0) {
      var str = Number(obj.s1n1rumble) + 'rem';
      css = css + ' body.scbt-rumble .chat-history--message { font-size: ' + str + '; line-height: ' + Number(obj.s1n1rumble)  + '; } ';
    } else {
      css = css + ' body.scbt-rumble .chat-history--message { font-size: initial; line-height: unset; } ';
    }
  }

  // text only chat
  if (obj.s1b1rumble === true) {
    css = css + ' body.scbt-rumble .chat-history--row img, body.scbt-rumble .chat-history--row svg, body.scbt-rumble .chat-history--username, body.scbt-rumble .chat-history--badges-wrapper { visibility: hidden !important; } ';
  }
  if (obj.s1b1rumble === false) {
    css = css + ' body.scbt-rumble .chat-history--row img, body.scbt-rumble .chat-history--row svg, body.scbt-rumble .chat-history--username, body.scbt-rumble .chat-history--badges-wrapper { visibility: visible !important; } ';      
  }

  // Left handed screen. Flip screen so video is on the right and chat is on the left.
  if (obj.setting6b5 === true) {
    css = css + ' body.scbt-rumble .main-and-sidebar { flex-direction: row-reverse; } body.scbt-rumble .media-page-chat-aside-chat-wrapper-fixed { position: initial; } ';
  }
  if (obj.setting6b5 === false) {
    css = css + ' body.scbt-rumble .main-and-sidebar { flex-direction: row; } ';
  }

  // Upside down chat screen. Newest chat is on the top, oldest chat is on the bottom 
  if (obj.setting6b6 === true) {
     css = css + ' body.scbt-rumble #chat-history-list { display: flex; flex-direction: column-reverse; } ';
  }
  if (obj.setting6b6 === false) {
    css = css + ' body.scbt-rumble #chat-history-list { display: block; flex-direction: initial; } ';
  }

  // mouseover hover enlarge
  if (obj.setting6b7 === true) {
    css = css + ' body.scbt-rumble .chat-history--message:hover { font-size: 166% !important; } ';
  }
  if (obj.setting6b7 === false) {
    css = css + ' body.scbt-rumble .chat-history--message:hover { font-size: initial !important; } ';
  }

 if (css) {
    var head = document.getElementsByTagName('head')[0];
    var style = document.createElement('style');
    head.appendChild(style);
    style.type = 'text/css';
    if (style.styleSheet){
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }
  }
  obj = t = head = style = css = null; return false;
}


scbt.f.toggle_chats_rumble = function(parameter){
  // elemArr = scbt.f.get_arr_chats();
    console.log('doing scbt_something_for_rumble with parameter: ' + parameter);
    var elemArr = document.body.getElementsByClassName('chat-history--row');
    toSearchFor = '.chat-history--message, .chat-history--rant-text, .chat-history--notification-text';
    console.log(elemArr);

    if (scbt.n.visibilityChatShow === 1) {
      scbt.e.chatB.textContent = scbt.f.set_chat_parameter(parameter, scbt.n.visibilityChatShow);
      [].forEach.call(elemArr, function(elem) {
        elem.style.opacity = 0;

        if (parameter == 'mention') {
            var str = elem.textContent;
            if (str) {
              if ( str.indexOf('@') > -1 ) {
                scbt.f.chat_on(elem);
                elem.style.opacity = 1;
              }
            }
        }
        if (parameter == 'hashtag') {
            var str = elem.textContent;
            if (str) {
              if ( str.indexOf('#') > -1 ) {
                scbt.f.chat_on(elem);
                elem.style.opacity = 1;
              }
            }
        }
        if (parameter == 'owner') {
          var imgElemArr = elem.getElementsByTagName('img');
          [].forEach.call(imgElemArr, function(imgElm) {
            var alt = imgElm.getAttribute('alt');
            if (alt) {
              var a = alt.toLowerCase();
              if ( (a.indexOf('broadcaster') > -1) || (a.indexOf('owner') > -1) || (a.indexOf('admin') > -1) ) {
                scbt.f.chat_on(elem);
                elem.style.opacity = 1;
              }
            }
          });
        }
        if (parameter == 'sub') {
          var imgElemArr = elem.getElementsByTagName('img');
          [].forEach.call(imgElemArr, function(imgElm) {
            var alt = imgElm.getAttribute('alt');
            if (alt) {
              var a = alt.toLowerCase();
              if ( (a.indexOf('supporter') > -1) || (a.indexOf('supporter+') > -1) || (a.indexOf('sub') > -1) ) {
                scbt.f.chat_on(elem);
                elem.style.opacity = 1;
              }
            }
          });
        }
        if (parameter == 'moderator') {
          var imgElemArr = elem.getElementsByTagName('img');
          [].forEach.call(imgElemArr, function(imgElm) {
            var alt = imgElm.getAttribute('alt');
            if (alt) {
              var a = alt.toLowerCase();
              if (a.indexOf('moderator') > -1) {
                scbt.f.chat_on(elem);
                elem.style.opacity = 1;
              }
            }
          });
        }
        if (parameter == 'mod_sub') {
          var imgElemArr = elem.getElementsByTagName('img');
          [].forEach.call(imgElemArr, function(imgElm) {
            var alt = imgElm.getAttribute('alt');
            if (alt) {
              var a = alt.toLowerCase();
              if ( (a.indexOf('moderator') > -1) || (a.indexOf('supporter') > -1) || (a.indexOf('supporter+') > -1) || (a.indexOf('sub') > -1)  ) {
                scbt.f.chat_on(elem);
                elem.style.opacity = 1;
              }
            }
          });
        }
       if ( (parameter == 'vip') || (parameter == 'verified') ) {
        if (elem.classList.contains('vip') ) {
          scbt.f.chat_on(elem);
          elem.style.opacity = 1;
        }
      }
      if (parameter == 'donation') {
        if (elem.classList.contains('chat-history--rant') ) {
          scbt.f.chat_on(elem);
          elem.style.opacity = 1;
        }   
      }

    });
    scbt.n.visibilityChatShow = 2;
    parameter = str = a = alt = elemArr = elem = elem2Arr = elem2 = toSearchFor = arr = null; return false;
  }

  if (scbt.n.visibilityChatShow === 2) {
    scbt.e.chatB.textContent = scbt.f.set_chat_parameter(parameter, scbt.n.visibilityChatShow);
    [].forEach.call(elemArr, function(elem) {
      elem.style.opacity = 1;
      scbt.f.chat_blur(elem);

        if (parameter == 'mention') {
            var str = elem.textContent;
            if (str) {
              if ( str.indexOf('@') > -1 ) {
                elem.style.opacity = 0;
              }
            }
        }
        if (parameter == 'hashtag') {
            var str = elem.textContent;
            if (str) {
              if ( str.indexOf('#') > -1 ) {
                elem.style.opacity = 0;
              }
            }
        }
        if (parameter == 'owner') {
          var imgElemArr = elem.getElementsByTagName('img');
          [].forEach.call(imgElemArr, function(imgElm) {
            var alt = imgElm.getAttribute('alt');
            if (alt) {
              var a = alt.toLowerCase();
              if ( (a.indexOf('broadcaster') > -1) || (a.indexOf('owner') > -1) || (a.indexOf('admin') > -1) ) {
                elem.style.opacity = 0;
              }
            }
          });
        }
        if (parameter == 'sub') {
          var imgElemArr = elem.getElementsByTagName('img');
          [].forEach.call(imgElemArr, function(imgElm) {
            var alt = imgElm.getAttribute('alt');
            if (alt) {
              var a = alt.toLowerCase();
              if ( (a.indexOf('supporter') > -1) || (a.indexOf('supporter+') > -1) || (a.indexOf('sub') > -1) ) {
                elem.style.opacity = 0;
              }
            }
          });
        }
        if (parameter == 'moderator') {
          var imgElemArr = elem.getElementsByTagName('img');
          [].forEach.call(imgElemArr, function(imgElm) {
            var alt = imgElm.getAttribute('alt');
            if (alt) {
              var a = alt.toLowerCase();
              if (a.indexOf('moderator') > -1) {
                elem.style.opacity = 0;
              }
            }
          });
        }
        if (parameter == 'mod_sub') {
          var imgElemArr = elem.getElementsByTagName('img');
          [].forEach.call(imgElemArr, function(imgElm) {
            var alt = imgElm.getAttribute('alt');
            if (alt) {
              var a = alt.toLowerCase();
              if ( (a.indexOf('moderator') > -1) || (a.indexOf('supporter') > -1) || (a.indexOf('supporter+') > -1) || (a.indexOf('sub') > -1)  ) {
                elem.style.opacity = 0;
              }
            }
          });
        }
       if ( (parameter == 'vip') || (parameter == 'verified') ) {
        if (elem.classList.contains('vip') ) {
          elem.style.opacity = 0;
        }
      }
      if (parameter == 'donation') {
        if (elem.classList.contains('chat-history--rant') ) {
          elem.style.opacity = 0;
        }   
      }

      });
      scbt.n.visibilityChatShow = 3;
      parameter = str = a = alt = elemArr = elem = elem2Arr = elem2 = toSearchFor = arr = null; return false;
    }

    if (scbt.n.visibilityChatShow === 3) {
      scbt.e.chatB.textContent = '';
      [].forEach.call(elemArr, function(elem) {
        scbt.f.chat_off(elem);
        elem.style.opacity = 1;
      });
      scbt.n.visibilityChatShow = 1;
      parameter = str = a = alt = elemArr = elem = elem2Arr = elem2 = toSearchFor = arr = null; return false;
    }
  return false;
}
