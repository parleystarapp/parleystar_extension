/* SPDX-FileCopyrightText: © 2024 promising future digital media llc. All rights reserved. <admin@parleystar.com> */
/* SPDX-License-Identifier: Mozilla Public License 1.1 (MPL-1.1) */
console.log('loading youtube.js');

scbt.f.chat_clean_youtube = function(obj, elem){
  var elemArr = [];
  var str = null;
  var stampArr = new Date().toLocaleTimeString().replace(/ /g, '').split(':'); // = 11:34:03AM  ['12', '11', '42PM']
  var timestamp = stampArr[0] + ':' + stampArr[1] + stampArr[2].slice(2);

  var classArr = elem.classList;
  if (classArr.contains('user-notice-line') || classArr.contains('donation') || classArr.contains('anevent') ) {
    obj.anevent = 1;
  }
  if (classArr.contains('sub') ) {
    obj.sub = 1;
  }
  if (classArr.contains('moderator') ) {
    obj.moderator = 1;
  }
  if (classArr.contains('founder') ) {
    obj.founder = 1;
  }
  if ( classArr.contains('vip') || classArr.contains('verified') ) {
    obj.verified = 1;
  }
  if (classArr.contains('owner') ) {
    obj.owner = 1;
  }
  if (classArr.contains('staff') ) {
    obj.staff = 1;
  }
  if (classArr.contains('gifter') ) {
    obj.gifter = 1;
  }

  if (elem.id) {
    obj.itemid = elem.id;
  }

  var elemArr = elem.querySelectorAll('#timestamp');
  if (elemArr[0] && elemArr[0].textContent) { 
      obj.timestamp = elemArr[0].textContent;
  }
  
  obj.username = null;
  var elemArr = elem.querySelectorAll('#author-name');
  if (elemArr[0] && elemArr[0].textContent) {
    obj.username = elemArr[0].textContent;
    obj = scbt.f.get_obj_cleaned_username_from_obj(obj);
  }  

  obj.message = null;
  var elemArr = elem.querySelectorAll('#message');
  if (elemArr[0] && elemArr[0].textContent) {
    str = elemArr[0].textContent;
    obj.message = str;
    if (str.indexOf('@') > -1) {
      obj.mention = 1;
    }
    if (str.indexOf('#') > -1) {
      obj.hashtag = 1;
    }
    obj = scbt.f.get_obj_cleaned_message_from_obj(obj);
    if (obj.message == null || obj.message.trim() === '') { } else {
      str = str.replace(/(?:\r\n|\r|\n)/g, '');
      str = str.split("\t").join("");
      str = str.split("\"").join("");
      str = str.split("\'").join("");
      obj.message = str;
    }
  }
  
  if ( elem.getAttribute('author-type') == 'member') {
    obj.sub = 1;
  }

  if ( elem.getAttribute('author-type') == 'moderator') {
    obj.moderator = 1;
  }

  if ( elem.getAttribute('author-type') == 'owner') {
    obj.owner = 1;
  }

  var elemArr = elem.querySelectorAll('#chip-badges .yt-live-chat-author-chip');
  if (elemArr[0] && elemArr[0].ariaLabel == 'Verified') {
    obj.verified = 1;
  }
  
  var elemArr = elem.getElementsByClassName('yt-live-chat-paid-message-renderer');
  if (elemArr[0]) {
    obj.itemid = elem.id;
    obj.donation = 1;
    obj.anevent = 1;
    str = elem.textContent;
    str = str.replace(/\/‘’‚“”„"`~«´<>/g, '');
    str = str.replaceAll(',', ' ');
    obj.message = str;
    // var n = $( elem ).find( '#author-name' );
    // obj.username = n[0].textContent;
    obj.timestamp = timestamp;
  }

  var elemArr = elem.getElementsByClassName('yt-live-chat-membership-item-renderer');
  if (elemArr[0]) {
    obj.itemid = elem.id;
    obj.newSub = 1;
    obj.anevent = 1;
    str = elem.textContent;
    str = str.replace(/\/‘’‚“”„"`~«´<>/g, '');
    str = str.replaceAll(',', ' ');
    obj.message = str;
    // var n = $( elem ).find( '#author-name' );
    // obj.username = n[0].textContent;
    obj.timestamp = timestamp;
  }

  var elemArr = elem.getElementsByClassName('yt-live-chat-paid-sticker-renderer');
  if (elemArr[0]) {
    obj.itemid = elem.id;
    obj.donation = 1;
    obj.anevent = 1;
    str = elem.textContent;
    str = str.replace(/\/‘’‚“”„"`~«´<>/g, '');
    str = str.replaceAll(',', ' ');
    obj.message = str;
    // var n = $( elem ).find( '#author-name' );
    // obj.username = n[0].textContent;
    obj.timestamp = timestamp;
  }

  if ( (obj.itemid == 'contents' ) || (obj.itemid == 'item-scroller' ) || (obj.itemid == 'docked-messages' ) || (obj.itemid == 'item-offset' ) || (obj.itemid == 'items' ) || (obj.itemid == 'empty-state-message' ) || (obj.itemid == 'show-more' ) ) {
    var obj = {};
  }
  special = stampArr = str = elemArr = elemArr2 = elemArr3 = timestamp = imgs = alt = imgElm = imgElemArr = itemId = a = elem = null; return obj;
}


scbt.f.chat_make_decisions_youtube = function(obj, elem, settings){
  if (settings.sz1b1youtube === true) {
    var elemArr = elem.getElementsByTagName('img');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    var elemArr = elem.getElementsByTagName('i');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    var elemArr = elem.getElementsByTagName('svg');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    var elemArr = elem.getElementsByTagName('canvas');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    elemArr = elm2 = null;
  }

  // Highlight sub messages in chat in this hex colour h1
  if (settings.sz2c1youtube != '#000000') {
    if (obj.sub === 1) {
      obj.isHighlighted = settings.sz2c1youtube;
    }
  }
  // Highlight gifter messages in chat in this hex colour h2
  if (settings.sz2c9youtube != '#000000') {
    if (obj.gifter === 1) {
      obj.isHighlighted = settings.sz2c9youtube;
    }
  }
  // Highlight VIP messages in chat in this hex colour h3 
  if (settings.sz2c5youtube != '#000000') {
    if (elem.classList.contains('vip')) {
      obj.isHighlighted = settings.sz2c5youtube;
    }
  }
  // Highlight founder messages in chat in this hex colour  
  if (settings.sz2c6youtube != '#000000') {
    if (obj.founder === 1) {
      obj.isHighlighted = settings.sz2c6youtube;
    }
  }
  // Highlight OG messages in chat in this hex colour  
  if (settings.sz2c7youtube != '#000000') {
    if (obj.og === 1) {
      obj.isHighlighted = settings.sz2c7youtube;
    }
  }
  // Highlight owner messages in chat in this hex colour  
  if (settings.sz2c8youtube != '#000000') {
    if (obj.owner === 1) {
      obj.isHighlighted = settings.sz2c8youtube;
    }
  }
  // Highlight moderator messages in chat in this hex colour  
  if (settings.sz2c2youtube != '#000000') {
      if (obj.moderator === 1) {
        obj.isHighlighted = settings.sz2c2youtube;
      }
  }
  // Mute owner/streamer messages in chat in dim text  
  if (settings.sz3b2 === true) {
      if (obj.owner === 1) {
        obj.isMuted = 1;
      }
  }
  // Hide owner/streamer messages in chat  
  if (settings.sz4b2 === true) {
      if (obj.owner === 1) {
        obj.isHidden = 1;
      }
  }
  if ( (obj.owner === 1) || (obj.moderator === 1) || (obj.sub === 1) || (obj.newSub === 1) || (obj.verified === 1) ) { } else {
    // scbtmuted4 Mute non moderator/sub messages in chat  
    if (settings.sz3b1 === true) {
      obj.isMuted = 1;
    }
    // scbthidden4 Hide non moderator/sub messages in chat 
    if (settings.sz4b1 === true) {
      obj.isHidden = 1;
    }
  }
  // scbtmuted3 Mute bot messages  
  if (settings.sz3b4 === true) {
    if (obj.username && obj.message) {
      if (obj.isBot === 1) {
        obj.isMuted = 1;
      }
    }
  }
  // scbthidden3 Hide bot  messages 
  if (settings.sz4b4 === true) {
    if (obj.username && obj.message) {
      if (obj.isBot === 1) {
        obj.isHidden = 1;
      }
    }
  }
  // Highlight mention messages in chat in this hex colour 
  if (settings.sz2c3youtube != '#000000') {
    if (obj.mention === 1) {
      obj.isHighlighted = settings.sz2c3youtube;
    }
  }
  // Highlight hashtag messages in chat in this hex colour  
  if (settings.sz2c4youtube != '#000000') {
    if (obj.hashtag === 1) {
      obj.isHighlighted = settings.sz2c4youtube;
    }
  }
  // Mute @ mention messages in chat in dim text  
  if (settings.sz3b3 === true) {
    if (obj.mention === 1) {
      obj.isMuted = 1;
    }
  }
  // Hide @ mention messages in chat 
  if (settings.sz4b3 === true) {
    if (obj.mention === 1) {
      obj.isHidden = 1;
    }
  }
  
  elem = settings = null; return obj;
}


scbt.f.style_with_obj_of_changes_youtube = function(obj){
  var css = '';
  console.log('style_with_obj_of_changes_youtube obj', obj);

  // Background of chat in this hex colour.
  if (obj.sz1c1youtube) {
    if (obj.sz1c1youtube == '#000000' || obj.sz1c1youtube == '') {
      css = css + ' .yt-live-chat-item-list-renderer { background-color: asdf !important; } ';
    }
    if (obj.sz1c1youtube == '#ffffff') {
      css = css + ' .yt-live-chat-item-list-renderer { background-color: transparent !important; } ';
    }
    if ( (obj.sz1c1youtube != '#ffffff') && (obj.sz1c1youtube != '#000000') && (obj.sz1c1youtube != '') ) {
      css = css + ' .yt-live-chat-item-list-renderer { background-color: ' + obj.sz1c1youtube + ' !important; } ';
    }
  }

  // User names in chat in this hex colour 
  if (obj.sz1c2youtube) {
    if (obj.sz1c2youtube == '#000000' || obj.sz1c2youtube == '') {
      css = css + ' #author-name { color: var(--yt-live-chat-secondary-text-color) !important; text-shadow: 0px; }  #author-name.member.yt-live-chat-author-chip { color: var(--yt-live-chat-sponsor-color) !important; text-shadow: 0px; }  #author-name.moderator.yt-live-chat-author-chip { color: var(--yt-live-chat-moderator-color) !important; text-shadow: 0px; } ';
    }
    if (obj.sz1c2youtube == '#ffffff') {
      css = css + ' #author-name { color: var(--yt-live-chat-secondary-text-color) !important; text-shadow: 0px; }  #author-name.member.yt-live-chat-author-chip { color: var(--yt-live-chat-sponsor-color) !important; text-shadow: 0px; }  #author-name.moderator.yt-live-chat-author-chip { color: var(--yt-live-chat-moderator-color) !important; text-shadow: 0px; } ';
    }
    if ( (obj.sz1c2youtube != '#ffffff') && (obj.sz1c2youtube != '#000000') && (obj.sz1c2youtube != '') ) {
      css = css + ' #author-name { color:' + obj.sz1c2youtube + ' !important; text-shadow: 0.5px 0.5px darkslategrey; } ';
    }
  }

  if (obj.sz4b7 === true) {
    css = css + ' #author-name { visibility: hidden !important; }  #author-name.member.yt-live-chat-author-chip { visibility: hidden !important; }  #author-name.moderator.yt-live-chat-author-chip { visibility: hidden !important; } ';
  }
  if (obj.sz4b7 === false) {
    css = css + ' #author-name { visibility: visible !important; }  #author-name.member.yt-live-chat-author-chip { visibility: visible !important; }  #author-name.moderator.yt-live-chat-author-chip { visibility: visible !important; } ';
  }

  // Highlights of chat in this colour
  if (obj.sz1c3youtube) {
    if (obj.sz1c3youtube != '#000000') {
      scbt.c.scbtBorderColor = obj.sz1c3youtube;
    } else {
      scbt.c.scbtBorderColor = '#ff0000';
    }
  }

  // Chat messages in this colour
  if (obj.sz1c4youtube) {
    console.log('yes1');
    if (obj.sz1c4youtube == '#000000' || obj.sz1c4youtube == '') {
      css = css + ' .yt-live-chat-item-list-renderer #content #message { color: var(--yt-live-chat-primary-text-color,var(--yt-spec-text-primary)) !important; text-shadow: 0px; } ';
    }
    if (obj.sz1c4youtube == '#ffffff') {
      css = css + ' .yt-live-chat-item-list-renderer #content #message { color: transparent !important; text-shadow: 0.3px 0.3px dimgray; } ';
    }  
    if ( (obj.sz1c4youtube != '#ffffff') && (obj.sz1c4youtube != '#000000') && (obj.sz1c4youtube != '') ) {
      console.log('yes2');
      css = css + ' .yt-live-chat-item-list-renderer #content #message { color: ' + obj.sz1c4youtube + ' !important; text-shadow: 0.5px 0.5px darkslategrey; } ';
    }
  }

  // Chat font sizes
  if (obj.sz1n1youtube) {
    if ( Number(obj.sz1n1youtube) > 0) {
      var str = Number(obj.sz1n1youtube) + 'rem';
      css = css + ' .yt-live-chat-item-list-renderer #content #message { font-size: ' + str + '; line-height: ' + Number(obj.sz1n1youtube)  + '; } ';
    } else {
      css = css + ' #content.yt-live-chat-text-message-renderer #message { font-size: initial; line-height: unset; } ';
    }
  }

  // text only chat
  if (obj.sz1b1youtube === true) {
    css = css + ' #author-photo, #chat-badges, #message img, #message svg, #timestamp, #message i, #message canvas { visibility: hidden !important; } ';
  }
  if (obj.sz1b1youtube === false) {
    css = css + ' #author-photo, #chat-badges, #message img, #message svg, #timestamp, #message i, #message canvas { visibility: visible !important; } ';      
  }

  // Left handed screen. Flip screen so video is on the right and chat is on the left.
  if (obj.sz5b16 === true) {
    css = css + ' #columns.ytd-watch-flexy { flex-direction: row-reverse; } ';
  }
  if (obj.sz5b16 === false) {
    css = css + ' body.scbt-youtube #columns.ytd-watch-flexy { flex-direction: row; } ';
  }

  // Upside down chat screen. Newest chat is on the top, oldest chat is on the bottom 
  if (obj.sz5b17 === true) {
     css = css + ' #item-list #contents #item-scroller { display: flex; flex-direction: column-reverse; } ';
  }
  if (obj.sz5b17 === false) {
    css = css + ' #item-list #contents #item-scroller { display: block; flex-direction: column; } ';
  }

  // mouseover hover enlarge
  if (obj.sz5b18 === true) {
    css = css + ' #content.yt-live-chat-text-message-renderer #message:hover { font-size: 166% !important; } .yt-live-chat-item-list-renderer img:hover, .yt-live-chat-item-list-renderer svg:hover { height: 7rem !important; width: auto !important; font-size: 3rem; } ';
  }
  if (obj.sz5b18 === false) {
    css = css + ' #content.yt-live-chat-text-message-renderer #message:hover { font-size: initial !important; } .yt-live-chat-item-list-renderer img:hover, .yt-live-chat-item-list-renderer svg:hover { height: initial !important; width: auto !important; font-size: initial; } ';
  }

  if (css) {
    var head = document.getElementsByTagName('head')[0];
    var style = document.createElement('style');
    head.appendChild(style);
    style.type = 'text/css';
    if (style.styleSheet){
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }

    var iframeRef = document.querySelectorAll('iframe#chatframe');
    if (iframeRef[0] && iframeRef[0].contentWindow && iframeRef[0].contentWindow.document && iframeRef[0].contentWindow.document.head) {
      head = iframeRef[0].contentWindow.document.head;
      var style = document.createElement('style');
      head.appendChild(style);
      style.type = 'text/css';
      if (style.styleSheet){
        style.styleSheet.cssText = css;
      } else {
        style.appendChild(document.createTextNode(css));
      }
    }
  }
  obj = t = head = style = iframeRef = css = null; return false;
}


scbt.f.toggle_chats_youtube = function(parameter){
  var elemArr = [];
  var toSearchFor = '';

  console.log('doing scbt.f.toggle_chats_youtube with parameter: ', parameter);

  if ( (parameter == 'mention') || (parameter == 'hashtag') ) {
    elemArr = scbt.e.scbtYTChatIframe.body.querySelectorAll('.yt-live-chat-text-message-renderer .yt-live-chat-text-message-renderer');
    console.log('doing toggle_chats_youtube with elemarr1: ', elemArr);
  } else {
    elemArr = scbt.f.get_arr_chats();
    console.log('doing toggle_chats_youtube with elemarr2: ', elemArr);
  }
  toSearchFor = 'xyz';

  if (scbt.n.visibilityChatShow === 1) {
    scbt.e.scbtChatB.textContent = scbt.f.set_chat_parameter(parameter, scbt.n.visibilityChatShow);
    [].forEach.call(elemArr, function(elem) {
      elem.style.opacity = 0;
      if ( (parameter == 'owner') || (parameter == 'moderator') || (parameter == 'sub') || (parameter == 'member') || (parameter == 'mod_sub') ) {
        if (parameter == 'sub') { parameter = 'member'; }
        if ( elem.getAttribute('author-type') ) { 
          toSearchFor = elem.getAttribute('author-type');
        } else {
          toSearchFor = 'xyz';
        }
        if ( (parameter != 'mod_sub' && toSearchFor == parameter) || (parameter == 'mod_sub' && toSearchFor != 'xyz') ) {
          // if (toSearchFor.indexOf(parameter) > -1 ) {
          scbt.f.chat_on(elem);
          elem.style.opacity = 1;
        }
      }
      
      if ( (parameter == 'vip') || (parameter == 'verified') ) {
        if (elem.classList.contains('vip') ) {
          scbt.f.chat_on(elem);
          elem.style.opacity = 1;
        }
        var elem2Arr = elem.querySelectorAll('#chip-badges .yt-live-chat-author-chip');
        if (elem2Arr[0]) {
          if (elem2Arr[0].ariaLabel == 'Verified') {
            elem.style.opacity = 1;
          }
        }
        var elem3Arr = elem.getElementsByClassName('vip');
        [].forEach.call(elem3Arr, function(elem3) {
            elem.style.opacity = 1;
        });
      }

      if (parameter == 'donation') {
        var elem2Arr = elem.querySelectorAll('#card.yt-live-chat-paid-sticker-renderer, #card.yt-live-chat-paid-message-renderer');
        [].forEach.call(elem2Arr, function(elem2) {
          elem.style.opacity = 1;
        });
      }

      if (parameter == 'mention') {
        if (elem.id == 'message') {
          var str = elem.textContent;
          if ( str.indexOf('@') > -1 ) {
            scbt.f.chat_on(elem);
            elem.style.opacity = 1;
          }
        }
      }

      if (parameter == 'hashtag') {
        if (elem.id == 'message') {
          var str = elem.textContent;
          if ( str.indexOf('#') > -1 ) {
            scbt.f.chat_on(elem);
            elem.style.opacity = 1;
          }
        }
      }
  });
  scbt.n.visibilityChatShow = 2;
  parameter = str = a = alt = elemArr = elem = elem2Arr = elem2 = toSearchFor = arr = null; return false;
}

if (scbt.n.visibilityChatShow === 2) {
  scbt.e.scbtChatB.textContent = scbt.f.set_chat_parameter(parameter, scbt.n.visibilityChatShow);
  [].forEach.call(elemArr, function(elem) {
      elem.style.opacity = 1;
      scbt.f.chat_blur(elem);

      if ( (parameter == 'owner') || (parameter == 'moderator') || (parameter == 'sub') || (parameter == 'member') || (parameter == 'mod_sub') ) {
        if (parameter == 'sub') { parameter = 'member'; }
        if (parameter == 'sub') { parameter = 'member'; }
        if ( elem.getAttribute('author-type') ) { 
          toSearchFor = elem.getAttribute('author-type');
        } else {
          toSearchFor = 'xyz';
        }
        if ( (parameter != 'mod_sub' && toSearchFor == parameter) || (parameter == 'mod_sub' && toSearchFor != 'xyz') ) {
          elem.style.opacity = 0;
        }
      }

      if ( (parameter == 'vip') || (parameter == 'verified') ) {
        var elem2Arr = elem.querySelectorAll('#chip-badges .yt-live-chat-author-chip');
        if (elem2Arr[0]) {
          if (elem2Arr[0].ariaLabel == 'Verified') {
            elem.style.opacity = 0;
          }
        }
        var elem3Arr = elem.getElementsByClassName('vip');
        [].forEach.call(elem3Arr, function(elem3) {
          elem.style.opacity = 0;
        });
        if (elem.classList.contains('vip') ) {
          elem.style.opacity = 0;
        }
      }

      if (parameter == 'donation') {
        var elem2Arr = elem.querySelectorAll('#card.yt-live-chat-paid-sticker-renderer, #card.yt-live-chat-paid-message-renderer');
        [].forEach.call(elem2Arr, function(elem2) {
          elem.style.opacity = 0;
        });
      }

      if (parameter == 'mention') {
        if (elem.id == 'message') {
          var str = elem.textContent;
          if ( str.indexOf('@') > -1 ) {
            elem.style.opacity = 0;
          }
        }
      }

      if (parameter == 'hashtag') {
        if (elem.id == 'message') {
          var str = elem.textContent;
          if ( str.indexOf('#') > -1 ) {
            scbt.f.chat_on(elem);
            elem.style.opacity = 0;
          }
        }
      }
    });
    scbt.n.visibilityChatShow = 3;
    parameter = str = a = alt = elemArr = elem = elem2Arr = elem2 = toSearchFor = arr = null; return false;
  }

  if (scbt.n.visibilityChatShow === 3) {
    scbt.e.scbtChatB.textContent = '';
    [].forEach.call(elemArr, function(elem) {
      scbt.f.chat_off(elem);
      elem.style.opacity = 1;
    });
    scbt.n.visibilityChatShow = 1;
    parameter = str = a = alt = elemArr = elem = elem2Arr = elem2 = toSearchFor = arr = null; return false;
  }
  return false;
}

