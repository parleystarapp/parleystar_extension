async function requestFromBackground(obj) { return new Promise((res,rej)=> chrome.runtime.sendMessage(obj, response=> res(response)) ) }

var colors = {
    kick_green:`#53fc18`,
    kick_green_trans:`#53fc1830`,
    red:`#ef5350`,
}

var streamTimeDurationSeconds = ()=> getTimeValueFromInputString(document.getElementsByClassName('vjs-remaining-time')?.[0]?.innerText);

var getChanellNameFromDOM = ()=> document.getElementsByClassName('stream-username')?.[0]?.getElementsByTagName('span')?.[0]?.innerText?.trim()?.toLowerCase() //? /(?<=kick\.com)[\w-]+/.exec(window.location.href)?.[0] : null;//aleks-kk
var getChannelSlugFromDOM = ()=> /(?<=kick\.com\/)[\w-]+/.exec(document.getElementsByClassName('stream-username')?.[0]?.getElementsByTagName('a')?.[0]?.href)?.[0] //? /(?<=kick\.com)[\w-]+/.exec(window.location.href)?.[0] : null;
var streamerStillLive = () => document.getElementsByClassName('vjs-live-display')?.[0]
const convertCreatedAtTime = (s)=> s?.replace(/\s/,'T').replace(/$/,'.000Z');

function domplate(output) {
    var el = document.createElement('textarea');
    document.body.appendChild(el);
    el.value = output;
    el.select();
    document.execCommand('copy');
    el.outerHTML = '';
}

function getTimeValueFromInputString(s) {
    const intFromStringTime = (v,k)=>new RegExp(`(\\d+)${k}`,'i').exec(v)?.[0]?.replace(/^0/, '') ? parseInt(new RegExp(`(\\d+)${k}`,'i').exec(v)?.[0]?.replace(/^0/, '')) : 0;
    const intFromColonTime = (v)=>v.split(/:/).map(i=>i.trim()).filter(i=>/\d+/.test(s)).map(i=>parseInt(i));
    let sh = /:/.test(s) ? intFromColonTime(s).at(-3) * 3600 : intFromStringTime(s, 'h') * 3600;
    let sm = /:/.test(s) ? intFromColonTime(s).at(-2) * 60 : intFromStringTime(s, 'm') * 60;
    let ss = /:/.test(s) ? intFromColonTime(s).at(-1) : intFromStringTime(s, 's');
    return (sh + sm + ss);
}
function parseTimeOffset(s){
    // content_offset_seconds: 1427.737
    const timeString = (n,k)=> `${n >= 1 && n < 10 ? '0'+n.toString() : n >= 10 && n >= 1 ? n : '00'}${k}`;
    let hours = (s/3600);
    let minutes = hours.toString().replace(/\d+(?=\.)/,'');
    let seconds = 60 * parseFloat(minutes);
    let time_arr = [
        Math.floor(hours),
        Math.floor(60 * minutes),
        Math.floor(60*seconds.toString().replace(/\d+(?=\.)/,''))
    ];
    return `${timeString(time_arr[0],'h')}${timeString((time_arr[1] >= 60 ? 0 : time_arr[1]),'m')}${timeString((time_arr[2] >= 60 ? 0 : time_arr[2]),'s')}`;
}//new version. 24 Aug 2023.-AndreB

const unqDiveKey = (a,o,keys) => a.filter(i=> o.hasOwnProperty(dive(i,keys)) ? false : (o[dive(i,keys)] = true) );
const dive = (ob,keys)=> keys.map(itm=> ob = ob?.[itm] ).at(-1);
const cleanKey = (s)=> s?.toLowerCase()?.replace(/-/g,'_')?.replace(/^_/g,'')?.replace(/_$/g,'')?.replace(/[_]+/g,'_');

const subArr = (r,n)=>r.reduceRight((a,b,c,d)=>[...a, d.splice(0, n)], []);
const btoaJSON = (s)=>btoa(encodeURIComponent(JSON.stringify(s)))
const atobJSON = (s)=>JSON.parse(decodeURIComponent(atob(s)))

function chopSequence(logs,key){
    let next_logs = [[logs[0]]];
    for(let i=1; i<logs.length; i++){
        if(logs[i][key] == logs[(i-1)][key] +1) next_logs.at(-1).push(logs[i]);
        else next_logs.push([logs[i]])
    }
    return next_logs;
}

const delay = (ms)=>new Promise(res=>setTimeout(res, ms));
const rando = (n)=>Math.round(Math.random() * n);
var unqHsh = (a,o) => a.filter(i=> o.hasOwnProperty(i) ? false : (o[i] = true));
var cleanObject = (ob) => ob ?
  Object.entries(ob).reduce((r, [k, v]) => {
    if( v != null && v != undefined && v !== "" && ( ['string','number','boolean','function','symbol'].some(opt=> typeof v == opt) || (typeof v == 'object' && ((Array.isArray(v) && v.length) || (Array.isArray(v) != true) ) ) ) ) { 
      r[k] = v; 
      return r;
    } else { 
     return r; 
    }
  }, {}) : null;


function translateDateToXAgo(time){
    var now = new Date().getTime();
    let timestamp = new Date(time).getTime();
    return (now-timestamp)/86400000 >= 1 ? Math.round((now-timestamp)/86400000) : (now-timestamp)/86400000;
}


function formatJSONstring(s){
    if(/\{\w+:/.test(s)){
        try{
        return JSON.parse(JSON.stringify(s.split(/,(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))/).map(line=> line.split(/(?<=\w+):/) ).map(kv=> {
                let cleankey = kv[0]?.replace(/^\{/,'')?.replace(/^\b|\b$/g,'"')?.replace(/^'|'$/g,'"');
                let cleanval = tryJSON(kv[1]?.replace(/\}$/,'')?.replace(/^'|'$/g,'"'));
                let decoded = tryATOB(cleanval)
                return {...{ [cleankey]:cleanval},...(decoded ? {[`atob_${cleankey}`]:decoded} : {})};
            }).reduce((a,b)=> { return {...a,...b} })))
        }
        catch(err){return s}
    }else{ return s}
}
function tryATOB(s){
    if(/==$/.test(s)){
        try { return atob(s); } catch(err) { return s}
    }else {return false}
}
function tryJSON(s,type){ 
    try{ return JSON.parse(s) } 
    catch(err){
        try { return type == 'formatJSONstring' ? formatJSONstring(s) : s; }
        catch(err){ return s; }
    }    
}
function getCookieAsJSON(){
    return document.cookie.split(/; /).map(i=> i.replace(/\b=(?!$|=)/,'__________').split(/__________/)).map(kv=> {
        let cleanval = typeof kv[1] == 'string' ? tryJSON(decodeURIComponent(kv[1]),'formatJSONstring') : kv[1];
        let decoded = tryJSON(tryATOB(cleanval),'formatJSONstring');
        return {
            ...{
                [cleanKey(kv[0])]: cleanval
            },
            ...(decoded ? {[`atob_${cleanKey(kv[0])}`]:decoded} : {})
        }
    }).reduce((a,b)=> { return {...a,...b}});
}








async function fetchText(url){
    try{
        var res = await fetch(url);
        var text = await res.text();
// console.log(text);
        return text;
    } catch(err){
        return null;
    }
}

async function fetchJSON(url,opt){
    try{
        var res = await fetch(url,opt);
        var d = await res.json();
// console.log(d);
        return d;
    } catch(err){
        return null;
    }
}





function dateString(d) {
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var date = new Date(d);
    return `${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()}`;
}


async function parseURI(d) {
    var reader = new FileReader();
    reader.readAsDataURL(d);
    return new Promise((res,rej)=> reader.onload = (e)=>res(e.target.result))
}


async function getArrayBufferWithRefObj(params){
    var start_time = new Date().getTime();
    try{
        var res = await fetch(params.src);
        var array_buffer = await res.arrayBuffer();
        return {...params,...{array_buffer:array_buffer,fetch_duration:(new Date().getTime()-start_time)}}
    }catch(err){
        return {...params,...{failed:true,fetch_duration:(new Date().getTime()-start_time)}};
    }
}
async function getDownloadableArrayBuffer(url) {
    /*fetch version:
    try{
        var res = await fetch(url);
        return await res.arrayBuffer();
    }catch(err){
        return false;
    }

    */
    function getResBlob(url) {
        return new Promise((res)=>{
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.responseType = 'arraybuffer';
            xhr.onload = function(e) {
                if (this.status == 200) {
                    res(this.response)
                } else {
                    res(false);
                }
            };
            xhr.send();
        })
    }
    return await getResBlob(url);
}


function createDownloadHTML(params) {
    var {ref_elm,id} = params; 
    if(gi(document,id)) gi(document,id).outerHTML = '';
    
    const body_width = ref_elm.getBoundingClientRect().width;
    const download_bar_width = body_width;
    let cont = ele('div');
    a(cont, [['id', id], ['style', `width: minmax(200px,${download_bar_width}px); background: #000000; border: 2px solid #1c1c1c; border-radius: 0.2em;`]]);
    ref_elm.insertBefore(cont,ref_elm.firstChild);
    let perc = ele('div');
    a(perc, [['id', id+'downloading_percentage_bar'],['style', `width: 0px; background-color: ${colors.kick_green}; height: 30px; border-bottom-right-radius: 0.2em; border-top-right-radius: 0.2em; transition: all 1s;`]]);
    cont.appendChild(perc);
    let txt = ele('div');
    a(txt, [['id', id+'downloading_percentage_txt'], ['style', `color: #ffffff; width: minmax(200px,${download_bar_width}px);`]]);
    perc.appendChild(txt);
    txt.innerHTML = 'initiating download...';
}

function updatePillFillProgress(progress){
    let pillfiller_progress = document.getElementById('pillfiller_progress');
    let calculated_progress = progress*0.59;
    
    pillfiller_progress.setAttribute('offset',`${calculated_progress}%`)
}
function updateDownloadBar(params){ 
    const {text,img,iteration,total_results,status,ref_elm,id} = params;
    if(!gi(document,id)) createDownloadHTML({ref_elm:ref_elm,id:id});
    const body_width = ref_elm.getBoundingClientRect().width;
    const download_bar_width = body_width;
    let cont = gi(document,id);
    if(cont){
        let perc = gi(document,id+'downloading_percentage_bar');
        let txt = gi(document,id+'downloading_percentage_txt');
        cont.style.width = `${download_bar_width}px;}`;
        perc.style.width = `${( download_bar_width * ( iteration / total_results ) )}px`;


        inlineStyler(txt,`{transform: translate(5px,-12px); font-size:0.9em;}`);

        if(iteration == 0){
            txt.innerHTML = `<div style="display: grid; grid-template-columns: 52px 82px ${download_bar_width-(52+82+(8+8))}px; grid-gap: 8px;">
                <div class="completion_status" style="transform:translate(0px,15px);">${Math.ceil( ( iteration / total_results ) * 10000)/100}%</div>
                <div style="transform:translate(0px,15px);">complete</div>
                <div class="download_text_holder">
                    <div class="download_text_scroll">${text}</div>
                </div>
            </div>`;
        }else{
            txt.getElementsByClassName('completion_status')[0].innerText = `${(Math.ceil( ( iteration / total_results ) * 10000)/100)}%`;
        }
        if(status !== true) {
            cont.outerHTML = '';
        };
    }
}

async function downloadVideoSelection(params){ 
    var {cont_ob,ref_elm,m3u8_data,vod_data,channel_info,dl_btn} = params
    var resolutions = Array.from(cn(cont_ob.cont,'res_option')).map(r=> Math.min(...r.getAttribute('data-res_option').split(/x/)?.map(vv=> parseInt(vv.trim()))))// ensures we get the correct resolution order regardless of landscap or portrait mode;
    let resolution_selection = cont_ob.cont.getElementsByClassName('res_option checked')[0] ? Math.min(...cont_ob.cont.getElementsByClassName('res_option checked')[0]?.getAttribute('data-res_option')?.split(/x/).map(vv=> parseInt(vv.trim()))) : resolutions[0];
    let middle_cont = cont_ob.middle_cont;
    let selected_src = m3u8_data?.filter(r=> ['group_id','resolution','name'].some(key=> r[key]== resolution_selection))?.[0]?.src || m3u8_data?.[0]?.src;
    let ts_data = await requestFromBackground({cmd:'getTSData',params:{src:selected_src}});
    dl_btn.outerHTML = '';
    chopSelectionByResolution({
        resolutions:resolutions,
        resolution_selection:resolution_selection,
        range_values:ts_data,
        ref_elm:middle_cont,
        cont_ob:cont_ob,
        vod_data:vod_data,
    })
}


async function chopSelectionByResolution(params){//TODO: add stop/cancel button
    var {vod_data,cont_ob,resolutions,resolution_selection,range_values,ref_elm} = params; 
    var res_multiplier = resolutions.findIndex(r=> r == resolution_selection) > -1 ? resolutions.findIndex(r=> r == resolution_selection)+1 : 1;
    var chop_num = (150 * res_multiplier);
    var chopped_range = subArr(range_values,chop_num);
    // console.log(chopped_range)
    createDownloadHTML({ref_elm:ref_elm,id:'download_container'});
    await delay(111);
    for(let ii=0; ii<chopped_range.length; ii++){
        await downloadRange({
            chopped_range:chopped_range[ii],
            ref_elm:ref_elm,
            chop_index:ii,
            total:chopped_range.flat().length,
            num_chop:chopped_range.length,
            chop_num:chop_num,
            vod_data:vod_data,
        });
    }
    //update the main progress bar
    updateDownloadBar({
        iteration:10,
        total_results:10,
        status:flase,
        text:`of total`,
        ref_elm:ref_elm,
        id:'download_container'
    });
}

async function downloadRange(params){
    var {vod_data,chopped_range,ref_elm,chop_index,total,num_chop,chop_num} = params;
    var blobs = [];
    createDownloadHTML({ref_elm:ref_elm,id:'download_container_'+chop_index});
    for(let i=0; i<chopped_range.length; i++){
        updateDownloadBar({
            iteration:((chop_num*chop_index)+i),//((num_chop*(chop_index+1))+i),
            total_results:total,
            status:true,
            text:`This VOD download is broken up into ${num_chop} segments. Be sure you have enabled kick.com to download multiple files within your browser's download settings.`,
            ref_elm:ref_elm,
            id:'download_container'
        });
        //update the segment progress bar
        updateDownloadBar({
            iteration:i,
            total_results:chopped_range.length,
            status:true,
            text:`Downloading segment ${(chop_index+1)}. Downloads must be chopped up into multiple files to prevent your browser from crashing.`,
            ref_elm:ref_elm,
            id:'download_container_'+chop_index
        });
        updatePillFillProgress((((chop_num*chop_index)+i)/total))

        let vid_blob = await getDownloadableArrayBuffer(chopped_range[i].src);
        blobs.push(vid_blob);
    }
    let blob_dl = new Blob(blobs,{
        type: 'video/mp4; codecs="avc1.64000d,mp4a.40.2"'
    });
    let link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob_dl);
    link.download = `${chop_index} ${vod_data?.slug || vod_data?.channel_name || vod_data?.start_timestamp}.m4v`;
    link.click();
    // console.log('downloading segment')
    await delay(1000);
    updateDownloadBar({
        iteration:10,
        total_results:10,
        status:false,
        text:'',
        ref_elm:ref_elm,
        id:'download_container_'+chop_index
    });
    delay(3000);
    if(link) link.remove();
}







const snakeKeyify = (s) => s?.toLowerCase()?.trim()?.replace(/\W+/g,'_');
const valCleaner = (s) => s?.replace(/^"/,'')?.replace(/"$/,'');

function getResolutions(text){
    let matches = text.match(/#EXT-X-MEDIA:.+?\n#EXT-X-[A-Z-]+:.+?\n.+?m3u8/g)
    let resolutions = matches?.length ? Array.from(matches).map(m=> {
        let rows = m.split(/\n/)
        let keyed_vals = rows.filter(r=> /^.+?:/.test(r)).map(row=> 
            row?.replace(/^.+?:/,'')?.split(/[,](?=(?:[^\"]|\"[^\"]*\")*$)/).map(v=> 
                v.split(/=/)
            ).map(kv=> 
                ({[snakeKeyify(kv[0])]:valCleaner(kv[1])})
            ).reduce((a,b)=> {return {...a,...b}})
        ).reduce((a,b)=> {return {...a,...b}})
        // console.log(keyed_vals);
        return {
            ...keyed_vals,
            ...(/^https/.test(rows.at(-1)) ? {src:rows.at(-1)} : {path:rows.at(-1)})
        }
    }) : [];
    return resolutions
}
async function getLiveStreamResolutions(params){
    var channel_info = await getChannelInfo(params);
    var text = await fetchText(channel_info?.playback_url);
    return getResolutions(text);
}

// async function getLiveTSData(params){
//     let ts_data = await requestFromBackground({cmd:'getTSData',params:params})
// }