/* SPDX-FileCopyrightText: © 2024 promising future digital media llc. All rights reserved. <admin@parleystar.com> */
/* SPDX-License-Identifier: Mozilla Public License 1.1 (MPL-1.1) */
// ************** START FILTER functions
console.log('doing banned.js', scbt);
scbt.f.save_word_list = function(json, listType){
  if (listType == 'sexualterms') {
    scbt.o.sexualterms = json;
    if (scbt.o.sexualterms && typeof scbt.o.sexualterms == 'object') {
        Object.entries(scbt.o.sexualterms).forEach((entry) => {
        scbt.a.blockedWords.push(entry[1]);
      });
    }
  }
  if (listType == 'profanity') {
    scbt.o.profanity = json;
    if (scbt.o.profanity && typeof scbt.o.profanity == 'object') {
        Object.entries(scbt.o.profanity).forEach((entry) => {
        scbt.a.blockedWords.push(entry[1]);
      });
    }
  }
  if (listType == 'political') {
    scbt.o.political = json;
    if (scbt.o.political && typeof scbt.o.political == 'object') {
        Object.entries(scbt.o.political).forEach((entry) => {
        scbt.a.blockedWords.push(entry[1]);
      });
    }
  }
  if (listType == 'negative') {
    scbt.o.negative = json;
    if (scbt.o.negative && typeof scbt.o.negative == 'object') {
        Object.entries(scbt.o.negative).forEach((entry) => {
        scbt.a.blockedWords.push(entry[1]);
      });
    }
  }
  var json = listType = entry = null; return false;
}

scbt.f.get_sexual_json = function(){
  var url = chrome.runtime.getURL('./sexualterms.json');
  fetch(url)
  .then((response) => response.json())
  .then((json) => scbt.f.save_word_list(json, 'sexualterms') );
}

scbt.f.get_profanity_json = function(){
  var url = chrome.runtime.getURL('./profanity.json');
  fetch(url)
  .then((response) => response.json())
  .then((json) => scbt.f.save_word_list(json, 'profanity') );
}

scbt.f.get_political_json = function(){
  var url = chrome.runtime.getURL('./political.json');
  fetch(url)
  .then((response) => response.json())
  .then((json) => scbt.f.save_word_list(json, 'political') );
}

scbt.f.get_negative_json = function(){
  var url = chrome.runtime.getURL('./negative.json');
  fetch(url)
  .then((response) => response.json())
  .then((json) => scbt.f.save_word_list(json, 'negative') );
}

scbt.f.get_sexual_json();
scbt.f.get_profanity_json();
scbt.f.get_political_json();
scbt.f.get_negative_json();
var url = response = json = null;
// ************** END FILTER functions