/* SPDX-FileCopyrightText: © 2024 promising future digital media llc. All rights reserved. <admin@parleystar.com> */
/* SPDX-License-Identifier: Mozilla Public License 1.1 (MPL-1.1) */
// *************** commands/keybinds functions
console.log('loading commands.js');

scbt.f.user_command1 = function(){
  scbt.f.theatre_mode();
  return false; 
}

scbt.f.user_command2 = function(){
  console.log('streamcommand2 from commands');
  if (scbt.e.scbtSideMenu.classList.contains('scbt-bl') ) {
    scbt.e.scbtSideMenu.classList.remove('scbt-bl');
  } else {
    scbt.e.scbtSideMenu.classList.add('scbt-bl');
  }
  var e = {};
  e.target = {};
  e.target.id = 'scbt22';
  scbt.f.search_chat_toggle(e);
  setTimeout(function() {
    if (scbt.b.searchBarActive === true) {
      var e = {};
      e.target = {};
      e.target.id = 'scbtChatSearchFullButton';
      e.delay = 1;
      scbt.f.search_saved_chat(e);
    }
    e = null; return false;
  }, 2000);
}

scbt.f.user_command3 = function(){
  console.log('streamcommand3 from commands');
  if (scbt.e.scbtSideMenu.classList.contains('scbt-bl') ) {
    scbt.e.scbtSideMenu.classList.remove('scbt-bl');
  } else {
    scbt.e.scbtSideMenu.classList.add('scbt-bl');
  }
  var e = {};
  e.target = {};
  e.target.id = 'scbt22';
  scbt.f.search_chat_toggle(e);
  e = null; return false;
}

scbt.f.user_command4 = function(){
  console.log('streamcommand4 from commands');
  if (scbt.e.scbtSideMenu.classList.contains('scbt-bl') ) {
    scbt.e.scbtSideMenu.classList.remove('scbt-bl');
  } else {
    scbt.e.scbtSideMenu.classList.add('scbt-bl');
  }
  var e = {};
  e.target = {};
  e.target.id = 'scbt23';
  scbt.f.search_chat_toggle(e);
  e = null; return false;
}


scbt.f.keybind_close = function(){
  var elemArr = document.body.getElementsByClassName('scbt-options-wrapper');
  [].forEach.call(elemArr, function(elem) {
    elem.classList.remove('scbt-bl');
  });
  scbt.e.scbtSettingsMenu.classList.remove('scbt-bl');
  scbt.e.scbtChatInput.focus();
  elemArr = elem = null; return false;
}


scbt.f.options_turn_on_keybinds = function(){
  scbt.e.scbtChatA.textContent = '';
  scbt.e.scbtChatB.textContent = '';
  scbt.e.scbtChatTitle.classList.add('scbt-bl');

  window.addEventListener('keydown', function(e) {

    if ( (e.keyCode == '16') && (e.altKey === true) )  {
      // console.log('you pressed shift + Alt to toggle the main menu');
      if (e.preventDefault) { e.preventDefault(); }
      if (scbt.e.scbtSideMenu.classList.contains('scbt-bl') ) {
        scbt.e.scbtSideMenu.classList.remove('scbt-bl');
        scbt.e.scbtChatInput.focus();
        return false;
      } else {
        scbt.e.scbtSideMenu.classList.add('scbt-bl');
        document.body.getElementsByClassName('scbt0')[0].focus();
        return false;
      }
    }

    if ( (e.keyCode == '90') && (e.altKey === true) )  {
      // console.log('you pressed Z + Alt to toggle the scbtChatToggleMenu');
      if (e.preventDefault) { e.preventDefault(); }
      if (scbt.e.scbtChatToggleMenu.classList.contains('scbt-bl') ) {
        scbt.e.scbtChatToggleMenu.classList.remove('scbt-bl');
        scbt_helper_keybind_close();
        scbt.e.scbtChatInput.focus();
        return false;
      } else {
        scbt.e.scbtChatToggleMenu.classList.add('scbt-bl');
        document.body.getElementsByClassName('scbt4')[0].focus();
        return false;
      }
    }

    if ( (e.keyCode == '88') && (e.altKey === true) )  {
      // console.log('you pressed Z + Alt to toggle the options menu');
      if (e.preventDefault) { e.preventDefault(); }
      if (scbt.e.scbtSettingsMenu.classList.contains('scbt-bl') ) {
        scbt.f.keybind_close();
        scbt.e.scbtChatInput.focus();
        return false;
      } else {
        scbt.e.scbtSettingsMenu.classList.add('scbt-bl');
        document.body.getElementsByClassName('scbt0')[0].focus();
        return false;
      }
    }


    if ( (e.keyCode == '67') && (e.altKey === true) )  {
      // console.log('you pressed X + Alt to toggle the options menu bring up hide menu');
      if (e.preventDefault) { e.preventDefault(); }
      if (scbt.e.scbtSettingsMenu.classList.contains('scbt-bl') ) {
        scbt.f.keybind_close();
        scbt.e.scbtChatInput.focus();
        return false;
      } else {
        scbt.e.scbtSettingsMenu.classList.add('scbt-bl');
        // document.body.querySelectorAll('#scbtOptionsHideTitle')[0].click();
        setTimeout(function() {
          // document.body.querySelectorAll('#scbtOptionsHideWrapper')[0].scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
          // document.body.querySelectorAll('#scbthidden1')[0].focus();
          return false;
        }, 1500);
      }
    }


    if ( (e.keyCode == '86') && (e.altKey === true) )  {
      // console.log('you pressed C + Alt to toggle the options menu bring up feature menu');
      if (e.preventDefault) { e.preventDefault(); }
      if (scbt.e.scbtSettingsMenu.classList.contains('scbt-bl') ) {
        scbt.f.keybind_close();
        scbt.e.scbtChatInput.focus();
        return false;
      } else {
        scbt.e.scbtSettingsMenu.classList.add('scbt-bl');
        // document.body.querySelectorAll('#scbtOptionsFeaturesTitle')[0].click();
        setTimeout(function() {
          // document.body.querySelectorAll('#scbtOptionsFeaturesWrapper')[0].scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
          // document.body.querySelectorAll('#scbtfeature1')[0].focus();
          return false;
        }, 1500);
      }
    }


    if ( (e.keyCode == '66') && (e.altKey === true) )  {
      // console.log('you pressed V + Alt to toggle the options menu bring up save menu');
      if (e.preventDefault) { e.preventDefault(); }
      if (scbt.e.scbtSettingsMenu.classList.contains('scbt-bl') ) {
        scbt.f.keybind_close();
        scbt.e.scbtChatInput.focus();
        return false;
      } else {
        scbt.e.scbtSettingsMenu.classList.add('scbt-bl');
        // document.body.querySelectorAll('#scbtOptionsSavedTitle')[0].click();
        setTimeout(function() {
          // document.body.querySelectorAll('#scbtOptionsSavedWrapper')[0].scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
          return false;
        }, 1500);
      }
    }


    if ( (e.keyCode == '81') && (e.altKey === true) )  {
      // console.log('you pressed Q + Alt to toggle Broadcaster Messages Only');
      if (e.preventDefault) { e.preventDefault(); }
      scbt.f.toggle_chats('owner');
      return false;
    }
    if ( (e.keyCode == '87') && (e.altKey === true) )  {
      // console.log('you pressed W + Alt to toggle Moderator Messages Only');
      if (e.preventDefault) { e.preventDefault(); }
      scbt.f.toggle_chats('moderator');
      return false;
    }
    if ( (e.keyCode == '69') && (e.altKey === true) )  {
      // console.log('you pressed E + Alt to toggle Sub Messages Only');
      if (e.preventDefault) { e.preventDefault(); }
      scbt.f.toggle_chats('sub');
      return false;
    }
    if ( (e.keyCode == '82') && (e.altKey === true) )  {
      // console.log('you pressed R + Alt to toggle Sub + Moderator Messages Only');
      if (e.preventDefault) { e.preventDefault(); }
      scbt.f.toggle_chats('mod_sub');
      return false;
    }
    if ( (e.keyCode == '84') && (e.altKey === true) )  {
      // console.log('you pressed T + Alt to toggle VIP Only');
      if (e.preventDefault) { e.preventDefault(); }
      scbt.f.toggle_chats('vip');
      return false;
    }
    if ( (e.keyCode == '89') && (e.altKey === true) )  {
      // console.log('you pressed Y + Alt to toggle Donation Chats Only');
      if (e.preventDefault) { e.preventDefault(); }
      scbt.f.toggle_chats('donation');
      return false;
    }
    if ( (e.keyCode == '85') && (e.altKey === true) )  {
      // console.log('you pressed U + Alt to toggle Mention Messages Only');
      if (e.preventDefault) { e.preventDefault(); }
      scbt.f.toggle_chats('mention');
      return false;
    }
    if ( (e.keyCode == '73') && (e.altKey === true) )  {
      // console.log('you pressed I + Alt to toggle Hashtag Messages Only');
      if (e.preventDefault) { e.preventDefault(); }
      scbt.f.toggle_chats('hashtag');
      return false;
    }
    if ( (e.keyCode == '79') && (e.altKey === true) )  {
      // console.log('you pressed O + Alt to toggle OG Messages Only'); 
      if (e.preventDefault) { e.preventDefault(); }
      scbt.f.toggle_chats('og');
      return false;
    }
    if ( (e.keyCode == '80') && (e.altKey === true) )  {
      // console.log('you pressed P + Alt to toggle text only chat');  // scbt13
      if (e.preventDefault) { e.preventDefault(); }
      scbt.f.chat_text_only();
      return false;
    }
    if ( (e.keyCode == '219') && (e.altKey === true) )  {
      // console.log('you pressed [ + Alt to  View Top of Chat'); // scbt14
      if (e.preventDefault) { e.preventDefault(); }
      scbt.f.chat_up_to_top();
      return false;
    }
    if ( (e.keyCode == '221') && (e.altKey === true) )  {
      // console.log('you pressed ] + Alt to  View Bottom of Chat'); // scbt15
      if (e.preventDefault) { e.preventDefault(); }
      scbt.f.chat_down_to_bottom();
      return false;
    }
    if ( (e.keyCode == '220') && (e.altKey === true) )  {
      // console.log('you pressed  + Alt to  Change Chat Font Size'); // scbt20
      if (e.preventDefault) { e.preventDefault(); }
      scbt.f.chat_font_size();
      return false;
    }
    e = null; return false;
  });
} // end helper_turn_on_keybinds_from_options