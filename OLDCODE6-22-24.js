// OLDCODE.js

'scbtSideMenu',
'scbtChatToggleMenu',
'scbtChatMenu',

if (scbt.o.settingsFromSync.sz5b6 === true) {
  // Show Side Menu 
  theHTML += '<div id="scbtSideMenu" class="scbtSideMenu"><button id="scbt14" class="scbt14" title="View Top of Chat" aria-label="View Top of Chat">⬆️</button><button id="scbt15" class="scbt15" title="View Bottom of Chat" aria-label="View Bottom of Chat">⬇️</button><button id="scbt20" class="scbt20" title="Change Chat Font Size" aria-label="Change Chat Font Size">aA</button><button id="scbt16" class="scbt16" title="Full Width Mode" aria-label="Full Width Mode"> &larr; </button><button id="scbt17" class="scbt17" title="Full Height Mode" aria-label="Full Height Mode"> &uarr; </button></div>';
}
if (scbt.o.settingsFromSync.sz5b7 === true) {
  // Show Chat Menu
  theHTML += '<div id="scbtChatToggleMenu" class="scbtChatToggleMenu"><button id="scbt4" class="scbt4" title="Toggle Broadcaster Messages Only" aria-label="Toggle Broadcaster Messages Only">👑</button><button id="scbt5" class="scbt5" title="Toggle Moderator Messages Only" aria-label="Toggle Moderator Messages Only">🔧</button><button id="scbt6" class="scbt6" title="Toggle Sub Messages Only" aria-label="Toggle Sub Messages Only">💰</button><button id="scbt7" class="scbt7" title="Toggle Sub + Moderator Messages Only" aria-label="Toggle Sub + Moderator Messages Only">💰🔧</button><button id="scbt8" class="scbt8" title="Toggle VIP + Verified Messages Only" aria-label="Toggle VIP + Verified Messages Only">🤵</button><button id="scbt9" class="scbt9" title="Toggle Donation Chats Only" aria-label="Toggle Donation Chats Only">$</button><button id="scbt11" class="scbt11" title="Toggle Mention Messages Only" aria-label="Toggle Mention Messages Only">@</button><button id="scbt12" class="scbt12" title="Toggle Hashtag Messages Only" aria-label="Toggle Hashtag Messages Only">#</button><button id="scbt10" class="scbt10" title="Toggle Non-Bot Messages Only" aria-label="Toggle Non-Bot Messages Only">🤖</button><button id="scbt13" class="scbt13" title="Toggle Text Messages Only" aria-label="Toggle Text Messages Only">ab</button></div>';
}
if (scbt.o.settingsFromSync.sz5b8 === true) {
  // Show Search Menu
  theHTML += '<div id="scbtChatMenu" class="scbtChatMenu"><search>  <label for="scbtChatSearchInputText" class="sr-only">Enter Your Text To Search Chat For:</label><input id="scbtChatSearchInputText" class="scbtChatSearchInputText" name="scbtChatSearchInputText" type="search" autocapitalize="off" autocorrect="off" spellcheck="false" minlength="1" maxlength="100" placeholder="Search for..." aria-label="Enter Your Text To Search Chat For" dir="auto" title="Enter Your Text To Search Chat For" enterkeyhint="search"><span class="validity"></span></search><label for="scbtChatSearchStartsWithButton" class="sr-only">Search for chat starting with...</label><button id="scbtChatSearchStartsWithButton" class="scbtChatSearchStartsWithButton" aria-label="Search for chat starting with..." title="Search for chat starting with..." type="button">✍️</button><label for="scbtChatSearchUserButton" class="sr-only">Search for user saying...</label><button id="scbtChatSearchUserButton" class="scbtChatSearchUserButton" aria-label="Search for user saying..." title="Search for user saying..." type="button">🧑</button><label for="scbtChatSearchKeywordButton" class="sr-only">Search for keyword...</label><button id="scbtChatSearchKeywordButton" class="scbtChatSearchKeywordButton" aria-label="Search for keyword..." title="Search for keyword..." type="button">📝</button><label for="scbtChatSearchEventsButton" class="sr-only">Search for events...</label><button id="scbtChatSearchEventsButton" class="scbtChatSearchEventsButton" aria-label="Search for events..." title="Search for events..." type="button">🗓️</button><label for="scbtChatSearchFullButton" class="sr-only">Show the full chat from the beginning of recording</label><button id="scbtChatSearchFullButton" class="scbtChatSearchFullButton" aria-label="Show the full chat from the beginning of recording" title="Show the full chat from the beginning of recording" type="button">full</button><label for="scbtChatSearchMultiButton" class="sr-only">Search all saved chats rather than current stream</label><button id="scbtChatSearchMultiButton" class="scbtChatSearchMultiButton" aria-label="Search all saved chats rather than current stream" title="Search all saved chats rather than current stream" type="button"> + </button></div>';
}

scbt.e.scbtChatToggleMenu = document.body.getElementsByClassName('scbtChatToggleMenu')[0];


.scbtChatMenu:hover, 
.scbtChatMenu:active,
.scbtChatMenu:focus,
.scbtChatMenu.focused {
  width: 350px;
  transition: width .55s ease;
}

width: fit-content;


scbt.e.scbtChatMenuToggleButton
scbt.f.toggle_chat_menu = function(e){


scbt.e.scbtChatImportButton = b.getElementsByClassName('scbtChatImportButton')[0];
scbt.e.scbtSortStreamsByServiceID = b.getElementsByClassName('scbtSortStreamsByServiceID')[0];
scbt.e.scbtSortStreamsByChannelID = b.getElementsByClassName('scbtSortStreamsByChannelID')[0];
scbt.e.scbtSortStreamsByVideoID = b.getElementsByClassName('scbtSortStreamsByVideoID')[0];
scbt.e.scbtSortStreamsByChan = b.getElementsByClassName('scbtSortStreamsByChan')[0];


