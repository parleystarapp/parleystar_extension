/* SPDX-FileCopyrightText: © 2024 promising future digital media llc. All rights reserved. <admin@parleystar.com> */
/* SPDX-License-Identifier: Mozilla Public License 1.1 (MPL-1.1) */
console.log('loading tiktok', scbt);

scbt.f.chat_clean_tiktok = function(obj, elem){
  var elemArr = [];
  var str = null;
  var special = false;
  var stampArr = new Date().toLocaleTimeString().replace(/ /g, '').split(':'); // = 11:34:03AM  ['12', '11', '42PM']
  var timestamp = stampArr[0] + ':' + stampArr[1] + stampArr[2].slice(2);
  obj.timestamp = timestamp;
  // HOST EVENT

  // SUB EVENT

  // GIFT EVENT 

  // username
  elemArr = elem.querySelectorAll("[data-e2e='message-owner-name']");
  if (elemArr[0] && elemArr[0].textContent) { 
    str = elemArr[0].textContent;
  }

  obj.username = null;
  if (str) {
    obj.username = str;
    obj = scbt.f.get_obj_cleaned_username_from_obj(obj);
  }

  str = null;
  // chat message 
  var elemArr = scbt.f.get_arr_message_elems_from_parent_element(elem);
  if (elemArr[0] && elemArr[0].textContent) { 
    str = elemArr[0].textContent;
  }

  obj.message = null;
  if (str) {
    obj.message = str;
    obj = scbt.f.get_obj_cleaned_message_from_obj(obj);
    if (obj.message.indexOf('@') > -1) {
      obj.mention = 1;
    }
    if (obj.message.indexOf('#') > -1) {
      obj.hashtag = 1;
    }
    if (obj.message == null || obj.message.trim() === '') { } else {
      obj.message = obj.message.replace(/(?:\r\n|\r|\n)/g, '');
      obj.message = obj.message.split("\t").join("");
      obj.message = obj.message.split("\"").join("");
      obj.message = obj.message.split("\'").join("");
    }
  }

  var imgElemArr = elem.getElementsByTagName('img');
  [].forEach.call(imgElemArr, function(imgElm) {
    var srcStr = imgElm.getAttribute('src');
    if (srcStr) {
      if (srcStr.indexOf('broadcaster') > -1) {
        obj.owner = 1;
      }
      if (srcStr.indexOf('staff') > -1) {
        obj.staff = 1;
      }
      if (srcStr.indexOf('sub') > -1) {
        obj.sub = 1;
      }
      if (srcStr.indexOf('moderater') > -1) {
        obj.moderator = 1;
      }
      if (srcStr.indexOf('gifter') > -1) {
        obj.gifter = 1;
      }
      if (srcStr.indexOf('fans_badge') > -1) {
        obj.og = 1;
      }
    }
  });

  if (elem.classList.contains('vip')) {
    obj.verified = 1;
  }

  if (obj.username && obj.message) {
    var um = obj.message;
    obj.itemid = obj.username + um.substring(0, 7); // + Math.floor(Math.random() * 100)
  }
  
  special = stampArr = str = elemArr = elemArr2 = elemArr3 = timestamp = imgs = alt = imgElm = imgElemArr = itemId = a = elem = null; return obj;
}


scbt.f.chat_make_decisions_tiktok = function(obj, elem, settings){
  
  if (settings.s1b1tiktok === true) {
    var elemArr = elem.getElementsByTagName('img');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    var elemArr = elem.getElementsByTagName('i');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    var elemArr = elem.getElementsByTagName('svg');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    var elemArr = elem.getElementsByTagName('canvas');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    elemArr = elm2 = null;
  }

  // Highlight sub messages in chat in this hex colour h1
  if (settings.s2c1tiktok != '#000000') {
    if (obj.sub === 1) {
      obj.isHighlighted = settings.s2c1tiktok;
    }
  }
  // Highlight gifter messages in chat in this hex colour h2
  if (settings.s2c9tiktok != '#000000') {
    if (obj.gifter === 1) {
      obj.isHighlighted = settings.s2c9tiktok;
    }
  }
  // Highlight VIP messages in chat in this hex colour h3 
  if (settings.s2c5tiktok != '#000000') {
    if (elem.classList.contains('vip')) {
      obj.isHighlighted = settings.s2c5tiktok;
    }
  }
  // Highlight founder messages in chat in this hex colour  
  if (settings.s2c6tiktok != '#000000') {
    if (obj.founder === 1) {
      obj.isHighlighted = settings.s2c6tiktok;
    }
  }
  // Highlight OG messages in chat in this hex colour  
  if (settings.s2c7tiktok != '#000000') {
    if (obj.og === 1) {
      obj.isHighlighted = settings.s2c7tiktok;
    }
  }
  // Highlight owner messages in chat in this hex colour  
  if (settings.s2c8tiktok != '#000000') {
    if (obj.owner === 1) {
      obj.isHighlighted = settings.s2c8tiktok;
    }
  }
  // Highlight moderator messages in chat in this hex colour  
  if (settings.s2c2tiktok != '#000000') {
      if (obj.moderator === 1) {
        obj.isHighlighted = settings.s2c2tiktok;
      }
  }
  // Mute owner/streamer messages in chat in dim text  
  if (settings.s3b2 === true) {
      if (obj.owner === 1) {
        obj.isMuted = 1;
      }
  }
  // Hide owner/streamer messages in chat  
  if (settings.s4b2 === true) {
      if (obj.owner === 1) {
        obj.isHidden = 1;
      }
  }
  if ( (obj.owner === 1) || (obj.moderator === 1) || (obj.sub === 1) || (obj.newSub === 1) || (obj.verified === 1) ) { } else {
    // scbtmuted4 Mute non moderator/sub messages in chat  
    if (settings.s3b1 === true) {
      obj.isMuted = 1;
    }
    // scbthidden4 Hide non moderator/sub messages in chat 
    if (settings.s4b1 === true) {
      obj.isHidden = 1;
    }
  }
  // scbtmuted3 Mute bot messages  
  if (settings.s3b4 === true) {
    if (obj.username && obj.message) {
      if (obj.isBot === 1) {
        obj.isMuted = 1;
      }
    }
  }
  // scbthidden3 Hide bot  messages 
  if (settings.s4b4 === true) {
    if (obj.username && obj.message) {
      if (obj.isBot === 1) {
        obj.isHidden = 1;
      }
    }
  }
  // Highlight mention messages in chat in this hex colour 
  if (settings.s2c3tiktok != '#000000') {
    if (obj.mention === 1) {
      obj.isHighlighted = settings.s2c3tiktok;
    }
  }
  // Highlight hashtag messages in chat in this hex colour  
  if (settings.s2c4tiktok != '#000000') {
    if (obj.hashtag === 1) {
      obj.isHighlighted = settings.s2c4tiktok;
    }
  }
  // Mute @ mention messages in chat in dim text  
  if (settings.s3b3 === true) {
    if (obj.mention === 1) {
      obj.isMuted = 1;
    }
  }
  // Hide @ mention messages in chat 
  if (settings.s4b3 === true) {
    if (obj.mention === 1) {
      obj.isHidden = 1;
    }
  }
  elem = settings = null;
  return obj;
}


scbt.f.style_with_obj_of_changes_tiktok = function(obj){
  var css = '';
  console.log('style_with_obj_of_changes_tiktok obj', obj);

  // Background of chat in this hex colour.
  if (obj.s1c1tiktok) {
    if (obj.s1c1tiktok == '#000000' || obj.s1c1tiktok == '') {
      css = css + ' body.scbt-tiktok [data-e2e="chat-room"] { background-color: asdf !important; } ';
    }
    if (obj.s1c1tiktok == '#ffffff') {
      css = css + ' body.scbt-tiktok [data-e2e="chat-room"] { background-color: transparent !important; } ';
    }
    if ( (obj.s1c1tiktok != '#ffffff') && (obj.s1c1tiktok != '#000000') && (obj.s1c1tiktok != '') ) {
      css = css + ' body.scbt-tiktok [data-e2e="chat-room"] { background-color: ' + obj.s1c1tiktok + ' !important; } ';
    }
  }

  // User names in chat in this hex colour
  if (obj.s1c2tiktok) {
    if (obj.s1c2tiktok == '#000000' || obj.s1c2tiktok == '') {
      css = css + ' body.scbt-tiktok [data-e2e="message-owner-name"] { color: asdf !important; text-shadow: 0px; } ';
    }
    if (obj.s1c2tiktok == '#ffffff') {
      css = css + ' body.scbt-tiktok [data-e2e="message-owner-name"] { color: transparent !important; text-shadow: 0.3px 0.3px dimgray; } ';
    }
    if ( (obj.s1c2tiktok != '#ffffff') && (obj.s1c2tiktok != '#000000') && (obj.s1c2tiktok != '') ) {
      css = css + ' body.scbt-tiktok [data-e2e="message-owner-name"] { color:' + obj.s1c2tiktok + ' !important; text-shadow: 0.5px 0.5px darkslategrey; } ';
    }
  }

  // Highlights of chat in this colour
  if (obj.s1c3tiktok) {
    if (obj.s1c3tiktok != '#000000') {
      scbt.c.scbtBorderColor = obj.s1c3tiktok;
    } else {
      scbt.c.scbtBorderColor = '#ff0000';
    }
  }

  // Chat messages in this colour 
  if (obj.s1c4tiktok) {
    if (obj.s1c4tiktok == '#000000' || obj.s1c4tiktok == '') {
      css = css + ' body.scbt-tiktok div[class*="DivComment"], body.scbt-tiktok [data-e2e="social-message"], body.scbt-tiktok [data-e2e="enter-message"], body.scbt-tiktok .tiktok-15hhtcj, body.scbt-tiktok .tiktok-8juc6m { color: asdf !important; text-shadow: 0px; } ';
    }
    if (obj.s1c4tiktok == '#ffffff') {
      css = css + ' body.scbt-tiktok div[class*="DivComment"], body.scbt-tiktok [data-e2e="social-message"], body.scbt-tiktok [data-e2e="enter-message"], body.scbt-tiktok .tiktok-15hhtcj, body.scbt-tiktok .tiktok-8juc6m { color: transparent !important; text-shadow: 0.3px 0.3px dimgray; } ';
    }  
    if ( (obj.s1c4tiktok != '#ffffff') && (obj.s1c4tiktok != '#000000') && (obj.s1c4tiktok != '') ) {
      css = css + ' body.scbt-tiktok div[class*="DivComment"], body.scbt-tiktok [data-e2e="social-message"], body.scbt-tiktok [data-e2e="enter-message"], body.scbt-tiktok .tiktok-15hhtcj, body.scbt-tiktok .tiktok-8juc6m { color: ' + obj.s1c4tiktok + ' !important; text-shadow: 0.5px 0.5px darkslategrey; } ';
    }
  }

  // Chat font sizes
  if (obj.s1n1tiktok) {
    if ( Number(obj.s1n1tiktok) > 0) {
      var str = Number(obj.s1n1tiktok) + 'rem';
      css = css + ' body.scbt-tiktok div[class*="DivComment"], body.scbt-tiktok [data-e2e="social-message"], body.scbt-tiktok [data-e2e="enter-message"], body.scbt-tiktok .tiktok-15hhtcj, body.scbt-tiktok .tiktok-8juc6m { font-size: ' + str + '; line-height: ' + Number(obj.s1n1tiktok)  + '; } ';
    } else {
      css = css + ' body.scbt-tiktok div[class*="DivComment"], body.scbt-tiktok [data-e2e="social-message"], body.scbt-tiktok [data-e2e="enter-message"], body.scbt-tiktok .tiktok-15hhtcj, body.scbt-tiktok .tiktok-8juc6m { font-size: initial; line-height: unset; } ';
    }
  }

  // text only chat
  if (obj.s1b1tiktok === true) {
    css = css + ' body.scbt-tiktok [data-e2e="chat-room"] img, body.scbt-tiktok [data-e2e="chat-room"] svg, body.scbt-tiktok [data-e2e="chat-room"] i, body.scbt-tiktok [data-e2e="chat-room"] canvas { visibility: hidden !important; } ';
  }
  if (obj.s1b1tiktok === false) {
    css = css + ' body.scbt-tiktok [data-e2e="chat-room"] img, body.scbt-tiktok [data-e2e="chat-room"] svg, body.scbt-tiktok [data-e2e="chat-room"] i, body.scbt-tiktok [data-e2e="chat-room"] canvas { visibility: visible !important; } ';      
  }

  // Left handed screen. Flip screen so video is on the right and chat is on the left.
  if (obj.sz5b16 === true) {
    css = css + ' body.scbt-tiktok #placeholder { flex-direction: row-reverse; } ';
  }
  if (obj.sz5b16 === false) {
    css = css + ' body.scbt-tiktok #placeholder .relative.flex.h-full.w-full { flex-direction: initial; } ';
  }

  // Upside down chat screen. Newest chat is on the top, oldest chat is on the bottom 
  if (obj.sz5b17 === true) {
     css = css + ' body.scbt-tiktok #placeholder { display: flex; flex-direction: column-reverse; } ';
  }
  if (obj.sz5b17 === false) {
    css = css + ' body.scbt-tiktok #placeholder { flex-direction: initial; display: block; } ';
  }

  // mouseover hover enlarge
  if (obj.setting6b7 === true) {
    css = css + ' body.scbt-tiktok div[class*="DivComment"]:hover, body.scbt-tiktok [data-e2e="social-message"]:hover, body.scbt-tiktok [data-e2e="enter-message"]:hover, body.scbt-tiktok .tiktok-15hhtcj:hover, body.scbt-tiktok .tiktok-8juc6m:hover { font-size: 175%; } ';
  }
  if (obj.setting6b7 === false) {
    css = css + ' body.scbt-tiktok div[class*="DivComment"]:hover, body.scbt-tiktok [data-e2e="social-message"]:hover, body.scbt-tiktok [data-e2e="enter-message"]:hover, body.scbt-tiktok .tiktok-15hhtcj:hover, body.scbt-tiktok .tiktok-8juc6m:hover { font-size: initial; } ';
  }

  if (css) {
    var head = document.getElementsByTagName('head')[0];
    var style = document.createElement('style');
    head.appendChild(style);
    style.type = 'text/css';
    if (style.styleSheet){
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }
  }
  obj = t = head = style = css = null; return false;
}


scbt.f.toggle_chats_tiktok = function(parameter){
  var elemArr = scbt.f.get_arr_chats();
  if (scbt.n.visibilityChatShow === 1) {
    scbt.e.scbtChatB.textContent = scbt.f.set_chat_parameter(parameter, scbt.n.visibilityChatShow);
    [].forEach.call(elemArr, function(elem) {
      elem.style.opacity = 0;

      if (parameter == 'owner') {
        var imgElemArr = elem.getElementsByTagName('img');
        [].forEach.call(imgElemArr, function(imgElm) {
          var srcStr = imgElm.getAttribute('src');
          if (srcStr) {
            if (srcStr.indexOf('broadcaster') > -1) {
              scbt.f.chat_on(elem);
              elem.style.opacity = 1;
            }
          }
        });
      } 
      if (parameter == 'sub') {
        var imgElemArr = elem.getElementsByTagName('img');
        [].forEach.call(imgElemArr, function(imgElm) {
          var srcStr = imgElm.getAttribute('src');
          if (srcStr) {
            if (srcStr.indexOf('sub') > -1) {
              scbt.f.chat_on(elem);
              elem.style.opacity = 1;
            }
          }
        });
      }
      if (parameter == 'moderator') {
        var imgElemArr = elem.getElementsByTagName('img');
        [].forEach.call(imgElemArr, function(imgElm) {
          var srcStr = imgElm.getAttribute('src');
          if (srcStr) {
            if (srcStr.indexOf('moderater') > -1) {
              scbt.f.chat_on(elem);
              elem.style.opacity = 1;
            }
          }
        });
      }
      if (parameter == 'mod_sub') {
        var imgElemArr = elem.getElementsByTagName('img');
        [].forEach.call(imgElemArr, function(imgElm) {
          var srcStr = imgElm.getAttribute('src');
          if (srcStr) {
            if (srcStr.indexOf('sub') > -1 || srcStr.indexOf('moderater') > -1) {
              scbt.f.chat_on(elem);
              elem.style.opacity = 1;
            }
          }
        });
      }
      if ( (parameter == 'vip') || (parameter == 'verified') ) {
        if (elem.classList.contains('vip') ) {
          scbt.f.chat_on(elem);
          elem.style.opacity = 1;
        }
        var imgElemArr = elem.getElementsByTagName('img');
        [].forEach.call(imgElemArr, function(imgElm) {
          var srcStr = imgElm.getAttribute('src');
          if (srcStr) {
            if (srcStr.indexOf('fans_badge') > -1) {
              scbt.f.chat_on(elem);
              elem.style.opacity = 1;
            }
          }
        });
      }
      if (parameter == 'gifter') {
        var imgElemArr = elem.getElementsByTagName('img');
        [].forEach.call(imgElemArr, function(imgElm) {
          var srcStr = imgElm.getAttribute('src');
          if (srcStr) {
            if (srcStr.indexOf('gifter') > -1) {
              scbt.f.chat_on(elem);
              elem.style.opacity = 1;
            }
          }
        });
      }
      if (parameter == 'mention') {
          var str = elem.textContent;
          if (str) {
            if ( str.indexOf('@') > -1 ) {
              scbt.f.chat_on(elem);
              elem.style.opacity = 1;
            }
          }
      }
      if (parameter == 'hashtag') {
          var str = elem.textContent;
          if (str) {
            if ( str.indexOf('#') > -1 ) {
              scbt.f.chat_on(elem);
              elem.style.opacity = 1;
            }
          }
        }
      if (parameter == 'donation') {
        var elem3Arr = elem.querySelectorAll('.tiktok-9wfq77, .tiktok-1fub1g4');
        if (elem3Arr[0]) {
          scbt.f.chat_on(elem);
          elem.style.opacity = 1;
        }
      }
  });
  scbt.n.visibilityChatShow = 2;
  parameter = str = a = alt = elemArr = elem = elem2Arr = elem2 = toSearchFor = arr = null; return false;
}

  if (scbt.n.visibilityChatShow === 2) {
    scbt.e.scbtChatB.textContent = scbt.f.set_chat_parameter(parameter, scbt.n.visibilityChatShow);
    [].forEach.call(elemArr, function(elem) {
        elem.style.opacity = 1;
        scbt.f.chat_blur(elem);
        
        if (parameter == 'owner') {
        var imgElemArr = elem.getElementsByTagName('img');
        [].forEach.call(imgElemArr, function(imgElm) {
          var srcStr = imgElm.getAttribute('src');
          if (srcStr) {
            if (srcStr.indexOf('broadcaster') > -1) {
              elem.style.opacity = 0;
            }
          }
        });
      } 
      if (parameter == 'sub') {
        var imgElemArr = elem.getElementsByTagName('img');
        [].forEach.call(imgElemArr, function(imgElm) {
          var srcStr = imgElm.getAttribute('src');
          if (srcStr) {
            if (srcStr.indexOf('sub') > -1) {
              elem.style.opacity = 0;
            }
          }
        });
      }
      if (parameter == 'moderator') {
        var imgElemArr = elem.getElementsByTagName('img');
        [].forEach.call(imgElemArr, function(imgElm) {
          var srcStr = imgElm.getAttribute('src');
          if (srcStr) {
            if (srcStr.indexOf('moderater') > -1) {
              elem.style.opacity = 0;
            }
          }
        });
      }
      if (parameter == 'mod_sub') {
        var imgElemArr = elem.getElementsByTagName('img');
        [].forEach.call(imgElemArr, function(imgElm) {
          var srcStr = imgElm.getAttribute('src');
          if (srcStr) {
            if (srcStr.indexOf('sub') > -1 || srcStr.indexOf('moderater') > -1) {
              elem.style.opacity = 0;
            }
          }
        });
      }
      if ( (parameter == 'vip') || (parameter == 'verified') ) {
        if (elem.classList.contains('vip') ) {
          scbt_helper_chat_on(elem);
          elem.style.opacity = 1;
        }
        var imgElemArr = elem.getElementsByTagName('img');
        [].forEach.call(imgElemArr, function(imgElm) {
          var srcStr = imgElm.getAttribute('src');
          if (srcStr) {
            if (srcStr.indexOf('fans_badge') > -1) {
              elem.style.opacity = 0;
            }
          }
        });
      }
      if (parameter == 'gifter') {
        var imgElemArr = elem.getElementsByTagName('img');
        [].forEach.call(imgElemArr, function(imgElm) {
          var srcStr = imgElm.getAttribute('src');
          if (srcStr) {
            if (srcStr.indexOf('gifter') > -1) {
              elem.style.opacity = 0;
            }
          }
        });
      }
      if (parameter == 'mention') {
          var str = elem.textContent;
          if (str) {
            if ( str.indexOf('@') > -1 ) {
              elem.style.opacity = 0;
            }
          }
      }
      if (parameter == 'hashtag') {
          var str = elem.textContent;
          if (str) {
            if ( str.indexOf('#') > -1 ) {
              elem.style.opacity = 0;
            }
          }
        }
      if (parameter == 'donation') {
        var elem3Arr = elem.querySelectorAll('.tiktok-9wfq77, .tiktok-1fub1g4');
        if (elem3Arr[0]) {
          elem.style.opacity = 0;
        }
      }
      // end
      });
      scbt.n.visibilityChatShow = 3;
    parameter = str = a = alt = elemArr = elem = elem2Arr = elem2 = toSearchFor = arr = null; return false;
  }

  if (scbt.n.visibilityChatShow === 3) {
    scbt.e.scbtChatB.textContent = '';
    [].forEach.call(elemArr, function(elem) {
      scbt.f.chat_off(elem);
      elem.style.opacity = 1;
    });
    scbt.n.visibilityChatShow = 1;
    parameter = str = a = alt = elemArr = elem = elem2Arr = elem2 = toSearchFor = arr = null; return false;
  }
  return false;
}
