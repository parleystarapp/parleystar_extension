


async function getKickVODs(params){
    var {channel_slug,slug} = params;
    return await fetchJSON(`https://kick.com/api/v2/channels/${(channel_slug || slug)}/videos`,{});
}
async function getFollowedChannels(){
    return await fetchJSON("https://kick.com/api/v2/channels/followed?cursor=0");
}
// getKickVOD(getCookieAsJSON())

async function getChannelInfo(params){
    var {slug,channel_slug} = params;
    return await fetchJSON(`https://kick.com/api/v1/channels/${slug || channel_slug}`);
}
async function kickChannelSearch(params){
    var {query} = params;
    var d = await fetchJSON("https://search.kick.com/multi_search", {
    "headers": {
        "accept": "application/json, text/plain, */*",
        "accept-language": "en-US,en;q=0.9",
        "content-type": "application/json",
        "sec-ch-ua": "\"Not/A)Brand\";v=\"99\", \"Microsoft Edge\";v=\"115\", \"Chromium\";v=\"115\"",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "\"Windows\"",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-site",
        "x-typesense-api-key": "nXIMW0iEN6sMujFYjFuhdrSwVow3pDQu"
    },
    "referrer": "https://kick.com/",
    "referrerPolicy": "strict-origin-when-cross-origin",
    "body": `{\"searches\":[{\"preset\":\"channel_search\",\"q\":\"${query}\"}]}`,
    "method": "POST",
    "mode": "cors",
    "credentials": "omit"
    });
    return d;
}

async function getLivestreamInfo(params){
    console.log("doing getLivestreamInfo with params ", params);

    var res = await fetch(`https://kick.com/api/v2/channels/${params?.channel_name}/livestream`);
    var d = await res.json();
    console.log("d ", d);
    var data = parseIVSDataFromLivestreamRes(d);
    console.log("parseIVSDataFromLivestreamRes ", data);
    var stuff = data?.src?.replace(/hls\/\w+\/playlist.m3u8/,'hls/master.m3u8');
    console.log("stuff ", stuff);
    
    let resolutions = await requestFromBackground({
        cmd:'getVideoStreamData',
        params:{
            source:data?.src?.replace(/hls\/\w+\/playlist.m3u8/,'hls/master.m3u8')
        }
    });
    let output = resolutions.map(r=> ({...r,...data})).map(r=> {
        return {
            ...r,
            ...{
                src:r.src?.replace(/(?<=media\/hls\/)\w+/,r.group_id),
                ts_url:r.ts_url?.replace(/(?<=media\/hls\/)\w+/,r.group_id),
            }
        }
    });
    let text = await fetchText(output?.[0].src);
    let playlist_source = parsePlaylistText(text);
    return output.map(r=> ({...r,...{playlist_source:playlist_source}}));
}

function parsePlaylistText(text){
    var header = /[\s\S\n]+?(?=#EXT-X-PROGRAM-DATE-TIME:)/.exec(text)?.[0]
    var first_chunk = /#EXT-X-PROGRAM-DATE[\s\n\S]+?\d+\.ts/.exec(text)?.[0]
    var footer = '#EXT-X-ENDLIST';
    return `${header}${first_chunk}\n${footer}`;
    // var total_sec_x = /#EXT-.+?TOTAL-SECS:(\d+).\d+/;
}
// async function getM3u8Data(params){
//     var res = await fetch(params?.src?.replace(/hls\/\w+\/playlist.m3u8/,'hls/master.m3u8'));
//     var text = await res.text();
//     console.log(text);
// }
function parseIVSDataFromLivestreamRes(d){
    var twoDigi = (s)=> s?.length == 1 ? `0${s}` : s;
    let match = /(?<=.kick.com\/video_thumbnails\/)(\w+)\/(\w+)/.exec(d?.data?.thumbnail?.src);
    let yyyy_mm_dd_hh_mm = /(\d{4})-(\d{1,2})-(\d{1,2})T(\d{1,2}):(\d{1,2}):/.exec(d?.data?.created_at)
    
    let data = {
        start_timestamp:new Date(d?.data?.created_at).getTime(),
        created_at:d?.data?.created_at,
        yyyy:twoDigi(yyyy_mm_dd_hh_mm?.[1]),
        mo:twoDigi(yyyy_mm_dd_hh_mm?.[2]),
        dd:twoDigi(yyyy_mm_dd_hh_mm?.[3]),
        hh:twoDigi(yyyy_mm_dd_hh_mm?.[4]),
        mm:twoDigi(yyyy_mm_dd_hh_mm?.[5]),
        channel_hash:match?.[1],
        vod_hash:match?.[2],
        is_livestream:true,
        channel_id:/\d+(?=\.channel\.)/.exec(d?.data?.playback_url)?.[0],
    }
    // return data;
    let ts_link = buildTSlinkFromIVSdata(data);
    let src = ts_link?.replace(/\d+\.ts/,'playlist.m3u8');
    return {
        ...data,
        ...{
            ts_url:ts_link,
            src:src,
        }
    }
    // https://stream.kick.com/ivs/v1/196233775518/lIkU90GkfSDB/2023/11/25/12/28/R1lFTrjBX23w/media/hls/master.m3u8
}

function buildTSlinkFromIVSdata(params){
    // console.log(['buildTSlinkFromIVSdata',params])
    var {vod_hash,channel_hash,channel_id,dd,hh,mm,mo,yyyy,group_id} = params;
    return `https://stream.kick.com/ivs/v1/${channel_id}/${channel_hash}/${yyyy}/${mo}/${dd}/${hh}/${mm}/${vod_hash}/media/hls/${group_id ? group_id : '720p60'}/1.ts`
}
function getLivestreamTSFilesFromNowDifferential(params){
    return Math.ceil(Math.round(new Date().getTime()-params?.start_timestamp)/1000/8)
}
// getLivestreamInfo({channel_name:'chudlogic'})

// kickChannelSearch({query:'ho'})

// async function tester(params){

// }
// tester()
function getUTCdateString(date){
    var zeroFront = (n,d)=> (n+d).toString().replace(/^1/,'')
    return `${date.getUTCFullYear()}-${zeroFront(100,(date.getUTCMonth()+1))}-${zeroFront(100,date.getUTCDate())}T${zeroFront(100,date.getUTCHours())}:${zeroFront(100,date.getUTCMinutes())}:${zeroFront(100,date.getUTCSeconds())}.${zeroFront(1000,date.getUTCMilliseconds())}Z`
}
var mostFrequent = (arr) =>
  Object.entries(    arr.reduce((a, v) => {
      a[v] = a[v] ? a[v] + 1 : 1;
      return a;
    }, {})  ).reduce((a, v) => (v[1] >= a[1] ? v : a), [null, 0])[0];

function getExtendStreamSource(params){
    var {text,start_timestamp} = params;
    var segments = text.match(/#EXT-X-PROGRAM-DATE[\s\n\S]+?\d+\.ts/g);
    var segment_data = segments?.length ? Array.from(segments).map(s=> ({
        date_time:/(?<=#EXT-X-PROGRAM-DATE-TIME:).+Z/.exec(s)?.[0],
        ts_path:/\d+\.ts/.exec(s)?.[0],
        ts_num:parseInt(/(\d+)\.ts/.exec(s)?.[1]),
        dur:parseFloat(/(?<=#EXTINF:)[\d\.]+/.exec(s)?.[0])
    })) : [];
    // console.log(segment_data);
    var segment_duration = mostFrequent(segment_data.map(r=> r.dur));
    var seconds_in_text = segment_data?.length ? segment_data.map(r=> r.dur).reduce((a,b)=> a+b) : 0;
    var stream_seconds_til_now = Math.ceil((new Date().getTime() - start_timestamp)/1000);
    var seconds_differential = (stream_seconds_til_now - seconds_in_text);
    var differential = Math.ceil(seconds_differential/ segment_duration);
    var last_timestamp = segment_data?.length ? new Date(segment_data.at(-1).date_time).getTime() : 0;
    var last_segment = segment_data.at(-1);
    
    
    var additional_segment_text = differential ? Array(differential+1).fill().map((r,i)=> `#EXT-X-PROGRAM-DATE:${getUTCdateString(new Date(last_timestamp+((segment_duration*i)*1000)))}
#EXTINF:${last_segment?.dur}.000,
${(last_segment?.ts_num+i)}.ts
`).reduce((a,b)=> a+b) : '';
    var updated_source = text.replace(/#EXT-X-ENDLIST/,additional_segment_text+'#EXT-X-ENDLIST').replace(/(?<=#ID3-EQUIV-TDTG:).+/,i=>getUTCdateString(new Date(i+'.000Z')).replace(/\.\d{3}Z/,'')).replace(/(?<=#EXT-X-TWITCH-TOTAL-SECS:)\d+/,stream_seconds_til_now).replace(/(?<=#EXT-X-MEDIA-SEQUENCE:)\d+/,((differential-1)+segment_data.length));
    // console.log(updated_source);
    return updated_source
}
