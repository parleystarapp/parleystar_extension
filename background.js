/* SPDX-FileCopyrightText: © 2024 promising future digital media llc. All rights reserved. <admin@parleystar.com> */
/* SPDX-License-Identifier: Mozilla Public License 1.1 (MPL-1.1) */
function foreground_message_director(request, sender, sendResponse) {
  if (request.loadServiceJS) {
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      chrome.scripting.executeScript({
        target: {tabId: tabs[0].id},
        files: [request.loadServiceJS +'.js']
      });
    });
    return false;
  }

  if (request.simulcastReceiver) {
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      // chrome.tabs.sendMessage(tabs[0].id, { simulcastReceiver: tabs[0].id } );
      var tabId = tabs[0].id;
      chrome.tabs.query({}, function(allTabs) {
        for (var tab of allTabs) {
          try {
            var response = chrome.tabs.sendMessage(tab.id, {simulcastReceiverTabId: tabId} );
          } catch (e) {
            console.error("WHATANError: ", e);
          }
        }
      });
    });
  }

  if (request.chatmessage && request.chatmessage.simulcastReceiverTabId) {
    request.chatmessage.wayofthewarrior = '3D0';
    chrome.tabs.sendMessage(request.chatmessage.simulcastReceiverTabId, { message: request.chatmessage } );
    return false;
  } else {
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      chrome.tabs.sendMessage(tabs[0].id, { scbtgg: request.chatmessage } );
      return false;
    });  
  }

  if (request.streamForOffline) {
    chrome.tabs.query({}, function(allTabs) {
      for (var tab of allTabs) {
        try {
          if ( (tab.url == 'https://archyved.com/') && (tab.incognito === false) && (tab.status == 'complete') ) {
            var response = chrome.tabs.sendMessage(tab.id, {streamChatToArchive: request.streamForOffline} );
          }
        } catch (e) {
          console.error("WHATANError: ", e);
        }
      }
    }); 
  }
}


chrome.storage.onChanged.addListener((changes, area) => {
  var obj = {};
  obj.changes = changes;
  obj.area = area;

  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.tabs.sendMessage(tabs[0].id, { scbtChanged: obj } );
    return false;
  });
});


chrome.tabs.onUpdated.addListener(function (tabId, info, tab) {
  if (info.status == 'complete') {
    chrome.tabs.sendMessage(tabId, { "tabUpdated": tab }, function(response) { return true; });  
  } else { return true; }
});


chrome.action.onClicked.addListener((tab) => {
  chrome.tabs.sendMessage(tab.id, { "clickedTab": tab }, function(response) { return true; });
  chrome.scripting.executeScript({
    target: {tabId: tab.id},
    files: ['open_side_menu.js']
  });
});


chrome.commands.onCommand.addListener((command) => {
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    var activeTab = tabs[0];
    chrome.tabs.sendMessage(activeTab.id, {"message": command}, function(response) { return true; });
  });
});

chrome.runtime.onMessage.addListener(foreground_message_director);
chrome.runtime.setUninstallURL('https://parleystar.com/uninstall');