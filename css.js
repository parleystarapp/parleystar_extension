// .drag_actor_head:hover {
//     background: linear-gradient(to bottom, transparent, transparent, transparent, transparent, transparent, ${colors.kick_green});
// }
// .drag_actor_right:hover {
//     background: linear-gradient(to left, transparent, transparent, transparent, transparent, transparent, ${colors.kick_green});
// }
// .drag_actor_left:hover {
//     background: linear-gradient(to right, transparent, transparent, transparent, transparent, transparent, ${colors.kick_green});
// }
// .drag_actor_foot:hover {
//     background: linear-gradient(to top, transparent, transparent, transparent, transparent, transparent, ${colors.kick_green});
// }
// box-shadow: #4f1b1a 1px 2px 1px 0px inset, #4f1b1a -1px -0px 0px 0px inset;

function addGoogleFont(url){
    let link = document.createElement('link');
    document.head.appendChild(link);
    link.setAttribute('href',url);
    link.setAttribute('rel',"stylesheet");    
}
addGoogleFont('https://fonts.googleapis.com/css2?family=Orbitron:wght@100;200;300;400;500;600;700;800;900&amp;display=swap')
addGoogleFont('https://fonts.googleapis.com/css2?family=Share+Tech+Mono:wght@100;200;300;400;500;600;700;800;900&amp;display=swap')

// #record_svg:hover #stop_rect_svg {
//     height: 10px;
//     width: 0px;
//     transform: translate(30px, 10px);
//     transition: 433ms ease-in-out;
// }
setHTMLCSS('kick_downloader_app_styles', `

    .copy_m3u8_cont:hover .copy_svg_cont{
        transform:scale(0.8,0.8);
        transition: 133ms ease-in-out;
    }

    #stop_tri_svg{
        transform: scale(1,1) translate(89px,89px);
    }
    
    #record_svg:hover #stop_tri_svg{
        transform: scale(5,5) translate(10px,10px);  
    }
  

  
    #rec_text_elm {
        font-family:"Orbitron", sans-serif;
        font-weight:800;
    }
    .orbitron {
        font-family:"Orbitron", sans-serif;
    }
    .record_stream_btn {
        cursor:pointer;
        
    }

    .opt_button {
        border-radius: 0.4em;
        background: linear-gradient(to bottom right, ${colors.kick_green_trans}, transparent, transparent, transparent);
    }
    .opt_button:active {
        background: linear-gradient(to bottom right, transparent, transparent, transparent, ${colors.kick_green_trans});
        box-shadow: #0d0b0f 1px 2px 1px 0px inset, ${colors.kick_green_trans} -1px -0px 0px 0px inset;
    }

    .drag_actor_head, .drag_actor_left, .drag_actor_right, .drag_actor_foot {
        cursor:grab;
    }
    .drag_actor_head:active{
        cursor:grabbing;
    }
    .drag_actor_left:active{
        cursor:grabbing;
    }
    .drag_actor_right:active{
        cursor:grabbing;
    }
    .drag_actor_foot:active{
        cursor:grabbing;
    }
    .drag_actor_head:hover {
        background: linear-gradient(to bottom, ${colors.kick_green_trans}, transparent, transparent, transparent);
    }
    .drag_actor_right:hover {
        background: linear-gradient(to left, ${colors.kick_green_trans}, transparent, transparent, transparent);
    }
    .drag_actor_left:hover {
        background: linear-gradient(to right, ${colors.kick_green_trans}, transparent, transparent, transparent);
    }
    .drag_actor_foot:hover {
        background: linear-gradient(to top, ${colors.kick_green_trans}, transparent, transparent, transparent);
    }
    
    .res_option {
        font-size: 1.1em;
        text-align: center; 
        user-select: none;
        text-align:center;
        cursor:pointer;
        border:1px solid transparent;
        border-radius:1em;
        transition: all 111ms;
        height: 2em;
        transform: translate(0px,-1px);
    }
    .unchecked:hover {
        border-radius:1em;
        background: linear-gradient(to bottom right, ${colors.kick_green_trans}, transparent, transparent, transparent);
    }
    .res_option:active {
        background:${colors.kick_green};
        background: linear-gradient(to bottom right, transparent, transparent, transparent, ${colors.kick_green});
        box-shadow: #0d0b0f 2px 4px 1px 0px inset, ${colors.kick_green} -1px -1px 1px 0px inset;
    }
    .unchecked {
        color:#ffffff;
        border-radius:1em;
        background: linear-gradient(to bottom right, ${colors.kick_green_trans}, transparent, transparent, transparent);
    }
    .checked {
        color:${colors.kick_green};
        border-radius:1em;
        background:${colors.kick_green};
        background: linear-gradient(to bottom right, transparent, transparent, transparent, ${colors.kick_green_trans});
        box-shadow: #0d0b0f 1px 2px 1px 0px inset, ${colors.kick_green_trans} -1px -0px 0px 0px inset;
    }
    #dl_btn_svg:hover {
        transition: all 133ms;
        transform: scale(0.9,0.9) translate(0px,1px);
    }
    .arrow_icon {
        transform: translate(0px,-28px);
    }
    #logo_main_svg:hover .st2 {
        fill:#48DB15;
    }
    #logo_main_svg:hover .icon_k {
        transform: scale(0.9,0.9) translate(5.9%,3.5%);;
        transition: all 133ms;
    }

    #logo_main_svg:hover .arrow_icon {
        transform: translate(0px,20px);
        transition: all 333ms ease-in-out;
    }


    .download_text_holder {
        max-height: 30px;
        overflow:hidden;
        position:relative;
        transform: translate(0px,15px);
    }
    .download_text_scroll {
        position:absolute;
        white-space: nowrap;
        height: 30px;
        animation-name: dl_slide;
        animation-duration: 30s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;
        -webkit-animation-name: dl_slide;
        -webkit-animation-duration: 30s;
        -webkit-animation-timing-function:linear;
        -webkit-animation-iteration-count: infinite;
    }
    
    @keyframes dl_slide {
        from { left:50%; transform: translate(50, 0); }
        to { left: -50%; transform: translate(-50%, 0); }
    }
    @-webkit-keyframes dl_slide {
        from { left:50%; transform: translate(50, 0); }
        to { left: -50%; transform: translate(-50%, 0); }
    }

    .scrolling_holder {
        max-height: 52px;
        overflow:hidden;
        position:relative;
        transform: translate(0px,16px);
    }
    .scrolling_text {
        position:absolute;
        white-space: nowrap;
        height: 52px;
        font-size: 0.8em;
    }
    .scrolling_text:hover {
        animation-name: slide;
        animation-duration: 10s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;
        -webkit-animation-name: slide;
        -webkit-animation-duration: 10s;
        -webkit-animation-timing-function:linear;
        -webkit-animation-iteration-count: infinite;
    }
    @keyframes slide {
        from { left:0%; transform: translate(0, 0); }
        to { left: -50%; transform: translate(-50%, 0); }
    }
    @-webkit-keyframes slide {
        from { left:0%; transform: translate(0, 0); }
        to { left: -50%; transform: translate(-50%, 0); }
    }
    
`);
// .res_option {
//     font-size: 1.1em;
//     text-align: center; 
//     user-select: none;
//     text-align:center;
//     cursor:pointer;
//     border:1px solid transparent;
//     transition: all 111ms;
//     height: 2em;
//     transform: translate(0px,4px);
// }
// .unchecked:hover {
//     border-radius:0.2em;
//     background: linear-gradient(to bottom right, ${colors.kick_green}, transparent, transparent, transparent);
//     box-shadow: #0d0b0f 1px 2px 1px 0px, ${colors.kick_green} -1px -1px 1px -0px;
// }
// .res_option:active {
//     background:${colors.kick_green};
//     background: linear-gradient(to bottom right, transparent, transparent, transparent, ${colors.kick_green});
//     box-shadow: #0d0b0f 2px 4px 1px 0px inset, ${colors.kick_green} -1px -1px 1px 0px inset;
// }
// .unchecked {
//     border-radius:0.2em;
//     background: linear-gradient(to bottom right, ${colors.kick_green}, transparent, transparent, transparent);
//     box-shadow: #0d0b0f 0px 1px 1px 0px, ${colors.kick_green} -1px -0px 1px -0px;
// }
// .checked {
//     background:${colors.kick_green};
//     background: linear-gradient(to bottom right, transparent, transparent, transparent, ${colors.kick_green});
//     box-shadow: #0d0b0f 1px 2px 1px 0px inset, ${colors.kick_green} -1px -0px 0px 0px inset;
// }
// .list_container {
//     width:
// }