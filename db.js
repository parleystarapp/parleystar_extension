/* SPDX-FileCopyrightText: © 2024 promising future digital media llc. All rights reserved. <admin@parleystar.com> */
/* SPDX-License-Identifier: Mozilla Public License 1.1 (MPL-1.1) */
// ***** DB FUNCTIONS
console.log('loading db.js');

scbt.f.get_binary_if_db_exists = async function(dbName){
  var dbFoundIs = false;
  if (dbName) { } else { return dbFoundIs; }
  var arr = await indexedDB.databases();
  if (arr.length > 0) { } else { return dbFoundIs; }
  var arrl = arr.length;
  dbName = dbName.toLowerCase().trim();
  for (var i = 0; i < arrl && !dbFoundIs; i++) {
    var dbNameToCheck = arr[i].name;
    if (dbNameToCheck) {
      dbNameToCheck = arr[i].name.toLowerCase().trim();
    }
    if (dbNameToCheck === dbName) {
      dbFoundIs = true;
    }
  }
  dbName = arr = arrl = i = dbNameToCheck = null; return dbFoundIs;
}


scbt.f.get_arr_of_all_dbNames = function(){
  console.log('doing get_arr_of_all_dbNames in db.js');
  scbt.a.savedStreams = [];
  indexedDB.databases().then((arr) => {
    console.log(arr);
    if (arr.length > 0) {
      scbt.e.scbtPreviousStreams.innerHTML = '';
      var arrl = arr.length;
      for (var i = 0; i < arrl; i++) {
        var dbName = arr[i].name;
        if (dbName.startsWith('savedchat') ) {
          var dbArr = scbt.f.get_arr_from_dbName_string(dbName);
          var obj = {};
          obj.dbName = dbName;
          obj.serviceid = dbArr[1];
          obj.channelid = dbArr[2];
          obj.videoid = dbArr[3];
          scbt.a.savedStreams.push(obj);
        }
      }
      console.log(scbt.a.savedStreams);
      scbt.f.handler_sort_saved_streams_by_serviceid();
      dbName = arr = arrl = i = dbArr = obj = null;
    }
  });
}


scbt.f.build_list_of_saved_stream_chat_by_arr = function(arr){
  console.log('doing build_list_of_saved_stream_chat_by_arr', arr);
  var arrl = arr.length;
  for (var i = 0; i < arrl; i++) {
    var dbName = 'savedchat' + '&' + arr[i].serviceid + '&' + arr[i].channelid + '&' + arr[i].videoid;
    var theHTML = "<p>";
    theHTML = theHTML + "<button title='Go To This Channel' class='az2' data-serviceid='" + arr[i].serviceid + "' data-videoid='" + arr[i].videoid + "' data-channelid='" + arr[i].channelid + "'>" + arr[i].channelid + "</button> on ";
    theHTML = theHTML + arr[i].serviceid + " :<br>";
    theHTML = theHTML + "<button title='View This Stream Chat' class='az3' data-dbName='" + dbName + "'>" + arr[i].videoid + "</button><br>";
    theHTML = theHTML + "<button title='Download This Stream Chat' class='az4' data-dbName='" + dbName + "'>💾</button><br>";
    theHTML = theHTML + "<button title='Delete This Stream Chat' class='az5' data-dbName='" + dbName + "'>🗑️</button><br>";
    theHTML = theHTML + "<button title='Load This Stream Chat When This Video Loads' class='az6' data-dbName='" + dbName + "'>🎯</button><br>";
    theHTML = theHTML + "<button title='Offline This Stream Chat' class='az7' data-dbName='" + dbName + "'>📴</button><br>";
    theHTML = theHTML + "</p><p>&nbsp;&nbsp;</p>";
    scbt.e.scbtPreviousStreams.insertAdjacentHTML('beforeend', theHTML);
  }
  arr = arrl = i = dbName = theHTML = null; return false;
}


scbt.f.build_chat_by_dbName_string = function(dbName){
  scbt.a.searchingMessageIds = [];
  var request = indexedDB.open(dbName, 10);

  request.onsuccess = function(successObj) {
    if (!request.result) { setTimeout(function(){ scbt.f.toast('Error: build chat for live stream result failed.'); }, 2700); return false; }
    var db = request.result;
    var transaction = db.transaction('chat', 'readonly');
    transaction.oncomplete = function(completeObj) {
      // console.log('oncomplete')
    };
    transaction.onabort = function(abortObj) {
      scbt.f.set_db_error_message(transaction.error); return false;
    };
    
    var store = transaction.objectStore('chat'); 
    store.getAll().onsuccess = function(successAllObj) {
      if (!successAllObj) { setTimeout(function(){ scbt.f.toast('Error: build chat for live stream e2 failed 1.'); }, 2700);  return false; }
      if (!successAllObj.target) { setTimeout(function(){ scbt.f.toast('Error: build chat for live stream e2 target failed 2.'); }, 2700);  return false; }
      if (!successAllObj.target.result) { setTimeout(function(){ scbt.f.toast('Error: build chat for live stream e2 target result failed 3.'); }, 2700);  return false; }
      if (!successAllObj.target.result.length || successAllObj.target.result.length < 1) { setTimeout(function(){ scbt.f.toast('Error: build chat for live stream not found 4.'); }, 2700);  return false; }
      
      scbt.v.dbNameToSearch = dbName;
      var arr = scbt.f.get_arr_from_dbName_string(scbt.v.dbNameToSearch);
      scbt.e.scbtSavedChat1.innerHTML = '';
      var chatObjs = successAllObj.target.result;
      var str = chatObjs[0].message;

      // scbt.e.scbtChatA.textContent = 'Saved Chat From ' + chatObjs[0].username + ' on ' + chatObjs[0].message;
      scbt.e.scbtChatA.textContent = 'saved chat from ' + arr[2] + ' on ' + arr[1] + ': ' + arr[3] + ' ' + str;
      scbt.e.scbtChatB.textContent = '';
      chatObjs = scbt.f.get_arr_sortedtimes_from_arr(chatObjs);
      [].forEach.call(chatObjs, function(chatObj) {
        var theHTML = scbt.f.build_chat_line_from_obj(chatObj);
        scbt.e.scbtSavedChat1.insertAdjacentHTML('beforeend', theHTML);
      });
      setTimeout(function() {
        // scbt.f.add_listener_for_username_insert_into_search();
        // scbt.f.add_listener_for_click_timestamp_go_to_video();
        var chatElmArr = document.body.getElementsByClassName('scbt-chat-line');
        [].forEach.call(chatElmArr, function(chatElm) {
           scbt.f.process_chat_line(chatElm, false);
        });
        scbt.f.chat_down_to_bottom();
        scbt.e.scbtDragSpot4.classList.add('scbt-forefront');
        scbt.e.scbtSavedChat1.classList.add('scbt-active');

        arr = chatObjs = chatObj =  dbName = str = theHTML = chatElmArr = chatElm =  null; return false;
      }, 2000);
    };
    store.getAll().onerror = function(errorAllObj) {
      scbt.f.set_db_error_message(errorAllObj);
      arr = chatObjs = chatObj =  dbName = str = theHTML = chatElmArr = chatElm =  null; return false;
    };
    store.getAll().onblocked = function(errorAllObj) {
      scbt.f.set_db_error_message(errorAllObj);
      arr = chatObjs = chatObj =  dbName = str = theHTML = chatElmArr = chatElm =  null; return false;
    };

  }; // request.onsuccess
  request.onerror = function(errorObj) {
    scbt.f.set_db_error_message(errorObj);
    arr = chatObjs = chatObj =  dbName = str = theHTML = chatElmArr = chatElm =  null; return false;
  };
  request.onblocked = function(errorObj) {
    scbt.f.set_db_error_message(errorObj);
    arr = chatObjs = chatObj =  dbName = str = theHTML = chatElmArr = chatElm =  null; return false;
  };
}


scbt.f.save_bulk_chat_from_dbName_arr = function(dbName, chatObjArr){
  return new Promise((resolve, reject) => {
    var request = indexedDB.open(dbName, 10);
    request.onupgradeneeded = function(e) {
      if (!request.result) {
        scbt.f.set_db_error_message(e); 
        return e;
      }
      var store = request.result.createObjectStore('chat', {
        keyPath: 'id',
        autoIncrement: true,
      });
      store.createIndex('itemid', 'itemid', {unique: true});
      store.createIndex('timestamp', 'timestamp', {unique: false});
      store.createIndex('username', 'username', {unique: false});
      store.createIndex('message', 'message', {unique: false});
      store.createIndex('sub', 'sub', {unique: false});
      store.createIndex('moderator', 'moderator', {unique: false});
      store.createIndex('owner', 'owner', {unique: false});
      store.createIndex('donation', 'donation', {unique: false});
      store.createIndex('newSub', 'newSub', {unique: false});
      store.createIndex('verified', 'verified', {unique: false});
      store.createIndex('gifter', 'gifter', {unique: false});
      store.createIndex('founder', 'founder', {unique: false});
      store.createIndex('og', 'og', {unique: false});
      store.createIndex('staff', 'staff', {unique: false});
      store.createIndex('anevent', 'anevent', {unique: false});
      store.transaction.oncomplete = (eee) => {
        scbt.f.toast('Status: chat imported successfully.');
        resolve('good');
      }
    };
    request.onsuccess = function(e) {
      var db = request.result;
      var transaction = db.transaction('chat', 'readwrite', { durability: 'relaxed' } );
      var objectStore = transaction.objectStore('chat');
      [].forEach.call(chatObjArr, function(chatObj) {
        objectStore.put(chatObj);
      });
      transaction.commit();
      scbt.f.toast('Status: Chat messages imported successfully');
      scbt.e.scbtChatImportButton.value = dbName = db = transaction = chatObjArr = chatObj = objectStore = null;
    };
  });
}


scbt.f.chat_delete_by_videoid = function(e){
  if (!e) { return false; }
  if (!e.srcElement) { return false; }
  if (!e.srcElement.dataset) { return false; }
  if (!e.srcElement.dataset.dbname) { return false; }
  if (e.preventDefault) { e.preventDefault(); }
  
  scbt.a.searchingMessageIds = [];
  setTimeout(function(){
    var request = indexedDB.deleteDatabase(e.srcElement.dataset.dbname);
    request.onsuccess = function(e2) {
      setTimeout(function(){ scbt.f.toast('Status: Chat messages from this stream successfully deleted'); scbt.f.get_arr_of_all_dbNames(); }, 3000);
      e = e2 = dbname = request = error = null; return false;
    }
    request.onerror = function(error) {
      scbt.f.set_db_error_message(error);
      e = e2 = dbname = request = error = null; return false;
    };
    request.onblocked = function(error) {
      scbt.f.set_db_error_message(error);
      e = e2 = dbname = request = error = null; return false;
    };
   }, 1000);
}


scbt.f.chat_mark_by_videoid = function(e){
  if (!e) { return false; }
  if (!e.srcElement) { return false; }
  if (!e.srcElement.dataset) { return false; }
  if (!e.srcElement.dataset.dbname) { return false; }
  if (e.preventDefault) { e.preventDefault(); }

  var str = localStorage.getItem(location.href);
  if (str) {
    localStorage.removeItem(location.href);
    setTimeout(function(){ scbt.f.toast('Status: Loading this video will NOT load this livestream chat automatically.'); e = str = null; return false; }, 2700);
  } else {
    localStorage.setItem(location.href, e.srcElement.dataset.dbname);
    setTimeout(function(){ scbt.f.toast('Status: Loading this video WILL load this livestream chat automatically.'); e = str = null; return false; }, 2700);
  }
}


scbt.f.chat_export_by_videoid = function(e){
  if (!e) { return false; }
  if (!e.srcElement) { return false; }
  if (!e.srcElement.dataset) { return false; }
  if (!e.srcElement.dataset.dbname) { return false; }
  if (e.preventDefault) { e.preventDefault(); }
  
  scbt.a.searchingMessageIds = [];
  var request = indexedDB.open(e.srcElement.dataset.dbname, 10);

  request.onsuccess = function(e2) {
    if (!request.result) { setTimeout(function(){ scbt.f.toast('Error: get database result for chat export result failed.'); }, 2700); return false; }
    var db = request.result;
    var transaction = db.transaction('chat', 'readonly');
    transaction.oncomplete = function() {
    };
    transaction.onabort = function() {
      scbt.f.set_db_error_message(transaction.error);
      setTimeout(function(){ scbt.f.toast('Error: get database result for chat export result failed.'); }, 2700); return false;
    };
    var store = transaction.objectStore('chat'); 
    
    store.getAll().onsuccess = function(e3) {
      var chatObjArr = e3.target.result;
      if (chatObjArr.length < 1) { setTimeout(function(){ scbt.f.toast('Error: this stream chat not found for display'); }, 2700); return false; }
      chatObjArr = scbt.f.get_arr_sortedtimes_from_arr(chatObjArr);
      var output = [];
      var titles = 'initial, message, itemid, timestamp, username, sub, moderator, owner, donation, newSub, VIP, gifter, founder, og, staff, anevent, id';
      output.push(titles);
      output.push('\n');
      
      [].forEach.call(chatObjArr, function(chatObj) {
        var row = '';
        var str = '';
        var str2 = '';
        var username = chatObj.username;
        if (username) {
          str = username.replace(/[^a-zA-Z0-9!._\-@\s]/g, ' ');
          str = str.replace(/(\r\n|\n|\r)/gm, "");
          str = str.replaceAll(',', ' ');
          str = str.trim();
        }
        var message = chatObj.message;
        if (message) {
          str2 = message.replace(/\/‘’,‚“”„"`~«´<>/g, ' ');
          str2 = str2.replace(/(\r\n|\n|\r)/gm, "");
          str2 = str2.replaceAll(',', ' ');
          str2 = str2.trim();  
        }
        row = row + str2 + ',';
        row = row + chatObj.itemid + ',';
        row = row + chatObj.timestamp + ',';
        row = row + str + ',';
        row = row + chatObj.sub + ',';
        row = row + chatObj.moderator + ',';
        row = row + chatObj.owner + ',';
        row = row + chatObj.donation + ',';
        row = row + chatObj.newSub + ',';
        row = row + chatObj.verified + ',';
        row = row + chatObj.gifter + ',';
        row = row + chatObj.founder + ',';
        row = row + chatObj.og + ',';
        row = row + chatObj.staff + ',';
        row = row + chatObj.anevent + ',';
        row = row + chatObj.id;
        output.push(row);
        output.push('\n');
      });
    
      var csvString = output.join();
      var output = null;
      csvString = csvString.replace(/\/'"`/g, '');
      csvString = csvString.replace(/%3D/g, '');
      var blob = scbt.f.get_csv_file_from_str(csvString);
      var csvName = e.srcElement.dataset.dbname + '-chatlog.csv';
      scbt.f.csv_download(blob, csvName);
      e = output = titles = chatObjArr = chatObj = str = str2 = username = message = row = blob = csvName = null; return false;
    };
    store.getAll().onerror = function(error2) {
      scbt.f.set_db_error_message(error2);
      e = error2 = request = null; return false;
    };
    store.getAll().onblocked = function(error2) {
      scbt.f.set_db_error_message(error2);
      e = error2 = request = null; return false;
    };
  };

  request.onerror = function(error) {
    scbt.f.set_db_error_message(error);
    e = error = request = null; return false;
  };
  request.onblocked = function(error) {
    scbt.f.set_db_error_message(error);
    e = error = request = null; return false;
  };
}




scbt.f.chat_export_to_offline = function(e){
  console.log('chat_export_to_offline 1', e);
  if (!e) { return false; }
  if (!e.srcElement) { return false; }
  if (!e.srcElement.dataset) { return false; }
  if (!e.srcElement.dataset.dbname) { return false; }
  if (e.preventDefault) { e.preventDefault(); }
  scbt.a.searchingMessageIds = [];
  var request = indexedDB.open(e.srcElement.dataset.dbname, 10);

  request.onsuccess = function(e2) {
    if (!request.result) { setTimeout(function(){ scbt.f.toast('Error: get database result for chat export result failed.'); }, 2700); return false; }
    console.log('add_listener_for_saved_chat_from_streams 4');
    var db = request.result;
    var transaction = db.transaction('chat', 'readonly');
    transaction.oncomplete = function() {
    };
    transaction.onabort = function() {
      scbt.f.set_db_error_message(transaction.error);
      setTimeout(function(){ scbt.f.toast('Error: get database result for chat export result failed.'); }, 2700); return false;
    };
    var store = transaction.objectStore('chat'); 
    
    store.getAll().onsuccess = function(e3) {
      console.log('add_listener_for_saved_chat_from_streams 5', e3);
      var chatObjArr = e3.target.result;
      if (chatObjArr.length < 1) { setTimeout(function(){ scbt.f.toast('Error: this stream chat not found for display'); }, 2700); return false; }
      chatObjArr = scbt.f.get_arr_sortedtimes_from_arr(chatObjArr);
      var output = [];
      var titles = 'initial, message, itemid, timestamp, username, sub, moderator, owner, donation, newSub, VIP, gifter, founder, og, staff, anevent, id';
      output.push(e.srcElement.dataset.dbname);
      output.push('\n');
      output.push(titles);
      output.push('\n');
      console.log('add_listener_for_saved_chat_from_streams 6', chatObjArr);
      
      [].forEach.call(chatObjArr, function(chatObj) {
        var row = '';
        var str = '';
        var str2 = '';
        var username = chatObj.username;
        if (username) {
          str = username.replace(/[^a-zA-Z0-9!._\-@\s]/g, ' ');
          str = str.replace(/(\r\n|\n|\r)/gm, "");
          str = str.replaceAll(',', ' ');
          str = str.trim();
        }
        var message = chatObj.message;
        if (message) {
          str2 = message.replace(/\/‘’,‚“”„"`~«´<>/g, ' ');
          str2 = str2.replace(/(\r\n|\n|\r)/gm, "");
          str2 = str2.replaceAll(',', ' ');
          str2 = str2.trim();  
        }
        row = row + str2 + ',';
        row = row + chatObj.itemid + ',';
        row = row + chatObj.timestamp + ',';
        row = row + str + ',';
        row = row + chatObj.sub + ',';
        row = row + chatObj.moderator + ',';
        row = row + chatObj.owner + ',';
        row = row + chatObj.donation + ',';
        row = row + chatObj.newSub + ',';
        row = row + chatObj.verified + ',';
        row = row + chatObj.gifter + ',';
        row = row + chatObj.founder + ',';
        row = row + chatObj.og + ',';
        row = row + chatObj.staff + ',';
        row = row + chatObj.anevent + ',';
        row = row + chatObj.id;
        output.push(row);
        output.push('\n');
      });
    
      console.log('add_listener_for_saved_chat_from_streams 7', output);
      var obj = {};
      obj.streamForOffline = output;
      var stuff = chrome.runtime.sendMessage({ streamForOffline: obj });
      console.log(stuff);
      e = output = titles = chatObjArr = chatObj = str = str2 = username = message = row = blob = csvName = null; return false;
    };
    store.getAll().onerror = function(error2) {
      scbt.f.set_db_error_message(error2);
      e = error2 = request = null; return false;
    };
    store.getAll().onblocked = function(error2) {
      scbt.f.set_db_error_message(error2);
      e = error2 = request = null; return false;
    };
  };

  request.onerror = function(error) {
    scbt.f.set_db_error_message(error);
    e = error = request = null; return false;
  };
  request.onblocked = function(error) {
    scbt.f.set_db_error_message(error);
    e = error = request = null; return false;
  };
}


scbt.f.get_usernames_for_mention_menu = function(){
  scbt.a.searchingMessageIds = [];
  var request = indexedDB.open(scbt.v.dbName, 10);
  request.onsuccess = function(e) {
    if (!request.result) { setTimeout(function(){ scbt.f.toast('Error: get database result for mention chat failed.'); }, 2700); return false; }
    var db = request.result;
    var transaction = db.transaction('chat', 'readonly');
    transaction.oncomplete = function() {
    };
    transaction.onabort = function() {
      scbt.f.set_db_error_message(transaction.error);
    };
    var store = transaction.objectStore('chat'); 
    store.getAll().onsuccess = function(e2) {
      if (!e2) { setTimeout(function(){ scbt.f.toast('Error: get database result e2 for mention chat failed.'); }, 2700); return false; }
      if (!e2.target) { setTimeout(function(){ scbt.f.toast('Error: get database result e2 target for mention chat failed.'); }, 2700);  return false; }
      if (!e2.target.result) { setTimeout(function(){ scbt.f.toast('Error: get database result e2 target result for mention chat failed.'); }, 2700); return false; }

      var chatObjArr = e2.target.result;
      if (chatObjArr.length < 1) { setTimeout(function(){ scbt.f.toast('Error: this stream chat not found for display'); }, 2700);  return false; }
      var sortedchats = chatObjArr.sort(function(a, b) {
        var nameA = a.username;
        if (nameA) {
          nameA = nameA.toUpperCase();
        }
        var nameB = b.username;
        if (nameB) {
          nameB = nameB.toUpperCase();
        }
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }
        return 0;
      });
      var i = 0;
      [].forEach.call(sortedchats, function(item) {
        var theHTML = '';
        if ( scbt.a.usernames.indexOf(item.username) < 0) {
          theHTML = theHTML + "<li><button class='scbtusername' id='scbtusername" + i + "' tabindex='0'>" + item.username + "</button></li>";
          i = i + 1;
          scbt.a.usernames.push(item.username);
          var elemArr = scbt.e.scbtMentionMenu.getElementsByTagName('ul');
          if (elemArr[0]) {
            elemArr[0].insertAdjacentHTML('afterbegin', theHTML );
          }
        }
      });
       return false;      
    };
    store.getAll().onerror = function(error2) {
      scbt.f.set_db_error_message(error2);
      return false;
    };
    store.getAll().onblocked = function(error2) {
      scbt.f.set_db_error_message(error2);
      return false;
    };
  }
  request.onerror = function(error) {
    scbt.f.set_db_error_message(error);
    return false;
  };
  request.onblocked = function(error) {
    scbt.f.set_db_error_message(error);
    return false;
  };
  
}


scbt.f.search_multiple_saved_chat = function(e){
  if (e) { if (e.preventDefault) { e.preventDefault(); } }
  var str = scbt.f.get_str_for_search();
  console.log('doing search_multiple_saved_chat with: ' + str, e);

  searchType = '';
  toastMessage = 'Searching...';
  toastMessage2 = '';
  labelMessage = ''; 
  if (e.target.id == 'scbtChatSearchStartsWithButton') {
    searchType = 'startswith';
    toastMessage = str + ' beginning a message is being searched for';
    toastMessage2 = str + ' not found starting message';
    labelMessage = ' chat starting with: ' + str;
  }
  if (e.target.id == 'scbtChatSearchUserButton') {
    searchType = 'byuser';
    toastMessage = ' messages from ' + str + ' being searched for ';
    toastMessage2 = str + ' username has no saved chat messages';
    labelMessage = ' chat from username: ' + str;
  }
  if (e.target.id == 'scbtChatSearchKeywordButton') {
    searchType = 'bykeyword';
    toastMessage = str + ' in a message is being searched for ';
    toastMessage2 = str + ' not found in chat';
    labelMessage = ' chat by phrase: ' + str;
  }
  if (e.target.id == 'scbtChatSearchEventsButton') {
    searchType = 'byevent';
    toastMessage = ' stream event search ';
    toastMessage2 = ' events not found ';
    labelMessage = ' stream events';
  }
  if (e.target.id == 'scbtChatSearchFullButton') {
    searchType = 'full';
    str = 'test';
    toastMessage = ' full chat search ';
    toastMessage2 = ' chat not found ';
    labelMessage = ' full stream chat';
  }
  if (str.length < 3) { return false; }
  
  scbt.e.scbtSavedChat1.innerHTML = '';
  scbt.f.toast(toastMessage);  
  scbt.a.savedStreams = [];

  indexedDB.databases().then((arr) => {
    if (arr.length > 0) {
      var arrl = arr.length;
      for (var i = 0; i < arrl; i++) {
        var dbName = arr[i].name;
        if (dbName.startsWith('savedchat') ) {
          scbt.a.savedStreams.push(dbName);

          (async () => {
            var request = await indexedDB.open(dbName, 10);
            request.onsuccess = function(e) {
              if (!request.result) { setTimeout(function(){ scbt.f.toast('Error: get database for multiple search chat result failed.'); }, 2700);  return false; }
              scbt.a.searchingMessageIds = [];
              var db = request.result;
              var transaction = db.transaction('chat', 'readonly');
              transaction.oncomplete = function() {
              };
              transaction.onabort = function() {
                scbt.f.set_db_error_message(transaction.error);
              };
              var store = transaction.objectStore('chat'); 
              // (async () => {
              store.getAll().onsuccess = function(e2) {
                if (!e2) { setTimeout(function(){ scbt.f.toast('Error: get database for multiple search chat e2 failed.'); }, 2700);  return false; }
                if (!e2.target) { setTimeout(function(){ scbt.f.toast('Error: get database for multiple search chat e2 target failed.'); }, 2700);  return false; }
                if (!e2.target.result) { setTimeout(function(){ scbt.f.toast('Error: get database for multiple search chat e2 target result failed.'); }, 2700);  return false; }
                var chatObjArr = e2.target.result;
                if (chatObjArr.length < 1) { scbt.f.toast('Error: this stream chat not found for display');  return false; }
                scbt.e.scbtChatA.textContent = 'Saved Chat From All';
                scbt.e.scbtChatB.textContent = labelMessage;

                console.log("chatObjArr", chatObjArr);
                chatObjArr = scbt.f.get_arr_sortedtimes_from_arr(chatObjArr);

                if (searchType == 'bykeyword') {
                  [].forEach.call(chatObjArr, function(chatObj) {
                    if (chatObj.message) {
                      if (chatObj.message.toLowerCase().includes(str.toLowerCase() ) ) {
                        var theHTML = scbt.f.build_chat_line_from_obj(chatObj, chatObjArr[0]);
                        scbt.e.scbtSavedChat1.insertAdjacentHTML('beforeend', theHTML);
                      }
                    }
                  });
                }

                if (searchType == 'byuser') {
                  [].forEach.call(chatObjArr, function(chatObj) {
                    if (chatObj.username) {
                      if (chatObj.username.toLowerCase().includes(str.toLowerCase() ) ) {
                        var theHTML = scbt.f.build_chat_line_from_obj(chatObj, chatObjArr[0]);
                        scbt.e.scbtSavedChat1.insertAdjacentHTML('beforeend', theHTML);
                      }
                    }
                  });
                }

                if (searchType == 'full') {
                  [].forEach.call(chatObjArr, function(chatObj) {
                    var theHTML = scbt.f.build_chat_line_from_obj(chatObj, chatObjArr[0]);
                    scbt.e.scbtSavedChat1.insertAdjacentHTML('beforeend', theHTML);
                  });
                }

                if (searchType == 'startswith') {
                  [].forEach.call(chatObjArr, function(chatObj) {
                    if (chatObj.message) {
                      if (chatObj.message.toLowerCase().indexOf(str.toLowerCase()) == 0) {
                        var theHTML = scbt.f.build_chat_line_from_obj(chatObj, chatObjArr[0]);
                        scbt.e.scbtSavedChat1.insertAdjacentHTML('beforeend', theHTML);
                      }
                    }
                  });
                }

                setTimeout(function() {
                  // scbt.f.add_listener_for_username_insert_into_search();
                  // scbt.f.add_listener_for_click_timestamp_go_to_video();
                  // scbt.f.chat_down_to_bottom();
                  var chatElmArr = document.getElementsByClassName('scbt-chat-line');
                  [].forEach.call(chatElmArr, function(chatElm) {
                     scbt.f.process_chat_line(chatElm, false);
                  });
                  scbt.e.scbtDragSpot4.classList.add('scbt-forefront');
                  scbt.e.scbtSavedChat1.classList.add('scbt-active');
                }, 2000);
            };
            store.getAll().onerror = function(error2) {
              scbt.f.set_db_error_message(error2);
              return false;
            };
            store.getAll().onblocked = function(error2) {
              scbt.f.set_db_error_message(error2);
              return false;
            };
          }; // request.onsuccess
          request.onerror = function(error) {
            scbt.f.set_db_error_message(error);
            return false;
          };
          request.onblocked = function(error) {
            scbt.f.set_db_error_message(error);
            return false;
          };
      })(); // async

        }
      }
    }
  }); // indexedDB.databases().then
return false;
}


scbt.f.search_saved_chat = function(e){
  scbt.a.searchingMessageIds = [];
  if (e) { if (e.preventDefault) { e.preventDefault(); } }
  // if (scbt.v.dbNameToSearch) { } else { setTimeout(function(){ scbt.f.toast('Error: no active or loaded chat to search.'); }, 500); return false; }
  var str = scbt.f.get_str_for_search();
  console.log('doing search_saved_chat with: ' + str, e);
  if (scbt.e.scbtChatSearchMultiButton.classList.contains('scbt-multiple')) {
    if (e.target.id == 'scbtChatSearchStartsWithButton') {
      scbt.f.search_multiple_saved_chat(e);
      e = str = null; return false;
    }
    if (e.target.id == 'scbtChatSearchUserButton') {
      scbt.f.search_multiple_saved_chat(e);
      e = str = null; return false;
    }
    if (e.target.id == 'scbtChatSearchKeywordButton') {
      scbt.f.search_multiple_saved_chat(e);
      e = str = null; return false;
    }
  }
  searchType = '';
  toastMessage = 'Searching...';
  toastMessage2 = '';
  labelMessage = ''; 
  if (e.target.id == 'scbtChatSearchStartsWithButton') {
    searchType = 'startswith';
    toastMessage = str + ' beginning a message is being searched for';
    toastMessage2 = str + ' not found starting message';
    labelMessage = ' chat starting with: ' + str;
  }
  if (e.target.id == 'scbtChatSearchUserButton') {
    searchType = 'byuser';
    toastMessage = ' messages from ' + str + ' being searched for ';
    toastMessage2 = str + ' username has no saved chat messages';
    labelMessage = ' chat from username: ' + str;
  }
  if (e.target.id == 'scbtChatSearchKeywordButton') {
    searchType = 'bykeyword';
    toastMessage = str + ' in a message is being searched for ';
    toastMessage2 = str + ' not found in chat';
    labelMessage = ' chat by phrase: ' + str;
  }
  if (e.target.id == 'scbtChatSearchEventsButton') {
    searchType = 'byevent';
    str = 'test';
    toastMessage = ' stream events ';
    toastMessage2 = ' events not found ';
    labelMessage = ' stream events';
  }
  if (e.target.id == 'scbtChatSearchFullButton') {
    searchType = 'full';
    str = 'test';
    toastMessage = ' full chat search ';
    toastMessage2 = ' chat not found ';
    labelMessage = ' full stream chat';
  }

  if (str.length < 3) { return false; }

  scbt.e.scbtSavedChat1.innerHTML = '';
  scbt.f.toast(toastMessage);
  var request = indexedDB.open(scbt.v.dbNameToSearch, 10);

  request.onsuccess = function(e) {
    if (!request.result) { scbt.f.toast('Error: get database for live stream chat failed.'); return false; }
    var db = request.result;
    var transaction = db.transaction('chat', 'readonly');
    transaction.oncomplete = function() {
      
    };
    transaction.onabort = function() {
      scbt.f.set_db_error_message(transaction.error);
    };
    var store = transaction.objectStore('chat'); 
    store.getAll().onsuccess = function(e2) {

      if (!e2) { setTimeout(function(){ scbt.f.toast('Error: get database for search chat e2 failed.'); }, 2700); return false; }
      if (!e2.target) { setTimeout(function(){ scbt.f.toast('Error: get database for search chat e2 target failed.'); }, 2700); return false; }
      if (!e2.target.result) { setTimeout(function(){ scbt.f.toast('Error: get database for search chat e2 target result failed.'); }, 2700);  return false; }
      var chatObjArr = e2.target.result;

      if (chatObjArr.length < 1) { scbt.f.toast('Error: this stream chat not found for display'); return false; }

      var str2 = chatObjArr[0].message;
      var dbArr = scbt.f.get_arr_from_dbName_string(scbt.v.dbNameToSearch);
      scbt.e.scbtSavedChat1.innerHTML = '';
      chatObjArr = scbt.f.get_arr_sortedtimes_from_arr(chatObjArr);
      
      scbt.e.scbtChatA.textContent = dbArr[2] + ' on ' + dbArr[1] + ': ' + dbArr[3] + ' ' + str2;
      scbt.e.scbtChatB.textContent = labelMessage;
      
      if (searchType == 'byevent') {
        [].forEach.call(chatObjArr, function(chatObj) {
          if ( (chatObj.anevent == 1) || (chatObj.staff === 1) ) {
            var classString = scbt.f.get_str_for_chat_classes_from_obj(chatObj);
            if (chatObj.message) {
              if (chatObj.message.indexOf('clips.kick.com') > -1) {
                var m = chatObj.message;
                var mArr = m.split('-thumbnail.jpeg');
                var mString = mArr[0];
                var mmArr = mString.split('https://clips.kick.com/clips/');
                var clipID = mmArr[1];
                var mArr = m.split('img ');
                var imgElm = mArr[0] + "<br /><video class='vjs-tech' webkit-playsinline='' playsinline='' controls src='https://clips.kick.com/clips/" + clipID + ".mp4'></video>";
                var theHTML = "<p class='" + classString + "'><span>" + "<span class='author-name'>" + chatObj.username + "</span></span><b>" + imgElm + "</b></p>";
              } else {
                var theHTML = "<p class='" + classString + "'><span>" + " : <span class='author-name'>" + chatObj.username + "</span> </span><b>" + chatObj.message + "</b></p>";
              }
            }
            scbt.e.scbtSavedChat1.insertAdjacentHTML('beforeend', theHTML);  
          }
        });
      }

      if (searchType == 'bykeyword') {
        [].forEach.call(chatObjArr, function(chatObj) {
          if (chatObj.message) {
            if (chatObj.message.toLowerCase().includes(str.toLowerCase() ) ) {
              var theHTML = scbt.f.build_chat_line_from_obj(chatObj);
              scbt.e.scbtSavedChat1.insertAdjacentHTML('beforeend', theHTML);
            }
          }
        });
      }

      if (searchType == 'byuser') {
        [].forEach.call(chatObjArr, function(chatObj) {
          if (chatObj.username) {
            if (chatObj.username.toLowerCase().includes(str.toLowerCase() ) ) {
              var theHTML = scbt.f.build_chat_line_from_obj(chatObj);
              scbt.e.scbtSavedChat1.insertAdjacentHTML('beforeend', theHTML);
            }
          }
        });
      }

      if (searchType == 'full') {
        [].forEach.call(chatObjArr, function(chatObj) {
          var theHTML = scbt.f.build_chat_line_from_obj(chatObj);
          scbt.e.scbtSavedChat1.insertAdjacentHTML('beforeend', theHTML);
        });
      }

      if (searchType == 'startswith') {
        [].forEach.call(chatObjArr, function(chatObj) {
          if (chatObj.message) {
            if (chatObj.message.toLowerCase().indexOf(str.toLowerCase()) == 0) {
              var theHTML = scbt.f.build_chat_line_from_obj(chatObj);
              scbt.e.scbtSavedChat1.insertAdjacentHTML('beforeend', theHTML);
            }
          }
        });
      }

      setTimeout(function() {
        // scbt.f.add_listener_for_username_insert_into_search();
        // scbt.f.add_listener_for_click_timestamp_go_to_video();
        // scbt.f.chat_down_to_bottom();
        var chatElmArr = document.getElementsByClassName('scbt-chat-line');
        [].forEach.call(chatElmArr, function(chatElm) {
           scbt.f.process_chat_line(chatElm, false);
        });
        scbt.e.scbtDragSpot4.classList.add('scbt-forefront');
        scbt.e.scbtSavedChat1.classList.add('scbt-active');
      }, 2000);

    }; // store.getAll().onsuccess
    store.getAll().onerror = function(error2) {
      scbt.f.set_db_error_message(error2);
      return false;
    };
    store.getAll().onblocked = function(error2) {
      scbt.f.set_db_error_message(error2);
      return false;
    };
  }; // request.onsuccess
  request.onerror = function(error) {
    scbt.f.set_db_error_message(error);
    return false;
  };
  request.onblocked = function(error) {
    scbt.f.set_db_error_message(error);
    return false;
  };
}


scbt.f.set_db_for_saving = function(dbName, isSaving){
  console.log('doing set_db_for_saving 1', dbName);
  if (!dbName) { 
    console.error('Error: chat database not supported');
    setTimeout(function(){ scbt.f.toast('Error: chat database not supported'); }, 2700);
    return false;
  }
  var request = indexedDB.open(dbName, 10);

  request.onsuccess = function (successObj) {
   console.log('doing set_db_for_saving 2', successObj);
   scbt.f.chat_listen(isSaving);
   return request;
  };
  request.onerror = function (errorObj) {
    console.log('doing set_db_for_saving 3', errorObj);
    scbt.f.set_db_error_message(errorObj);
    return errorObj;
  };
  request.onblocked = function (blockedObj) {
    console.log('doing set_db_for_saving 4', blockedObj);
    scbt.f.set_db_error_message(blockedObj);
    return blockedObj;
  };
  request.onupgradeneeded = function(upgradeObj) {
    console.log('doing set_db_for_saving 5', upgradeObj);
    if (!request.result) {
      scbt.f.set_db_error_message(upgradeObj); 
      return upgradeObj;
    }
    var store = request.result.createObjectStore('chat', {
      keyPath: 'id',
      autoIncrement: true,
    });

    store.createIndex('itemid', 'itemid', {unique: true});
    store.createIndex('timestamp', 'timestamp', {unique: false});
    store.createIndex('username', 'username', {unique: false});
    store.createIndex('message', 'message', {unique: false});
    store.createIndex('sub', 'sub', {unique: false});
    store.createIndex('moderator', 'moderator', {unique: false});
    store.createIndex('owner', 'owner', {unique: false});
    store.createIndex('donation', 'donation', {unique: false});
    store.createIndex('newSub', 'newSub', {unique: false});
    store.createIndex('verified', 'verified', {unique: false});
    store.createIndex('gifter', 'gifter', {unique: false});
    store.createIndex('founder', 'founder', {unique: false});
    store.createIndex('og', 'og', {unique: false});
    store.createIndex('staff', 'staff', {unique: false});
    store.createIndex('anevent', 'anevent', {unique: false});

    store.transaction.oncomplete = (completeObj) => {
      console.log('doing set_db_for_saving 6', completeObj);
      request.result.close();
      var obj = {};
      obj.anevent = 0;
      obj.donation = 0;
      obj.founder = 0;
      obj.gifter = 0;
      obj.itemid = '1111';
      obj.mention = 0;
      obj.message = new Date().toISOString().slice(0, 10);
      obj.moderator = 0;
      obj.newSub = 0;
      obj.og = 0;
      obj.owner = 0;
      obj.staff = 0;
      obj.sub = 0;
      obj.timestamp = '00:00AM';
      obj.username = scbt.v.channelid;
      obj.verified = 0;
      scbt.f.set_db_save_chat_obj(obj);
      return false;
    }
  };
}


scbt.f.set_db_save_chat_obj = function(obj){
  console.log('doing set_db_save_chat_obj 1', obj);
  var request = indexedDB.open(scbt.v.dbName, 10);
  request.onsuccess = function(successObj) {
    console.log('doing set_db_save_chat_obj 2', successObj);
    var db = successObj.target.result;
    var transaction = db.transaction('chat', 'readwrite');
    transaction.oncomplete = function(completeObj) {
      console.log('doing set_db_save_chat_obj 3', completeObj);
    };
    transaction.onabort = function(errorObj) {
      console.log('doing set_db_save_chat_obj 4', errorObj);
      scbt.f.set_db_error_message(errorObj);
      db = request = obj = successObj = completeObj = errorObj = addObj = null; return false;
    };
    transaction.onerror = function(errorObj) {
      console.log('doing set_db_save_chat_obj 5', errorObj);
      scbt.f.set_db_error_message(errorObj);
      db = request = obj = successObj = completeObj = errorObj = addObj = null; return false;
    };
    var store = transaction.objectStore('chat');
    store.add(obj).onsuccess = function(addObj) {
      console.log('doing set_db_save_chat_obj 6', addObj);
      scbt.f.chat_listen(true);
      db = request = obj = successObj = completeObj = errorObj = addObj = null; return false;
    };
  };
}


scbt.f.get_str_dbName = function(isSaving){
  console.log('doing scbt_get_str_dbName 1 ', isSaving);
  if (scbt.v.dbName) {
    return scbt.v.dbName;
  } else {
    console.log('doing scbt_get_str_dbName 2');
    scbt.v.dbName = 'savedchat' + '&' + scbt.v.serviceid + '&' + scbt.v.channelid + '&' + scbt.v.videoid;
    scbt.v.dbNameToSearch = scbt.v.dbName;
    scbt.f.set_db_for_saving(scbt.v.dbName, isSaving);
    return scbt.v.dbName;
  }
}


scbt.f.toggle_search_multiple_chat = function(){
  if (scbt.e.scbtChatSearchMultiButton.classList.contains('scbt-multiple')) {
    scbt.e.scbtChatSearchMultiButton.classList.remove('scbt-multiple');
  } else {
    scbt.e.scbtChatSearchMultiButton.classList.add('scbt-multiple');
  }
  return false;
}


scbt.f.toggle_user_chats_menu = function(e){
  var channelid = scbt.f.get_str_channelid();
  var channelArr = [];
  indexedDB.databases().then((arr) => {
    if (arr.length > 0) {
      var arrl = arr.length;
      for (var i = 0; i < arrl; i++) {
        var dbName = arr[i].name;
        if (dbName.startsWith('savedchat') ) {
          if (dbName.indexOf(channelid) > -1) {
            channelArr.push(dbName);
          }
        }
      }
      scbt.f.scbt_some_stuff(channelid, channelArr);
      return false;
    }
  });
}


scbt.f.load_local_chat = async function(dbStr){
  var dbExists = await scbt.f.get_binary_if_db_exists(dbStr);
  if (dbExists === true) {
    // loading chat database from localStorage
    var e = {};
    e.srcElement = {};
    e.srcElement.dataset = {};
    e.srcElement.dataset.dbname = dbStr;
    scbt.f.chat_load_by_videoid(e);
  }
  return false;
}


scbt.f.get_csv_file_from_str = function(str){
  if (str) {
    var csv_mime_type = 'text/csv';
    return new Blob([str], {type: csv_mime_type});
  }
}


scbt.f.csv_download = function(blob, filename){
  if (blob && filename) {
    var a = document.createElement('a');
    a.setAttribute('download', filename);
    var url = URL.createObjectURL(blob);
    a.setAttribute('href', url);
    a.click();
    URL.revokeObjectURL(url);
  }
  blob = filename = a = url = null; return false;
}


scbt.f.csv_import_chat_log_from_chatarr = function(filename, chatArr){
  scbt.f.toast('Status: starting to import chat');
  scbt.a.searchingMessageIds = [];
  if (filename && chatArr) { } else { setTimeout(function(){ scbt.f.toast('Error: chat not found to import'); }, 2700); return false; }

  var fileNameArr = filename.split('-chatlog');
  fileNameArr = fileNameArr[0];
  var fileNamePartsArr = fileNameArr.split('&');
  var one = fileNamePartsArr[0];    // savedchat
  var two = fileNamePartsArr[1];    // kick
  var three = fileNamePartsArr[2];  // streamer
  var four = fileNamePartsArr[3];   // abc123
  var dbName = one + '&' + two + '&' + three + '&' + four;
    
  if (chatArr.length > 1) {
    var chatObj = {};
    var chatObjArr = [];
    chatArr = chatArr.slice(1);
    var arrl = chatArr.length;
    for (var i = 0; i < arrl; i++) {
      if (chatArr[i][1]) {
        chatArr[i] = chatArr[i].slice(1);
        chatObj.message = chatArr[i][0];
        chatObj.itemid = chatArr[i][1];
        chatObj.timestamp = chatArr[i][2];
        chatObj.username = chatArr[i][3];
        chatObj.sub = Number(chatArr[i][4]);
        chatObj.moderator = Number(chatArr[i][5]);
        chatObj.owner = Number(chatArr[i][6]);
        chatObj.donation = Number(chatArr[i][7]);
        chatObj.newSub = Number(chatArr[i][8]);
        chatObj.verified = Number(chatArr[i][9]);
        chatObj.gifter = Number(chatArr[i][10]);
        chatObj.founder = Number(chatArr[i][11]);
        chatObj.og = Number(chatArr[i][12]);
        chatObj.staff = Number(chatArr[i][13]);
        chatObj.anevent = Number(chatArr[i][14]);
        chatObjArr.push(chatObj);
        chatObj = {};
      }
    }
    scbt.f.save_bulk_chat_from_dbName_arr(dbName, chatObjArr);
    filename = chatArr = fileNameArr = fileNamePartsArr = one = two = three = four = dbName = arrl = i = chatObj = null; return false;
  }
  
}


scbt.f.add_listener_for_uploading_chatlog = function(e){
  console.log("add_listener_for_uploading_chatlog 1");
  scbt.e.scbtChatImportButton.addEventListener('change', function(e2) {
    console.log("add_listener_for_uploading_chatlog 2");
    if (this.files && this.files[0]) {
      console.log("add_listener_for_uploading_chatlog 3");
      var myFile = this.files[0];
      if (myFile.name.substring(myFile.name.length - 11) != 'chatlog.csv') {
        setTimeout(function(){ scbt.f.toast('Error: file not supported to import'); return false; }, 2700);
      }
      if ( (myFile.size < 8) || (myFile.size > 100000000) ) {
        setTimeout(function(){ scbt.f.toast('Error: file not supported to import'); return false; }, 2700);
      }
      if ( myFile.type != 'text/csv' ) {
        setTimeout(function(){ scbt.f.toast('Error: file not supported to import'); return false; }, 2700);
      }

      var reader = new FileReader();
      reader.addEventListener('load', function(e3) {
        scbt.f.toast('Status: importing chat now.');
        var csvdata = e3.target.result;
        // scbt_helper_csv_parse_chat_log(csvdata);
        var chatLineArr = [];
        if (csvdata) {
          var newLinebrk = csvdata.split("\n");
          var arrl = newLinebrk.length;
          for (var i = 0; i < arrl; i++) {
            chatLineArr.push(newLinebrk[i].split(','));
          }
          scbt.f.csv_import_chat_log_from_chatarr(myFile.name, chatLineArr);
        }
        csvdata = chatLineArr = newLinebrk = arrl = null; return false;
      });
      reader.readAsBinaryString(myFile);
    }
  });
}


scbt.f.add_listener_for_importing_settings = function(e){
  console.log("add_listener_for_importing_settings 1");
  scbt.e.scbtSettingsImportButton.addEventListener('change', function(e2) {
    console.log("add_listener_for_importing_settings 2");
    if (this.files && this.files[0]) {
      console.log("add_listener_for_importing_settings 3");
      var myFile = this.files[0];
      if (myFile.name != 'parleystar_settings.json') {
        setTimeout(function(){ scbt.f.toast('Error: file name not supported to import'); return false; }, 2700);
      }
      if ( (myFile.size < 8) || (myFile.size > 100000000) ) {
        setTimeout(function(){ scbt.f.toast('Error: file size not supported to import'); return false; }, 2700);
      }
      if ( myFile.type != 'text/json' ) {
        setTimeout(function(){ scbt.f.toast('Error: file type not supported to import'); return false; }, 2700);
      }
      var reader = new FileReader();
      reader.addEventListener('load', function(e3) {
        scbt.f.toast('Status: importing chat now.');
        console.log( JSON.parse(reader.result) );
        var jsonObj = JSON.parse(reader.result);
        var arr = Object.getOwnPropertyNames(jsonObj);
        [].forEach.call(arr, async function(item) {
          if (item.indexOf('sz') !== -1) {
            var saveResult = await scbt.f.set_setting_item(item, jsonObj[item]);
            console.log(saveResult);
          }
        });
      });
      reader.readAsText(myFile);
    }
  });
}


scbt.f.set_setting_item = async function(objName, objProp){
  console.log("saving " + objName + " : " + objProp);
  var str = chrome.storage.sync.set({ [objName]: objProp}, function(error) { scbt.f.show_settings_error(error); });
  return str;
}


scbt.f.add_listener_for_exporting_settings = function(e){
  console.log("add_listener_for_exporting_settings 1");
  scbt.e.scbtSettingsExportButton.addEventListener('click', function(e2) {
    var str = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(window.scbt.o.settingsFromSync));
    var downloadAnchorNode = document.createElement('a');
    downloadAnchorNode.setAttribute('href', str);
    downloadAnchorNode.setAttribute('download', 'parleystar_settings.json');
    document.body.appendChild(downloadAnchorNode);
    downloadAnchorNode.click();
    downloadAnchorNode.remove();
    scbt.f.toast('Success: Settings exported.');
    return false;
  });
}


scbt.f.add_listener_for_deleting_settings = function(dontReload){
  // chrome.storage.sync.clear();
  console.log("add_listener_for_deleting_settings 1");
  scbt.e.scbtSettingsDeleteButton.addEventListener('click', function(e2) {
    if ( confirm('Are you sure you want to delete all of your ParleyStar extension settings?') == true) {
      scbt.f.settings_delete();
    } else {
      return false;
    }
  });
}


scbt.f.settings_delete = async function(){
  console.log("settings_delete 1");
  var bigArr = [];
  var arr = Object.getOwnPropertyNames(scbt.a);
  [].forEach.call(arr, function(item) {
      bigArr.push(item);
  });
  var arr = Object.getOwnPropertyNames(scbt.b);
  [].forEach.call(arr, function(item) {
      bigArr.push(item);
  });
  var arr = Object.getOwnPropertyNames(scbt.n);
  [].forEach.call(arr, function(item) {
      bigArr.push(item);
  });
  var arr = Object.getOwnPropertyNames(scbt.c);
  [].forEach.call(arr, function(item) {
      bigArr.push(item);
  });
  var arr = Object.getOwnPropertyNames(scbt.v);
  [].forEach.call(arr, function(item) {
      bigArr.push(item);
  });
  var str = bigArr.toString();
  chrome.storage.sync.remove(bigArr, function(){
    var error = chrome.runtime.lastError;
    arr = bigArr = null;
    if (error) {
      console.log(error);
      scbt.f.show_settings_error(error);
      error = null;
      return false;
    } else {
      setTimeout(function() { location.reload(); }, 3000);
    }
  });
}


scbt.f.search_for_saved_chat = async function(){
  var str = localStorage.getItem(location.href);

  if (str) {
    var dbExists = await scbt.f.get_binary_if_db_exists(str);
    if (dbExists === true) {
      // loading chat database from localStorage
      var e = {};
      e.srcElement = {};
      e.srcElement.dataset = {};
      e.srcElement.dataset.dbname = str;
      scbt.f.chat_load_by_videoid(e);
      return false;
    }
  }

  var serviceidStr = scbt.f.get_str_serviceid();
  var channelidStr = scbt.f.get_str_channelid();
  var videoidStr = scbt.f.get_str_videoid();

  if (serviceidStr && channelidStr && videoidStr) {
    var dbStr = 'savedchat' + '&' + serviceidStr + '&' + channelidStr + '&' + videoidStr;
    var dbExists = await scbt.f.get_binary_if_db_exists(dbStr);
    if (dbExists === true) {
      // loading chat database from indexeddb
      var e = {};
      e.srcElement = {};
      e.srcElement.dataset = {};
      e.srcElement.dataset.dbname = dbStr;
      scbt.f.chat_load_by_videoid(e);
      return false;
    }
    if (scbt.v.serviceid == 'kick') {
      scbt.f.load_local_chat_from_api(serviceidStr, channelidStr, videoidStr);
    }
  }
  
  if (serviceidStr && channelidStr && videoidStr) {
    // loading chat database from web api
    scbt.f.load_chat_replay_from_api(serviceidStr, channelidStr, videoidStr);
    return false;
  }

  scbt.f.toast('Status: No VOD chat found for this video.');
  return false;
}



scbt.f.handler_sort_saved_streams_by_serviceid = function(e){
  console.log('doing handler_sort_saved_streams_by_serviceid');
  if (e) { if (e.preventDefault) { e.preventDefault(); } }
  scbt.e.scbtPreviousStreams.innerHTML = '';
  scbt.a.savedStreams.sort((a,b) => (a.serviceid.toLowerCase() > b.serviceid.toLowerCase()) ? 1 : ((b.serviceid.toLowerCase() > a.serviceid.toLowerCase()) ? -1 : 0));
  scbt.f.build_list_of_saved_stream_chat_by_arr(scbt.a.savedStreams);
  scbt.f.add_listener_for_saved_chat_from_streams(scbt.a.savedStreams);
  e = a = b = null; return false;
}


scbt.f.handler_sort_saved_streams_by_channelid = function(e){
  console.log('doing handler_sort_saved_streams_by_channelid');
  if (e) { if (e.preventDefault) { e.preventDefault(); } }
  scbt.e.scbtPreviousStreams.innerHTML = '';
  scbt.a.savedStreams.sort((a,b) => (a.channelid.toLowerCase() > b.channelid.toLowerCase()) ? 1 : ((b.channelid.toLowerCase() > a.channelid.toLowerCase()) ? -1 : 0));
  scbt.f.build_list_of_saved_stream_chat_by_arr(scbt.a.savedStreams);
  scbt.f.add_listener_for_saved_chat_from_streams(scbt.a.savedStreams);
  e = a = b = null; return false;
}


scbt.f.handler_sort_saved_streams_by_videoid = function(e){
  console.log('doing handler_sort_saved_streams_by_videoid');
  if (e) { if (e.preventDefault) { e.preventDefault(); } }
  scbt.e.scbtPreviousStreams.innerHTML = '';
  scbt.a.savedStreams.sort((a,b) => (a.videoid.toLowerCase() < b.videoid.toLowerCase()) ? 1 : ((b.videoid.toLowerCase() < a.videoid.toLowerCase()) ? -1 : 0));
  console.log(scbt.a.savedStreams);
  scbt.f.build_list_of_saved_stream_chat_by_arr(scbt.a.savedStreams);
  scbt.f.add_listener_for_saved_chat_from_streams(scbt.a.savedStreams);
  e = a = b = null; return false;
}


scbt.f.handler_sort_saved_streams_by_current = function(e){
  if (e) { if (e.preventDefault) { e.preventDefault(); } }
  scbt.e.scbtPreviousStreams.innerHTML = '';
  scbt.a.savedStreams.sort((a,b) => (a.videoid.toLowerCase() > b.videoid.toLowerCase()) ? 1 : ((b.videoid.toLowerCase() > a.videoid.toLowerCase()) ? -1 : 0));

  var arrl = scbt.a.savedStreams.length;
  var arr = [];
  // var str = scbt.f.get_str_channelid();
  // if (str) {
  //  str = str.toLowerCase();
  // }
  
  for (var i = 0; i < arrl; i++) {
    if (scbt.a.savedStreams[i].channelid.toLowerCase() == scbt.v.channelid) {
      arr.unshift(scbt.a.savedStreams[i]);
    } else {
      arr.push(scbt.a.savedStreams[i]);
    }
  }
  scbt.f.build_list_of_saved_stream_chat_by_arr(arr);
  scbt.f.add_listener_for_saved_chat_from_streams(arr);
  e = a = b = i = arrl = arr = null; return false;
}


scbt.f.handler_click_username_insert_into_search = function(e){
  if (e) {
    if (e.preventDefault) { e.preventDefault(); }
    if (e.target) {
      if (e.target.textContent) {
        var elemArr = document.body.getElementsByClassName('scbtChatSearchInputText');
        if (elemArr[0]) {
          elemArr[0].value = e.target.textContent;
        }
        elemArr = null; e = null;
      }
    }
  }
  return false;
}


scbt.f.set_chat_arr_to_database = async function(chatArr){
  console.log('doing set_chat_arr_to_database', chatArr);
  console.log(chatArr[0]);

  if (chatArr && chatArr.length > 3) {
    var dbExists = await scbt.f.get_binary_if_db_exists(chatArr[0]);
    if (dbExists === true) {
      console.log('yes it exists, do nothing');
    } else {
      console.log('no');
      var arrl = chatArr.length;
      var chatObj = {};
      var chatObjArr = [];
      var arr = [];

      for (var i = 0; i < arrl; i++) {
        if (chatArr[i] && chatArr[i].indexOf(',') > -1) {
          if (i > 3) {
            arr = chatArr[i].split(',');
            chatObj.message = arr[0];
            chatObj.itemid = arr[1];
            chatObj.timestamp = arr[2];
            chatObj.username = arr[3];
            chatObj.sub = Number(arr[4]);
            chatObj.moderator = Number(arr[5]);
            chatObj.owner = Number(arr[6]);
            chatObj.donation = Number(arr[7]);
            chatObj.newSub = Number(arr[8]);
            chatObj.verified = Number(arr[9]);
            chatObj.gifter = Number(arr[10]);
            chatObj.founder = Number(arr[11]);
            chatObj.og = Number(arr[12]);
            chatObj.staff = Number(arr[13]);
            chatObj.anevent = Number(arr[14]);
            chatObjArr.push(chatObj);
            chatObj = {};
          }
        }
      }
      scbt.f.save_bulk_chat_from_dbName_arr(chatArr[0], chatObjArr);
    }
  }
  return false;
}


scbt.f.add_listener_for_chat_close = function(e){
  console.log("add_listener_for_chat_close 1");
  scbt.e.scbtDragSpot1CloseButton.addEventListener('click', function(e2) {
    scbt.e.scbtSimulcastChat1.innerHTML = '';
    scbt.e.scbtDragSpot1.classList.remove('scbt-forefront');
    scbt.e.scbtSavedChat1.classList.remove('scbt-active');
    scbt.e.scbtChatA.innerHTML = '';
    scbt.e.scbtChatB.innerHTML = '';
    return false;
  });
  scbt.e.scbtDragSpot2CloseButton.addEventListener('click', function(e2) {
    scbt.e.scbtSimulcastChat2.innerHTML = '';
    scbt.e.scbtDragSpot2.classList.remove('scbt-forefront');
    scbt.e.scbtSavedChat1.classList.remove('scbt-active');
    scbt.e.scbtChatA.innerHTML = '';
    scbt.e.scbtChatB.innerHTML = '';
    return false;
  });
  scbt.e.scbtDragSpot3CloseButton.addEventListener('click', function(e2) {
    scbt.e.scbtSimulcastChat3.innerHTML = '';
    scbt.e.scbtDragSpot3.classList.remove('scbt-forefront');
    scbt.e.scbtSavedChat1.classList.remove('scbt-active');
    scbt.e.scbtChatA.innerHTML = '';
    scbt.e.scbtChatB.innerHTML = '';
    return false;
  });
  scbt.e.scbtDragSpot4CloseButton.addEventListener('click', function(e2) {
    scbt.e.scbtSavedChat1.innerHTML = '';
    scbt.e.scbtDragSpot4.classList.remove('scbt-forefront');
    scbt.e.scbtSavedChat1.classList.remove('scbt-active');
    scbt.e.scbtChatA.innerHTML = '';
    scbt.e.scbtChatB.innerHTML = '';
    return false;
  });
}


// ************* end DB functions