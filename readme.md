<!-- SPDX-FileCopyrightText: © 2024 promising future digital media llc. All rights reserved. <admin@parleystar.com> -->
<!-- SPDX-License-Identifier: Mozilla Public License 1.1 (MPL-1.1) -->

# ParleyStar browser extension for computers and mobile devices live stream chat
<hr/>
### [English Documentation](#summary)
<hr/>
## <span id="summary">Summary</span>

## Welcome!

Thank you for downloading and trying this!  Written by a single person in the USA, we hope this helps you keep up with chat in small to large live streams and especially multi-streaming/simulcasting on different platforms. 

## Basic Overview

Installing this in your Chrome-based browser will enhance the streaming and chatting experience on livestreams on desktop/laptop/Chromebook computers, and on mobile Android devices.

## How to use this for desktop/laptop usage

This free browser extension is an add-on or plugin or extension to chromium based browsers such as Google Chrome, Brave Browser, Microsoft Edge, Vivaldi, Epic, Torch, Opera GX Gaming, Orion, Arc to name a few. It can be completely added or removed at any time, and the site will continue to work separately as is. This doesn't change the basic functionality of any website or app.

## How to use this for Android phones and devices.

This free browser extension is an add-on or plugin or extension to mobile Android devices using chromium based browsers that can use browser extensions such as Kiwi, Lemur, Yandex, Mask, Microsoft Edge, Samsung Internet. It can be completely added or removed at any time, and the site will continue to work separately separately as is. This has only been tested on the Kiwi browser so far. You can find Kiwi browser for free in the Google Play Store or F-Droid Store or APKpure.com or at https://kiwibrowser.com/ . Kiwi is based on Google Chrome browser but with the added ability to use browser extensions like on on desktop. This provides extra features for the website version of the video/streaming website in the browser, not the app.

### Online Version
> Download from: 

### Standalone Version
> Download from: 

### Standalone Installation
> 1. Download the file
> 2. Open up browser, and enter ```chrome://extensions/``` in the address bar.
> 3. Turn on Developer Mode via the switch at the top right corner of the window.
> 4. Click and drag the downloaded file into the viewport.

### Why is there download and "developer mode" instructions?
> This is intended to be a base or infrastructure that people can build upon or alter to their liking. It is intended to be something a streamer or heavy stream chatter can delete stuff in it they don't like, add stuff they may want, and use how they want -- the definition of "open source" or "accountable/transparent".

## Which websites does it work on now?
6/2024: Live streams on Kick.com, Rumble.com, Tiktok.com, Twitch.tv, Rumble.com, Youtube.com
Later 2024: Arena.TV, Instagram.com, Noice.com, Odysee.com, Shareplay.tv, X.com

## Does this work on iPhones, iPads, Windows Phones, Firefox, or something you haven't mentioned here?
No.

## Tracking/Security/Privacy
This extension saves information to your browser locally if you choose such as options/preferences you set, chat that you save, and browser version which you can export out to yourself via CSV file and is deleted when you uninstall the extension. There are buttons in the extension UI for you to export or delete your data saved at any time. We save none of your options/preferences "in the cloud" at this time.  We currently use no tracking or advertising or cookies at this time of 6/2024. This may change and you will be notified if it does. We are currently a very small team and do not have time or resources for large privacy policy scope or legal/public relations/social/customer service policy other than to provide the basic technical features needed to the streamers for their usage.

## How to get help with using this or questions or issues
File an issue here on this repository
email support@parleystar.com 

## Settings Menu
- This extension adds an settings menu to the streaming website if you choose to save settings for yourself which will happen on each page load. These options/preferences are saved to your computer and not sent out to the internet. You can export these settings to a file and import them on other devices to use if you choose. This uses the Google Chrome settings sync feature so that if you are using Google Chrome browser and logged in to your Google account on that browser, if you set settings such as chat color to green on your Chrome browser on your PC, your Chrome browser logged in on your other logged-in devices such as Chromebook will also show chat color in green. If you are not logged into your Google/Youtube account on the browser, the settings will not sync across devices and you will need to do that manually if you want.

## Export / Offline
- This extension can work online or offline. If online, you can view the livestream chat on the streaming website. If offline, you can view livestream chat in exported CSV files in spreadsheet programs like Excel, or on our offline-enabled web app viewers.

## Be Featured
- If you use this browser extension in your streaming and would like to be featured on one of our webpages, reach out to us and let us know!  Thanks!

## Source/Funding/Leadership/etc
This is currently funded/hosted 100% by the creator/developer, who is a person not associated with any live streaming or video/media company in any formal way. This is not funded or associated with any major corporation or any of the streaming platforms listed here. This is not associated with or funded by any political party.

## How to help
- If you have ideas and would like to help in programming, UI design or funding, you may email us at support@parleystar.com .  Thanks!

## Voice Features
- This extension provides voice activation features for controlling the live stream chat. These features are optional and can be turned on or off at any time. When the voice activation is turned on, your computer web browser is always listening for you to say certain voice commnds and the goal is hands-free scrolling, searching and manipulation of the live stream chat on your computer.

## Keybind Features
- This extension provides keyboard keybind features for controlling the live stream chat. These features are optional and can be turned on or off at any time. When the keybind is turned on, your computer web browser is always listening for you to push certain keybinds and the goal is mouse-free scrolling, searching and manipulation of the live stream chat on your computer.

## Version A
- This is released under Google Chrome Manifest version 3 (2022) and may only work with browsers/operating systems updated in 2022 and beyond.

## Version B
- This is released in 2024 and follows this versioning structure. 1.6.1.2024.1 . Version 1 released on June 1st in year 2024 and the 1st release of that day. 1.6.1.2024.2 would be the 2nd release of that day.

## License
This is released under [MOZILLA PUBLIC LICENSE Version 1.1]https://www.mozilla.org/en-US/MPL/1.1/ . Copyright © 2024 promising future digital media llc. written by biolithic. All rights reserved.

Have fun!