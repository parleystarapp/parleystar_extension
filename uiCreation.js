async function initUI(){//TODO: need to check when stream ends. a raid will lose everything
 
    await delay(2222)
    function addApplicationMenuBtn(){
        if(gi(document,'menu_btn_cont')) gi(document,'menu_btn_cont').remove();
        let navbar = cn(document,'main-navbar')?.[0];
// console.log(navbar)
        let nav_rect = navbar.getBoundingClientRect()
        let menu_btn_cont = ele('div');
        inlineStyler(menu_btn_cont,`{width:${nav_rect.height-20}px; height:${nav_rect.height-20}px;}`);
        a(menu_btn_cont,[['id','menu_btn_cont']]);
        navbar.firstChild.nextSibling.insertBefore(
            menu_btn_cont,
            navbar.firstChild.nextSibling.firstChild
        );
        window.addEventListener("resize", ()=> {
            Array.from(cn(document,'menu_container'))?.forEach(elm=> {
                setContainerDimensionsInWindow({
                    cont:elm,
                    middle_cont:cn(document,'middle_cont')?.[0]
                })                
            });            
            // if(window.innerWidth * 1.33 < window.innerHeight){
            //     inlineStyler(menu_btn_cont,`{position:fixed; left:${menu_btn_cont.getBoundingClientRect().left}px; top:10px; z-index:${topZIndexer()};}`);
            // }else{
            //     a(menu_btn_cont,[['style',`width:${nav_rect.height-20}px; height:${nav_rect.height-20}px;`]]);
            // }
        });
        // (window.innerWidth < window.innerHeight ? navbar.firstChild : navbar.firstChild.nextSibling);
        let menu_btn_icon = ele('div');
        menu_btn_icon.innerHTML = app_icons.logo_svg;
        inlineStyler(menu_btn_icon,`{width:${nav_rect.height-20}px; height:${nav_rect.height-20}px; cursor:pointer; border-radius: 0.2em;}`);
        menu_btn_cont.appendChild(menu_btn_icon);
// console.log('added');
        menu_btn_cont.onclick = createApplicationMenu
    }

    addApplicationMenuBtn()



    async function viewVODfromVideoPage(params){
        // console.log(['viewVODfromVideoPage',params])
        // let channel_slug = getChannelSlugFromDOM();
        // console.log(['viewVODfromVideoPage',channel_slug])
        let current_uuid = /(?<=kick\.com\/video\/)[\w-]+/.exec(window.location.href)?.[0];
        let vods = await getKickVODs(params?.channel);
        // console.log(['viewVODfromVideoPage',vods])
        let target_vod = vods.filter(r=> r?.video?.uuid == current_uuid)?.[0];
        // console.log(['target_vod',target_vod])
        buildVODViewer({
            ref_elm:params?.ref_elm,
            channel:params?.channel,
            vod:target_vod
        })
    }

    // async function generateSignInView(){

    // }
    

    // var current_stream_ts_files = [];
    var current_stream_ts_extracted_data = [];
    var user_is_recording = false;
    var cache_num = 150 /* each segment is 2 min, so 75 == 2.5 min */

    const isStreamerPage=()=> !/kick.com\/video\//.test(window.location.href) && /kick.com\/[\w-]+$/.test(window.location.href);
    const isVODPage=()=> /kick.com\/video\//.test(window.location.href);
    const getStreamInfoElm = ()=> document.getElementsByClassName('stream-info')?.[0];
    if(getStreamInfoElm() && !isVODPage()){
        let stream_info_elm = getStreamInfoElm();
        let stream_info = await getChannelInfo({channel_slug:/(?<=kick.com\/)[\w-]+$/.exec(window.location.href)?.[0]})
        // convertCreatedAtTime
        createRecordOptionBtn({stream_info:stream_info,parent_elm:stream_info_elm,id:'record_livestream_cont'});
    }

    async function addFollowers(menu_elms){

        let following = await getFollowedChannels();
        // console.log(['following',following])

        inlineStyler(menu_elms?.middle_cont,`{display:grid; grid-template-rows: ${Array(following?.channels?.length ? following?.channels?.length +1 : 1).fill().map((s)=> '32px ').reduce((a,b)=> a+b).trim()}; grid-gap: 8px;}`);

        for(let i=0; i<following?.channels?.length; i++ ){
            let r = following?.channels?.[i];
            let cont = ele('div');
            a(cont,[['class','opt_button']]);
            menu_elms?.middle_cont.appendChild(cont);
            inlineStyler(cont,`{border-top-left-radius: 1em; display:grid; grid-template-columns:32px 1fr 62px; grid-gap:8px; cursor: pointer;}`);
            
                let avatar = ele('img');
                a(avatar,[['src',r.profile_picture]]);
                inlineStyler(avatar,`{margin: auto; border-radius:50%;}`);
                cont.appendChild(avatar);
                
                // let vods = await getKickVODs(r);
                let num_display = ele('div');
                a(num_display,[['class','num_display'],['data-channel_info',btoaJSON(r)]]);
                inlineStyler(num_display,`{margin: auto;}`);
                cont.appendChild(num_display);
                // num_display.innerText = `${vods?.length} ${vods?.length != 1 ? 'VODs' : 'VOD'} available`;

                cont.onclick = ()=> buildVODsSelection(cont,r,null);

                let live_icon = ele('div');
                inlineStyler(live_icon,`{margin: auto;}`);
                cont.appendChild(live_icon);
                live_icon.innerHTML = r?.is_live 
                ? `
                <div style="margin: auto; display:grid; grid-template-columns:12px 1fr; grid-gap:3px;">
                    <div style="margin: auto; width: 12px; height:12px; border-radius: 50%; background-color:${colors.kick_green};"></div>
                    <div>${r.viewer_count}</div>
                </div>` 
                    : `<svg style="margin: auto;" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10.0823 2.24252C9.63166 2.1069 9.15042 2.0369 8.65606 2.0369C6.09675 2.0369 3.98805 3.94873 3.67306 6.42054C2.18122 6.50804 1 7.74176 1 9.25548C1 9.82859 1.17062 10.3623 1.45936 10.8086L10.0823 2.24252Z" fill="currentColor"></path><path d="M13.5957 6.13175C13.4644 5.42739 13.18 4.77116 12.7863 4.20242L14.7594 2.24247L13.5257 1L12.9963 1.52499L1.32408 13.1228L1.24096 13.206L2.47468 14.4484L4.83712 12.0991H11.7188C13.53 12.0991 15 10.6291 15 8.81793C15 7.70671 14.4444 6.72236 13.5957 6.13175Z" fill="currentColor"></path></svg>`
            if(i == (following?.channels?.length-1)) await addNumVODsAvail(menu_elms?.middle_cont)
        }
        
        setContainerDimensionsInWindow(menu_elms);
    }
    async function addNumVODsAvail(parent_elm){//addNumVODsAvail
        let num_vods_elms = Array.from(cn(parent_elm,'num_display'))
        for(let i=0; i<num_vods_elms.length; i++){
            // console.log(num_vods_elms?.[i])
            if(num_vods_elms?.[i]){
                let r = atobJSON(num_vods_elms?.[i]?.getAttribute('data-channel_info'));
                console.log(r);
                let vods = await getKickVODs(r);
                if(num_vods_elms[i]) num_vods_elms[i].innerText = `${vods?.length} ${vods?.length != 1 ? 'VODs' : 'VOD'} available`;
            }
        }
        return true;
    }

    async function createApplicationMenu(){
        let rect = this.getBoundingClientRect();
        let cont_params = {
            id:'kick_application_menu',
            top: rect.top,
            left: rect.left,
            width: 500,
        }
        // console.log(cont_params)
        let menu_elms = createBasicContainer(cont_params);
        // console.log(menu_elms)
        
        let search_cont = ele('div');
        menu_elms.middle_cont.appendChild(search_cont);
        inlineStyler(search_cont,`{background:#000000; display:grid; grid-template-columns: 1fr 22px;}`);
            let search_input = ele('input');
            search_cont.appendChild(search_input);
            a(search_input,[['placeholder','channel search']]);
            search_input.onkeyup = async (e)=> {
                if(e.key == 'Enter' || search_input?.value?.trim()?.length > 1){
                    // console.log(e.key);
                    let results = await kickChannelSearch({query:search_input?.value?.trim()});
                    // console.log(results);
                    buildSearchResultsCont(search_cont,results);
                }
                
            }
            inlineStyler(search_input,`{transform:translate(4px,0px); margin: auto; border-radius: 0.2em; background: transparent; outline: none;}`);
                let search_icon = ele('div');
                inlineStyler(search_icon,`{margin: auto;}`);
                search_icon.innerHTML = app_icons.search_icon;
                search_cont.appendChild(search_icon);
        let is_vod_page = isVODPage();
        let is_streamer_page = isStreamerPage();
                // if(!is_streamer_page && !is_vod_page) addFollowers(menu_elms)
                addFollowers(menu_elms)
        if(is_vod_page){
            // console.log('is_vod_page',is_vod_page)
            let channel_slug = getChannelSlugFromDOM();
            var channel_info = await getChannelInfo({channel_slug:channel_slug});
            viewVODfromVideoPage({ref_elm:search_cont,channel:channel_info})
        }
        if(is_streamer_page) {
            createLivestreamViewer({channel_name: getChanellNameFromDOM()});
            let channel_slug = /(?<=kick.com\/)\w+/.exec(window.location.href)?.[0];
            let channel_info = await getChannelInfo({channel_slug:channel_slug});
            let vods = await getKickVODs({...channel_info,...{channel_slug:channel_slug}});
            // console.log(channel_info,vods);
            buildVODsSelection(menu_elms.cont,channel_info,vods)
        }
                
                
        setContainerDimensionsInWindow(menu_elms);
        // let vods = await getKickVODs({
            
            // })
    }
        
    function createRecordOptionBtn(params){//document.getElementsByClassName('stream-info')?.[0]
        var {id,parent_elm,stream_info} = params;
        if(gi(document,id)) gi(document,id).remove();
        // cn(document,'main-navbar')[0].firstChild.nextSibling
        let cont_elms = createBasicContainer({
            css:`{color:#fffffd;}`,
            ref_elm:cn(document,'main-navbar')[0].firstChild.nextSibling.firstChild.nextSibling,
            parent_elm:cn(document,'main-navbar')[0].firstChild.nextSibling,
            attach_method:'insertBefore',
            header_height:56,
            id:id,
        });
        a(params.head_mover,[['style','display: none;}'],['class','record_adjustments']]);

        //hidethese
        ['head_mover','tbody','footer'].forEach(key=> inlineStyler(cont_elms[key],'{display:none;}'))
        cont_elms.cls_btn.innerHTML = '';
        inlineStyler(cont_elms.cont,`{font-size: 12px; width: 36px; height: 36px; border-radius:50%; background-color:#000000; border:2px solid ${colors.kick_green}; color:${colors.kick_green};}`);
        inlineStyler(cont_elms.header,`{grid-template-columns: 0px 1fr 36px;}`);
        let rec_btn = ele('div')
        a(rec_btn,[['title','Click to Record Livestream.']])
        cont_elms.header.appendChild(rec_btn);
        rec_btn.innerHTML = `<svg title="record livestream" transform="translate(-15,-15)" style="background-color:transparent;" id="record_svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 180 180" preserveAspectRatio="none">
        <circle display="${(user_is_recording ? 'contents' :'none')}" cx="100" cy="100" r="57" id="thin-halo" fill="#ef5350" stroke="#ef5350" stroke-width="22" stroke-dasharray="360,20000" transform="rotate(-126,100,100)">
        </circle> 
        <circle display="${(user_is_recording ? 'contents' :'none')}" cx="100" cy="100" r="57" id="red-halo" fill="none" stroke="#ef5350" stroke-width="22" stroke-dasharray="180,20000" transform="rotate(-126,100,100)">
        </circle>
        <circle display="${(user_is_recording ? 'contents' :'none')}" cx="100" cy="100" r="57" id="black-halo" fill="none" stroke="#000000" stroke-width="15" stroke-dasharray="0,360" transform="rotate(-126,100,100)">
        </circle>
        <text title="record livestream" id="rec_text_elm" text-anchor="middle" x="100" y="110" style="font-size: 3em;" fill="#000000">REC</text>
        
        <rect style="display:none;" id="stop_rect_svg" rx="10" ry="10" x="70" y="70" fill="#000000" height="60" width="60"></rect>
        
        <path style="display:none; transform: rotate(29deg);" id="stop_tri_svg" d="M8.72798 15.795L3.72798 7.795C3.10356 6.79593 3.82183 5.5 4.99998 5.5L15 5.5C16.1781 5.5 16.8964 6.79593 16.272 7.795L11.272 15.795C10.6845 16.735 9.31549 16.735 8.72798 15.795Z" fill="${colors.kick_green}"/>
        </svg>`
        // <path style="display:grid;" id="stop_tri_svg" d="M296.494737,3608.57322 L292.500752,3606.14219 C291.83208,3605.73542 291,3606.25002 291,3607.06891 L291,3611.93095 C291,3612.7509 291.83208,3613.26444 292.500752,3612.85767 L296.494737,3610.42771 C297.168421,3610.01774 297.168421,3608.98319 296.494737,3608.57322" fill="${colors.kick_green}"></path>
        inlineStyler(rec_btn,`{cursor:pointer; width: 56px; height: 56px; background-color:transparent;}`);//font-size: 12px; width: 36px; height: 36px; padding: 8px; border-radius:50%; background-color:transparent; border:2px solid ${colors.kick_green}; color:${colors.kick_green};
        
        // cont_elms.head_mover.innerHTML = `<div style="margin: auto; transform:translate(10px,-6px)">Click "REC" again to download.</div>`;
        // // let btn = cont_elms.cls_btn;
        // a(cont_elms.cls_btn,[['class','record_stream_btn'],['data-recorder_dashboard_state','false']]);

        // // inlineStyler(cont_elms.header,`{display:grid; grid-template-columns: 56px 1fr; height: 56px;}`);

        reverseRun();
        

        // inlineStyler(cont_elms.cont,`{transition: all 533ms;}`);
            // let btn_text = ele('div');
            // inlineStyler(btn_text,`{margin:auto;}`);
            // cont_elms.cls_btn.appendChild(btn_text);
            // btn_text.innerText = 'REC';
        rec_btn.onclick = ()=> {
            // console.log('pre initLivestreamRecorder')
            initLivestreamRecorder({...cont_elms,...{rec_btn:rec_btn,channel_slug:/(?<=kick.com\/)[\w-]+$/.exec(window.location.href)?.[0]}})
        };
        initCurrentLiveStreamBlobLogger({...cont_elms,...{
            stream_info:stream_info,
            id:'record_livestream_cont',
            current_window_href:window.location.href,
            channel_slug:/(?<=kick.com\/)[\w-]+$/.exec(window.location.href)
        }})
        
    }
    

    async function initLivestreamRecorder(params){
        var {cont,cls_btn,rec_btn,header,middle_cont} = params;
        // console.log(params)
        var cont_rect = cont.getBoundingClientRect();
        var container_width = 260;
        var parent_rect = cont.parentElement.getBoundingClientRect();
        var dif_rect = atobJSON(middle_cont.getAttribute('data-css'));
        // console.log(['dif_rect',dif_rect])

        // initVideo({
        //     parent_elm:middle_cont,
        //     ref_elm:middle_cont.firstChild,
        //     attach_method:"insertBefore",
        //     // records
        // })

        //user_is_recording = !user_is_recording;
        if(user_is_recording){//if user is presently recording, stop and minimize the view
            ['head_mover','tbody','footer'].forEach(key=> {if(params?.[key]) inlineStyler(params[key],'{display: none;}')});
            inlineStyler(cont,`{width: 36px; transition:all 100ms ease-in-out; border-radius: 50%; border:2px solid ${colors.kick_green}; color:${colors.kick_green};}`);
            inlineStyler(gi(document,'stop_rect_svg'),`{display:none;}`);
            // inlineStyler(gi(document,'stop_tri_svg'),`{display:none;}`);
            if(gi(document,'rec_text_elm')) inlineStyler(gi(document,'rec_text_elm'),`{display:grid;}`)//${colors.red}
            a(rec_btn,[['title','Click to record livestream.']]);
            
            user_is_recording = !user_is_recording;
            downloadCurrentRecording(params);
            // ['stop_rect_svg','stop_tri_svg'].forEach(key=> inlineStyler(gi(document,key),`{display:none;}`));


        }else{//open view
            // console.log('open recording view')
            // console.log(
            //     params
            // )
            a(rec_btn,[['title','Click to stop and download progress. The recording will automatically download a segment every 15 minutes.']]);
            gi(document,'record_svg').onmouseenter = ()=> {
                if(gi(document,'rec_text_elm') && getComputedStyle(gi(document,'rec_text_elm')).color != 'rgb(83, 252, 24)') {
                    inlineStyler(gi(document,'stop_rect_svg'),`{display:grid;}`);
                    inlineStyler(gi(document,'rec_text_elm'),`{display:none;}`);
                }
                if(gi(document,'rec_text_elm') && getComputedStyle(gi(document,'rec_text_elm')).color == 'rgb(83, 252, 24)'){
                    inlineStyler(gi(document,'stop_tri_svg'),`{display:grid;}`);
                    inlineStyler(gi(document,'rec_text_elm'),`{display:none;}`);
                }
            }
            gi(document,'record_svg').onmouseleave = ()=> {
                inlineStyler(gi(document,'stop_rect_svg'),`{display:none;}`);
                if(gi(document,'rec_text_elm')) {
                    inlineStyler(gi(document,'rec_text_elm'),`{display:grid;}`)
                    inlineStyler(gi(document,'stop_tri_svg'),`{display:none;}`);
                }
                // inlineStyler(gi(document,'rec_text_elm'),`{border:2px solid transparent; color:#000000;}`)//${colors.red}
            }
            inlineStyler(header,`{grid-template-columns: 0px 1fr 36px;}`);
            a(params.head_mover,[['style','display: grid;}'],['class','record_adjustments']]);
            // a(gi(document,'record_svg'),[['transform',"translate(-8,-8)"]]);
            await delay(10);
            inlineStyler(cont,`{width: ${container_width/2}px; transition:all 100ms ease-in-out; border-top-left-radius: 1em; border-bottom-left-radius:1em;}`);// position:fixed; top: ${cont_rect.top}px;
            await delay(66);
            inlineStyler(cont,`{width: ${container_width}px; transition:all 100ms ease-in-out; border-top-left-radius: 0.2em; border-bottom-left-radius:0.2em; border-bottom-right-radius:2em; border-top-right-radius:2em;}`);// position:fixed; top: ${cont_rect.top}px;
            await delay(100);
            inlineStyler(cont,`{border:2px solid transparent; color:${colors.red};}`)//${colors.red}

            let stream_seconds = streamTimeDurationSeconds();
            let started_recording = current_stream_ts_extracted_data?.[0] ? ((new Date(current_stream_ts_extracted_data[0].datetime).getTime()) - (new Date().getTime() - (stream_seconds*1000)))/1000 : 0;

            createRecordingStatusBar({ /*domUtils.js*/
                parent_elm:document.getElementsByClassName('record_adjustments')[0],
                ref_elm:null,
                attach_method:'appendChild',
                stored_seconds:current_stream_ts_extracted_data?.length ? current_stream_ts_extracted_data.map(r=> r.duration_s).reduce((a,b)=> a+b) : 0,
                stream_seconds:stream_seconds,
                stream_rec_start:started_recording,
            });
            user_is_recording = !user_is_recording
        }

        // console.log(current_stream_ts_extracted_data)
    }

    function addCircleElm(params){
        var {outer_html,id,rotate_pos,stroke_w,stroke_color,parent_elm,ref_elm,attach_method} = params;
        if(gi(document,id)) gi(document,id).remove();
        let circle = ele('circle');
        parent_elm[attach_method](circle,ref_elm)
        circle.outerHTML = outer_html;
    	return gi(document,id)
    }
    
    async function circleAnimate(){
        let outer_html = `<circle cx="100" cy="100" r="57" id="red-halo" fill="none" stroke="#ef5350" stroke-width="22" stroke-dasharray="0,20000" transform="rotate(-126,100,100)"></circle>`;
        var circle = addCircleElm({outer_html:outer_html,id:'red-halo',rotate_pos:-90,stroke_w:22,stroke_color:'#ef5350',attach_method:'insertBefore',ref_elm:gi(document,'rec_text_elm'),parent_elm:gi(document,'record_svg')})
        //circle.style.zIndex = '999999';
        var interval = 30;
        var angle = 0;
        var angle_increment = 6;
        var target_elms = ['thin-halo','red-halo','black-halo'].map(key=> gi(document,key));
        
        for(let i=angle; i<=360; i=i+angle_increment){
            if(!streamerStillLive){ downloadCurrentRecording({channel_slug:/(?<=kick.com\/)[\w-]+$/.exec(window.location.href)?.[0]})}
            if(streamerStillLive() && current_stream_ts_extracted_data?.length){
                let stream_seconds = streamTimeDurationSeconds();
                let started_recording = ((new Date(current_stream_ts_extracted_data[0].datetime).getTime()) - (new Date().getTime() - (stream_seconds*1000)))/1000
                // let recorded_display = parseTimeOffset(((stream_seconds-4)-started_recording));
                updateRecordingStatusBar({
                    stored_seconds:current_stream_ts_extracted_data.map(r=> r.duration_s).reduce((a,b)=> a+b),
                    stream_seconds:stream_seconds,
                    stream_rec_start:started_recording,
                });
                // if(gi(document,'stream_time_stored_in_memory')) gi(document,'stream_time_stored_in_memory').innerHTML = `<span style="font-family:'Share Tech Mono', sans-serif; color:${colors.kick_green};">${recorded_display}</span><span>&nbsp;stored in memory.</span>`;
            }
            a(circle,[["stroke-dasharray",`${i},20000`]]);//,['display',(user_is_recording ? "contents" : 'none')]
            if(user_is_recording) {
                target_elms.forEach(elm=> elm.removeAttribute('display'))
                gi(document,'rec_text_elm').setAttribute('fill','#000000;');//colors.red
            } else {
                target_elms.forEach(elm=> elm.setAttribute('display','none'))
                gi(document,'rec_text_elm').setAttribute('fill',colors.kick_green);
            }
            await delay(interval);
            // if(!user_is_recording) break;
        }
        reverseRun()
    }
    
    async function reverseRun(){//${user_is_recording ? 'display="contents"' : 'display="none"'} 
        var outer_html = `<circle cx="100" cy="100" r="57" id="black-halo" fill="none" stroke="#000000" stroke-width="15" stroke-dasharray="0,360" transform="rotate(-126,100,100)"></circle>`;
        var circle = addCircleElm({outer_html:outer_html,id:'black-halo',rotate_pos:-90,stroke_w:10,stroke_color:'#000000',attach_method:'insertBefore',ref_elm:gi(document,'rec_text_elm'),parent_elm:gi(document,'record_svg')})
        var interval = 30;
        var angle = 0;
        var angle_increment = 6;        
        var target_elms = ['thin-halo','red-halo','black-halo'].map(key=> gi(document,key));
        
        for(let i=angle; i<=360; i=i+angle_increment){
            a(circle,[["stroke-dasharray",`${i},360`]]);//,['display',(user_is_recording ? "contents" : 'none')]
            if(user_is_recording) {
                target_elms.forEach(elm=> elm.removeAttribute('display'));
                gi(document,'rec_text_elm').setAttribute('fill','#000000;');//colors.red
            } else {
                target_elms.forEach(elm=> elm.setAttribute('display','none'))
                gi(document,'rec_text_elm').setAttribute('fill',colors.kick_green);
            }
            await delay(interval);
            // if(!user_is_recording) break;
        }
        circleAnimate()
    }
    
    function getStreamQualitySelection(){
        let selected_text = document.getElementsByClassName('vjs-menu-item vjs-selected')?.length ? Array.from(document.getElementsByClassName('vjs-menu-item vjs-selected'))?.filter(elm=> /\bauto\b|\d{3}p(\d|\b)/i.test(elm.textContent))?.[0]?.textContent : null;
        console.log(selected_text)
        let resolutions = ['auto','1080p','720p','480p','360p','160p','144p'];
        let i = resolutions.findIndex(r=> new RegExp(`\\b${r}(\\b|\\d)`,'i').test(selected_text));
        return i > 1 ? resolutions[i] : '1080p';
    }
    

    async function initCurrentLiveStreamBlobLogger(params){//{current_window_href,channel_slug:}
        var {current_window_href,channel_slug,stream_info} = params;
        
        let stream_start_timestamp = new Date(convertCreatedAtTime(stream_info?.livestream?.created_at)).getTime();

        let resolutions = await getLiveStreamResolutions(params);
        let selected_resolution = params?.resolution_selection ? resolutions?.filter(r=> r.resolution == params?.resolution_selection)?.[0] : resolutions[0];

        let status_elm_cont = ele('div');
        params.middle_cont.appendChild(status_elm_cont);
            let status_text = ele('div');
            status_elm_cont.appendChild(status_text);
            

        while(window.location.href == current_window_href){
            let start_time = new Date().getTime();
            let ts_data = await requestFromBackground({cmd:'getTSData',params:selected_resolution});
            let ts_time_dif = new Date().getTime()-start_time;
            let not_extracted = ts_data.filter(r=> !current_stream_ts_extracted_data.some(ts=> ts.timestamp == r.timestamp) );
            let fetch_duration = await recentStreamSegmentsLoopExtractor(not_extracted);
            let current_response_duration_total = ts_time_dif + fetch_duration

            let current_stream_block_duration = ts_data?.length ? ts_data.map(r=> r.duration_ms).reduce((a,b)=> a+b) : 0;
            
            let next_loop_delay = (current_stream_block_duration - current_response_duration_total)-5000; //remove 5 seconds so we do not miss anything.
            await delay(next_loop_delay);
            // current_stream_ts_files
        }
        async function recentStreamSegmentsLoopExtractor(segments){
            
            var fetch_duration = 0;
            for(let i=0; i<segments.length; i++){
                // each 1080p segment is approximately 3.9mb. This means we should not attempt to store more than 5  minutes in the background as a pre-record buffer. The max would be 15 minutes before issues arise. 
                let res_ob = await getArrayBufferWithRefObj(segments[i]);
                current_stream_ts_extracted_data.push(res_ob);

                fetch_duration = fetch_duration+res_ob.fetch_duration;
                if(!user_is_recording && current_stream_ts_extracted_data.length > cache_num){ current_stream_ts_extracted_data.shift();} // start deleting records from the front at the 5 minute rewind mark if the user has not requested to start recording.

                if(current_stream_ts_extracted_data.map(r=> r.duration_s).reduce((a,b)=> a+b) >= 900) {
                    console.log(current_stream_ts_extracted_data)
                    downloadCurrentRecording(params);
                    //430 == 30 min
                }
            }
            return fetch_duration;
        }
    }
    
    async function downloadCurrentRecording(params){
        let extracted = current_stream_ts_extracted_data;
        current_stream_ts_extracted_data = [];
        let stream_info = await getChannelInfo(params);
        let stream_start_timestamp = new Date(convertCreatedAtTime(stream_info?.livestream?.created_at)).getTime();
        let record_start_timestamp = extracted?.[0] ? parseTimeOffset((new Date(extracted[0].datetime).getTime() - stream_start_timestamp)/1000) : '';

        let resolutions = await getLiveStreamResolutions(params);
        let selected_resolution = params?.resolution_selection ? resolutions?.filter(r=> r.resolution == params?.resolution_selection)?.[0] : resolutions[0];
        let ts_data = await requestFromBackground({cmd:'getTSData',params:selected_resolution});
        let not_extracted = ts_data.filter(r=> !extracted.some(ts=> ts.timestamp == r.timestamp) );
        for(let i=0; i<not_extracted.length; i++){
            let res_ob = await getArrayBufferWithRefObj(not_extracted[i]);
            extracted.push(res_ob);
        }
        let blobs = extracted.filter(r=> r.array_buffer).map(r=> r.array_buffer)
        let blob_dl = new Blob(blobs,{
            type: 'video/mp4; codecs="avc1.64000d,mp4a.40.2"'
        });
        let link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob_dl);
        link.download = `${params.channel_slug} ${record_start_timestamp} to ${parseTimeOffset((new Date(extracted.at(-1).datetime).getTime() - stream_start_timestamp)/1000)}.mp4`;
        link.click();
        await delay(2000);
        
        if(link) link.remove();
    }


    
    async function buildVODsSelection(ref_elm,channel,vods){
        let rect = ref_elm.getBoundingClientRect();
        let cont_params = {
            id:'vod_selection_menu',
            top: rect.height,
            left: rect.left,
            width: 640,
        }
        let vod_sel_elms = createBasicContainer(cont_params);

        if(!channel?.profile_picture){
            let channel_info = await getChannelInfo(channel);
            var channel = {
                ...channel,
                ...{
                    profile_picture:channel_info?.user?.profile_pic,
                    user_username:channel_info?.user?.username,
                    channel_slug:channel_info?.slug,
                }
            };
        }

        if(!vods){
            var vods = await getKickVODs(channel);
        }
        // console.log(channel,vods)

        vod_sel_elms.head_mover.innerHTML = `
        <div style="display:grid; grid-template-columns: 32px 1fr; grid-gap:8px;">
            <img style="margin: auto; height:28px; width:28px; border-radius:50%; transform: translate(0px,4px);" src="${channel.profile_picture}"></img>
            <div style="margin: auto;">${channel.user_username} VODs</div>
        </div>`

        inlineStyler(vod_sel_elms?.middle_cont,`{display:grid; grid-template-rows: ${Array(vods?.length ? vods?.length +1 : 1).fill().map((s,i)=> i == 0 ? '1px ': '52px ').reduce((a,b)=> a+b).trim()}; grid-gap: 8px;}`);

        let gap_elm = ele('div');
        vod_sel_elms.middle_cont.appendChild(gap_elm);
        inlineStyler(gap_elm,`{height:0px;}`);

        for(let i=0; i<vods.length; i++){
            let v = vods[i];
            let cont = ele('div');
            a(cont,[['class','opt_button']]);
            vod_sel_elms.middle_cont.appendChild(cont);
            inlineStyler(cont,`{overflow: hidden; height: 52px; display:grid; grid-template-columns:92px 84px minmax(102px,260px) 84px; grid-gap:8px; cursor: pointer;}`);
            cont.onclick = ()=> buildVODViewer({ref_elm:cont,channel:channel,vod:v});

                let thumbnail = ele('img');
                a(thumbnail,[['srcset',v.thumbnail.srcset]]);
                inlineStyler(thumbnail,`{height: 52px; margin:auto;}`);//border-radius: 0.2em; 
                cont.appendChild(thumbnail);

                let start_time = ele('div');
                inlineStyler(start_time,`{margin:auto; font-size: 0.8em;}`);
                let x_days_ago = translateDateToXAgo(v.start_time);
                start_time.innerText = x_days_ago >= 1 ? `${x_days_ago} days ago` : `${(Math.round(24*(x_days_ago*100))/100)} hrs ago`;
                cont.appendChild(start_time);

                let title = ele('div');
                a(title,[['class','scrolling_holder']]);
                inlineStyler(title,`{transform: translate(0px,16px);}`);
                title.innerHTML = `<div class="scrolling_text">${v.session_title}</div>`;
                cont.appendChild(title);

                let duration = ele('div');
                inlineStyler(duration,`{margin:auto; font-size: 0.8em;}`);
                cont.appendChild(duration);
                duration.innerText = `${(Math.round((v.duration/3600000)*100)/100)} hrs`;

                //TODO change to x days/weeks/months ago

        }
        setContainerDimensionsInWindow(vod_sel_elms)
        await delay(111);
        setContainerDimensionsInWindow(vod_sel_elms)//running this twice to account for changes to width impacting changes to height.

    }

    async function buildSearchResultsCont(ref_elm,results){
        let rect = ref_elm.getBoundingClientRect();

        let cont_params = {
            id:'search_results_dropdown',
            top: rect.top+rect.height,
            left: rect.left,
            width: rect.width+32,
        }
        let results_elms = createBasicContainer(cont_params);
        
        inlineStyler(results_elms.tbody,`{grid-template-columns: 32px 1fr;}`);
        ['header','footer','right_side'].forEach(key=> results_elms[key].remove());//'right_side','left_side'
        inlineStyler(results_elms.cont,`{border: 0px solid transparent;}`);
        let search_results = results?.results?.[0]?.hits?.map(r=> r.document);
        // console.log(
        //     search_results
        // )

        
        for(let i=0; i<search_results?.length; i++ ){
            let r = search_results?.[i];
            let cont = ele('div');
            a(cont,[['class','opt_button'],['data-channel_info',btoaJSON(r)]]);
            results_elms?.middle_cont.appendChild(cont);
            inlineStyler(cont,`{display:grid; grid-template-columns:1fr 52px 52px; grid-gap:8px; cursor: pointer;}`);
            cont.onmouseover = addNumVodOnElmRef;

                let slug = ele('div');
                // a(slug,[['src',r.profile_picture]]);
                inlineStyler(slug,`{transform:translate(4px,1px);}`);
                cont.appendChild(slug);
                slug.innerText = r.slug;                
                cont.onclick = ()=> {
                    if(gi(document,'search_results_dropdown')) gi(document,'search_results_dropdown').remove();
                    buildVODsSelection(cont,r,null);
                }

                let num_display = ele('div');
                a(num_display,[['class','num_vods_display']]);
                inlineStyler(num_display,`{margin:auto; font-size:0.76em;}`);
                // inlineStyler(num_display,`{margin: auto;}`);
                cont.appendChild(num_display);

                let live_icon = ele('div');
                inlineStyler(live_icon,`{margin: auto;}`);
                cont.appendChild(live_icon);
                live_icon.innerHTML = r?.is_live 
                ? `
                    <div style="text-align: center; margin: auto; width: 52px; border-radius: 0.4em; background-color:${colors.kick_green};">LIVE</div>` 
                    : `<svg style="margin: auto;" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10.0823 2.24252C9.63166 2.1069 9.15042 2.0369 8.65606 2.0369C6.09675 2.0369 3.98805 3.94873 3.67306 6.42054C2.18122 6.50804 1 7.74176 1 9.25548C1 9.82859 1.17062 10.3623 1.45936 10.8086L10.0823 2.24252Z" fill="currentColor"></path><path d="M13.5957 6.13175C13.4644 5.42739 13.18 4.77116 12.7863 4.20242L14.7594 2.24247L13.5257 1L12.9963 1.52499L1.32408 13.1228L1.24096 13.206L2.47468 14.4484L4.83712 12.0991H11.7188C13.53 12.0991 15 10.6291 15 8.81793C15 7.70671 14.4444 6.72236 13.5957 6.13175Z" fill="currentColor"></path></svg>`
        }
    
        setContainerDimensionsInWindow(results_elms)
    }

    async function addNumVodOnElmRef(){
        let r = atobJSON(this.getAttribute('data-channel_info'));
        let num_vods_display = this?.getElementsByClassName('num_vods_display')?.[0];
        if(this.getAttribute('data-num_vods') === null){
            this.setAttribute('data-num_vods','true');
            let vods = await getKickVODs(r);            
            // console.log(r);
            if(num_vods_display) num_vods_display.innerText = `${vods?.length} ${vods?.length != 1 ? 'VODs' : 'VOD'}`;
        }
    }

    async function buildVODViewer(params){
        var {ref_elm,channel,vod}= params;
        let rect = ref_elm.getBoundingClientRect();
        let cont_params = {
            classname:'vod_viewer_cont',
            top: 1,
            left: 1,
            width: Math.floor(window.innerWidth * 0.7),
            top_z:true,
        }
        let vod_sel_elms = createBasicContainer(cont_params);
        // inlineStyler(cont_params.cont,'{left:1px;}');
        let m3u8_data = await requestFromBackground({cmd:'getVideoStreamData',params:vod})
        // console.log(m3u8_data);
        await createVideoViewerElement({
            cont_ob:vod_sel_elms,
            ref_elm:vod_sel_elms?.middle_cont,
            m3u8_data:m3u8_data,
            vod_data:vod,
            channel_info:channel,
        });

        vod_sel_elms.cont.style.width = `${window.innerWidth * 0.8}px`;
        // inlineStyler(vod_sel_elms.header,`{grid-template-columns: 32px 1fr 0px;}`)
        // let copy_m3u8 = ele('div');
        // a(copy_m3u8,[['title','copy m3u8 link to clipboard']]);
        // inlineStyler(copy_m3u8,`{cursor:pointer; transform:translate(-26px,2px); height:32px; width:32px;}`);
        // vod_sel_elms.header.appendChild(copy_m3u8)
        // copy_m3u8.innerHTML = app_icons.copy_m3u8;
        // copy_m3u8.onclick = ()=> domplate(m3u8_data?.[0].src)

        setContainerDimensionsInWindow(vod_sel_elms);

        // let vod_m3u8 = await fetchText(vod?.source)
    }

    async function changeResolutionSelection() {
        Array.from(document.getElementsByClassName('res_option')).forEach(elm=>{
            elm.className = 'res_option unchecked';
        });
        this.className = 'res_option checked';
        let video = this.parentElement.parentElement.parentElement.getElementsByTagName('video')?.[0];
        let current_time = video.currentTime;
        let m3u8_data = atobJSON(this.getAttribute('data-video_res_ob'));
        console.log(['m3u8_data',m3u8_data])
        var enc = new TextEncoder("utf-8");
        
        if(m3u8_data.is_livestream){
            var source_file = getExtendStreamSource({...m3u8_data,...{text:m3u8_data?.playlist_source}})
        }else{
            var source_file = await fetchText(m3u8_data.src);
        }
        if(hls) hls.destroy();
        hls = new Hls();
        var source_text = source_file?.replace(/\d+\.ts/g,i=> `${m3u8_data.src?.replace(/playlist.m3u8/,'')}${i}`);
        
        a(video,[['src',m3u8_data.src]]);
        hls.loadSource(URL.createObjectURL(new Blob([enc.encode(source_text)])));
        hls.attachMedia(video);
        hls.on(Hls.Events.MANIFEST_PARSED, ()=>{
            video.volume = 0.5;
            video.currentTime = current_time;
            // video.ontimeupdate = updateLiveStreamViewer;
        });
        video.ontimeupdate = (e)=> {
            updateLiveStreamViewer({video:video,current_time:video.currentTime});
        }
        // updateVideoViewerResolution()
    }

    
    async function addDownloadOptions(params){
        var {cont_ob,ref_elm,m3u8_data,vod_data,channel_info,current_res_selection} = params

        let cont = ele('div');
        ref_elm.appendChild(cont);
        inlineStyler(cont,`{display:grid; grid-template-columns: ${Array(m3u8_data.length+2).fill().map((p,i,r)=> `${i >= (r.length -2) ? 32 : 42}px `).reduce((a,b)=> a+b).trim()}; grid-gap:1px;}`);
        m3u8_data.forEach(v=> {
            let res_option = ele('div');
            a(res_option, [['class', `res_option ${(current_res_selection && new RegExp(current_res_selection,'i').test(v?.group_id) ? 'checked' : 'unchecked')}`], ['data-video_res_ob',btoaJSON(v)], ['data-res_option', v?.resolution],['title',v?.group_id]]);
            
            inlineStyler(res_option, `{margin: auto; font-size:0.6em; overflow: clip; padding:2px;}`);
            res_option.innerText = v?.name?.replace(/(?<=p).+/,'')
            cont.appendChild(res_option);
            res_option.onclick = changeResolutionSelection;
        });
        if(!Array.from(cn(cont,'res_option')).some(opt=> /\bchecked/.test(opt.className))) a(cn(cont,'res_option')[0],[['class','res_option checked']]);
        
        let dl_btn = ele('div');
        dl_btn.innerHTML = app_icons.dl_svg;
        a(dl_btn,[['title',`Download VOD`],['data-stream-btoa', btoaJSON({m3u8_data:m3u8_data,vod_data:vod_data,channel_info:channel_info})]]);
        inlineStyler(dl_btn,`{cursor:pointer;}`);
        cont.appendChild(dl_btn);
        dl_btn.onclick = ()=> downloadVideoSelection({...params,...{dl_btn:dl_btn}})


        let copy_m3u8 = ele('div');
        a(copy_m3u8,[['class','copy_m3u8_cont'],['title','copy m3u8 link to clipboard']]);
        inlineStyler(copy_m3u8,`{cursor:pointer; height:32px; width:32px;}`);
        cont.appendChild(copy_m3u8);
        copy_m3u8.innerHTML = app_icons.copy_m3u8;
        copy_m3u8.onclick = ()=> domplate(m3u8_data?.[0].src)

        /*
            TODO options variable based on authorization. premium users will be able to clip
        */
        // getTSData
    }

    async function createLivestreamViewer(params){
        var {channel_name} = params;
        // var channel_name = channel_name ? channel_name : getChanellNameFromDOM();
        let cont_params = {
            classname:'livestream_viewer_cont',
            top: 1,
            left: 1,
            width: Math.floor(window.innerWidth * 0.7),
            top_z:true,
        }
        let vod_sel_elms = createBasicContainer(cont_params);

        var livestream_data = await getLivestreamInfo(params)
        // console.log(['livestream_data',livestream_data])
        await createVideoViewerElement({
            cont_ob:vod_sel_elms,
            ref_elm:vod_sel_elms?.middle_cont,
            m3u8_data:livestream_data,
            vod_data:{channel_name:channel_name},
            channel_name:channel_name,
        });
        setContainerDimensionsInWindow(cont_params)
    }

    // async function attachLivestreamInitBtn(){
    //     for(let i=0; i<1000; i++){
    //         await delay(15);
    //         let control_bar = gi(document,'video-holder');
    //         if(control_bar){
    //             control_bar.onmouseenter =()=>{
    //                 let pause_btn = Array.from(document.getElementsByClassName('vjs-control-text'))?.filter(r=> r.innerText == "Pause")?.[0]?.parentElement;
    //                 inlineStyler(pause_btn,`{color:${colors.kick_green};}`);
    //                 pause_btn.onclick = ()=> createLivestreamViewer({channel_name: getChanellNameFromDOM()});
    //             }
    //             let pause_btn = Array.from(document.getElementsByClassName('vjs-control-text'))?.filter(r=> r.innerText == "Pause")?.[0]?.parentElement;
    //             if(pause_btn){
    //                 inlineStyler(pause_btn,`{color:${colors.kick_green};}`)
    //                 pause_btn.onclick = ()=> createLivestreamViewer({channel_name: getChanellNameFromDOM()});
    //             }                
    //         }
    //     }
    // }
    // attachLivestreamInitBtn()
    
        
    // }
    
    // function remapSourceText()
    async function createVideoViewerElement(params){
        var {cont_ob,ref_elm,m3u8_data,vod_data,channel_info} = params;
        var selected_res_index = Array.from(Array.from(document.getElementsByClassName('vjs-menu-button vjs-menu-button-popup vjs-control vjs-button')).filter(r=> /Quality/.test(r.getElementsByClassName('vjs-control-text')?.[0]?.innerText))?.[0].getElementsByClassName("vjs-menu-item")).findIndex(r=> /vjs-selected/.test(r.className)) - 1;
        
        var selected_res = cn(Array.from(document.getElementsByClassName('vjs-menu-button vjs-menu-button-popup vjs-control vjs-button')).filter(r=> /Quality/.test(r.getElementsByClassName('vjs-control-text')?.[0]?.innerText))?.[0].getElementsByClassName("vjs-menu-item")[selected_res_index+1],'vjs-menu-item-text')?.[0]?.innerText;

        addDownloadOptions({...params,...{current_res_selection:selected_res}});
        var enc = new TextEncoder("utf-8");
        hls = new Hls();

        var target_m3u8_data = selected_res_index > -1 ? m3u8_data?.[selected_res_index] : m3u8_data?.[0];
        if(target_m3u8_data?.is_livestream){
            var source_file = getExtendStreamSource({...target_m3u8_data,...{text:target_m3u8_data.playlist_source}})
        }else{
            var source_file = await fetchText(target_m3u8_data.src);
        }
        var source_text = source_file?.replace(/\d+\.ts/g,i=> `${target_m3u8_data?.src?.replace(/playlist.m3u8/,'')}${i}`)
        // console.log(['target_m3u8_data',target_m3u8_data])

        let video = ele('video');
        a(video,[['controls', ''], ['autoplay', ''], ['webkit-playsinline', ''], ['playsinline', '']]);
        ref_elm.appendChild(video);
        a(video,[['src',target_m3u8_data.src]]);
        hls.loadSource(URL.createObjectURL(new Blob([enc.encode(source_text)])));
        hls.attachMedia(video);
        hls.on(Hls.Events.MANIFEST_PARSED, ()=>{
            video.volume = 0.5;
            if(target_m3u8_data?.is_livestream) {
                let time = (((new Date().getTime() - target_m3u8_data?.start_timestamp)/1000)-10)
                // console.log(['time',time])
                video.currentTime = time;
                // video.pause();
                document.getElementsByTagName('video')[0].pause();
            }
        });
        cont_ob.cls_btn.onclick = ()=>{
            hls.destroy();
            cont_ob.cont.remove();
            // let vid_play = Array.from(document.getElementsByClassName('vjs-control-text'))?.filter(r=> r.innerText == "Play")?.[0]?.parentElement;
            // if(vid_play) vid_play.click();
        }
        video.ontimeupdate = (e)=> {
            updateLiveStreamViewer({video:video,current_time:video.currentTime});
        }
        // if(target_m3u8_data?.is_livestream) {
        //     let time = (((new Date().getTime() - target_m3u8_data?.start_timestamp)/1000)-10)
        //     console.log(['time',time])
        //     video.currentTime = time;
        // }
        
        return true;
    }

    async function updateLiveStreamViewer(params) {
        let {video,current_time} = params;

        let target_m3u8_data = atobJSON(cn(video.parentElement,'res_option checked')?.[0].getAttribute('data-video_res_ob'));
        if (video && !isNaN(video.duration) && ((current_time + 2) >= video.duration)){
            var source_file = getExtendStreamSource({...target_m3u8_data,...{text:target_m3u8_data.playlist_source}})
            var source_text = source_file?.replace(/\d+\.ts/g,i=> `${target_m3u8_data?.src?.replace(/playlist.m3u8/,'')}${i}`)
            var enc = new TextEncoder("utf-8");
            hls.loadSource(URL.createObjectURL(new Blob([enc.encode(source_text)])));
            // video.volume = volume;
            hls.on(Hls.Events.MANIFEST_PARSED, ()=>{
                video.currentTime = current_time;
            });
            
        }
        // if (twitch_vid_elm && video && !twitch_vid_elm?.paused && !video.paused) twitch_vid_elm.pause()
    }
    
// function changeSource(source) {
//     let vid = this.getVideoEl();
//     if (this.hls) { this.hls.destroy(); }
//     this.hls = new Hls();
//     this.hls.loadSource(source);
//     this.hls.attachMedia(vid);
//     this.hls.on(Hls.Events.MANIFEST_PARSED, () => { });
// }


    function createBasicContainer(params){
        if(gi(document,params?.id)) gi(document,params?.id).remove();
        const main_width = (window.innerWidth * 0.7);

        let cont = ele('div');
        a(cont,[['class',`menu_container${params?.classname ? ' '+params?.classname : ''}`],['id',params?.id]].filter(r=> r[1]));
        inlineStyler(cont, params.css || `{position: fixed; top: ${params?.top+'px' || '5px'}; width: ${params?.width+'px' || main_width}px; left: ${params?.left+'px' || '5px'}; z-index: ${topZIndexer()}; background: #191b1f; border-radius: 0.2em; border: 2px solid ${colors.kick_green}; box-shadow:0px 2px 2px 4px rgba(0,0,0,0.6);}`);
        if(params.parent_elm){
            if(params.attach_method) params.parent_elm[params.attach_method](cont,(params.ref_elm))
            else params.parent_elm.appendChild(cont)            
        }else{
            document.body.appendChild(cont);
        }
        // if(params?.top_z) cont.onclick = topIndexHover;
        cont.onclick = topIndexHover;

            let header = ele('div');
            cont.appendChild(header);
            inlineStyler(header, `{height: ${(params?.header_height || 32)}px; display: grid; grid-template-columns: 32px 1fr; grid-gap:0px; user-select: none;}`); //background: linear-gradient(to top right, transparent, transparent, transparent, ${colors.kick_green}); 

                let cls_btn = document.createElement('div');
                cls_btn.innerHTML = app_icons.close;
                header.appendChild(cls_btn);
                inlineStyler(cls_btn, `{cursor: pointer;}`);
                cls_btn.onclick = ()=>{ cont.remove(); };

                let head_mover = document.createElement('div');
                a(head_mover,[['class','drag_actor_head']]);
                inlineStyler(head_mover, `{text-align: left;}`);
                header.appendChild(head_mover);
                head_mover.onmouseover = dragElement;

            let tbody = ele('div');
            cont.appendChild(tbody);
            a(tbody,[['class','menu_body']])
            inlineStyler(tbody, `{display: grid; grid-template-columns: 32px 1fr 32px;}`);

                let left_side = ele('div');
                tbody.appendChild(left_side);
                a(left_side,[['class','drag_actor_left']]);
                left_side.onmouseover = dragElement;
                inlineStyler(left_side, `{height:100%;}`);

                let middle_cont = ele('div');
                inlineStyler(middle_cont,`{overflow: auto;}`);
                a(middle_cont,[['class','middle_cont'],['data-css',btoaJSON({height:-(26+((params?.header_height || 32))),width:-64})]]);
                tbody.appendChild(middle_cont);

                let right_side = ele('div');
                tbody.appendChild(right_side);
                a(right_side,[['class','drag_actor_right']]);
                right_side.onmouseover = dragElement;
                inlineStyler(right_side, `{height:100%;}`);

            let footer = ele('div');
            cont.appendChild(footer);
            inlineStyler(footer,`{height: 22px; display:grid; grid-template-columns: 1fr 22px; user-select: none;}`);// background: linear-gradient(to bottom left, transparent, transparent, transparent, ${colors.kick_green});

                let foot_label = ele('div');
                footer.appendChild(foot_label);
                // inlineStyler(foot_label, `{cursor:grab;}`);
                a(foot_label,[['class','drag_actor_foot']]);
                foot_label.onmouseover = dragElement;

                let foot_resizer = ele('div');
                a(foot_resizer,[['data-resize-classes','menu_container,middle_cont']]);
                inlineStyler(foot_resizer,`{width:22px; height: 22px; cursor:nw-resize;}`)
                footer.appendChild(foot_resizer);
                foot_resizer.innerHTML = app_icons.resize;
                foot_resizer.onmouseover = adjustElementSize;

        return {
            cont:cont,
            header:header,
            cls_btn:cls_btn,
            head_mover:head_mover,
            tbody:tbody,
            left_side:left_side,
            middle_cont:middle_cont,
            right_side:right_side,
            footer:footer,
            foot_label:foot_label,
            foot_resizer:foot_resizer,
        }
    }


    const page_change_monitor_object = {
        id: 'page_change_monitor',
        fn: initUI,
        page_context: {
            href: window.location.href
        }
    };
    
    function monitorURLChanges(){
        const url = window.location.href;
        if(page_change_monitor_object?.page_context?.href != url) {
            if(gi(document,'record_livestream_cont')){
                if(user_is_recording == true){
                    downloadCurrentRecording({channel_slug:/(?<=kick.com\/)[\w-]+$/.exec(page_change_monitor_object?.page_context?.href)?.[0]});
                    user_is_recording = !user_is_recording;
                }else{
                    gi(document,'record_livestream_cont').remove();
                    current_stream_ts_extracted_data = [];
                    user_is_recording = false;
                }
            }
            page_change_monitor_object.fn();
            // console.log('has changed',url);

            page_change_monitor_object['page_context']['href'] = url;
        }
    }
    document.body.onmousemove = monitorURLChanges;

    // 
    
}


initUI()

