
const cn = (o,s)=>o ? o.getElementsByClassName(s) : null;
const tn = (o,s)=>o ? o.getElementsByTagName(s) : null;
const gi = (o,s)=>o ? o.getElementById(s) : null;
const ele = (t)=>document.createElement(t);
const attr = (o,k,v)=>o ? o.setAttribute(k, v) : false;
const a = (l,r)=>r.forEach(i=>attr(l, i[0], i[1]));

function inlineStyler(elm, css) {
    if (elm) {
        Object.entries(JSON.parse(css.replace(/(?<=:)\s*(\b|\B)(?=.+?;)/g, '"').replace(/(?<=:\s*.+?);/g, '",').replace(/[a-zA-Z-]+(?=:)/g, k=>k.replace(/^\b/, '"').replace(/\b$/, '"')).replace(/\s*,\s*}/g, '}'))).forEach(kv=>{
            elm.style.setProperty([kv[0]], kv[1], 'important')
        }
        );
    }
}
function topZIndexer() {
    let n = new Date().getTime() / 1000000;
    let r = (n - Math.floor(n)) * 100000;
    return (Math.ceil(n + r) * 10);
}

function topIndexHover() {
    this.style.zIndex = topZIndexer();
}

function setHTMLCSS(style_id, css_text) {
    if (document.getElementById(`${style_id}_style`))
        document.getElementById(`${style_id}_style`).outerHTML = '';
    let csselm = ele('style');
    a(csselm, [['class', `${style_id}_style`]]);
    document.head.appendChild(csselm);
    csselm.innerHTML = css_text;
}

function dragElement() {
    var acted_elm = this;
    var el = this.parentElement.parentElement;
    // inlineStyler(acted_elm,`{background: linear-gradient(to bottom left, transparent, transparent, transparent, ${colors.kick_green});}`);
    // el.style.zIndex = topZIndexer();
    var pos1 = 0
      , pos2 = 0
      , pos3 = 0
      , pos4 = 0;
    if (document.getElementById(this.id))
        document.getElementById(this.id).onmousedown = dragMouseDown;
    else
        this.onmousedown = dragMouseDown;
    function dragMouseDown(e) {
        // inlineStyler(acted_elm,`{background-color:transparent;}`)
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        document.onmousemove = elementDrag;
    }
    function elementDrag(e) {
        // inlineStyler(acted_elm,`{background-color:transparent;}`)
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        el.style.top = (el.offsetTop - pos2) + "px";
        el.style.left = (el.offsetLeft - pos1) + "px";
        el.style.opacity = "0.85";
        // el.style.transition = "opacity 700ms";
        el.style.zIndex = topZIndexer();
    }
    function closeDragElement() {
        document.onmouseup = null;
        document.onmousemove = null;
        el.style.opacity = "1";
        el.style.zIndex = topZIndexer();
        // inlineStyler(acted_elm,`{background-color:transparent;}`)
    }
}

function adjustElementSize(){
    var cont = this.parentElement.parentElement;
    // var tbod = cn(cont,'menu_body')?.[0]
    let resize_elm_classes = this.getAttribute('data-resize-classes').split(/,/);
    // var cont = document.getElementsByClassName(resize_elm_classes[0])?.[0];
    var tbod = cont.getElementsByClassName(resize_elm_classes[1])?.[0];
    let tbod_css = atobJSON(tbod.getAttribute('data-css'))
    // let header_pxs = cbod?.firstChild.style.gridTemplateColumns.split(/\s/).map(r=> /[\d\.]+/.exec(r)?.[0]).filter(r=> r).map(r=> parseFloat(r));
    // let min_width = header_pxs?.length ? header_pxs.reduce((a,b)=> a+b)+60 : 120;
    let min_width = 100;
    let min_height = 98;
    var foot_height = 0;
    var pos1 = 0,    pos2 = 0,    pos3 = 0,    pos4 = 0;
    var width = cont.getBoundingClientRect().width;
    var height = cont.getBoundingClientRect().height;

    if (document.getElementById(this.id)) document.getElementById(this.id).onmousedown = dragMouseDown;
    else this.onmousedown = dragMouseDown;
    function dragMouseDown(e) {
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        document.onmousemove = elementDrag;
    }
    function elementDrag(e) {
        let moved_width = (width - (pos3 - e.clientX))
        let moved_height = (height - (pos4 - e.clientY))
        let main_width = moved_width < min_width ? min_width : moved_width;
        let main_height = moved_height < min_height ? min_height : Math.floor(((height - (pos4 - e.clientY)) - (foot_height)));
        
        inlineStyler(cont,`{width: ${main_width}px;${ moved_width < min_width ? '' : ' height: '+main_height+'px; '}z-index: ${topZIndexer()};}`);
        if(tbod){
            inlineStyler(tbod,`{width: ${((main_width+tbod_css.width))}px;${ moved_width < min_width ? '' : ' height: '+(main_height+tbod_css.height)+'px;'}opacity: 0.5; }`);
        }
    }
    function closeDragElement() {
        document.onmouseup = null;
        document.onmousemove = null;
        if(tbod) tbod.style.opacity = '1';
    }
}
function setContainerDimensionsInWindow(elm){
    // console.log(['setContainerDimensionsInWindow',elm])
    var {cont,middle_cont} = elm;
    let wh = window.innerHeight;
    let ww = window.innerWidth;
    let main_rect = cont.getBoundingClientRect();
    let middle_rect = middle_cont.getBoundingClientRect();
    let middle_css = atobJSON(middle_cont.getAttribute('data-css'))
    if(main_rect.bottom > wh || middle_rect.bottom > wh){
        // console.log('main_rect.height')
        let c_height = (main_rect.height > 200 && main_rect.height < 500 ? main_rect.height : wh);
        let m_height = (main_rect.height > 200 && main_rect.height < 500 ? (main_rect.height+middle_css.height) : ((wh+middle_css.height)));
        inlineStyler(cont,`{top:${wh-c_height}px; height:${c_height}px;}`);
        inlineStyler(middle_cont,`{height:${m_height}px;}`);
    }
    if(main_rect.right >= (ww-10) || middle_rect.right > (ww-10)){
        // console.log('main_rect.width')
        let c_width = (main_rect.width > 300 && main_rect.width < 600 ? main_rect.width : ww);
        let m_width = (main_rect.width > 300 && main_rect.height < 600 ? (main_rect.width+middle_css.width) : ((ww+middle_css.width)));

        inlineStyler(cont,`{left:0px; width:${c_width}px;}`);
        inlineStyler(middle_cont,`{width:${m_width}px;}`);
    }

}




function createRecordingStatusBar(params){
    var {parent_elm,ref_elm,attach_method,stored_seconds,stream_seconds,stream_rec_start} = params;
    
    if(!gi(document,'recording_information_cont')){
        var parent_rect = parent_elm.getBoundingClientRect();
        var pixel_to_sec_ratio = (900/parent_rect.width);
    // var stream_rec_start = stream_seconds
        var max = parent_rect.width;
        var completed = ((stored_seconds/pixel_to_sec_ratio)/parent_rect.width) * max;
        var diff = (((stream_seconds-stream_rec_start)/pixel_to_sec_ratio)/parent_rect.width) * max;

        let cont = ele('div');
        a(cont,[['id','recording_information_cont']])
        parent_elm[attach_method](cont,ref_elm);
        inlineStyler(cont,`{display:grid; grid-template-columns:${completed}px ${diff}px ${(max- (completed+diff))}px; grid-gap:0px; width:${max}px; height:15px;}`);
            let stored_prog = ele('div');
            a(stored_prog,[['class','stored_prog']]);
            cont.appendChild(stored_prog);
            inlineStyler(stored_prog,`{height:10px; transform:translate(0px, 20px); width:${completed}px; background-color:${colors.kick_green};color:${colors.kick_green};}`);
                let stored_time = ele('div');
                a(stored_time,[['class','orbitron stored_time_text']]);
                inlineStyler(stored_time,`{transform:translate(0px,-18px);}`)
                stored_prog.appendChild(stored_time);
                stored_time.innerText = parseTimeOffset(stored_seconds).replace(/00\w/g,'');

            let stream_diff = ele('div');
            a(stream_diff,[['class','stream_diff']]);
            cont.appendChild(stream_diff);
            inlineStyler(stream_diff,`{height:10px; transform:translate(0px,20px); width:${diff}px; background-color:${colors.red};}`);
    }
}

function updateRecordingStatusBar(params){
    var {parent_elm,ref_elm,attach_method,stored_seconds,stream_seconds,stream_rec_start} = params;
    if(gi(document,'recording_information_cont')){
        var {stored_prog,stream_diff,stored_time_text} = ['stored_prog','stream_diff','stored_time_text'].map(r=> ({[r]:gi(document,'recording_information_cont').getElementsByClassName(r)?.[0]})).reduce((a,b)=> {return {...a,...b}});
        
        var parent_rect = document.getElementsByClassName('record_adjustments')[0].getBoundingClientRect();
        var pixel_to_sec_ratio = (900/parent_rect.width);
        var max = parent_rect.width;
        var completed = ((stored_seconds/pixel_to_sec_ratio)/parent_rect.width) * max;
        var diff = (
            (
                (
                    (
                        stream_seconds-(stream_rec_start+stored_seconds)
                    )
                ) / pixel_to_sec_ratio
            ) / parent_rect.width
        ) * max;

        inlineStyler(gi(document,'recording_information_cont'),`{grid-template-columns:${completed}px ${diff}px ${(max- (completed+diff))}px; grid-gap:0px; width:${max}px;}`);

        inlineStyler(stored_prog,`{width:${completed}px;}`);
        inlineStyler(stream_diff,`{width:${diff}px;}`);
        stored_time_text.innerText = parseTimeOffset(stored_seconds).replace(/00[hm]/g,'');
    }
}
// function createRecordingStatusBar(params){
//     var {parent_elm,ref_elm,attach_method,stored_seconds,stream_seconds} = params;

//     var parent_rect = parent_elm.getBoundingClientRect();


//     let cont = ele('div');
//     parent_elm[attach_method](cont,ref_elm);
//     inlineStyler(cont,`{display:grid; grid-template-columns:${}; grid-gap:0px;}`)
// }