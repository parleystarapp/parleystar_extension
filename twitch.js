/* SPDX-FileCopyrightText: © 2024 promising future digital media llc. All rights reserved. <admin@parleystar.com> */
/* SPDX-License-Identifier: Mozilla Public License 1.1 (MPL-1.1) */
console.log('loading twitch', scbt);

scbt.f.chat_clean_twitch = function(obj, elem){
  var elemArr = [];
  var str = null;

  var elemArr = elem.getElementsByClassName('chat-line__timestamp');
  if (elemArr[0]) {
    var timestamp = elemArr[0].textContent;
    var timestr = new Date().toLocaleString('en-US', { hour: 'numeric', hour12: true });
    var timeArr = timestr.split(' ');
    if (timeArr[1]) {
      timestamp = timestamp + timeArr[1];
    }
  } else {
     var stampArr = new Date().toLocaleTimeString().replace(/ /g, '').split(':'); // = 11:34:03AM  ['12', '11', '42PM']
     var timestamp = stampArr[0] + ':' + stampArr[1] + stampArr[2].slice(2);
  }
  obj.timestamp = timestamp;

  var classArr = elem.classList;
  if (classArr.contains('user-notice-line') || classArr.contains('donation') || classArr.contains('anevent') ) {
    obj.anevent = 1;
  }
  if (classArr.contains('sub') ) {
    obj.sub = 1;
  }
  if (classArr.contains('moderator') ) {
    obj.moderator = 1;
  }
  if (classArr.contains('founder') ) {
    obj.founder = 1;
  }
  if ( classArr.contains('vip') || classArr.contains('verified') ) {
    obj.verified = 1;
  }
  if (classArr.contains('owner') ) {
    obj.owner = 1;
  }
  if (classArr.contains('staff') ) {
    obj.staff = 1;
  }
  if (classArr.contains('gifter') ) {
    obj.gifter = 1;
  }

  var imgElemArr = elem.getElementsByTagName('img');
  [].forEach.call(imgElemArr, function(imgElm) {
    var a = imgElm.getAttribute('alt').toLowerCase();
    if ( (a.indexOf('subscriber') > -1) || (a.indexOf('founder') > -1) ) {
      obj.sub = 1;
    }
    if (a.indexOf('moderator') > -1) {
      obj.moderator = 1;
    }
    if (a.indexOf('broadcaster') > -1) {
      obj.owner = 1;
    }
    if ( (a.indexOf('cheer') > -1) && (!imgElm.classList.contains('chat-badge')) ) {
      obj.donation = 1;
      obj.anevent = 1;
    }
    if ( (a.indexOf('vip') > -1) || (a.indexOf('verified') > -1) ) {
      obj.verified = 1;
    }
    if (a.indexOf('founder') > -1) {
      obj.founder = 1;
    }
    if (a.indexOf('sub gifter') > -1) {
      obj.gifter = 1;
    }
  });

  obj = scbt.f.chat_get_username(obj, elem);
  obj = scbt.f.get_obj_cleaned_username_from_obj(obj);

  obj = scbt.f.chat_get_message(obj, elem);
  obj = scbt.f.get_obj_cleaned_message_from_obj(obj);

  if ( (obj.timestamp) && (obj.username) && (obj.message) ) {
    var str = obj.message.substring(0,33);
    str = str.replace(/[^a-zA-Z0-9_\-@\s]/g,'');
    obj.itemid = obj.username + str;
  }

  special = stampArr = str = elemArr = elemArr2 = elemArr3 = timestamp = imgs = alt = imgElm = imgElemArr = itemId = a = elem = null; return obj;
}


scbt.f.chat_make_decisions_twitch = function(obj, elem, settings){
  
  if (settings.sz1b1twitch === true) {
    var elemArr = elem.getElementsByTagName('img');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    var elemArr = elem.getElementsByTagName('i');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    var elemArr = elem.getElementsByTagName('svg');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    var elemArr = elem.getElementsByTagName('canvas');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    elemArr = elm2 = null;
  }

  // Highlight sub messages in chat in this hex colour h1
  if (settings.sz2c1twitch != '#000000') {
    if (obj.sub === 1) {
      obj.isHighlighted = settings.sz2c1twitch;
    }
  }
  // Highlight gifter messages in chat in this hex colour h2
  if (settings.sz2c9twitch != '#000000') {
    if (obj.gifter === 1) {
      obj.isHighlighted = settings.sz2c9twitch;
    }
  }
  // Highlight VIP messages in chat in this hex colour h3 
  if (settings.sz2c5twitch != '#000000') {
    if (elem.classList.contains('vip')) {
      obj.isHighlighted = settings.sz2c5twitch;
    }
  }
  // Highlight founder messages in chat in this hex colour  
  if (settings.sz2c6twitch != '#000000') {
    if (obj.founder === 1) {
      obj.isHighlighted = settings.sz2c6twitch;
    }
  }
  // Highlight OG messages in chat in this hex colour  
  if (settings.sz2c7twitch != '#000000') {
    if (obj.og === 1) {
      obj.isHighlighted = settings.sz2c7twitch;
    }
  }
  // Highlight owner messages in chat in this hex colour  
  if (settings.sz2c8twitch != '#000000') {
    if (obj.owner === 1) {
      obj.isHighlighted = settings.sz2c8twitch;
    }
  }
  // Highlight moderator messages in chat in this hex colour  
  if (settings.sz2c2twitch != '#000000') {
      if (obj.moderator === 1) {
        obj.isHighlighted = settings.sz2c2twitch;
      }
  }
  // Mute owner/streamer messages in chat in dim text  
  if (settings.sz3b2 === true) {
      if (obj.owner === 1) {
        obj.isMuted = 1;
      }
  }
  // Hide owner/streamer messages in chat  
  if (settings.sz4b2 === true) {
      if (obj.owner === 1) {
        obj.isHidden = 1;
      }
  }
  if ( (obj.owner === 1) || (obj.moderator === 1) || (obj.sub === 1) || (obj.newSub === 1) || (obj.verified === 1) ) { } else {
    // scbtmuted4 Mute non moderator/sub messages in chat  
    if (settings.sz3b1 === true) {
      obj.isMuted = 1;
    }
    // scbthidden4 Hide non moderator/sub messages in chat 
    if (settings.sz4b1 === true) {
      obj.isHidden = 1;
    }
  }
  // scbtmuted3 Mute bot messages  
  if (settings.sz3b4 === true) {
    if (obj.username && obj.message) {
      if (obj.isBot === 1) {
        obj.isMuted = 1;
      }
    }
  }
  // scbthidden3 Hide bot  messages 
  if (settings.sz4b4 === true) {
    if (obj.username && obj.message) {
      if (obj.isBot === 1) {
        obj.isHidden = 1;
      }
    }
  }
  // Highlight mention messages in chat in this hex colour 
  if (settings.sz2c3twitch != '#000000') {
    if (obj.mention === 1) {
      obj.isHighlighted = settings.sz2c3twitch;
    }
  }
  // Highlight hashtag messages in chat in this hex colour  
  if (settings.sz2c4twitch != '#000000') {
    if (obj.hashtag === 1) {
      obj.isHighlighted = settings.sz2c4twitch;
    }
  }
  // Mute @ mention messages in chat in dim text  
  if (settings.sz3b3 === true) {
    if (obj.mention === 1) {
      obj.isMuted = 1;
    }
  }
  // Hide @ mention messages in chat 
  if (settings.sz4b3 === true) {
    if (obj.mention === 1) {
      obj.isHidden = 1;
    }
  }
  elem = settings = null;
  return obj;
}


scbt.f.style_with_obj_of_changes_twitch = function(obj){
  var css = '';
  console.log('style_with_obj_of_changes_twitch obj', obj);

  // Background of chat in this hex colour.
  if (obj.sz1c1twitch) {
    if (scbt.b.vod === true) {
      if (obj.sz1c1twitch == '#000000' || obj.sz1c1twitch == '') {
        css = css + ' body.scbt-twitch .video-chat { background-color: asdf !important; } ';
      }
      if (obj.sz1c1twitch == '#ffffff') {
        css = css + ' body.scbt-twitch .video-chat { background-color: transparent !important; } ';
      }
      if ( (obj.sz1c1twitch != '#ffffff') && (obj.sz1c1twitch != '#000000') && (obj.sz1c1twitch != '') ) {
        css = css + ' body.scbt-twitch .video-chat { background-color: ' + obj.sz1c1twitch + ' !important; } ';
      }
    } else {
      if (obj.sz1c1twitch == '#000000' || obj.sz1c1twitch == '') {
        css = css + ' body.scbt-twitch .chat-scrollable-area__message-container { background-color: asdf !important; } ';
      }
      if (obj.sz1c1twitch == '#ffffff') {
        css = css + ' body.scbt-twitch .chat-scrollable-area__message-container { background-color: transparent !important; } ';
      }
      if ( (obj.sz1c1twitch != '#ffffff') && (obj.sz1c1twitch != '#000000') && (obj.sz1c1twitch != '') ) {
        css = css + ' body.scbt-twitch .chat-scrollable-area__message-container { background-color: ' + obj.sz1c1twitch + ' !important; } ';
      }
    }
  }

  // User names in chat in this hex colour
  if (obj.sz1c2twitch) {
    if (obj.sz1c2twitch == '#000000' || obj.sz1c2twitch == '') {
      css = css + ' body.scbt-twitch .chat-author__display-name { color: asdf !important; text-shadow: 0px; } ';
    }
    if (obj.sz1c2twitch == '#ffffff') {
      css = css + ' body.scbt-twitch .chat-author__display-name { color: transparent !important; text-shadow: 0.3px 0.3px dimgray; } ';
    }
    if ( (obj.sz1c2twitch != '#ffffff') && (obj.sz1c2twitch != '#000000') && (obj.sz1c2twitch != '') ) {
      css = css + ' body.scbt-twitch .chat-author__display-name { color:' + obj.sz1c2twitch + ' !important; text-shadow: 0.5px 0.5px darkslategrey; } ';
    }
  }

  // Highlights of chat in this colour
  if (obj.sz1c3twitch) {
    if (obj.sz1c3twitch != '#000000') {
      scbt.c.scbtBorderColor = obj.sz1c3twitch;
    } else {
      scbt.c.scbtBorderColor = '#ff0000';
    }
  }

  // Chat messages in this colour
  if (obj.sz1c4twitch) {
    // body.scbt-twitch .user-notice-line, body.scbt-twitch .chat-line__message--cheer-amount
    if (obj.sz1c4twitch == '#000000' || obj.sz1c4twitch == '') {
      css = css + ' body.scbt-twitch .text-fragment, body.scbt-twitch .mention-fragment, body.scbt-twitch .chat-line__message-container p, body.scbt-twitch .user-notice-line, body.scbt-twitch .chat-line__message--cheer-amount { color: asdf !important; text-shadow: 0px; } ';
    }
    if (obj.sz1c4twitch == '#ffffff') {
      css = css + ' body.scbt-twitch .text-fragment, body.scbt-twitch .mention-fragment, body.scbt-twitch .chat-line__message-container p, body.scbt-twitch .user-notice-line, body.scbt-twitch .chat-line__message--cheer-amount { color: transparent !important; text-shadow: 0.3px 0.3px dimgray; } ';
    }  
    if ( (obj.sz1c4twitch != '#ffffff') && (obj.sz1c4twitch != '#000000') && (obj.sz1c4twitch != '') ) {
      css = css + ' body.scbt-twitch .text-fragment, body.scbt-twitch .mention-fragment, body.scbt-twitch .chat-line__message-container p, body.scbt-twitch .user-notice-line, body.scbt-twitch .chat-line__message--cheer-amount { color: ' + obj.sz1c4twitch + ' !important; text-shadow: 0.5px 0.5px darkslategrey; } ';
    }
  }

  // Chat font sizess
  if (obj.sz1n1twitch) {
    if ( Number(obj.sz1n1twitch) > 0) {
      var str = Number(obj.sz1n1twitch) + 'rem';
      css = css + ' body.scbt-twitch .text-fragment, body.scbt-twitch .mention-fragment, body.scbt-twitch .chat-line__message-container p, body.scbt-twitch .user-notice-line, body.scbt-twitch .chat-line__message--cheer-amount { font-size: ' + str + '; line-height: ' + Number(obj.sz1n1twitch)  + '; } ';
    } else {
      css = css + ' body.scbt-twitch .text-fragment, body.scbt-twitch .mention-fragment, body.scbt-twitch .chat-line__message-container p, body.scbt-twitch .user-notice-line, body.scbt-twitch .chat-line__message--cheer-amount { font-size: initial; line-height: unset; } ';
    }
  }

  // text only chat
  if (obj.sz1b1twitch === true) {
    css = css + ' body.scbt-twitch .text-fragment img, body.scbt-twitch .text-fragment svg, body.scbt-twitch .text-fragment i, body.scbt-twitch .mention-fragment img, body.scbt-twitch .mention-fragment svg, body.scbt-twitch .mention-fragment i, body.scbt-twitch .chat-line__message-container img, body.scbt-twitch .chat-line__message-container svg, body.scbt-twitch .pinned-chat__pinned-by, body.scbt-twitch .chat-image__container { visibility: hidden !important; } ';
  }
  if (obj.sz1b1twitch === false) {
    css = css + ' body.scbt-twitch .text-fragment img, body.scbt-twitch .text-fragment svg, body.scbt-twitch .text-fragment i, body.scbt-twitch .mention-fragment img, body.scbt-twitch .mention-fragment svg, body.scbt-twitch .mention-fragment i, body.scbt-twitch .chat-line__message-container img, body.scbt-twitch .chat-line__message-container svg, body.scbt-twitch .pinned-chat__pinned-by, body.scbt-twitch .chat-image__container { visibility: visible !important; } ';      
  }

  // Left handed screen. Flip screen so video is on the right and chat is on the left.
  if (obj.sz5b16 === true) {
    css = css + ' body.scbt-twitch #placeholder { flex-direction: row-reverse; } ';
  }
  if (obj.sz5b16 === false) {
    css = css + ' body.scbt-twitch #placeholder { flex-direction: row; } ';
  }

  // Upside down chat screen. Newest chat is on the top, oldest chat is on the bottom 
  if (obj.sz5b17 === true) {
     css = css + ' body.scbt-twitch #placeholder { display: flex; flex-direction: column-reverse; } ';
  }
  if (obj.sz5b17 === false) {
    css = css + ' body.scbt-twitch #placeholder { display: block; flex-direction: column; } ';
  }

  // mouseover hover enlarge
  if (obj.sz5b18 === true) {
    css = css + ' body.scbt-twitch #placeholder:hover { font-size: 166% !important; } ';
  }
  if (obj.sz5b18 === false) {
    css = css + ' body.scbt-twitch #placeholder:hover { font-size: initial !important; } ';
  }

  if (css) {
    var head = document.getElementsByTagName('head')[0];
    var style = document.createElement('style');
    head.appendChild(style);
    style.type = 'text/css';
    if (style.styleSheet){
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }
  }
  obj = t = head = style = css = null; return false;
}


  scbt.f.toggle_chats_twitch = function(parameter){
      if (scbt.b.mobile === true) {
        var elemArr = document.body.querySelectorAll('main div div ul li, .user-notice-line');
        var toSearchFor = '.chat-badge';
      }
      if (scbt.b.mobile != true) {
        var elem2Arr = document.body.getElementsByClassName('va-vod-chat');
        if (elem2Arr[0]) {
          var elemArr = document.body.querySelectorAll('.video-chat__message-list-wrapper li .vod-message');
        } else {
          var elemArr = document.body.querySelectorAll('.chat-line__message, .user-notice-line');
        }
        toSearchFor = '.chat-badge';
      }

      if (scbt.n.visibilityChatShow === 1) {
        scbt.e.scbtChatB.textContent = scbt.f.set_chat_parameter(parameter, scbt.n.visibilityChatShow);
        [].forEach.call(elemArr, function(elem) {
          elem.style.opacity = 0;

          if (parameter == 'owner') {
            var imgElemArr = elem.getElementsByTagName('img');
            [].forEach.call(imgElemArr, function(imgElem) {
              var alt = imgElem.getAttribute('alt');
              if (alt) {
                var a = alt.toLowerCase();
                if ( (a.indexOf('broadcaster') > -1) || (a.indexOf('owner') > -1) ) {
                  scbt.f.chat_on(elem);
                  elem.style.opacity = 1;
                }
              }
            });
          }
          if (parameter == 'sub') {
            var imgElemArr = elem.getElementsByTagName('img');
            [].forEach.call(imgElemArr, function(imgElem) {
              var alt = imgElem.getAttribute('alt');
              if (alt) {
                var a = alt.toLowerCase();
                if ( (a.indexOf('subscriber') > -1) || (a.indexOf('founder') > -1) || (a.indexOf('gifter') > -1) ) {
                  scbt.f.chat_on(elem);
                  elem.style.opacity = 1;
                }
              }
            });
          }
          if (parameter == 'moderator') {
            var imgElemArr = elem.getElementsByTagName('img');
            [].forEach.call(imgElemArr, function(imgElem) {
              var alt = imgElem.getAttribute('alt');
              if (alt) {
                var a = alt.toLowerCase();
                if (a.indexOf('moderator') > -1) {
                  scbt.f.chat_on(elem);
                  elem.style.opacity = 1;
                }
              }
            });
          }
          if (parameter == 'mod_sub') {
            var imgElemArr = elem.getElementsByTagName('img');
            [].forEach.call(imgElemArr, function(imgElem) {
              var alt = imgElem.getAttribute('alt');
              if (alt) {
                var a = alt.toLowerCase();
                if ( (a.indexOf('moderator') > -1) || (a.indexOf('subscriber') > -1) || (a.indexOf('founder') > -1) || (a.indexOf('gifter') > -1)  ) {
                  scbt.f.chat_on(elem);
                  elem.style.opacity = 1;
                }
              }
            });
          }
          if ( (parameter == 'vip') || (parameter == 'verified') ) {
            if (elem.classList.contains('vip') ) {
              scbt_chat_on(elem);
              elem.style.opacity = 1;
            }
            var imgElemArr = elem.getElementsByTagName('img');
            [].forEach.call(imgElemArr, function(imgElem) {
              var alt = imgElem.getAttribute('alt');
              if (alt) {
                var a = alt.toLowerCase();
                if ( (a.indexOf('vip') > -1) || (a.indexOf('verified') > -1) ) {
                  scbt.f.chat_on(elem);
                  elem.style.opacity = 1;
                }
              }
            });
          }
          if (parameter == 'mention') {
              var str = elem.textContent;
              if (str) {
                if ( str.indexOf('@') > -1 ) {
                  scbt.f.chat_on(elem);
                  elem.style.opacity = 1;
                }
              }
          }
          if (parameter == 'hashtag') {
              var str = elem.textContent;
              if (str) {
                if ( str.indexOf('#') > -1 ) {
                  scbt.f.chat_on(elem);
                  elem.style.opacity = 1;
                }
              }
            }
          if (parameter == 'donation') {
            var elem3Arr = elem.getElementsByClassName('chat-line__message--cheer-amount');
            if (elem3Arr[0]) {
              scbt.f.chat_on(elem);
              elem.style.opacity = 1;
            }
          }

      });
      scbt.n.visibilityChatShow = 2;
      parameter = str = a = alt = elemArr = elem = elem2Arr = elem2 = toSearchFor = arr = null; return false;
    }


      if (scbt.n.visibilityChatShow === 2) {
        scbt.e.scbtChatB.textContent = scbt.f.set_chat_parameter(parameter, scbt.n.visibilityChatShow);
        [].forEach.call(elemArr, function(elem) {
            elem.style.opacity = 1;
            scbt.f.chat_blur(elem);
            
            if (parameter == 'owner') {
              var imgElemArr = elem.getElementsByTagName('img');
              [].forEach.call(imgElemArr, function(imgElem) {
                var alt = imgElem.getAttribute('alt');
                if (alt) {
                  var a = alt.toLowerCase();
                  if ( (a.indexOf('broadcaster') > -1) || (a.indexOf('owner') > -1) ) {
                    elem.style.opacity = 0;
                  }
                }
              });
            }
            if (parameter == 'sub') {
              var imgElemArr = elem.getElementsByTagName('img');
              [].forEach.call(imgElemArr, function(imgElem) {
                var alt = imgElem.getAttribute('alt');
                if (alt) {
                  var a = alt.toLowerCase();
                  if ( (a.indexOf('subscriber') > -1) || (a.indexOf('founder') > -1) || (a.indexOf('gifter') > -1) ) {
                    elem.style.opacity = 0;
                  }
                }
              });
            }
            if (parameter == 'moderator') {
              var imgElemArr = elem.getElementsByTagName('img');
              [].forEach.call(imgElemArr, function(imgElem) {
                var alt = imgElem.getAttribute('alt');
                if (alt) {
                  var a = alt.toLowerCase();
                  if (a.indexOf('moderator') > -1) {
                    elem.style.opacity = 0;
                  }
                }
              });
            }
            if (parameter == 'mod_sub') {
              var imgElemArr = elem.getElementsByTagName('img');
              [].forEach.call(imgElemArr, function(imgElem) {
                var alt = imgElem.getAttribute('alt');
                if (alt) {
                  var a = alt.toLowerCase();
                  if ( (a.indexOf('moderator') > -1) || (a.indexOf('subscriber') > -1) || (a.indexOf('founder') > -1) || (a.indexOf('gifter') > -1)  ) {
                    elem.style.opacity = 0;
                  }
                }
              });
            }
             if ( (parameter == 'vip') || (parameter == 'verified') ) {
              if (elem.classList.contains('vip') ) {
                elem.style.opacity = 0;
              }
              var imgElemArr = elem.getElementsByTagName('img');
              [].forEach.call(imgElemArr, function(imgElem) {
                var alt = imgElem.getAttribute('alt');
                if (alt) {
                  var a = alt.toLowerCase();
                  if ( (a.indexOf('vip') > -1) || (a.indexOf('verified') > -1) ) {
                    elem.style.opacity = 0;
                  }
                }
              });
            }
            if (parameter == 'mention') {
                var str = elem.textContent;
                if (str) {
                  if ( str.indexOf('@') > -1 ) {
                    elem.style.opacity = 0;
                  }
                }
            }
            if (parameter == 'hashtag') {
                var str = elem.textContent;
                if (str) {
                  if ( str.indexOf('#') > -1 ) {
                    elem.style.opacity = 0;
                  }
                }
              }
            if (parameter == 'donation') {
              var elem3Arr = elem.getElementsByClassName('chat-line__message--cheer-amount');
              if (elem3Arr[0]) {
                elem.style.opacity = 0;
              }
            }

          });
          scbt.n.visibilityChatShow = 3;
        parameter = str = a = alt = elemArr = elem = elem2Arr = elem2 = toSearchFor = arr = null; return false;
      }

      if (scbt.n.visibilityChatShow === 3) {
        scbt.e.scbtChatB.textContent = '';
        [].forEach.call(elemArr, function(elem) {
          scbt.f.chat_off(elem);
          elem.style.opacity = 1;
        });
        scbt.n.visibilityChatShow = 1;
        parameter = str = a = alt = elemArr = elem = elem2Arr = elem2 = toSearchFor = arr = null; return false;
      }
  return false;
}


scbt.f.chat_get_username = function(obj, elem){
  var elemArr = elem.querySelectorAll('.video-chat__message-author');
  if (elemArr[0]) {
    obj.username = elemArr[0].textContent;
    return obj;
  }
  var elemArr = elem.querySelectorAll('.chatter-name'); 
  if (elemArr[0]) {
    obj.username = elemArr[0].textContent;
    return obj;
  }
  elemArr = elem.querySelectorAll('.chat-author__display-name');
  if (elemArr[0]) {
    obj.username = elemArr[0].textContent;
    return obj;
  }
  elemArr = elem.querySelectorAll('.chat-line__username');
  if (elemArr[0]) {
    obj.username = elemArr[0].textContent;
    return obj;
  }
  if (elem.classList.contains('user-notice-line') ) {
    obj.username = 'Twitch';
  }
  return obj;
}


scbt.f.chat_get_message = function(obj, elem){
  if (elem.classList.contains('user-notice-line') ) {
    obj.message = elem.textContent;
  } else {
    obj.message = '';
    var elemArr = elem.querySelectorAll('.text-fragment');
    if (elemArr[0]) {
      [].forEach.call(elemArr, function(chat) {
        if (chat.textContent) {
          obj.message = obj.message + chat.textContent + ' ';
        }
      });
    }
    var elemArr = elem.querySelectorAll('.mention-fragment');
    if (elemArr[0]) {
      obj.mention = 1;
      [].forEach.call(elemArr, function(chat) {
        if (chat.textContent) {
          obj.message = obj.message + ' ' + chat.textContent;
        }
      });
    }
    var elemArr = elem.querySelectorAll('.chat-line__message--cheer-amount');
    if (elemArr[0]) {
      [].forEach.call(elemArr, function(chat) {
        var msg = chat.textContent;
        obj.donation = 1;
        if (msg != '') {
          obj.message = obj.message + ' ' + msg + ' bit cheer - ' + msg;
        }
      });
    }
  }
  return obj;
}

