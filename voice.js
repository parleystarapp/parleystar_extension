/* SPDX-FileCopyrightText: © 2024 promising future digital media llc. All rights reserved. <admin@parleystar.com> */
/* SPDX-License-Identifier: Mozilla Public License 1.1 (MPL-1.1) */
// ************** START VOICE functions
console.log('loading voice.js');

scbt.f.chat_speak = function(str){
  if (str) {
    var strMatch = str.toLowerCase().trim();
    var arr = scbt.s.scbtfeature12.split(',');
    var arrl = arr.length;
    for (var i = 0; i < arrl; i++) {
      var str2 = arr[i];
      var str2Match = str2.toLowerCase().trim();
      if ( strMatch.includes(str2Match) ) {
        if ( scbt.a.spokenWords.includes(str) ) {  } else {
          scbt.a.spokenWords.push(str);
          var synth = window.speechSynthesis;
          var voices = synth.getVoices();
          var utterance = new SpeechSynthesisUtterance(t);
          utterance.voice = voices[0];
          synth.speak(utterance);
          utterance.addEventListener('start', e => {
            // console.log('speak start of ' + str);
          });
          utterance.addEventListener('end', e => {
            // console.log('speak end of ' + str);
            str = strMatch = arr = arrl = i = str2 = strMatch = synth = voices = null; return false;
          });
        }
      }
    }
  }
}


scbt.f.turn_off_voice_commands = function(){
  window.recognition = null;
  window.SpeechRecognition = null;
  window.SpeechGrammarList = null;
  window.SpeechRecognitionEvent = null;
  return false;
}


scbt.f.turn_on_voice_commands = function(){
  window.recognition = new (window.SpeechRecognition || window.webkitSpeechRecognition || window.mozSpeechRecognition || window.msSpeechRecognition)();
  window.recognition.continuous = true;
  window.recognition.lang = 'en-US';
  window.recognition.interimResults = false;
  window.recognition.maxAlternatives = 1;
  window.recognition.start();

  window.recognition.onresult = function(e) {
    if (e) {
      var str = e.results[e.results.length - 1][0].transcript;
      if (str) {
        str = str.replace(/[^a-zA-Z0-9_\-@\s]/g, '');
        str = str.trim();
        str = str.toLowerCase();
        
        if (str == 'computer toggle chat') {
          scbt.f.command1(); return false;
        }
        if (str == 'computer search chat') {
          scbt.f.command2(); return false;
        }
        if (str == 'computer search all chat') {
          scbt.f.command4(); return false;
        }
        if (str == 'computer chat top') {
          scbt.f.chat_up_to_top(); return false;
        }
        if (str == 'computer chat bottom') {
          scbt.f.chat_down_to_bottom(); return false;
        }
      }
    }
  }

  window.recognition.onspeechend = function(e) {
    // console.log('onspeechend speech');
    window.recognition.stop();
    scbt.f.turn_off_voice_commands();
    scbt.f.turn_on_voice_commands();
  }

  window.recognition.onnomatch = function(e) {
    // console.log('onnomatch speech');
  }

  window.recognition.onerror = function(e) {
    console.error('on speech error', e);
    scbt.f.turn_off_voice_commands();
    scbt.f.turn_on_voice_commands();
    return false;
  }
}

// ************** END VOICE functions