/* SPDX-FileCopyrightText: © 2024 promising future digital media llc. All rights reserved. <admin@parleystar.com> */
/* SPDX-License-Identifier: Mozilla Public License 1.1 (MPL-1.1) */
console.log('loading kick');

scbt.f.chat_clean_kick = function(obj, elem){
  var elemArr = [];
  var str = null;
  var special = false;
  var stampArr = new Date().toLocaleTimeString().replace(/ /g, '').split(':'); // = 11:34:03AM  ['12', '11', '42PM']
  var timestamp = stampArr[0] + ':' + stampArr[1] + stampArr[2].slice(2);

  // HOST EVENT
  var elemArr = elem.getElementsByClassName('chatroom-event-host__container');
  if (elemArr[0]) {
    special = true;
    var elem2Arr = elem.getElementsByClassName('chatroom-event-host__label .text-white');
    obj.username = 'anonymous';
    obj.message = 'just hosted with viewers';
    if (elem2Arr[0] && elem2Arr[0].textContent) { 
      obj.username = elem2Arr[0].textContent;
    }
    var elemArr3 = elem.getElementsByClassName('chatroom-event-host__label');
    if (elemArr3[0] && elemArr3[0].textContent) { 
      obj.message = elemArr3[0].textContent;
    }
    obj.timestamp = timestamp;
    obj.anevent = 1;
  }

  // SUB EVENT
  var elemArr = elem.getElementsByClassName('chatroom-event-sub__container');
  if (elemArr[0]) {
    special = true;
    obj.username = 'anonymous';
    obj.message = 'just subscribed for 1 month';
    var elem2Arr = elem.getElementsByClassName('chatroom-event-sub__label .text-white');
    if (elem2Arr[0] && elem2Arr[0].textContent) { 
      obj.username = elem2Arr[0].textContent;
    }
    var elem3Arr = elem.getElementsByClassName('chatroom-event-sub__label');
    if (elem3Arr[0] && elem3Arr[0].textContent) { 
      obj.message = elem3Arr[0].textContent;
    }
    obj.timestamp = timestamp;
    obj.sub = 1;
    obj.newSub = 1;
    obj.anevent = 1;
  }

  // GIFT EVENT
  var elemArr = elem.getElementsByClassName('chatroom-event-subgift__container');
  if (elemArr[0]) {
    special = true;
    obj.username = 'anonymous';
    obj.message = 'has gifted 1 subscription to the community';
    var elem2Arr = elem.getElementsByClassName('chatroom-event-subgift__label .text-white');
    if (elem2Arr[0] && elem2Arr[0].textContent) { 
      obj.username = elem2Arr[0].textContent;
    }
    var elem3Arr = elem.getElementsByClassName('chatroom-event-subgift__label');
    if (elem3Arr[0] && elem3Arr[0].textContent) { 
      obj.message = elem3Arr[0].textContent;
    }
    obj.timestamp = timestamp;
    obj.sub = 1;
    obj.gifter = 1;
    obj.newSub = 1;
    obj.anevent = 1;
  }

  // CLIP EVENT
  var elemArr = elem.getElementsByClassName('chat-entry-clip');
  if (elemArr[0]) {
    special = true;
    obj.username = 'anonymous';
    obj.message = 'made a nice clip.';

    var elem2Arr = elemArr[0].getElementsByClassName('text-primary');

    if (elem2Arr[0] && elem2Arr[0].textContent) { 
      obj.username = elem2Arr[0].textContent;
    }
    var elem3Arr = elemArr[0].getElementsByClassName('chat-entry-clip-info');
    if (elem3Arr[0] && elem3Arr[0].textContent) { 
      obj.message = elem3Arr[0].textContent;
    }
    var imgElemArr = elemArr[0].getElementsByTagName('img');
    if (imgElemArr[0]) {
      obj.message = obj.message + ' img ' + imgElemArr[0].src;
    }
    obj.timestamp = timestamp;
    obj.anevent = 1;
  }

  // regular chat
  if (special === false) {
    var elemArr = elem.querySelectorAll('.inline-block.text-xs.font-semibold.text-gray-400');
    if (elemArr[0] && elemArr[0].textContent) { 
      obj.timestamp = elemArr[0].textContent;
    }

    var elemArr = elem.getElementsByClassName('chat-entry-username');
    if (elemArr[0]) { } else {
      var elemArr = elem.getElementsByClassName('chat-message-identity');
    }
    
    obj.username = null;
    if (elemArr[0] && elemArr[0].textContent) { 
      obj.username = elemArr[0].textContent;
      obj = scbt.f.get_obj_cleaned_username_from_obj(obj);
    }

    var elemArr = elem.getElementsByClassName('chat-entry-content');
    obj.message = null;
    if (elemArr[0] && elemArr[0].textContent) {
      obj.message = elemArr[0].textContent;
      obj = scbt.f.get_obj_cleaned_message_from_obj(obj);
      if (obj.message.indexOf('@') > -1) {
        obj.mention = 1;
      }
      if (obj.message.indexOf('#') > -1) {
        obj.hashtag = 1;
      }

      if (obj.message == null || obj.message.trim() === '') { } else { 
        obj.message = obj.message.replace(/(?:\r\n|\r|\n)/g, '');
        obj.message = obj.message.split("\t").join("");
        obj.message = obj.message.split("\"").join("");
        obj.message = obj.message.split("\'").join("");
      }
    }

    if (elem.classList.contains('kicks') ) {
      obj.donation = 1;
      obj.anevent = 1;
    }

    obj.sub = scbt.f.get_binary_role_from_chat_message(elem, 'sub'); // 0 or 1
    obj.moderator = scbt.f.get_binary_role_from_chat_message(elem, 'moderator');
    obj.founder = scbt.f.get_binary_role_from_chat_message(elem, 'founder');
    obj.verified = scbt.f.get_binary_role_from_chat_message(elem, 'verified');
    obj.og = scbt.f.get_binary_role_from_chat_message(elem, 'og');
    obj.owner = scbt.f.get_binary_role_from_chat_message(elem, 'owner');
    obj.gifter = scbt.f.get_binary_role_from_chat_message(elem, 'gifter');
    obj.staff = scbt.f.get_binary_role_from_chat_message(elem, 'staff');
  }

  if (obj.username && obj.message) {
    if (special === true) {
      obj.itemid = Date.now();
    } else {
      if (elem.dataset.chatEntry) {
        obj.itemid = elem.dataset.chatEntry;
      } else {
        var um = obj.message;
        obj.itemid = obj.username + um.substring(0, 6);  
      }
    }
  }
  
  elem = elemArr = str = special = stampArr = timestamp = elem2Arr = elemArr3 = imgElemArr = um = null; return obj;
}


scbt.f.chat_make_decisions_kick = function(obj, elem, settings){
  
  if (settings.sz1b1kick === true) {
    var elemArr = elem.getElementsByTagName('img');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    var elemArr = elem.getElementsByTagName('i');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    var elemArr = elem.getElementsByTagName('svg');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    var elemArr = elem.getElementsByTagName('canvas');
    [].forEach.call(elemArr, function(elm2) {
      elm2.style.display = 'none';
    });
    elemArr = elm2 = null;
  }

  // Highlight sub messages in chat in this hex colour h1
  if (settings.sz2c1kick != '#000000') {
    if (obj.sub === 1) {
      obj.isHighlighted = settings.sz2c1kick;
    }
  }
  // Highlight gifter messages in chat in this hex colour h2
  if (settings.sz2c9kick != '#000000') {
    if (obj.gifter === 1) {
      obj.isHighlighted = settings.sz2c9kick;
    }
  }
  // Highlight VIP messages in chat in this hex colour h3 
  if (settings.sz2c5kick != '#000000') {
    if (elem.classList.contains('vip')) {
      obj.isHighlighted = settings.sz2c5kick;
    }
  }
  // Highlight founder messages in chat in this hex colour  
  if (settings.sz2c6kick != '#000000') {
    if (obj.founder === 1) {
      obj.isHighlighted = settings.sz2c6kick;
    }
  }
  // Highlight OG messages in chat in this hex colour  
  if (settings.sz2c7kick != '#000000') {
    if (obj.og === 1) {
      obj.isHighlighted = settings.sz2c7kick;
    }
  }
  // Highlight owner messages in chat in this hex colour  
  if (settings.sz2c8kick != '#000000') {
    if (obj.owner === 1) {
      obj.isHighlighted = settings.sz2c8kick;
    }
  }
  // Highlight moderator messages in chat in this hex colour  
  if (settings.sz2c2kick != '#000000') {
      if (obj.moderator === 1) {
        obj.isHighlighted = settings.sz2c2kick;
      }
  }
  // Mute owner/streamer messages in chat in dim text  
  if (settings.sz3b2 === true) {
      if (obj.owner === 1) {
        obj.isMuted = 1;
      }
  }
  // Hide owner/streamer messages in chat  
  if (settings.sz4b2 === true) {
      if (obj.owner === 1) {
        obj.isHidden = 1;
      }
  }
  if ( (obj.owner === 1) || (obj.moderator === 1) || (obj.sub === 1) || (obj.newSub === 1) || (obj.verified === 1) ) { } else {
    // scbtmuted4 Mute non moderator/sub messages in chat  
    if (settings.sz3b1 === true) {
      obj.isMuted = 1;
    }
    // scbthidden4 Hide non moderator/sub messages in chat 
    if (settings.sz4b1 === true) {
      obj.isHidden = 1;
    }
  }
  // scbtmuted3 Mute bot messages  
  if (settings.sz3b4 === true) {
    if (obj.username && obj.message) {
      if (obj.isBot === 1) {
        obj.isMuted = 1;
      }
    }
  }
  // scbthidden3 Hide bot  messages 
  if (settings.sz4b4 === true) {
    if (obj.username && obj.message) {
      if (obj.isBot === 1) {
        obj.isHidden = 1;
      }
    }
  }
  // Highlight mention messages in chat in this hex colour 
  if (settings.sz2c3kick != '#000000') {
    if (obj.mention === 1) {
      obj.isHighlighted = settings.sz2c3kick;
    }
  }
  // Highlight hashtag messages in chat in this hex colour  
  if (settings.sz2c4kick != '#000000') {
    if (obj.hashtag === 1) {
      obj.isHighlighted = settings.sz2c4kick;
    }
  }
  // Mute @ mention messages in chat in dim text  
  if (settings.sz3b3 === true) {
    if (obj.mention === 1) {
      obj.isMuted = 1;
    }
  }
  // Hide @ mention messages in chat 
  if (settings.sz4b3 === true) {
    if (obj.mention === 1) {
      obj.isHidden = 1;
    }
  }
  elem = settings = null;
  return obj;
}


scbt.f.style_with_obj_of_changes_kick = function(obj){
  var css = '';
  console.log('style_with_obj_of_changes_kick', obj);

  // Background of chat in this hex colour.
  if (obj.sz1c1kick) {
    if (obj.sz1c1kick == '#000000' || obj.sz1c1kick == '') {
      css = css + ' body.scbt-kick #chatroom { background-color: asdf !important; } ';
    }
    if (obj.sz1c1kick == '#ffffff') {
      css = css + ' body.scbt-kick #chatroom { background-color: transparent !important; } ';
    }
    if ( (obj.sz1c1kick != '#ffffff') && (obj.sz1c1kick != '#000000') && (obj.sz1c1kick != '') ) {
      css = css + ' body.scbt-kick #chatroom { background-color: ' + obj.sz1c1kick + ' !important; } ';
    }
  }

  // User names in chat in this hex colour
  if (obj.sz1c2kick) {
    if (obj.sz1c2kick == '#000000' || obj.sz1c2kick == '') {
      css = css + ' body.scbt-kick .chat-entry-username { color: asdf !important; text-shadow: 0px; } ';
    }
    if (obj.sz1c2kick == '#ffffff') {
      css = css + ' body.scbt-kick .chat-entry-username { color: transparent !important; text-shadow: 0.3px 0.3px dimgray; } ';
    }
    if ( (obj.sz1c2kick != '#ffffff') && (obj.sz1c2kick != '#000000') && (obj.sz1c2kick != '') ) {
      css = css + ' body.scbt-kick .chat-entry-username { color:' + obj.sz1c2kick + ' !important; text-shadow: 0.5px 0.5px darkslategrey; } ';
    }
  }

  // Highlights of chat in this colour
  if (obj.sz1c3kick) {
    if (obj.sz1c3kick != '#000000') {
      scbt.c.scbtBorderColor = obj.sz1c3kick;
    } else {
      scbt.c.scbtBorderColor = '#ff0000';
    }
  }

  // Chat messages in this colour
  if (obj.sz1c4kick) {
    if (obj.sz1c4kick == '#000000' || obj.sz1c4kick == '') {
      css = css + ' body.scbt-kick .chat-entry-content { color: asdf !important; text-shadow: 0px; } ';
    }
    if (obj.sz1c4kick == '#ffffff') {
      css = css + ' body.scbt-kick .chat-entry-content { color: transparent !important; text-shadow: 0.3px 0.3px dimgray; } ';
    }  
    if ( (obj.sz1c4kick != '#ffffff') && (obj.sz1c4kick != '#000000') && (obj.sz1c4kick != '') ) {
      css = css + ' body.scbt-kick .chat-entry-content { color: ' + obj.sz1c4kick + ' !important; text-shadow: 0.5px 0.5px darkslategrey; } ';
    }
  }

  // Chat font sizes
  if (obj.sz1n1kick) {
    if ( Number(obj.sz1n1kick) > 0) {
      var str = Number(obj.sz1n1kick) + 'rem';
      css = css + ' body.scbt-kick .chat-entry-content { font-size: ' + str + '; line-height: ' + Number(obj.sz1n1kick)  + '; } ';
    } else {
      css = css + ' body.scbt-kick .chat-entry-content { font-size: initial; line-height: unset; } ';
    }
  }

  // text only chat
  if (obj.sz1b1kick === true) {
    css = css + ' body.scbt-kick .text-xs.font-semibold.text-gray-400, body.scbt-kick .chat-entry img, body.scbt-kick .chat-entry svg, body.scbt-kick .chat-entry i, body.scbt-kick .chat-entry canvas { visibility: hidden !important; } ';
  }
  if (obj.sz1b1kick === false) {
    css = css + ' body.scbt-kick .text-xs.font-semibold.text-gray-400, body.scbt-kick .chat-entry img, body.scbt-kick .chat-entry svg, body.scbt-kick .chat-entry i, body.scbt-kick .chat-entry canvas { visibility: visible !important; } ';      
  }

  // Left handed screen. Flip screen so video is on the right and chat is on the left.
  if (obj.sz5b16 === true) {
    css = css + ' body.scbt-kick #main-view .relative.flex.h-full.w-full { flex-direction: row-reverse; } ';
  }
  if (obj.sz5b16 === false) {
    css = css + ' body.scbt-kick #main-view .relative.flex.h-full.w-full { flex-direction: initial; } ';
  }

  // Upside down chat screen. Newest chat is on the top, oldest chat is on the bottom 
  if (obj.sz5b17 === true) {
     css = css + ' body.scbt-kick #chatroom .overflow-y-scroll.py-3 { display: flex; flex-direction: column-reverse; } ';
  }
  if (obj.sz5b17 === false) {
    css = css + ' body.scbt-kick #chatroom .overflow-y-scroll.py-3 { flex-direction: initial; display: block; } ';
  }

  // mouseover hover enlarge
  if (obj.sz5b18 === true) {
    css = css + ' body.scbt-kick .chat-entry-content:hover { font-size: 250% !important; } body.scbt-kick .chat-emote-container { height: 250%; width: 250%; } ';
  }
  if (obj.sz5b18 === false) {
    css = css + ' body.scbt-kick .chat-entry-content:hover { font-size: 100% !important; } body.scbt-kick .chat-emote-container { height: asdf; width: asdf; } ';
  }

  if (css) {
    var head = document.getElementsByTagName('head')[0];
    var style = document.createElement('style');
    head.appendChild(style);
    style.type = 'text/css';
    if (style.styleSheet){
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }
  }
  obj = t = head = style = css = null; return false;
}


scbt.f.get_binary_role_from_chat_message = function(elem, parameter){
  var toReturn = 0;

  if (parameter == 'sub') {
    if ( 
      (elem.classList.contains('sub')) || 
      (elem.querySelector('img[alt="Subscriber"]')) || 
      (elem.querySelector('[data-v-df7f331e]') ) ||
      (elem.querySelector('#badge-subscriber-gradient-1') )
    ) {
      toReturn = 1;
      elem = null; parameter = null; return toReturn;
    }
  }

  if (parameter == 'moderator') {
    if ( 
      (elem.classList.contains('moderator')) || 
      (elem.querySelector('[data-v-43d962e8]') ) || 
      (elem.querySelector('path[style="fill: rgb(0, 199, 255);"]') ) 
    ) {
      toReturn = 1;
      elem = null; parameter = null; return toReturn;
    }
  }

  if (parameter == 'founder') {
    if ( 
      (elem.classList.contains('founder')) || 
      (elem.querySelector('[data-v-62b88e44]')) || 
      (elem.querySelector('#badge-founder-gradient'))
    ) {
      toReturn = 1;
      elem = null; parameter = null; return toReturn;
    }
  }

  if (parameter == 'verified') {
    if ( 
      (elem.classList.contains('verified')) || 
      (elem.classList.contains('vip')) || 
      (elem.querySelector('[data-v-899c5d7d]')) || 
      (elem.querySelector('[data-v-935db34c]')) ||
      (elem.querySelector('[data-v-f55c3249]')) || 
      (elem.querySelector('#badge-verified-gradient')) || 
      (elem.querySelector('#badge-vip-gradient')) || 
      (elem.querySelector('.base-icon.ml-1.text-primary')) 
    ) {
      toReturn = 1;
      elem = null; parameter = null; return toReturn;
    }
  }

  if (parameter == 'vip') {
    if ( 
      (elem.classList.contains('vip')) || 
      (elem.querySelector('[data-v-f55c3249]')) || 
      (elem.querySelector('#badge-vip-gradient'))
    ) {
      toReturn = 1;
      elem = null; parameter = null; return toReturn;
    }
  }

  if (parameter == 'og') {
    if ( 
      (elem.classList.contains('og')) || 
      (elem.querySelector('[data-v-935db34c]')) || 
      (elem.querySelector('#badge-og-gradient-1'))
    ) {
      toReturn = 1;
      elem = null; parameter = null; return toReturn;
    }
  }

  if (parameter == 'owner') {
    if ( 
      (elem.classList.contains('owner')) || 
      (elem.querySelector('[data-v-11d1cc91]')) || 
      (elem.querySelector('[data-v-1c3105ea]')) || 
      (elem.querySelector('#badge-host-gradient-1'))
    ) {
      toReturn = 1;
      elem = null; parameter = null; return toReturn;
    }
  }

  if (parameter == 'staff') {
    if ( 
      (elem.classList.contains('staff')) || 
      (elem.querySelector("svg g#surface1 path[d='M 8 14 L 5 11 L 7 5 L 9 5 L 11 11 Z M 8 14 ']")) || 
      (elem.querySelector('[data-v-9200dfef]')) || 
      (elem.querySelector('path[d="M2.07324 1.33331H6.51991V4.29331H7.99991V2.81331H9.47991V1.33331H13.9266V5.77998H12.4466V7.25998H10.9599V8.73998H12.4466V10.22H13.9266V14.6666H9.47991V13.1866H7.99991V11.7066H6.51991V14.6666H2.07324V1.33331Z"]') )
    ) {
      toReturn = 1;
      elem = null; parameter = null; return toReturn;
    }
  }

  if (parameter == 'gifter') {
    if ( 
       (elem.classList.contains('gifter')) || 
       (elem.querySelector("svg g[clip-path='url(#clip0_301_17810)']")) || 
       (elem.querySelector("svg g[clip-path='url(#clip0_301_17825)']")) ||
       (elem.querySelector("svg g[clip-path='url(#clip0_301_17815)']")) || 
       (elem.querySelector("svg g[clip-path='url(#clip0_301_17830)']"))

    ) {
      toReturn = 1;
      elem = null; parameter = null; return toReturn;
    }
  }

  elem = parameter = null; return toReturn;
}


scbt.f.toggle_chats_kick = function(parameter){
  elemArr = scbt.f.get_arr_chats();

      if (scbt.n.visibilityChatShow === 1) {
        scbt.e.scbtChatB.textContent = scbt.f.set_chat_parameter(parameter, scbt.n.visibilityChatShow);
        [].forEach.call(elemArr, function(elem) {
          elem.style.opacity = 0;
          if (parameter == 'mention') {
            var str = elem.textContent;
            if (str) {
              if ( str.indexOf('@') > -1 ) {
                scbt.f.chat_on(elem);
                elem.style.opacity = 1;
              }
            }
          }
          if (parameter == 'hashtag') {
            var str = elem.textContent;
            if (str) {
              if ( str.indexOf('#') > -1 ) {
                scbt.f.chat_on(elem);
                elem.style.opacity = 1;
              }
            }
          }
          if (parameter == 'owner') {
            var binary = scbt.f.get_binary_role_from_chat_message(elem, 'owner');
            if (binary > 0) {
              scbt.f.chat_on(elem);
              elem.style.opacity = 1;
            }
          }
          if (parameter == 'sub') {
            var binary = scbt.f.get_binary_role_from_chat_message(elem, 'sub');
            if (binary > 0) {
              scbt.f.chat_on(elem);
              elem.style.opacity = 1;
            }
          }
          if (parameter == 'moderator') {
            var binary = scbt.f.get_binary_role_from_chat_message(elem, 'moderator');
            if (binary > 0) {
              scbt.f.chat_on(elem);
              elem.style.opacity = 1;
            }
          }
          if (parameter == 'mod_sub') {
            var binary = scbt.f.get_binary_role_from_chat_message(elem, 'sub');
            var binary2 = scbt.f.get_binary_role_from_chat_message(elem, 'moderator');
            if ( (binary > 0) || (binary2 > 0)) {
              scbt.f.chat_on(elem);
              elem.style.opacity = 1;
            }
          }
          if ( (parameter == 'vip') || (parameter == 'verified') ) {
            var binary = scbt.f.get_binary_role_from_chat_message(elem, 'verified');
            if (binary > 0) {
              scbt.f.chat_on(elem);
              elem.style.opacity = 1;
            }
          }
        });
        scbt.n.visibilityChatShow = 2;
        parameter = str = a = alt = elemArr = elem = elem2Arr = elem2 = toSearchFor = arr = null; return false;
      }

      if (scbt.n.visibilityChatShow === 2) {
        scbt.e.scbtChatB.textContent = scbt.f.set_chat_parameter(parameter, scbt.n.visibilityChatShow);
        [].forEach.call(elemArr, function(elem) {
          elem.style.opacity = 1;
          scbt.f.chat_blur(elem);
          if (parameter == 'mention') {
            var str = elem.textContent;
            if (str) {
              if ( str.indexOf('@') > -1 ) {
                elem.style.opacity = 0;
              }
            }
          }
          if (parameter == 'hashtag') {
            var str = elem.textContent;
            if (str) {
              if ( str.indexOf('#') > -1 ) {
                elem.style.opacity = 0;
              }
            }
          }
          if (parameter == 'owner') {
            var binary = scbt.f.get_binary_role_from_chat_message(elem, 'owner');
            if (binary > 0) {
              elem.style.opacity = 0;
            }
          }
          if (parameter == 'sub') {
            var binary = scbt.f.get_binary_role_from_chat_message(elem, 'sub');
            if (binary > 0) {
              elem.style.opacity = 0;
            }
          }
          if (parameter == 'moderator') {
            var binary = scbt.f.get_binary_role_from_chat_message(elem, 'moderator');
            if (binary > 0) {
              elem.style.opacity = 0;
            }
          }
          if (parameter == 'mod_sub') {
            var binary = scbt.f.get_binary_role_from_chat_message(elem, 'sub');
            var binary2 = scbt.f.get_binary_role_from_chat_message(elem, 'moderator');
            if ( (binary > 0) || (binary2 > 0)) {
              elem.style.opacity = 0;
            }
          }
         if ( (parameter == 'vip') || (parameter == 'verified') ) {
            var binary = scbt.f.get_binary_role_from_chat_message(elem, 'verified');
            if (binary > 0) {
              elem.style.opacity = 0;
            }
          }
        });
        scbt.n.visibilityChatShow = 3;
        parameter = str = a = alt = elemArr = elem = elem2Arr = elem2 = toSearchFor = arr = null; return false;
      }

      if (scbt.n.visibilityChatShow === 3) {
        scbt.e.scbtChatB.textContent = '';
        [].forEach.call(elemArr, function(elem) {
          scbt.f.chat_off(elem);
          elem.style.opacity = 1;
        });
        scbt.n.visibilityChatShow = 1;
        parameter = str = a = alt = elemArr = elem = elem2Arr = elem2 = toSearchFor = arr = null; return false;
      }
    }


scbt.f.toggle_clips_menu = async function(e){
  if (scbt.b.popout === true) {
    scbt.f.toast(' Clips browser not available on pop-out chat. Please try in desktop or mobile viewing. '); return false;
  }
  scbt.e.scbtSettingsMenu.close("");
  scbt.e.scbtLoading.classList.add('scbt-bl');

  if (e) { if (e.preventDefault) { e.preventDefault(); } }
  var elemArr = document.body.getElementsByClassName('scbtClipsToggleMenu');
  if (elemArr[0]) {
    if (elemArr[0].classList.contains('scbt-bl') ) {
      elemArr[0].classList.remove('scbt-bl');
    } else {
      elemArr[0].classList.add('scbt-bl');
    }
  }
  var elemArr = document.body.getElementsByClassName('scbtClipsMenuWrapper');
  if (elemArr[0]) {
    if (elemArr[0].classList.contains('scbt-bl') ) {
      elemArr[0].classList.remove('scbt-bl');
    } else {
      elemArr[0].classList.add('scbt-bl');
    }
  }

  if (location == 'https://kick.com/') {
    var theHTML = '<h2>Recent Clips From Followed</h2>';
    scbt.e.scbtClipsMenuContent.insertAdjacentHTML('beforeend', theHTML);
    theHTML = '<h2>Recent Streams From Followed</h2>';
    scbt.e.scbtStreamsMenuContent.insertAdjacentHTML('beforeend', theHTML);

    // if followers are in localstorage
    var str = localStorage.getItem('scbtFollowedChannels');
    if (str) {
      scbt.a.followedChannels = JSON.parse(str);
      scbt.a.followedChannels = scbt.f.shuffle_array(scbt.a.followedChannels);
      console.log(scbt.a.followedChannels);
      await scbt.f.loop_through_followed('Clip');
      await scbt.f.loop_through_followed('Stream');
    } else {
      scbt.f.get_followed_channels_from_api('Clip');
    }

  }
  return false;
}


scbt.f.loop_through_followed = async function(videoTypeStr){
  console.log('doing loop_through_followed for ' + videoTypeStr);
  var arrl = scbt.a.followedChannels.length;
  for (var i = 0; i < arrl; i++) {
    var today = 'scbtFollowed' + videoTypeStr + new Date().toISOString().slice(0, 10) + scbt.a.followedChannels[i];
    var str = localStorage.getItem(today); 
    if (str) { 
      console.log('yes localstorage for ' + today);
      var videoObj = JSON.parse(str);
      scbt.f.build_recent_feed(videoObj);
    } else {
      if (videoTypeStr == 'Clip') {
        await scbt.f.get_clips_from_api(scbt.a.followedChannels[i]);
      }
      if (videoTypeStr == 'Stream') {
        await scbt.f.get_stream_from_api(scbt.a.followedChannels[i]);
      }
    }
  }
  scbt.e.scbtLoading.classList.remove('scbt-bl');
  return false;
}


scbt.f.get_stream_from_api = function(username){
  fetch('https://kick.com/api/v2/channels/' + username + '/videos/latest',
    {
      method: 'GET',
      mode: 'cors',
      cache: 'no-cache',
    })
    .then(function(res){ if (res.ok) { return res.json(); } else { return Promise.reject(res.status); } })
    .then(function(resp){ 
    if ( (resp == null || resp.length === 0) || (isNaN(resp) == false) ) { console.error('api error1', resp); return false; } else {
      if (resp == 'error') {
        console.error('api error2', resp); serviceid = channelid = videoid = res = arr = arrl = arr2 = resp = null; return false;
      } else {

        if (typeof resp != 'object') { console.error('api error2', resp); return false; }
        if (resp.data) { } else { console.error('api error3', resp); return false; }
          var videoObj = scbt.f.get_videoObj();
          if (resp.data.video && resp.data.video.uuid) {
            videoObj.video_id = resp.data.video.uuid;  
          }
          if (resp.data.thumbnail && resp.data.thumbnail.src) {
            videoObj.video_thumbnail = resp.data.thumbnail.src;
          }
          if (resp.data && resp.data.session_title) {
            videoObj.video_title = resp.data.session_title;  
          }
          var today = 'scbtFollowedStream' + new Date().toISOString().slice(0, 10) + username;
          localStorage.setItem(today, JSON.stringify(videoObj)); 
          scbt.f.build_recent_feed(videoObj);
        }
      }
    })
  .catch(error => {
     console.log(error); serviceid = channelid = videoid = res = arr = arrl = arr2 = resp = error = null; return false;
  });
}



scbt.f.get_clips_from_api = function(username){
  fetch('https://kick.com/api/v2/channels/' + username + '/clips?cursor=0&sort=date&time=all', 
  {
    method: 'GET',
    mode: 'cors',
    cache: 'no-cache',
  })
  .then(function(res){ if (res.ok) { return res.json(); } else { return Promise.reject(res.status); } })
  .then(function(resp){ 
  if ( (resp == null || resp.length === 0) || (isNaN(resp) == false) ) { console.error('api error1', resp); return false; } else {
    if (resp == 'error') {
      console.error('api error2', resp); serviceid = channelid = videoid = res = arr = arrl = arr2 = resp = null; return false;
    } else {
      if (typeof resp != 'object') { console.error('api error2', resp); return false; }
      if (resp.clips) { } else { console.error('api error3', resp); return false; }

      var arrl = resp.clips.length;
      if (arrl > 3) { arrl = 3; }
      for (var i = 0; i < arrl; i++) {
        var videoObj = scbt.f.get_videoObj();
        videoObj = scbt.f.get_videoObj_populated(videoObj, resp.clips[i]);
        var today = 'scbtFollowedClip' + new Date().toISOString().slice(0, 10) + username;
        localStorage.setItem(today, JSON.stringify(videoObj));
        scbt.f.build_recent_feed(videoObj);
      }

      }
    }
  })
  .catch(error => {
     console.error(error); serviceid = channelid = videoid = res = arr = arrl = arr2 = resp = error = null; return false;
  });
}


scbt.f.get_followed_channels_from_api = function(nextCursor){
  if (nextCursor == 'Clip') {
    nextCursor = 0;
    scbt.v.getWhat = 'clips';
  }
  if (nextCursor == 'Stream') {
    nextCursor = 0;
    scbt.v.getWhat = 'streams';
  }

  fetch('https://kick.com/api/v2/channels/followed?cursor=' + nextCursor,
  {
      method: 'GET',
      mode: 'cors',
      cache: 'no-cache',
    })
    .then(function(res){ if (res.ok) { return res.json(); } else { return Promise.reject(res.status); } })
    .then(function(resp){ 
    if ( (resp == null || resp.length === 0) || (isNaN(resp) == false) ) { console.error('api error1', resp); } else {
      if (resp == 'error') {
        console.log('api error2'); serviceid = channelid = videoid = res = arr = arrl = arr2 = resp = null; return false;
      } else {
        if (typeof resp != 'object') { console.error('api error2', resp); return false; }

          if (resp.nextCursor && resp.nextCursor != '0') {
            scbt.v.nextCursor = resp.nextCursor;
          } else {
            scbt.v.nextCursor = null;
          }
          if (resp.channels) { } else { console.error('api error3', resp); return false; }

          var arrl = resp.channels.length;
          for (var i = 0; i < arrl; i++) {
            scbt.a.followedChannels.push(resp.channels[i].user_username);
          }

          if (scbt.v.nextCursor) {
            setTimeout(function(){ scbt.f.get_followed_channels_from_api(scbt.v.nextCursor); }, 3000); return false;
          } else {
            localStorage.setItem('scbtFollowedChannels', JSON.stringify(scbt.a.followedChannels)); 
            scbt.f.toast(' Done loading followed channels ');
            // if (scbt.v.getWhat == 'clips') {
            //   scbt.f.loop_through_followed('Clip');
            // }
            // if (scbt.v.getWhat == 'streams') {
            //   scbt.f.loop_through_followed('Stream');
            // }
          }

        }
      }
    })
  .catch(error => {
     console.error(error); serviceid = channelid = videoid = res = arr = arrl = arr2 = resp = error = null; return false;
  });
}


scbt.f.shuffle_array = function(arr){
  var currentIndex = arr.length;
  var randomIndex;
  while (currentIndex != 0) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;
    [arr[currentIndex], arr[randomIndex]] = [arr[randomIndex], arr[currentIndex]];
  }
  return arr;
}


scbt.f.get_videoObj = function(arr){
  var videoObj = {};
  videoObj.video_id = '';                  
  videoObj.video_thumbnail = '';           
  videoObj.video_title = '';               
  
  videoObj.video_url = '';                 
  videoObj.video_duration = '';            
  videoObj.video_view_count = '';          
  videoObj.video_likes_count = '';         
  videoObj.video_created_at = '';          
   
  videoObj.video_parent_category = '';     
  videoObj.video_category_slug = '';       
  videoObj.video_category_responsive = ''; 

  videoObj.video_channel_slug = '';        
  videoObj.video_channel_username = '';    

  videoObj.video_creator_slug = '';        
  videoObj.video_creator_username = '';    
  return videoObj;
}


scbt.f.get_videoObj_populated = function(videoObj, clipObj){
  if (clipObj) {
      if (clipObj.id) {
        videoObj.video_id = clipObj.id;
      }
      if (clipObj.thumbnail_url) {
        videoObj.video_thumbnail = clipObj.thumbnail_url;
      }
      if (clipObj.title) {
        videoObj.video_title = clipObj.title;
      }

      if (clipObj.video_url) {
        videoObj.video_url = clipObj.video_url;
      }
      if (clipObj.duration) {
        videoObj.video_duration = clipObj.duration;
      }
      if (clipObj.view_count) {
        videoObj.video_view_count = clipObj.view_count;
      }
      if (clipObj.likes_count) {
        videoObj.video_likes_count = clipObj.likes_count;
      }

      if (clipObj.created_at) {
        var created_atArr = clipObj.created_at.split('T');
        videoObj.video_created_at = created_atArr[0];
      }

      if (clipObj.category && clipObj.category.parent_category) {
        videoObj.video_parent_category = clipObj.category.parent_category;
      }
      if (clipObj.category && clipObj.category.slug) {
        videoObj.video_category_slug = clipObj.category.slug;
      }
      if (clipObj.category && clipObj.category.responsive) {
        videoObj.video_category_responsive = clipObj.category.responsive;
      }

      if (clipObj.channel && clipObj.channel.slug) {
        videoObj.video_channel_slug = clipObj.channel.slug;
      }
      if (clipObj.channel && clipObj.channel.username) {
        videoObj.video_channel_username = clipObj.channel.username;
      }

      if (clipObj.creator && clipObj.creator.slug) {
        videoObj.video_creator_slug = clipObj.creator.slug;
      }
      if (clipObj.creator && clipObj.creator.username) {
        videoObj.video_creator_username = clipObj.creator.username;
      }
    }
  return videoObj;
}


scbt.f.clear_recent_clips_begin = function(e){
  if (!e) { return false; }
  if (e) {
    if (e.preventDefault) { e.preventDefault(); }
    if (!e.target) { return false; }
    if (!e.target.id) { return false; }

    // Followed Feed Clips
    if (e.target.id == 'scbt30d') {
      for (var str in localStorage) {
        if (str) {
          if ( str.indexOf('scbtFollowedClip') > -1) {
            localStorage.removeItem(str);
          }
          if ( str.indexOf('scbtFollowedChannels') > -1) {
            localStorage.removeItem(str);
          }
        }
      }
      scbt.f.toast('Success: followed feed cache cleared.');
      return false;
    }

    // Followed Feed Streams
    if (e.target.id == 'scbt31d') {
      for (var str in localStorage) {
        if (str) {
          if ( str.indexOf('scbtFollowedStream') > -1) {
            localStorage.removeItem(str);
          }
          if ( str.indexOf('scbtFollowedChannels') > -1) {
            localStorage.removeItem(str);
          }
        }
      }
      scbt.f.toast('Success: followed feed cache cleared.');
      return false;
    }

    // Fortnite Clips
    if (e.target.id == 'scbt32d') {
      for (var str in localStorage) {
        if (str) {
          if ( str.indexOf('scbtfortniteClip') > -1) {
            localStorage.removeItem(str);
          }
        }
      }
      scbt.f.toast('Success: Fortnite feed cache cleared.');
      return false;
    }

    // Call of Duty Warzone Clips
    if (e.target.id == 'scbt33d') {
      for (var str in localStorage) {
        if (str) {
          if ( str.indexOf('scbtcall-of-duty-warzone-2.0Clip') > -1) {
            localStorage.removeItem(str);
          }
        }
      }
      scbt.f.toast('Success: Call of Duty Warzone feed cache cleared.');
      return false;
    }

    // Valorant Clips
    if (e.target.id == 'scbt34d') {
      for (var str in localStorage) {
        if (str) {
          if ( str.indexOf('scbtvalorantClip') > -1) {
            localStorage.removeItem(str);
          }
        }
      }
      scbt.f.toast('Success: Valorant feed cache cleared.');
      return false;
    }

    // Music Clips
    if (e.target.id == 'scbt35d') {
      for (var str in localStorage) {
        if (str) {
          if ( str.indexOf('scbtlive-musicClip') > -1) {
            localStorage.removeItem(str);
          }
        }
      }
      scbt.f.toast('Success: Live Music feed cache cleared.');
      return false;
    }

    // Chatting Clips
    if (e.target.id == 'scbt36d') {
      for (var str in localStorage) {
        if (str) {
          if ( str.indexOf('scbtjust-chattingClip') > -1) {
            localStorage.removeItem(str);
          }
        }
      }
      scbt.f.toast('Success: Just Chatting feed cache cleared.');
      return false;
    }

  }
  return false;
}


scbt.f.load_recent_clips_begin = function(e){
  if (!e) { return false; }
  if (e) {
    if (e.preventDefault) { e.preventDefault(); }
    if (!e.target) {  return false; }
    if (!e.target.id) {  return false; }
      
      scbt.e.scbtSettingsMenu.classList.remove('scbt-bl');
      
      scbt.e.scbtLoading.classList.add('scbt-bl');
      var elemArr = document.body.getElementsByClassName('scbtClipsMenuContent');
      if (elemArr[0]) {
        elemArr[0].innerHTML = '';
      }
      var elemArr = document.body.getElementsByClassName('scbtClipsMenuTitle');
      if (elemArr[0]) {
        elemArr[0].innerHTML = '';
      }

      var today = 'scbtFollowedChannels' + new Date().toISOString().slice(0, 10);
      var str = localStorage.getItem(today);
      if (str) {
        scbt.a.followedChannels = JSON.parse(str);
        scbt.a.followedChannels = scbt_shuffle_array(scbt.a.followedChannels);
      }
      // Followed Feed Clips 
      if (e.target.id == 'scbt30') {
        elemArr[0].innerHTML = '<h3>Recent Clips from Followed</h3>';
        if (str) {
          scbt.f.loop_through_followed('Clip');
          return false;
        } else {
          scbt.f.get_followed_channels_from_api('Clip');
          return false;
        }
      }
      // Followed Feed Streams
      if (e.target.id == 'scbt31') {
        elemArr[0].innerHTML = '<h3>Recent Stream from Followed</h3>';
        if (str) {
          scbt.f.loop_through_followed('Stream');
          return false;
        } else {
          scbt.f.get_followed_channels_from_api('Stream');
          return false;
        }
      }

      // Fortnite Clips
      if (e.target.id == 'scbt32') {
        elemArr[0].innerHTML = '<h3>Recent Fortnite Clips</h3>';
        var today = 'scbtfortniteClip' + new Date().toISOString().slice(0, 10) + '1';
        var str = localStorage.getItem(today);
        if (str) {
          scbt.f.loop_through_recent('fortnite');
          return false;
        } else {
          scbt.f.get_recent_clips_from_api('fortnite');
          return false;
        }
      }

      // Call of Duty Warzone Clips
      if (e.target.id == 'scbt33') {
        elemArr[0].innerHTML = '<h3>Recent Call of Duty Clips</h3>';
        var today = 'scbtcall-of-duty-warzone-2.0Clip' + new Date().toISOString().slice(0, 10) + '1';
        var str = localStorage.getItem(today);
        if (str) {
          scbt.f.loop_through_recent('call-of-duty-warzone-2.0');
          return false;
        } else {
          scbt.f.get_recent_clips_from_api('call-of-duty-warzone-2.0');
          return false;
        }
      }

      // Valorant Clips
      if (e.target.id == 'scbt34') {
        elemArr[0].innerHTML = '<h3>Recent Valorant Clips</h3>';
        var today = 'scbtvalorantClip' + new Date().toISOString().slice(0, 10) + '1';
        var str = localStorage.getItem(today);
        if (str) {
          scbt.f.loop_through_recent('valorant');
          return false;
        } else {
          scbt.f.get_recent_clips_from_api('valorant');
          return false;
        }
      }

      // Music Clips
      if (e.target.id == 'scbt35') {
        elemArr[0].innerHTML = '<h3>Recent Music Clips</h3>';
        var today = 'scbtlive-musicClip' + new Date().toISOString().slice(0, 10) + '1';
        var str = localStorage.getItem(today);
        if (str) {
          scbt.f.loop_through_recent('live-music');
          return false;
        } else {
          scbt.f.get_recent_clips_from_api('live-music');
          return false;
        }
      }

      // Chatting Clips
      if (e.target.id == 'scbt36') {
        elemArr[0].innerHTML = '<h3>Recent Chatting Clips</h3>';
        var today = 'scbtjust-chattingClip' + new Date().toISOString().slice(0, 10) + '1';
        var str = localStorage.getItem(today);
        if (str) {
          scbt.f.loop_through_recent('just-chatting');
          return false;
        } else {
          scbt.f.get_recent_clips_from_api('just-chatting');
          return false;
        }
      }

    }
  return false;
} 





scbt.f.loop_through_recent = async function(categoryStr){
  for (var i = 0; i < 20; i++) {
    var today = 'scbt' + categoryStr + 'Clip' + new Date().toISOString().slice(0, 10)  + i;
    var str = localStorage.getItem(today); 
    if (str) { 
      var videoObj = JSON.parse(str);
      scbt.f.build_recent_feed(videoObj);
    } else {
      await scbt.f.get_recent_clips_from_api(categoryStr);
    }
  }
  
  var ul = document.querySelector('.scbtClipsMenuContent');
  for (var i = ul.children.length; i >= 0; i--) {
    ul.appendChild(ul.children[Math.random() * i | 0]);
  }

  scbt.e.scbtLoading.classList.remove('scbt-bl');
  return false;
}


scbt.f.get_recent_clips_from_api = function(categoryStr){
  if (!categoryStr) {  return false; }
  var endpoint = 'https://kick.com/api/v2/categories/' + categoryStr + '/clips?cursor=0&sort=date&time=day';
  fetch(endpoint,
  {
    method: 'GET',
    mode: 'cors',
    cache: 'no-cache',
  })
  .then(function(res){ if (res.ok) { return res.json(); } else { return Promise.reject(res.status); } })
  .then(function(resp){ 
  if ( (resp == null || resp.length === 0) || (isNaN(resp) == false) ) { console.error('api error1', resp); return false; } else {
    if (resp == 'error') {
      console.log('api error2', resp); serviceid = channelid = videoid = res = arr = arrl = arr2 = resp = null; return false;
    } else {
      if (typeof resp != 'object') { console.log('api error2', resp); return false; }
      if (resp.clips) { } else { console.log('api error3', resp); return false; }

      var arrl = resp.clips.length;
      if (arrl > 20) { arrl = 20; }
      for (var i = 0; i < arrl; i++) {
        var videoObj = scbt.f.get_videoObj();
        videoObj = scbt.f.get_videoObj_populated(videoObj, resp.clips[i]);
        var today = 'scbt' + categoryStr + 'Clip' + new Date().toISOString().slice(0, 10) + i;
        localStorage.setItem(today, JSON.stringify(videoObj));
        scbt.f.build_recent_feed(videoObj);
      }

      scbt.e.scbtLoading.classList.remove('scbt-bl');
    }
  }
  })
  .catch(error => {
     console.log(error); serviceid = channelid = videoid = res = arr = arrl = arr2 = resp = error = null; return false;
  });
  return false;
}


scbt.f.build_recent_feed = function(videoObj){
  console.log('doing build_recent_feed for ', videoObj);
  var theHTML = '';
  theHTML = theHTML + '<div class="rounded p-2 transition-all duration-300 ease-trail hover:bg-secondary-lighter">';
  theHTML = theHTML + '<div class="group relative block aspect-video cursor-pointer overflow-hidden">';
  theHTML = theHTML + '<div class="pointer-events-none absolute left-0 top-0 z-10 hidden h-full w-full rounded opacity-0 lg:block"></div>';
  
  theHTML = theHTML + '<div class="absolute left-0 top-0 h-full w-full bg-secondary">';
  theHTML = theHTML + '<div data-v-36ee32dd="" class="relative h-full w-full">';
  
  if ( videoObj.video_url && videoObj.video_url.indexOf('.mp4') > -1 ) {
    theHTML = theHTML + '<video class="vjs-tech w-full object-cover h-full" webkit-playsinline="" playsinline="" controls poster="' + videoObj.video_thumbnail + '" src="' + videoObj.video_url + '" sizes="18vw"></video>';
  } 
  else if ( videoObj.video_url && videoObj.video_url.indexOf('playlist') > -1 ) {
    theHTML = theHTML + '<a href="https://kick.com/' + videoObj.video_channel_slug + '?clip=' + videoObj.video_id + '">' + '<video class="vjs-tech w-full object-cover h-full" webkit-playsinline="" playsinline="" controls poster="' + videoObj.video_thumbnail + '" sizes="18vw"></video></a>';
  }
  else {
    theHTML = theHTML + '<a href="https://kick.com/video/' + videoObj.video_id + '"><video class="vjs-tech w-full object-cover h-full" webkit-playsinline="" playsinline=""  poster="' + videoObj.video_thumbnail + '" sizes="18vw"></video></a>';
  }

  theHTML = theHTML + '</div>';
  theHTML = theHTML + '</div>';

  if (videoObj.video_duration) {
    theHTML = theHTML + '<span class="absolute left-2 top-2 rounded bg-black/80 px-1 text-xs font-light text-white">' + videoObj.video_duration + ' sec</span>';
  }

  if (videoObj.video_view_count) {
    theHTML = theHTML + '<span class="absolute bottom-2 left-2 block truncate rounded bg-black/80 px-1 text-right text-xs font-light text-white" style="max-width: 45%;">' + videoObj.video_view_count + 'views-' + videoObj.video_likes_count + '</span>';
    theHTML = theHTML + '<span class="absolute bottom-2 right-2 block truncate rounded bg-black/80 px-1 text-right text-sm font-light text-white" style="max-width: 45%;">' + videoObj.video_created_at + '</span>';
  }

  theHTML = theHTML + '</div>';

  theHTML = theHTML + '<div class="mt-3 flex w-full items-start justify-between overflow-hidden">';
  theHTML = theHTML + '<div class="flex w-full grow items-stretch space-x-3">';
  theHTML = theHTML + '<div class="leading-tight">';
  
  if (videoObj.video_parent_category) {
    theHTML = theHTML + '<a target="_blank" href="/categories/' + videoObj.video_parent_category + '/' + videoObj.video_category_slug + '" class="block h-full w-12 bg-secondary-light">';
    theHTML = theHTML + '<div data-v-36ee32dd="" class="relative h-full w-full">';
    theHTML = theHTML + '<img data-v-36ee32dd="" srcset="' + videoObj.video_category_responsive + '" src="' + videoObj.video_category_responsive + '" class="w-full object-cover h-full" alt="' + videoObj.video_category_slug + '" sizes="4vw"></div></a>';
  }
  theHTML = theHTML + '</div>';
  
  theHTML = theHTML + '<div class="grow overflow-hidden">';
  
  if ( videoObj.video_id && videoObj.video_id.indexOf('clip') > -1 ) {
    theHTML = theHTML + '<a href="https://kick.com/' + videoObj.video_channel_slug + '?clip=' + videoObj.video_id + '"  class="block cursor-pointer truncate font-semibold lg:hover:text-primary">' + videoObj.video_title + '</a>';
  } else {
    theHTML = theHTML + '<a href="https://kick.com/video/' + videoObj.video_id + '"  class="block cursor-pointer truncate font-semibold lg:hover:text-primary">' + videoObj.video_title + '</a>';
  }

  theHTML = theHTML + '<div class="truncate leading-none">';
  theHTML = theHTML + '<a aria-current="page" href="/' + videoObj.video_channel_slug + '" class="router-link-active router-link-exact-active text-xs font-semibold lg:hover:text-primary">' + videoObj.video_channel_username + '</a>';
  theHTML = theHTML + '</div>';

  if (videoObj.video_creator_slug) {
    theHTML = theHTML + '<div class="truncate leading-none">';
    theHTML = theHTML + '<a target="_blank"  href="/' + videoObj.video_creator_slug + '" class="text-xs font-semibold text-gray-400 lg:hover:text-primary">Clipped by' + videoObj.video_creator_username + '</a>';
    theHTML = theHTML + '</div>';
  }

  theHTML = theHTML + '</div></div></div></div>';

  if (videoObj.video_category_slug != '') {
    scbt.e.scbtClipsMenuContent.insertAdjacentHTML('beforeend', theHTML);
  } else {
    scbt.e.scbtStreamsMenuContent.insertAdjacentHTML('beforeend', theHTML);
  }
  theHTML = videoObj = null; return false;
}


scbt.f.hide_mature_elements_on_page_load = function(){
  console.log("BBBBBBB - 3");
  if ( (scbt.o.settingsFromSync.sz4b14 != '1') && (scbt.o.settingsFromSync.sz4b15 != '1') && (scbt.o.settingsFromSync.sz4b16 != '1') && (scbt.o.settingsFromSync.sz4b17 != '1') && (scbt.o.settingsFromSync.sz4b18 != '1') && (scbt.o.settingsFromSync.sz4b19 != '1') && (scbt.o.settingsFromSync.sz4b20 != '1') && (scbt.o.settingsFromSync.sz4b21 != '1') ) {
    return false;
  }

  console.log("BBBBBBB - 4");
  var elems = document.body.querySelectorAll('.sidebar .item-categories');
  var elemsz2 = document.body.querySelectorAll('.carousel__slide a');
  var elemsz3 = document.body.querySelectorAll('.subcategory-card img');
  var as = document.body.getElementsByTagName('a');
  var elemsz4 = document.body.querySelectorAll('.card-content .card-category-name');

  // scbthidden22
  // 18+
  if (scbt.o.settingsFromSync.sz4b14 > 0) {
    var tags = document.body.querySelectorAll('.card-category-tags .category-tag-component');
    [].forEach.call(tags, function(tag) {
      if (tag) {
        if (tag.textContent == '18+') {
          var p1 = tag.parentElement;
          if (p1) {
            var p2 = p1.parentElement;
            if (p2) {
              var p3 = p2.parentElement;
              if (p3) {
                  var p4 = p3.parentElement;
                  if (p4) {
                    p4.style.visibility = 'hidden';
                  }
              }
            }
          }
        }
      }
    });
  }

// scbthidden15
// https://kick.com/categories/alternative/other-tv-shows-movies
  if (scbt.o.settingsFromSync.sz4b15 > 0) {
      [].forEach.call(elems, function(elem) {
        if (elem) {
          if ( elem.textContent == 'Other TV Shows Movies' || elem.textContent == 'Other, Watch Party') {
            var p1 = elem.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                p2.style.visibility = 'hidden';
              }
            }
          }
        }
      });

      [].forEach.call(elemsz2, function(elem2) {
        if (elem2) {
          if ( elem2.textContent == 'Other TV Shows Movies' || elem2.textContent == 'Other, Watch Party') {
            var p1 = elem2.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                p2.style.visibility = 'hidden';
              }
            }
          }
        }
      });

      [].forEach.call(elemsz3, function(elem3) {
        if (elem3) {
          if (elem3.alt == 'Other TV Shows Movies' || elem3.alt == 'Other, Watch Party') {
            var p1 = elem3.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                var p3 = p2.parentElement;
                if (p3) {
                  p3.style.visibility = 'hidden';
                }
              }
            }
          }
        }
      });

      [].forEach.call(elemsz4, function(elem4) {
        if (elem4) {
          if (elem4.textContent == 'Other TV Shows Movies' || elem4.textContent == 'Other, Watch Party') {
            var p1 = elem4.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                var p3 = p2.parentElement;
                if (p3) {
                  p3.style.visibility = 'hidden';
                }
              }
            }
          }
        }
      });

      [].forEach.call(as, function(atag) {
        if (atag) {
          if (atag.href) {
            if ( atag.href.indexOf('movies') > -1 ) {
              atag.style.visibility = 'hidden';
            }
          }
        }
      });
  }

// scbthidden16
// https://kick.com/categories/alternative/just-sleeping
  if (scbt.o.settingsFromSync.sz4b16 > 0) {
      [].forEach.call(elems, function(elem) {
        if (elem) {
          if (elem.textContent == 'Just Sleeping') {
            var p1 = elem.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                p2.style.visibility = 'hidden';
              }
            }
          }
        }
      });

      [].forEach.call(elemsz2, function(elem2) {
        if (elem2) {
          if (elem2.textContent == 'Just Sleeping') {
            var p1 = elem2.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                p2.style.visibility = 'hidden';
              }
            }
          }
        }
      });

      [].forEach.call(elemsz3, function(elem3) {
        if (elem3) {
          if (elem3.alt == 'Just Sleeping') {
            var p1 = elem3.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                var p3 = p2.parentElement;
                if (p3) {
                  p3.style.visibility = 'hidden';
                }
              }
            }
          }
        }
      });

      [].forEach.call(elemsz4, function(elem4) {
        if (elem4) {
          if (elem4.textContent == 'Just Sleeping') {
            var p1 = elem4.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                var p3 = p2.parentElement;
                if (p3) {
                  p3.style.visibility = 'hidden';
                }
              }
            }
          }
        }
      });

      [].forEach.call(as, function(atag) {
        if (atag) {
          if (atag.href) {
            if ( atag.href.indexOf('just-sleeping') > -1 ) {
              atag.style.visibility = 'hidden';
            }
          }
        }
      });
  }


// scbthidden17
// https://kick.com/categories/irl/asmr
  if (scbt.o.settingsFromSync.sz4b17 > 0) {
      [].forEach.call(elems, function(elem) {
        if (elem) {
          if (elem.textContent == 'ASMR') {
            var p1 = elem.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                p2.style.visibility = 'hidden';
              }
            }
          }
        }
      });

      [].forEach.call(elemsz2, function(elem2) {
        if (elem2) {
          if (elem2.textContent == 'ASMR') {
            var p1 = elem2.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                p2.style.visibility = 'hidden';
              }
            }
          }
        }
      });

      [].forEach.call(elemsz3, function(elem3) {
        if (elem3) {
          if (elem3.alt == 'ASMR') {
            var p1 = elem3.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                var p3 = p2.parentElement;
                if (p3) {
                  p3.style.visibility = 'hidden';
                }
              }
            }
          }
        }
      });

      [].forEach.call(elemsz4, function(elem4) {
        if (elem4) {
          if (elem4.textContent == 'ASMR') {
            var p1 = elem4.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                var p3 = p2.parentElement;
                if (p3) {
                  p3.style.visibility = 'hidden';
                }
              }
            }
          }
        }
      });

      [].forEach.call(as, function(atag) {
        if (atag) {
          if (atag.href) {
            if ( atag.href.indexOf('asmr') > -1 ) {
              atag.style.visibility = 'hidden';
            }
          }
        }
      });
  }


// scbthidden18
// https://kick.com/categories/creative/body-art
  if (scbt.o.settingsFromSync.sz4b18 > 0) {
      [].forEach.call(elems, function(elem) {
        if (elem) {
          if (elem.textContent == 'Body Art') {
            var p1 = elem.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                p2.style.visibility = 'hidden';
              }
            }
          }
        }
      });

      [].forEach.call(elemsz2, function(elem2) {
        if (elem2) {
          if (elem2.textContent == 'Body Art') {
            var p1 = elem2.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                p2.style.visibility = 'hidden';
              }
            }
          }
        }
      });

      [].forEach.call(elemsz3, function(elem3) {
        if (elem3) {
          if (elem3.alt == 'Body Art') {
            var p1 = elem3.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                var p3 = p2.parentElement;
                if (p3) {
                  p3.style.visibility = 'hidden';
                }
              }
            }
          }
        }
      });

      [].forEach.call(elemsz4, function(elem4) {
        if (elem4) {
          if (elem4.textContent == 'Body Art') {
            var p1 = elem4.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                var p3 = p2.parentElement;
                if (p3) {
                  p3.style.visibility = 'hidden';
                }
              }
            }
          }
        }
      });

      [].forEach.call(as, function(atag) {
        if (atag) {
          if (atag.href) {
            if ( atag.href.indexOf('body-art') > -1 ) {
              atag.style.visibility = 'hidden';
            }
          }
        }
      });
  }

// scbthidden19
// https://kick.com/categories/gambling
  if (scbt.o.settingsFromSync.sz4b19 > 0) {
      [].forEach.call(elems, function(elem) {
        if (elem) {
          if (elem.textContent == 'Slots & Casino') {
            var p1 = elem.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                p2.style.visibility = 'hidden';
              }
            }
          }
        }
      });

      [].forEach.call(elemsz2, function(elem2) {
        if (elem2) {
          if (elem2.textContent == 'Slots & Casino') {
            var p1 = elem2.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                p2.style.visibility = 'hidden';
              }
            }
          }
        }
      });

      [].forEach.call(elemsz3, function(elem3) {
        if (elem3) {
          if (elem3.alt == 'Slots & Casino') {
            var p1 = elem3.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                var p3 = p2.parentElement;
                if (p3) {
                  p3.style.visibility = 'hidden';
                }
              }
            }
          }
        }
      });

      [].forEach.call(elemsz4, function(elem4) {
        if (elem4) {
          if (elem4.textContent == 'Slots & Casino') {
            var p1 = elem4.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                var p3 = p2.parentElement;
                if (p3) {
                  p3.style.visibility = 'hidden';
                }
              }
            }
          }
        }
      });

      [].forEach.call(as, function(atag) {
        if (atag) {
          if (atag.href) {
            if ( atag.href.indexOf('gambling') > -1 ) {
              atag.style.visibility = 'hidden';
            }
          }
        }
      });
  }

// scbthidden20
// https://kick.com/categories/irl/pools-hot-tubs-bikinis  
  if (scbt.o.settingsFromSync.sz4b20 > 0) {
      [].forEach.call(elems, function(elem) {
        if (elem) {
          if (elem.textContent == 'Pools, Hot Tubs & Bikinis') {
            var p1 = elem.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                p2.style.visibility = 'hidden';
              }
            }
          }
        }
      });

      [].forEach.call(elemsz2, function(elem2) {
        if (elem2) {
          if (elem2.textContent == 'Pools, Hot Tubs & Bikinis') {
            var p1 = elem2.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                p2.style.visibility = 'hidden';
              }
            }
          }
        }
      });

      [].forEach.call(elemsz3, function(elem3) {
        if (elem3) {
          if (elem3.alt == 'Pools, Hot Tubs & Bikinis') {
            var p1 = elem3.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                var p3 = p2.parentElement;
                if (p3) {
                  p3.style.visibility = 'hidden';
                }
              }
            }
          }
        }
      });

      [].forEach.call(elemsz4, function(elem4) {
        if (elem4) {
          if (elem4.textContent == 'Pools, Hot Tubs & Bikinis') {
            var p1 = elem4.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                var p3 = p2.parentElement;
                if (p3) {
                  p3.style.visibility = 'hidden';
                }
              }
            }
          }
        }
      });

      [].forEach.call(as, function(atag) {
        if (atag) {
          if (atag.href) {
            if ( atag.href.indexOf('pools-hot-tubs-bikinis') > -1 ) {
              atag.style.visibility = 'hidden';
            }
          }
        }
      });
    }

// scbthidden21
// https://kick.com/categories/irl/just-chatting
   if (scbt.o.settingsFromSync.sz4b21 > 0) {
      // Following Recommended
      
      [].forEach.call(elems, function(elem) {
        if (elem) {
          if (elem.textContent == 'Just Chatting') {
            var p1 = elem.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                p2.style.visibility = 'hidden';
              }
            }
          }
        }
      });

      [].forEach.call(elemsz2, function(elem2) {
        if (elem2) {
          if (elem2.textContent == 'Just Chatting') {
            var p1 = elem2.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                p2.style.visibility = 'hidden';
              }
            }
          }
        }
      });

      [].forEach.call(elemsz3, function(elem3) {
        if (elem3) {
          if (elem3.alt == 'Just Chatting') {
            var p1 = elem3.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                var p3 = p2.parentElement;
                if (p3) {
                  p3.style.visibility = 'hidden';
                }
              }
            }
          }
        }
      });

      [].forEach.call(elemsz4, function(elem4) {
        if (elem4) {
          if (elem4.textContent == 'Just Chatting') {
            var p1 = elem4.parentElement;
            if (p1) {
              var p2 = p1.parentElement;
              if (p2) {
                var p3 = p2.parentElement;
                if (p3) {
                  p3.style.visibility = 'hidden';
                }
              }
            }
          }
        }
      });

      [].forEach.call(as, function(atag) {
        if (atag) {
          if (atag.href) {
            if ( atag.href.indexOf('just-chatting') > -1 ) {
              atag.style.visibility = 'hidden';
            }
          }
        }
      });
    }

  scbt.f.hide_mature_categories_on_scroll();
  scbt.f.hide_mature_livestreams_on_scroll();
  return false;
}


scbt.f.hide_mature_categories_on_scroll = function(){
  targetNode2 = null;
  if (scbt.o.categoryLoading) {
    if (scbt.o.categoryLoading.observer) {
      scbt.o.categoryLoading.observer.disconnect();
      scbt.o.categoryLoading.observer = null;
      targetNode2 = null;
      config2 = null;
      callback2 = null;
    }
  }
  targetNode2 = document.body.querySelector('.place-content-stretch');
  if (targetNode2) {
    var config2 = { 
                 childList: true,
                 attributes: false,
                 characterData: false,
                 subtree: false,
                 attributeOldValue: false,
                 characterDataOldValue: false 
               };

    var callback2 = function(mutationsList2, observer2) {
      for (let mutation2 of mutationsList2) {
          if (mutation2.type === 'childList') {
              if (mutation2.addedNodes) {
                var arrl2 = mutation2.addedNodes.length;
                for (var i=0; i < arrl2; i++) { 
                  
                  if (mutation2.addedNodes[0].nodeName == '#text' || mutation2.addedNodes[0].nodeName == '#comment') {
                    continue;
                  }
                  if (mutation2.addedNodes[i].nodeName == '#text' || mutation2.addedNodes[i].nodeName == '#comment') {
                    continue;
                  }

                  var xxx = mutation2.addedNodes[i].querySelectorAll('.category-tag-component');
                  if ( scbt.o.settingsFromSync.sz4b14 > 0 ) {
if ( 
  (mutation2.addedNodes[i].querySelectorAll('.category-tag-component')[0] && mutation2.addedNodes[i].querySelectorAll('.category-tag-component')[0].innerText == '18+' ) || 
  (mutation2.addedNodes[i].querySelectorAll('.category-tag-component')[1] && mutation2.addedNodes[i].querySelectorAll('.category-tag-component')[1].innerText == '18+' ) || 
  (mutation2.addedNodes[i].querySelectorAll('.category-tag-component')[2] && mutation2.addedNodes[i].querySelectorAll('.category-tag-component')[2].innerText == '18+' ) 
  ) {
                      mutation2.addedNodes[i].style.visibility = 'hidden';
                    }
                  }

                  if ( scbt.o.settingsFromSync.sz4b15 > 0 ) {
                    if ( mutation2.addedNodes[i].querySelector('img').alt == 'Other TV Shows Movies') {
                      mutation2.addedNodes[i].style.visibility = 'hidden';
                    }
                  }

                  if ( scbt.o.settingsFromSync.sz4b16 > 0 ) {
                    if ( mutation2.addedNodes[i].querySelector('img').alt == 'Just Sleeping') {
                      mutation2.addedNodes[i].style.visibility = 'hidden';
                    }
                  }

                  if ( scbt.o.settingsFromSync.sz4b17 > 0 ) {
                    if ( mutation2.addedNodes[i].querySelector('img').alt == 'ASMR') {
                      mutation2.addedNodes[i].style.visibility = 'hidden';
                    }
                  }

                  if ( scbt.o.settingsFromSync.sz4b18 > 0 ) {
                    if ( mutation2.addedNodes[i].querySelector('img').alt == 'Body Art') {
                      mutation2.addedNodes[i].style.visibility = 'hidden';
                    }
                  }

                  if ( scbt.o.settingsFromSync.sz4b19 > 0 ) {
                    if ( mutation2.addedNodes[i].querySelector('img').alt == 'Slots & Casino') {
                      mutation2.addedNodes[i].style.visibility = 'hidden';
                    }
                  }

                  if ( scbt.o.settingsFromSync.sz4b20 > 0 ) {
                    if ( mutation2.addedNodes[i].querySelector('img').alt == 'Pools, Hot Tubs & Bikinis') {
                      mutation2.addedNodes[i].style.visibility = 'hidden';
                    }
                  }

                  if ( scbt.o.settingsFromSync.sz4b21 > 0 ) {
                    if ( mutation2.addedNodes[i].querySelector('img').alt == 'Just Chatting') {
                      mutation2.addedNodes[i].style.visibility = 'hidden';
                    }
                  }

                }
                      
              }  // if (mutation.addedNodes) {
            }   // end if (mutation.type === 'childList') {      
       } // end for(const mutation of mutationsList) {
    };  // end var callback = function(mutationsList, observer) {

    if (scbt.o.categoryLoading) {
      scbt.o.categoryLoading.observer = new MutationObserver(callback2);
      scbt.o.categoryLoading.observer.observe(targetNode2, config2);
    }
    
  }
  return false;
}


scbt.f.hide_mature_livestreams_on_scroll = function(){
  targetNode3 = null;
  if (scbt.o.livestreamsLoading) {
    if (scbt.o.livestreamsLoading.observer) {
      scbt.o.livestreamsLoading.observer.disconnect();
      scbt.o.livestreamsLoading.observer = null;
      targetNode3 = null;
      config3 = null;
      callback3 = null;
    }
  }
  targetNode3 = document.body.querySelectorAll('#livestreams .place-content-stretch')[0];
  if (targetNode3) {
    var config3 = { childList: true,
                 attributes: false,
                 characterData: false,
                 subtree: false,
                 attributeOldValue: false,
                 characterDataOldValue: false 
               };

    var callback3 = function(mutationsList3, observer3) {
      for (let mutation3 of mutationsList3) {
          if (mutation3.type === 'childList') {
              if (mutation3.addedNodes) {
                var arrl3 = mutation3.addedNodes.length;
                for (var i=0; i < arrl3; i++) { 

                  if (mutation3.addedNodes[i]) {

                    if (mutation3.addedNodes[i].nodeName == '#text' || mutation3.addedNodes[i].nodeName == '#comment') {
                      continue;
                    }
                    if (mutation3.addedNodes[i] instanceof HTMLElement) {

                  if ( scbt.o.settingsFromSync.sz4b15 > 0 ) {
if ( mutation3.addedNodes[i].querySelector('.card-category-name').textContent == 'Other TV Shows Movies' || mutation3.addedNodes[i].querySelector('.card-category-name').textContent == 'Other, Watch Party' ) {
                      mutation3.addedNodes[i].style.visibility = 'hidden';
                    }
                  }

                  if ( scbt.o.settingsFromSync.sz4b16 > 0 ) {
                    if ( mutation3.addedNodes[i].querySelector('.card-category-name').textContent == 'Just Sleeping') {
                      mutation3.addedNodes[i].style.visibility = 'hidden';
                    }
                  }

                  if ( scbt.o.settingsFromSync.sz4b17 > 0 ) {
                    if ( mutation3.addedNodes[i].querySelector('.card-category-name').textContent == 'ASMR') {
                      mutation3.addedNodes[i].style.visibility = 'hidden';
                    }
                  }

                  if ( scbt.o.settingsFromSync.sz4b18 > 0 ) {
                    if ( mutation3.addedNodes[i].querySelector('.card-category-name').textContent == 'Body Art') {
                      mutation3.addedNodes[i].style.visibility = 'hidden';
                    }
                  }

                  if ( scbt.o.settingsFromSync.sz4b19 > 0 ) {
                    if ( mutation3.addedNodes[i].querySelector('.card-category-name').textContent == 'Slots & Casino') {
                      mutation3.addedNodes[i].style.visibility = 'hidden';
                    }
                  }

                  if ( scbt.o.settingsFromSync.sz4b20 > 0 ) {
                    if ( mutation3.addedNodes[i].querySelector('.card-category-name').textContent == 'Pools, Hot Tubs & Bikinis') {
                      mutation3.addedNodes[i].style.visibility = 'hidden';
                    }
                  }

                  if ( scbt.o.settingsFromSync.sz4b21 > 0 ) {
                    if ( mutation3.addedNodes[i].querySelector('.card-category-name').textContent == 'Just Chatting') {
                      mutation3.addedNodes[i].style.visibility = 'hidden';
                    }
                  }

                  if ( scbt.o.settingsFromSync.sz4b14 > 0 ) {
if ( 
  (mutation3.addedNodes[i].querySelectorAll('.category-tag-component')[0] && mutation3.addedNodes[i].querySelectorAll('.category-tag-component')[0].innerText == '18+' ) || 
  (mutation3.addedNodes[i].querySelectorAll('.category-tag-component')[1] && mutation3.addedNodes[i].querySelectorAll('.category-tag-component')[1].innerText == '18+' ) || 
  (mutation3.addedNodes[i].querySelectorAll('.category-tag-component')[2] && mutation3.addedNodes[i].querySelectorAll('.category-tag-component')[2].innerText == '18+' ) 
  ) {
                      mutation3.addedNodes[i].style.visibility = 'hidden';
                    }
                  }


                } // if (mutation3.addedNodes[i] instanceof HTMLElement)
              }   // if (mutation3.addedNodes[i]) {

            } // for (var i=0; i < mLength3; i++) { 

              }  // if (mutation.addedNodes) {
            }   // end if (mutation.type === 'childList') {      
       } // end for(const mutation of mutationsList) {
    };  // end var callback = function(mutationsList, observer) {

    if (scbt.o.livestreamsLoading) {
      scbt.o.livestreamsLoading.observer = new MutationObserver(callback3);
      scbt.o.livestreamsLoading.observer.observe(targetNode3, config3);
    }

  }
  return false;
}


scbt.f.check_video_speed = function(e){
  console.log('doing check_video_speed 1');
  if (e) {
    if (e.preventDefault) { e.preventDefault(); }
      if (e.target) {
        if (e.target.id) {
        } else {
          console.log('doing check_video_speed 2');
          var parentElm = e.target.parentElement;
          if (parentElm.id) {
            if (parentElm.id == 'scbtSpeed25') {
              document.body.getElementsByTagName('video')[0].playbackRate = 0.25;
            }
            if (parentElm.id == 'scbtSpeed50') {
              document.body.getElementsByTagName('video')[0].playbackRate = 0.50;
            }
            if (parentElm.id == 'scbtSpeed75') {
              document.body.getElementsByTagName('video')[0].playbackRate = 0.75;
            }
            if (parentElm.id == 'scbtSpeed1') {
              document.body.getElementsByTagName('video')[0].playbackRate = 1.0;
            }
            if (parentElm.id == 'scbtSpeed11') {
              document.body.getElementsByTagName('video')[0].playbackRate = 1.1;
            }
            if (parentElm.id == 'scbtSpeed125') {
              document.body.getElementsByTagName('video')[0].playbackRate = 1.25;
            }
            if (parentElm.id == 'scbtSpeed15') {
              document.body.getElementsByTagName('video')[0].playbackRate = 1.5;
            }
            if (parentElm.id == 'scbtSpeed2') {
              document.body.getElementsByTagName('video')[0].playbackRate = 2.0;
            }
          }
        }
      }
    }
  return false;
}


scbt.f.build_speed_button = function(){
  console.log('doing build_speed_button 1');
  var elemArr = document.body.querySelectorAll('.video-js .vjs-custom-control-spacer');
  if (elemArr[0]) {
    console.log('doing build_speed_button 2');
    elemArr[0].classList.add('vjs-menu-button-popup');
    var theHTML = '<button class="vjs-icon-hd vjs-icon-placeholder vjs-menu-button vjs-menu-button-popup vjs-button" type="button" aria-disabled="false" aria-haspopup="true" aria-expanded="false"><span class="vjs-icon-placeholder" aria-hidden="true"></span><span class="vjs-control-text" aria-live="polite">Video Speed</span></button><div class="vjs-menu scbt-speed"><ul class="vjs-menu-content" role="menu"><li id="scbtSpeed1" class="vjs-menu-item vjs-selected" tabindex="-1" role="menuitemradio" aria-disabled="false" aria-checked="true"><span class="vjs-menu-item-text">1</span><span class="vjs-control-text" aria-live="polite">, selected</span></li><li id="scbtSpeed25" class="vjs-menu-item" tabindex="-1" role="menuitemradio" aria-disabled="false" aria-checked="false"><span class="vjs-menu-item-text">.25</span><span class="vjs-control-text" aria-live="polite"></span></li><li id="scbtSpeed50" class="vjs-menu-item" tabindex="-1" role="menuitemradio" aria-disabled="false" aria-checked="false"><span class="vjs-menu-item-text">.5</span><span class="vjs-control-text" aria-live="polite"></span></li><li id="scbtSpeed75" class="vjs-menu-item" tabindex="-1" role="menuitemradio" aria-disabled="false" aria-checked="false"><span class="vjs-menu-item-text">.75</span><span class="vjs-control-text" aria-live="polite"></span></li><li id="scbtSpeed11" class="vjs-menu-item" tabindex="-1" role="menuitemradio" aria-disabled="false" aria-checked="false"><span class="vjs-menu-item-text">1.1</span><span class="vjs-control-text" aria-live="polite"></span></li><li id="scbtSpeed125" class="vjs-menu-item" tabindex="-1" role="menuitemradio" aria-disabled="false" aria-checked="false"><span class="vjs-menu-item-text">1.25</span><span class="vjs-control-text" aria-live="polite"></span></li><li id="scbtSpeed15" class="vjs-menu-item" tabindex="-1" role="menuitemradio" aria-disabled="false" aria-checked="false"><span class="vjs-menu-item-text">1.5</span><span class="vjs-control-text" aria-live="polite"></span></li><li id="scbtSpeed2" class="vjs-menu-item" tabindex="-1" role="menuitemradio" aria-disabled="false" aria-checked="false"><span class="vjs-menu-item-text">2</span><span class="vjs-control-text" aria-live="polite"></span></li></ul></div>';
    elemArr[0].insertAdjacentHTML('afterbegin', theHTML);
    elemArr[0].style.display = 'block';
    elemArr[0].addEventListener('click', scbt.f.toggle_video_speed_menu);
    setTimeout(function() {
      var arr = document.body.querySelectorAll('.scbt-speed')[0].getElementsByClassName('vjs-menu-item');
      var arrl = arr.length;
      for (var i = 0; i < arrl; i++) {
        arr[i].addEventListener('click', scbt.f.check_video_speed);
      }
    }, 1000);
  }
  return false;
}


scbt.f.build_share_button = function(){
  console.log('doing build_share_button 1');
  var elemArr = document.body.querySelectorAll('#main-view .items-center.justify-end');
  if (elemArr[0]) {
    console.log('doing build_share_button 2');
    var theHTML = '<button data-v-80dcaa82="" class="scbtCopyTimeStampButton variant-action size-sm !hidden sm:!flex" id="scbtCopyTimeStampButton"><div data-v-80dcaa82="" class="button-content"><div data-v-80dcaa82="" class="inner-label">Copy timestamp at <span id="scbtCopyTimeStamp" class="scbtCopyTimeStamp"></span></div></div></button>';
    elemArr[0].insertAdjacentHTML('afterbegin', theHTML);
    setTimeout(function() { 
      var elem2Arr = document.body.getElementsByClassName('scbtCopyTimeStampButton');
      if (elem2Arr[0]) {
        elem2Arr[0].addEventListener('click', scbt.f.share_button_handler);
      }
      elemArr = elem2Arr = theHTML = null; return false;
    }, 1000);
  }
}


scbt.f.share_button_handler = function(e){
  var elemArr = scbt.f.get_arr_video_elem();
  if (elemArr[0]) {
    if (elemArr[0].readyState > 0) {
      var theTime = elemArr[0].currentTime;
      var minutes = parseInt(theTime / 60, 10);
      var seconds = parseInt(theTime % 60);
      var elem = document.body.querySelector('#scbtCopyTimeStamp');
      if (elem) {
        elem.textContent = minutes + ':' + seconds + ' into the video';
      }
      scbt.v.linkToShare = location.origin + location.pathname + '?t=' + parseInt(theTime, 10);
      scbt.f.copy_text_to_clipboard(scbt.v.linkToShare);
      if (navigator.share) {
        navigator.share({
          title: 'From ' + scbt.v.channelid + ' on ' + scbt.v.serviceid,
          text: scbt.v.linkToShare,
          url: scbt.v.linkToShare,
        })
        .then(() => console.log('Successful share'))
        .catch((error) => console.error('Error sharing', error));
      }
    }
  }
  return false;
}



scbt.f.toggle_video_speed_menu = function(e) {
  if (e) {
    if (e.preventDefault) { e.preventDefault(); }
  }
  var elemArr = document.body.querySelectorAll('.video-js .vjs-custom-control-spacer');
  if (elemArr[0]) {
    if (elemArr[0].classList.contains('vjs-hover') ) {
      elemArr[0].classList.remove('vjs-hover');
    } else {
      elemArr[0].classList.add('vjs-hover');
    }
  }
  elemArr = e = null; return false;
}


scbt.f.load_live_vod = function(e) {
  if (scbt.b.popout === true) {
    scbt.f.toast(' Live VOD not available on pop-out chat. Please try in desktop or mobile viewing. '); return false;
  }
  var elemArr = document.body.getElementsByClassName('scbtVideo');
  if (elemArr[0] && ( (!elemArr[0].src) || (elemArr[0].src && elemArr[0].src != location.href)) ) {
    elemArr[0].classList.remove('scbt-bl');
    elemArr[0].src = '';
    return false;
  }
  fetch('https://kick.com/api/v1/channels' + location.pathname,
  {
    method: 'GET',
    mode: 'cors',
    cache: 'no-cache',
  })
  .then(function(res){ if (res.ok) { return res.json(); } else { return Promise.reject(res.status); } })
  .then(function(resp){ 
  if ( (resp == null || resp.length === 0) || (isNaN(resp) == false) ) { console.error('api error1', resp); } else {
    if (resp == 'error') {
      console.error('api error2', resp); serviceid = channelid = videoid = res = arr = arrl = resp = error = str = null; return false;
    } else {
        if (typeof resp === 'object') {
          if (resp && resp.previous_livestreams && resp.previous_livestreams[0].video && resp.previous_livestreams[0].video.uuid ) {
            var elemArr = document.body.getElementsByClassName('scbtVideo');
            if (elemArr[0]) {
              elemArr[0].src = 'https://kick.com/video/' + resp.previous_livestreams[0].video.uuid;
              elemArr[0].classList.add('scbt-bl');
              setTimeout(function(){ 
                var iframe = document.getElementById('scbtVideo');
                var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
                var elemArr = innerDoc.body.querySelectorAll('div.cursor-pointer.opacity-100');
                if (elemArr[0]) {
                  elemArr[0].click();
                }
                var elemArr = innerDoc.body.querySelectorAll('.main-html.h-screen.w-screen .flex.h-full.w-full > .flex.grow.overflow-hidden:first-of-type > div.relative:first-of-type');
                if (elemArr[0]) {
                  elemArr[0].style.display = 'none';
                }
                return false; 
              }, 4000);
            }
          }

        } else {
          console.error('api error3', resp); serviceid = channelid = videoid = res = arr = arrl = resp = error = str = null; return false;
        }

      }
    }
  })
  .catch(error => {
     console.error('api error4', error); serviceid = channelid = videoid = res = arr = arrl = resp = error = str = null; return false;
  });
  return false;
}



scbt.f.load_vod_chat = function() {
  console.log(' doing load_vod_chat 1');
  if (scbt.o.settingsFromSync.sz5b20 === true) {
    console.log(' doing load_vod_chat 2', scbt.a.savedStreams);
    if ( scbt.a.savedStreams ) {
      console.log(' doing load_vod_chat 3');
      // savedchat&kick&bige&2023-10-11-bige  location.pathname /video/
      // scbt.f.load_local_chat_from_api = function(serviceid, channelid, videoid){
      // created_at: "2024-06-01T21:32:56.000000Z"
      var arrl =  scbt.a.savedStreams.length;
      for (var i = 0; i < arrl; i++) {
        console.log(scbt.a.savedStreams[i].channelid + ' vs ' + scbt.v.channelid);
        if ( scbt.a.savedStreams[i].channelid.indexOf( scbt.v.channelid ) > -1) {
          var str = 'https://kick.com/api/v1' + location.pathname;
          scbt.f.load_local_chat_from_api(str);    
        }
      }
    }
  }
  str = null; return false;
}



scbt.f.load_local_chat_from_api = function(str){
  console.log(' doing load_local_chat_from_api 1 ', str);
  fetch(str,
  {
    method: 'GET',
    mode: 'cors',
    cache: 'no-cache',
  })
  .then(function(res){ if (res.ok) { return res.json(); } else { return Promise.reject(res.status); } })
  .then(function(resp){ 
  if ( (resp == null || resp.length === 0) || (isNaN(resp) == false) ) { console.error('api error1', resp); } else {
    if (resp == 'error') {
      console.error('api error2', resp); serviceid = channelid = videoid = res = arr = arrl = resp = error = str = null; return false;
    } else {
      console.log(' doing load_local_chat_from_api 2', resp);
      if (typeof resp === 'object') {
        console.log(' doing load_local_chat_from_api 3', resp.created_at);
        if (resp.created_at) {
          console.log(' doing load_local_chat_from_api 4', resp.created_at);
          var arr = resp.created_at.split('T'); // created_at: "2024-06-01 T 
          if (arr[0]) {
            console.log(' doing load_local_chat_from_api 5', arr[0]);
            var arrl =  scbt.a.savedStreams.length;
            for (var i = 0; i < arrl; i++) {
              console.log(scbt.a.savedStreams[i].videoid + ' vs ' + arr[0]);
              if ( scbt.a.savedStreams[i].videoid.indexOf( arr[0] ) > -1 ) {
                console.log(' doing load_local_chat_from_api 6', scbt.a.savedStreams[i].videoid);
                scbt.f.load_local_chat( scbt.a.savedStreams[i].dbName );    
              }
            }
          }
        }

      } else {
        console.error('api error3', resp); serviceid = channelid = videoid = res = arr = arrl = resp = error = str = null; return false;
      }

      }
    }
  })
  .catch(error => {
     console.error('api error4', error); serviceid = channelid = videoid = res = arr = arrl = resp = error = str = null; return false;
  });
  return false;
}



scbt.f.hide_mature_elements_on_page_load();
if (scbt.v.serviceid == 'kick' && scbt.b.vod === true) {
  setTimeout(function() { scbt.f.build_speed_button();  scbt.f.build_share_button(); scbt.f.load_vod_chat(); }, 10000);
}