/* SPDX-FileCopyrightText: © 2024 promising future digital media llc. All rights reserved. <admin@parleystar.com> */
/* SPDX-License-Identifier: Mozilla Public License 1.1 (MPL-1.1) */
if ( window.hasOwnProperty('scbt') ) {  } else { window.scbt = {} }
if ( window.scbt.hasOwnProperty('a') ) {  } else { window.scbt.a = {} }
if ( window.scbt.hasOwnProperty('b') ) {  } else { window.scbt.b = {} }
if ( window.scbt.hasOwnProperty('e') ) {  } else { window.scbt.e = {} }
if ( window.scbt.hasOwnProperty('f') ) {  } else { window.scbt.f = {} }
if ( window.scbt.hasOwnProperty('n') ) {  } else { window.scbt.n = {} }
if ( window.scbt.hasOwnProperty('o') ) {  } else { window.scbt.o = {} }  
if ( window.scbt.hasOwnProperty('c') ) {  } else { window.scbt.c = {} }
if ( window.scbt.hasOwnProperty('v') ) {  } else { window.scbt.v = {} }

// arrays
var arr = [
'blockedWords', 
'followedChannels', 
'savedStreams', 
'savingChatIds',
'searchingMessageIds',
'socialMedias',
'spokenWords', 
'usernames'
];
var len = arr.length;
for (var i = 0; i < len; i++) {
  var x = arr[i];  if ( scbt.a.hasOwnProperty(x) ){} else { scbt.a[x] = [] }
}
len = x = i = null;

// booleans
arr = [
'channelPage',
'fullScreenHeight',
'fullScreenWidth',
'keybindOn',
'mobile',
'popout',
'searchBarActive',
'vod',
'vodLoaded',
'vodLoadedComments',

'simulcastReceiver',
'simulcastSender',

'archyvedLoaded', // script has loaded
'arenaLoaded',
'instagramLoaded',
'kickLoaded',
'noiceLoaded',
'odyseeLoaded',
'rumbleLoaded',
'shareplayLoaded',
'tiktokLoaded',
'twitchLoaded',
'twitterLoaded',
'youtubeLoaded',

'sz1b1archyved', // text only mode
'sz1b1arena', 
'sz1b1instagram',
'sz1b1kick',
'sz1b1noice',
'sz1b1odysee',
'sz1b1rumble',
'sz1b1shareplay',
'sz1b1tiktok',
'sz1b1twitch',
'sz1b1twitter',
'sz1b1youtube',

'sz3b1', // non mod subs mute
'sz3b2', // streamer mute
'sz3b3', // mention mute
'sz3b4', // bot mute
'sz3b5', // follow mute
'sz3b6', // subscribe mute

'sz4b1', // non mod subs hide
'sz4b2', // streamer hide
'sz4b3', // mention hide
'sz4b4', // bot hide
'sz4b5', // follow hide
'sz4b6', // subscribe hide

'sz4b7', // usernames in chat hide
'sz4b8', // usernames on screen hide
'sz4b9', // sexual words hide
'sz4b10', // profanity words hide
'sz4b11', // political words hide
'sz4b12', // negative words hide
'sz4b13', // custom words hide

'sz4b14', // 18+ hide
'sz4b15', // other-tv-shows-movies  hide
'sz4b16', // just-sleeping  hide
'sz4b17', // asmr  hide
'sz4b18', // body-art  hide
'sz4b19', // gambling  hide
'sz4b20', // pools-hot-tubs hide
'sz4b21', // just-chatting hide

'sz4b22', // links hide

'sz5b1', // Load Save/Search Chat
'sz5b2', // Load Simulcast Chat
'sz5b3', // Load Chat Filters
'sz5b4', // Load Keybind Commands
'sz5b5', // Load Voice Control
// 'sz5b6',  Show Side Menu
// 'sz5b7', Show Chat Menu
'sz5b8', // Show Search Menu
'sz5b9', // Show Mention Menu
'sz5b10', // Show Simulcast Chat
'sz5b11', // Show Social Media Menu
'sz5b12', // basic keybinds
'sz5b13', // full keybinds
'sz5b14', // voice commands
'sz5b15', // chat on screen
'sz5b16', // left hand chat
'sz5b17', // upside down chat
'sz5b18', // hover enlarge
'sz5b19', // press to mention
'sz5b20', // load chat for vod auto
'sz5b21', // save chat auto

'sz5b30', // Simulcast is receiver
'sz5b31',  // Simulcast is sender
'sz5b32',  // Simulcast is sender
'sz5b33'  // Simulcast is sender
];
len = arr.length;
for (i = 0; i < len; i++) {
  x = arr[i];  if ( scbt.b.hasOwnProperty(x) ){} else { scbt.b[x] = false }
}
len = x = i = null;

// elements
var d = document.createElement("div");
document.body.appendChild(d);
var arr = [
  'scbtX',
  'scbtSettingsMenu',
  'scbtSettingsMenuForm',
  'scbtChatImportButton',
  'scbtSortStreamsByServiceID',
  'scbtSortStreamsByChannelID',
  'scbtSortStreamsByVideoID',
  'scbtSortStreamsByChan',
  'scbtPreviousStreams',
  
  'scbtChatMenu',
  'scbtChatMenuToggleButton',
  'scbtChatSearchInputText',
  'scbtChatSearchStartsWithButton',
  'scbtChatSearchUserButton',
  'scbtChatSearchKeywordButton',
  'scbtChatSearchEventsButton',
  'scbtChatSearchFullButton',
  'scbtChatSearchMultiButton',

  'scbt4',
  'scbt5',
  'scbt6',
  'scbt7',
  'scbt8',
  'scbt9',
  'scbt11',
  'scbt12',
  'scbt10',
  'scbt13',

  'scbtChatInput',
  'scbtChatTitle',
  'scbtChatA',
  'scbtChatB',
  'scbtCloseButton',
  'scbtDragSpot1',
  'scbtDragSpot2',
  'scbtDragSpot3',
  'scbtDragSpot4',
  'scbtSimulcastChat1',
  'scbtSimulcastChat2',
  'scbtSimulcastChat3',
  'scbtSavedChat1',
  'scbtMentionMenu',
  'scbtSettingsImportButton',
  'scbtSettingsExportButton',
  'scbtSettingsDeleteButton',
  'scbtLoading',
  'scbtClipsMenuWrapper',
  'scbtClipsMenuContent',
  'scbtStreamsMenuContent'
];
var len = arr.length;
for (var i = 0; i < len; i++) {
  var x = arr[i];  if ( scbt.e.hasOwnProperty(x) ){} else { scbt.e[x] = d; }
}
len = x = i = d = null;

// numbers
arr = [
'nonBotChatShow',
'subChatShow',
'textOnlyChatShow',
'verifiedChatShow',
'visibilityChatShow',
'vodSecondsTotal',
'vodMinutesLong',
'vodSecondsLong',

'sz1n1archyved', // fontsize
'sz1n1arena', 
'sz1n1instagram',
'sz1n1kick',
'sz1n1noice',
'sz1n1odysee',
'sz1n1rumble',
'sz1n1shareplay',
'sz1n1tiktok',
'sz1n1twitch',
'sz1n1twitter',
'sz1n1youtube'
];
len = arr.length;
for (i = 0; i < len; i++) {
  x = arr[i];  if ( scbt.n.hasOwnProperty(x) ){} else { scbt.n[x] = 1 }
}
len = x = i = null;

// objects
arr = [
'categoryLoading',
'livestreamsLoading',
'sexualterms',
'profanity',
'political',
'negative'
];
len = arr.length;
for (i = 0; i < len; i++) {
  x = arr[i];  if ( scbt.o.hasOwnProperty(x) ){} else { scbt.o[x] = {} }
}
len = x = i = null;

// ss
if ( scbt.c.hasOwnProperty('scbtBorderColor') ) {  } else { scbt.c.scbtBorderColor = '#ff0000' }

// color
arr = [
'sz1c1archyved', // background
'sz1c1arena', 
'sz1c1instagram',
'sz1c1kick',
'sz1c1noice',
'sz1c1odysee',
'sz1c1rumble',
'sz1c1shareplay',
'sz1c1tiktok',
'sz1c1twitch',
'sz1c1twitter',
'sz1c1youtube',

'sz1c2archyved', // username
'sz1c2arena', 
'sz1c2instagram',
'sz1c2kick',
'sz1c2noice',
'sz1c2odysee',
'sz1c2rumble',
'sz1c2shareplay',
'sz1c2tiktok',
'sz1c2twitch',
'sz1c2twitter',
'sz1c2youtube',

'sz1c3archyved', // highlighted
'sz1c3arena', 
'sz1c3instagram',
'sz1c3kick',
'sz1c3noice',
'sz1c3odysee',
'sz1c3rumble',
'sz1c3shareplay',
'sz1c3tiktok',
'sz1c3twitch',
'sz1c3twitter',
'sz1c3youtube',

'sz1c4archyved', // message
'sz1c4arena', 
'sz1c4instagram',
'sz1c4kick',
'sz1c4noice',
'sz1c4odysee',
'sz1c4rumble',
'sz1c4shareplay',
'sz1c4tiktok',
'sz1c4twitch',
'sz1c4twitter',
'sz1c4youtube',

'sz2c1archyved', // sub
'sz2c1arena', 
'sz2c1instagram',
'sz2c1kick',
'sz2c1noice',
'sz2c1odysee',
'sz2c1rumble',
'sz2c1shareplay',
'sz2c1tiktok',
'sz2c1twitch',
'sz2c1twitter',
'sz2c1youtube',

'sz2c2archyved', // mod
'sz2c2arena', 
'sz2c2instagram',
'sz2c2kick',
'sz2c2noice',
'sz2c2odysee',
'sz2c2rumble',
'sz2c2shareplay',
'sz2c2tiktok',
'sz2c2twitch',
'sz2c2twitter',
'sz2c2youtube',

'sz2c3archyved', // mention
'sz2c3arena', 
'sz2c3instagram',
'sz2c3kick',
'sz2c3noice',
'sz2c3odysee',
'sz2c3rumble',
'sz2c3shareplay',
'sz2c3tiktok',
'sz2c3twitch',
'sz2c3twitter',
'sz2c3youtube',

'sz2c4archyved', // hashtag
'sz2c4arena', 
'sz2c4instagram',
'sz2c4kick',
'sz2c4noice',
'sz2c4odysee',
'sz2c4rumble',
'sz2c4shareplay',
'sz2c4tiktok',
'sz2c4twitch',
'sz2c4twitter',
'sz2c4youtube',

'sz2c5archyved', // VIP
'sz2c5arena', 
'sz2c5instagram',
'sz2c5kick',
'sz2c5noice',
'sz2c5odysee',
'sz2c5rumble',
'sz2c5shareplay',
'sz2c5tiktok',
'sz2c5twitch',
'sz2c5twitter',
'sz2c5youtube',

'sz2c6archyved', // founder
'sz2c6arena', 
'sz2c6instagram',
'sz2c6kick',
'sz2c6noice',
'sz2c6odysee',
'sz2c6rumble',
'sz2c6shareplay',
'sz2c6tiktok',
'sz2c6twitch',
'sz2c6twitter',
'sz2c6youtube',

'sz2c7archyved', // OG
'sz2c7arena', 
'sz2c7instagram',
'sz2c7kick',
'sz2c7noice',
'sz2c7odysee',
'sz2c7rumble',
'sz2c7shareplay',
'sz2c7tiktok',
'sz2c7twitch',
'sz2c7twitter',
'sz2c7youtube',

'sz2c8archyved', // streamer
'sz2c8arena', 
'sz2c8instagram',
'sz2c8kick',
'sz2c8noice',
'sz2c8odysee',
'sz2c8rumble',
'sz2c8shareplay',
'sz2c8tiktok',
'sz2c8twitch',
'sz2c8twitter',
'sz2c8youtube',

'sz2c9archyved', // gifter
'sz2c9arena', 
'sz2c9instagram',
'sz2c9kick',
'sz2c9noice',
'sz2c9odysee',
'sz2c9rumble',
'sz2c9shareplay',
'sz2c9tiktok',
'sz2c9twitch',
'sz2c9twitter',
'sz2c9youtube'
];
len = arr.length;
for (i = 0; i < len; i++) {
  x = arr[i];  if ( scbt.c.hasOwnProperty(x) ){} else { scbt.c[x] = '#000000' }
}
len = x = i = null;

// variables/strings
arr = [
'channelid',
'serviceid',
'videoid',
'lastPage',
'dbName',
'dbNameToSearch',
'fontSize',
'fontUp',
'heightSize',
'searchChat',
'simulcastReceiverTabId', 
'lastPage',

'sz2v1', // VIP list
'sz2v2', // highlight words
'sz4v1', // Hide Words in Chat
'sz4v2', // Hide Chat from Usernames
'sz5v1', // speak these words

'sz5v30', // url of simulcast receiver
'sz5v31',  // url of simulcast sender
'sz5v32',  // url of simulcast sender
'sz5v33'  // url of simulcast sender
];
len = arr.length;
for (i = 0; i < len; i++) {
  x = arr[i];  if ( scbt.v.hasOwnProperty(x) ){} else { scbt.v[x] = '' }
}
len = x = i = null;

// functions
arr = [
'do_settings',

  'get_arr_chatbox_elem',
  'get_arr_chats',
  'get_arr_from_dbName_string',
  'get_arr_message_elems_from_parent_element',
  'get_arr_sortedtimes_from_arr',
  'get_arr_username_elems_from_parent_element',
  'get_arr_video_elem',
  'get_number_seconds_difference_from_two_timestamps',
  'get_obj_cleaned_message_from_obj',
  'get_obj_cleaned_username_from_obj',
  'get_obj_filter_blocked_links_from_obj',
  'get_obj_filter_blocked_users_from_obj',
  'get_obj_filter_blocked_words_from_obj',
  'get_obj_filter_highlighted_words_from_obj',
  'get_obj_filter_vip_users_from_obj',
  'get_str_channelid',
  'get_str_for_chat_classes_from_arr',
  'get_str_for_chat_classes_from_obj',
  'get_str_for_search',
  'get_str_military_hours_minutes_from_timestamp',
  'get_str_seconds_from_hour_minute_time',
  'get_str_serviceid',
  'get_str_videoid',

'get_vod_length',
'load_live_vod',

  'set_page_values_to_reset',
  'set_page_values_from_url',
  'set_chat_parameter',
  'set_db_error_message',
  'set_vod_length',

  'toast',
  'make_toast',
  'build_chat_line_from_obj',
  'build_chat_line_from_arr',
  'build_list_of_saved_stream_chat_by_arr',
  'build_top_x_menu',

  'chat_auto_show',
  'chat_blur',
  
  'chat_clean',
  'chat_clean_arena',
  'chat_clean_instagram',
  'chat_clean_kick',
  'chat_clean_noice',
  'chat_clean_odysee',
  'chat_clean_rumble',
  'chat_clean_shareplay',
  'chat_clean_tiktok',
  'chat_clean_twitch',
  'chat_clean_twitter',
  'chat_clean_youtube',

  'chat_make_decisions',
  'chat_make_decisions_arena',
  'chat_make_decisions_instagram',
  'chat_make_decisions_kick',
  'chat_make_decisions_noice',
  'chat_make_decisions_odysee',
  'chat_make_decisions_rumble',
  'chat_make_decisions_shareplay',
  'chat_make_decisions_tiktok',
  'chat_make_decisions_twitch',
  'chat_make_decisions_twitter',
  'chat_make_decisions_youtube',

  'style_with_obj_of_changes',
  'style_with_obj_of_changes_arena',
  'style_with_obj_of_changes_instagram',
  'style_with_obj_of_changes_kick',
  'style_with_obj_of_changes_noice',
  'style_with_obj_of_changes_odysee',
  'style_with_obj_of_changes_rumble',
  'style_with_obj_of_changes_shareplay',
  'style_with_obj_of_changes_tiktok',
  'style_with_obj_of_changes_twitch',
  'style_with_obj_of_changes_twitter',
  'style_with_obj_of_changes_youtube',

'drag',
'drop',
'allowDrop',
'toggle_simulcast_chat',
'add_listener_for_chat_close',

  'chat_down_to_bottom',
  'chat_focus',
  'chat_font_size',
  'chat_full_screen_height',
  'chat_full_screen_width',
  'chat_listen',
  'chat_load_by_videoid',
  'chat_non_bot',
  'chat_off',
  'chat_on',
  'chat_set_to_hide',
  'chat_set_to_highlight',
  'chat_set_to_mute',
  'chat_set_to_show',
  'chat_text_only',
  'chat_up_to_top',
  'copy_text_to_clipboard',
  'do_coming_soon_message',
  'full_screen_video',
  'go_to_stream',
  'go_to_timestamp_in_video',
  'is_element_visible',
  'notify_background_page',
  'process_chat_line',
  'share_native_disabled',
  'start_chat_logging',
  'theatre_mode',
  'show_settings_error',
  'handle_js_loading_error',

'toggle_chats',
'toggle_chats_kick',
'toggle_chats_noice',
'toggle_chats_odysee',
'toggle_chats_rumble',
'toggle_chats_tiktok',
'toggle_chats_twitch',
'toggle_chats_youtube',

'loading_event_1',
'loading_event_2',
'loading_event_3',

'script_js_loaded',
'get_sexual_json',
'get_profanity_json',
'get_political_json',
'get_negative_json',
'save_word_list',

'user_command1',
'user_command2',
'user_command3',
'user_command4',
'keybind_close',
'options_turn_on_keybinds',

'get_csv_file_from_str',
'csv_download',
'csv_import_chat_log_from_chatarr',
'add_listener_for_uploading_chatlog',

  'get_binary_if_db_exists',
  'get_arr_of_all_dbNames',
  'build_chat_by_dbName_string',
  'save_bulk_chat_from_dbName_arr',
  'chat_delete_by_videoid',
  'chat_mark_by_videoid',
  'chat_export_by_videoid',
  'chat_export_to_offline',
  'get_usernames_for_mention_menu',
  'search_multiple_saved_chat',
  'search_saved_chat',
  'set_db_for_saving',
  'set_db_save_chat_obj',
  'get_str_dbName',
  'toggle_user_chats_menu',
  'load_local_chat',
  'toggle_search_multiple_chat',

'handler_sort_saved_streams_by_serviceid',
'handler_sort_saved_streams_by_channelid',
'handler_sort_saved_streams_by_videoid',
'handler_sort_saved_streams_by_current',
'handler_click_username_insert_into_search',

'search_for_saved_chat',
'load_chat_replay_from_api',
'get_chat_for_replay_from_api',
'populate_chat_for_replay',

'scbt_helper_load_chat_replay_from_api',
'scbt_helper_loop_through_resources_from_api_for_chat_replay',
'scbt_helper_get_chat_for_replay_from_api',
'scbt_helper_populate_chat_for_replay',

'chat_speak',
'turn_off_voice_commands',
'turn_on_voice_commands',

'youtube_setup_1',
'youtube_setup_2',

'toggle_clips_menu',
'get_binary_role_from_chat_message',
'something_for_kick',
'shuffle_array',
'get_videoObj',
'get_videoObj_populated',
'clear_recent_clips_begin',
'load_recent_clips_begin',
'loop_through_followed',
'loop_through_recent',
'get_followed_channels_from_api',
'get_stream_from_api',
'get_clips_from_api',
'get_recent_clips_from_api',
'build_recent_feed',
'hide_mature_elements_on_page_load',
'hide_mature_categories_on_scroll',
'hide_mature_livestreams_on_scroll',
'check_video_speed',
'build_speed_button',
'build_share_button',
'share_button_handler',
'load_local_chat_from_api',
'scbt_toggle_video_speed_menu',
'add_listener_for_importing_settings',
'add_listener_for_exporting_settings',
'add_listener_for_deleting_settings',
'set_setting_item',
'settings_delete',
'set_chat_obj_to_database',
'set_settings_changes',
'load_vod_chat',
'do_loading_event_2',
'sz5b30_off',
'sz5b30_on',
'sz5b31_off',
'sz5b31_on',
'sz5b32_off',
'sz5b32_on',
'sz5b33_off',
'sz5b33_on',
'get_formatted_channelid',
'toggle_chat_menu',
'set_chat_arr_to_database'
];
len = arr.length;
for (i = 0; i < len; i++) {
  x = arr[i];  if ( scbt.f.hasOwnProperty(x) ){} else { scbt.f[x] = function(){} }
}
len = x = i = null;

if ( window.scbt.o.hasOwnProperty('settingsFromSync') ) {  } else { window.scbt.o.settingsFromSync = false; }
// end beginning


scbt.f.show_settings_error = function(error){
  if (error) {
    console.log('in show_settings_error: ', error);
    console.error(error);
  }
  if (chrome.runtime.lastError) {
    console.log(chrome.runtime.lastError);
    alert(chrome.runtime.lastError.message);
  }
  error = null; return false;
}


scbt.f.do_settings = function(settings){
  if (settings) {
    scbt.o.settingsFromSync = settings;
  }
  console.log('in do_settings, scbt.o.settingsFromSync is', scbt.o.settingsFromSync);

  // set up save handlers for checkboxes
  var x = null;
  var item = null;
  var error = null;
  var arr = Object.getOwnPropertyNames(scbt.b);
  [].forEach.call(arr, function(item) {
    if (item.indexOf('sz') !== -1) {
      x = document.getElementById(item);

      if (x) {
        if (scbt.o.settingsFromSync[item]) {
          x.checked = scbt.o.settingsFromSync[item];
          scbt.b[item] = scbt.o.settingsFromSync[item];
          if (scbt.o.settingsFromSync[item] === true) {
            if (item == 'sz5b15') {
              scbt.f.chat_auto_show();
            }
            var obj = {};
            obj[item] = scbt.o.settingsFromSync[item];
            scbt.f.style_with_obj_of_changes(obj);
          }
        } else {
          x.checked = false;
          scbt.b[item].checked = false;
          if (scbt.o.settingsFromSync[item] === false) {
            var obj = {};
            obj[item] = scbt.o.settingsFromSync[item];
            scbt.f.style_with_obj_of_changes(obj);
          }
        }
        x.addEventListener('change', e => {
          if (e.target.checked) {
            chrome.storage.sync.set({ [item]: true}, function(error) { scbt.f.show_settings_error(error); })
          } else {
            chrome.storage.sync.set({ [item]: false}, function(error) { scbt.f.show_settings_error(error); })
          }
        });
      }
    }
  });

  // set up save handlers for number inputs
  x = null;
  item = null;
  error = null;
  arr = Object.getOwnPropertyNames(scbt.n);
  [].forEach.call(arr, function(item) {
    if (item.indexOf('sz') !== -1) {
      x = document.getElementById(item);
      if (x) {
        if (scbt.o.settingsFromSync[item]) {
          x.value = scbt.o.settingsFromSync[item];
          scbt.n[item] = scbt.o.settingsFromSync[item];
          var obj = {};
          obj[item] = scbt.o.settingsFromSync[item];
          scbt.f.style_with_obj_of_changes(obj);
        } else {
          x.value = '0';
          scbt.n[item].value = '0';
        }
        x.addEventListener('change', e => {
          if (e.target.value) {
            chrome.storage.sync.set({ [item]: e.target.value}, function(error) { scbt.f.show_settings_error(error); })
          } else {
            chrome.storage.sync.set({ [item]: ''}, function(error) { scbt.f.show_settings_error(error); })
          }
        });
      }
    }
  });

  // set up save handlers for color inputs
  x = null;
  item = null;
  error = null;
  arr = Object.getOwnPropertyNames(scbt.c);
  [].forEach.call(arr, function(item) {
    if (item.indexOf('sz') !== -1) {
      x = document.getElementById(item);
      if (x) {
        if (scbt.o.settingsFromSync[item]) {
          x.value = scbt.o.settingsFromSync[item];
          scbt.c[item] = scbt.o.settingsFromSync[item];
          var obj = {};
          obj[item] = scbt.o.settingsFromSync[item];
          scbt.f.style_with_obj_of_changes(obj);
        } else {
          x.value = '#000000';
          scbt.c[item].value = '#000000';
        }
        x.addEventListener('change', e => {
          if (e.target.value) {
            chrome.storage.sync.set({ [item]: e.target.value}, function(error) { scbt.f.show_settings_error(error); })
          } else {
            chrome.storage.sync.set({ [item]: '#000000'}, function(error) { scbt.f.show_settings_error(error); })
          }
        });
      }
    }
  });

  // set up save handlers for text inputs
  x = null;
  item = null;
  error = null;
  var url = '';
  if ( location.href.indexOf('?') > -1) {
    url = location.href + '&p=' + scbt.v.serviceid + '-' + scbt.v.channelid;
  } else {
    url = location.href + '/?p=' + scbt.v.serviceid + '-' + scbt.v.channelid;
  }
  arr = Object.getOwnPropertyNames(scbt.v);
  [].forEach.call(arr, function(item) {
    if (item.indexOf('sz') !== -1) {
      x = document.getElementById(item);
      if (x) {
        if (scbt.o.settingsFromSync[item]) {
          x.value = scbt.o.settingsFromSync[item];
          scbt.v[item] = scbt.o.settingsFromSync[item];
          // console.log(' item1 is: ', item);
          // console.log(' item2 is: ', scbt.o.settingsFromSync[item]);
          if (item == 'sz5v30' && scbt.o.settingsFromSync[item] == location.href) {
            scbt.b.simulcastReceiver = true;
          }
          if (item == 'sz5v31' && scbt.o.settingsFromSync[item] == url) {
            scbt.b.simulcastSender = true;
          }
          if (item == 'sz5v32' && scbt.o.settingsFromSync[item] == url) {
            scbt.b.simulcastSender = true;
          }
          if (item == 'sz5v33' && scbt.o.settingsFromSync[item] == url) {
            scbt.b.simulcastSender = true;
          }
        } else {
          x.value = '';
          scbt.v[item].value = '';
        }
        x.addEventListener('change', e => {
          if (e.target.value) {
            chrome.storage.sync.set({ [item]: e.target.value}, function(error) { scbt.f.show_settings_error(error); })
          } else {
            chrome.storage.sync.set({ [item]: ''}, function(error) { scbt.f.show_settings_error(error); })
          }
        });
      }
    }
  });



  return false;  
} // end do_settings


// GET FUNCTIONS 
scbt.f.get_arr_of_all_dbNames = function(){
  console.log('doing get_arr_of_all_dbNames in fg.js');
  // alert('Click the Features tab - Load Abilities or Not - Save/Search/Import - to turn this feature on.');
  e = null; return false;
}


scbt.f.get_arr_chatbox_elem = function(){
  var elemArr = [];
  if (scbt.v.serviceid == 'arena') {
    // TODO
  }
  if (scbt.v.serviceid == 'instagram') {
    // TODO
  }
  if (scbt.v.serviceid == 'kick') {
    elemArr = document.body.getElementsByClassName('overflow-y-scroll py-3');
  }
  if (scbt.v.serviceid == 'noice') {
    // TODO
  }
  if (scbt.v.serviceid == 'odysee') {
    elemArr = document.body.getElementsByClassName('livestream__comments');
  }
  if (scbt.v.serviceid == 'rumble') {
    elemArr = document.body.querySelectorAll('#chat-history-list');
  }
  if (scbt.v.serviceid == 'shareplay') {
    // TODO
  }
  if (scbt.v.serviceid == 'tiktok') {
    elemArr = document.body.querySelectorAll("div[class*='DivChatMessageList']");
  }
  if (scbt.v.serviceid == 'twitch') {
    if (scbt.b.vod) {
      elemArr = document.body.getElementsByClassName('InjectLayout-sc-1i43xsx-0 fZajyj'); // InjectLayout-sc-1i43xsx-0 fZajyj, .video-chat__message-list-wrapper ul
    } else {
      elemArr = document.body.getElementsByClassName('chat-scrollable-area__message-container');
    }
  }
  if (scbt.v.serviceid == 'twitter') {
    // TODO
  }
  if (scbt.v.serviceid == 'youtube') {
    if (scbt.e.scbtYTChatIframe && scbt.e.scbtYTChatIframe.body) {
      elemArr = scbt.e.scbtYTChatIframe.body.querySelectorAll('#items.yt-live-chat-item-list-renderer');
      console.log(' in scbt.f.get_arr_chatbox_elem elemArr is: ', elemArr);
    }
  }
  return elemArr;
}


scbt.f.get_arr_chats = function(){
  var elemArr = [];
  if (scbt.v.serviceid == 'arena') {
    // TODO
  }
  if (scbt.v.serviceid == 'instagram') {
    // TODO
  }
  if (scbt.v.serviceid == 'kick') {
    elemArr = document.body.getElementsByClassName('mt-0.5');
  }
  if (scbt.v.serviceid == 'noice') {
    // TODO
  }
  if (scbt.v.serviceid == 'odysee') {
    elemArr = document.body.getElementsByClassName('livestream__comment');
  }
  if (scbt.v.serviceid == 'shareplay') {
    // TODO
  }
  if (scbt.v.serviceid == 'rumble') {
    elemArr = document.body.getElementsByClassName('chat-history--row');
  }
  if (scbt.v.serviceid == 'tiktok') {
    elemArr = document.body.querySelectorAll('[data-e2e="chat-message"], [data-e2e="social-message"], .tiktok-15hhtcj, .tiktok-1fub1g4');
  }
  if (scbt.v.serviceid == 'twitch') {
    if (scbt.b.vod) {
      elemArr = document.body.querySelectorAll('.video-chat__message-list-wrapper li');
    } else {
      elemArr = document.body.querySelectorAll('.text-fragment, .user-notice-line, .chat-line__message--cheer-amount');
    }
  }
  if (scbt.v.serviceid == 'twitter') {
    // TODO
  }
  if (scbt.v.serviceid == 'youtube') {
    if (scbt.e.scbtYTChatIframe && scbt.e.scbtYTChatIframe.body) {
      var elemArr2 = scbt.e.scbtYTChatIframe.body.getElementsByClassName('yt-live-chat-item-list-renderer');
      if (elemArr2[5]) {
        elemArr = elemArr2[5].getElementsByClassName('yt-live-chat-item-list-renderer');
      }
      console.log('doing get_arr_chats with elemArr', elemArr);
    }
  }
  return elemArr;
}


scbt.f.get_arr_from_dbName_string = function(str){
  var arr = [];
  if (typeof str === 'string') {
    arr = str.split('&');
  }
  str = null; return arr;
}


scbt.f.get_arr_message_elems_from_parent_element = function(elem){
  var elemArr = [];
  if (elem) {
    if (scbt.v.serviceid == 'arena') {
      // TODO
    }
    if (scbt.v.serviceid == 'instagram') {
      // TODO
    }
    if (scbt.v.serviceid == 'kick') {
      elemArr = elem.getElementsByClassName('chat-entry-content');
    }
    if (scbt.v.serviceid == 'noice') {
      // TODO
    }
    if (scbt.v.serviceid == 'odysee') {
      elemArr = elem.getElementsByClassName('livestream-comment__text');
    }
    if (scbt.v.serviceid == 'shareplay') {
      // TODO
    }
    if (scbt.v.serviceid == 'rumble') {
      elemArr = elem.getElementsByClassName('chat-history--message');
    } 
    if (scbt.v.serviceid == 'tiktok') {
      elemArr = elem.querySelectorAll("div[class*='DivComment']");
    }
    if (scbt.v.serviceid == 'twitch') {
      elemArr = elem.getElementsByClassName('text-fragment');
    }
    if (scbt.v.serviceid == 'twitter') {
      // TODO
    }
    if (scbt.v.serviceid == 'youtube') {
      elemArr = elem.querySelectorAll('#message');
      if (elemArr[0]) {
        elem = null; return elemArr;
      }
    }
  }
  elem = null; return elemArr;
}


scbt.f.get_arr_sortedtimes_from_arr = function(arr){
  if (!arr) {
    return false;
  }
  if (!arr[1]) {
    return arr;
  }
  if (!arr[1].timestamp) {
    return arr;
  }
  if (typeof arr[1].timestamp === 'number') {
    return arr;
  }
  if (arr[1].timestamp.indexOf(':') < 0) {
    return arr;
  }
  return arr.sort(function (a, b) {
    if (parseInt(a.timestamp.split(':')[0]) - parseInt(b.timestamp.split(':')[0]) === 0) {
      return parseInt(a.timestamp.split(':')[1]) - parseInt(b.timestamp.split(':')[1]);
    } else {
      return parseInt(a.timestamp.split(':')[0]) - parseInt(b.timestamp.split(':')[0]);
    }
  })
}


scbt.f.get_arr_username_elems_from_parent_element = function(elem){
  var elemArr = [];
  if (elem) {
    if (scbt.v.serviceid == 'arena') {
      // TODO
    }
    if (scbt.v.serviceid == 'instagram') {
      // TODO
    }
    if (scbt.v.serviceid == 'kick') {
      elemArr = elem.getElementsByClassName('chat-entry-username');
    }
    if (scbt.v.serviceid == 'noice') {
      // TODO
    }
    if (scbt.v.serviceid == 'odysee') {
      elemArr = elem.getElementsByClassName('livestream-comment__meta-information');
    }
    if (scbt.v.serviceid == 'shareplay') {
      // TODO
    }
    if (scbt.v.serviceid == 'rumble') {
      elemArr = elem.getElementsByClassName('chat-history--username');
    }
    if (scbt.v.serviceid == 'tiktok') {
      elemArr = elem.getElementsByClassName('[data-e2e="message-owner-name"]');
    }
    if (scbt.v.serviceid == 'twitch') {
      elemArr = elem.getElementsByClassName('video-chat__message-author');
      if (elemArr[0]) {
        elem = null; return elemArr;
      }
      elemArr = elem.getElementsByClassName('chatter-name'); 
      if (elemArr[0]) {
        elem = null; return elemArr;
      }
      elemArr = elem.getElementsByClassName('chat-author__display-name');
      if (elemArr[0]) {
        elem = null; return elemArr;
      }
      elemArr = elem.getElementsByClassName('chat-line__username');
      if (elemArr[0]) {
        elem = null; return elemArr;
      }
      if (elem.classList.contains('user-notice-line') ) {
        elemArr = null; return elem;
      }
    }
    if (scbt.v.serviceid == 'twitter') {
      // TODO
    }
    if (scbt.v.serviceid == 'youtube') {
      elemArr = elem.querySelectorAll('#author-name');
    }
  }
  elem = null; return elemArr;
}


scbt.f.get_arr_video_elem = function(){
  var elemArr = document.body.getElementsByTagName('video');
  return elemArr;
}


scbt.f.get_formatted_channelid = function(str){
  if (str) {
    str = str.trim();
    str = str.toLowerCase();
    str = str.replace(/ /g, '_');
    str = str.replace(/\W/g, '');
  }
  return str;
}


scbt.f.get_number_seconds_difference_from_two_timestamps = function(startTime, timeToCompare){
  var diff = Math.abs(new Date('2011/11/11 ' + startTime) - new Date('2011/11/11 ' + timeToCompare));
  var diff2 = Math.floor((diff/1000)/60);
  var seconds = diff2 * 60;
  startTime = timeToCompare = diff = diff2 = null; return seconds;
}


scbt.f.get_obj_cleaned_message_from_obj = function(obj){
  if (obj && obj.message) {
    obj.message = obj.message.toLowerCase();
    obj.message = obj.message.replace(/\/‘’‚“”„"`~«´<>/g, '');
    obj.message = obj.message.replace(/,/g, ' ');
    obj.message = obj.message.replaceAll("’", "");
    obj.message = obj.message.trim();
    if (obj.message.charAt(0) == '!') {
      obj.isBot = 1;
    }
  }
  return obj;
}


scbt.f.get_obj_cleaned_username_from_obj = function(obj){
  if (obj && obj.username) {
    obj.username = obj.username.toLowerCase();
    obj.username = obj.username.replace(/\/‘’‚“”„"`~«´<>/g, '');
    obj.username = obj.username.replace(/,/g, ' ');
    obj.username = obj.username.replace(/:/g, '');
    obj.username = obj.username.trim();
    if ( ( obj.username.indexOf('bot') > -1 ) || ( obj.username == 'streamelements') || ( obj.username == 'streamlabs') || ( obj.username == 'tifa lockhart') || ( obj.username == 'kicklet') ) {
      obj.isBot = 1;
    }
  }
  return obj;
}


scbt.f.get_obj_filter_blocked_links_from_obj = function(obj, elem){
  // return obj;

  var elemArr = scbt.f.get_arr_message_elems_from_parent_element(elem);
  if (elemArr[0] && elemArr[0].textContent) {
    elem = elemArr[0].textContent;
    if (elem) {
      if ( (elem.indexOf('http') > -1 ) || (elem.indexOf('.com') > -1 ) || (elem.indexOf('xxx') > -1 ) || (elem.indexOf('onlyfans') > -1 ) || (elem.indexOf('www.') > -1 ) ) {
        elemArr[0].textContent = '---';
        obj.message = '---';
      }
    }
  }
  elemArr = elem = null; return obj;
}


scbt.f.get_obj_filter_blocked_users_from_obj = function(obj, elem){
  var arr = [];
  var tOriginal = '';
  var tMatch = '';
  var t2Original = '';
  var t2Match = '';
  var arrl = 0;

  var tOriginal = obj.username;
  if (tOriginal) {
    tMatch = tOriginal.toLowerCase().trim();
    arr = scbt.o.settingsFromSync.sz4v2.split(',');
    arrl = arr.length;
    for (var i = 0; i < arrl; i++) {
      t2Original = arr[i];
      if (t2Original) {
        t2Match = t2Original.toLowerCase().trim();
        if ( tMatch == t2Match) {
          obj.isHidden = 1;
        }
      }
    }
  }
  elem = arr = tOriginal = tMatch = t2Original = t2Match = arrl = i = null; return obj;
}


scbt.f.get_obj_filter_blocked_words_from_obj = function(obj, elem){
  var arr = [];
  var arr2 = [];
  var tOriginal = '';
  var tMatch = '';
  var t2Original = '';
  var t2Match = '';
  var elemArr = [];
  var fullArr = [];
  var arrl = 0;

  if (scbt.a.blockedWords.length > 0) {
    arr = scbt.a.blockedWords; // TODO
  }
  if (obj.anevent == 1) {
    return obj;
  }
  
  elemArr = scbt.f.get_arr_message_elems_from_parent_element(elem);
  if (elemArr[0] && elemArr[0].textContent) {
    tOriginal = elemArr[0].textContent;
    tMatch = tOriginal.toLowerCase().trim();
    arr2 = scbt.o.settingsFromSync.sz4v1.split(','); 
    fullArr = arr.concat(arr2);
    arrl = fullArr.length;
    for (var i = 0; i < arrl; i++) {
      t2Original = fullArr[i];
      if (t2Original) {
        t2Match = t2Original.toLowerCase().trim();
        if ( tMatch.includes(t2Match) ) {
          var regexp = new RegExp(t2Match, 'gi');
          var cleaned = tOriginal.replace(regexp, ' xxx ');
          elemArr[0].textContent = cleaned;
          obj.message = cleaned;
          regexp = null; cleaned = null; 
        }
      }
    }
  }
  elem = arr = arr2 = tOriginal = tMatch = t2Original = t2Match = elemArr = fullArr = arrl = i = null; return obj;
}


scbt.f.get_obj_filter_highlighted_words_from_obj = function(obj, elem){
  var arr = [];
  var arr2 = [];
  var tOriginal = '';
  var tMatch = '';
  var t2Original = '';
  var t2Match = '';
  var elemArr = [];
  var fullArr = [];
  var arrl = 0;

  elemArr = scbt.f.get_arr_message_elems_from_parent_element(elem);
  if (elemArr[0] && elemArr[0].textContent) {
    tOriginal = obj.message;
    if (tOriginal) {
      tMatch = tOriginal.toLowerCase().trim();
      arr = scbt.o.settingsFromSync.sz2v2.split(',');
      arrl = arr.length;
      for (var i = 0; i < arrl; i++) {
        var t2Original = arr[i];
        if (t2Original) {
          t2Match = t2Original.toLowerCase().trim();
          if ( tMatch.includes(t2Match) ) {
            elemArr[0].style.setProperty('font-size', '3rem', 'important');
            elemArr[0].style.setProperty('line-height', '1', 'important');  
          }
        }
      }
    }
  }
  elem = arr = arr2 = tOriginal = tMatch = t2Original = t2Match = elemArr = fullArr = arrl = i = null; return obj;
}


scbt.f.get_obj_filter_vip_users_from_obj = function(obj, elem){
  var arr = [];
  var arr2 = [];
  var tOriginal = '';
  var tMatch = '';
  var t2Original = '';
  var t2Match = '';
  var elemArr = [];
  var fullArr = [];
  var arrl = 0;
  var elem2Arr = [];
  
  if (scbt.v.serviceid == 'arena') {
    // TODO
  }
  if (scbt.v.serviceid == 'instagram') {
    // TODO
  }
  if (scbt.v.serviceid == 'kick') {
    elemArr = elem.getElementsByClassName('chat-entry-username');
  }
  if (scbt.v.serviceid == 'noice') {
    elemArr = elem.getElementsByClassName('chat-noice');
  }
  if (scbt.v.serviceid == 'odysee') {
    elemArr = elem.getElementsByClassName('chat-history--username');
  }
  if (scbt.v.serviceid == 'shareplay') {
    // TODO
  }
  if (scbt.v.serviceid == 'rumble') {
    elemArr = elem.getElementsByClassName('chat-history--username');
  }
  if (scbt.v.serviceid == 'tiktok') {
    elemArr = scbt.f.get_arr_username_elems_from_parent_element(elem);
  }
  if (scbt.v.serviceid == 'twitch') {
    elemArr = elem.querySelectorAll('.chat-line__username');
  }
  if (scbt.v.serviceid == 'twitter') {
    // TODO
  }
  if (scbt.v.serviceid == 'youtube') {
    elemArr = elem.querySelectorAll('#author-name');
  }
  if (elemArr[0] && elemArr[0].textContent) {
    tOriginal = obj.username;
    if (tOriginal) {
      tMatch = tOriginal.toLowerCase().trim();
      arr = scbt.o.settingsFromSync.sz2v1.split(',');
      arrl = arr.length;
      for (var i = 0; i < arrl; i++) {
        t2Original = arr[i];
        if (t2Original) {
          t2Match = t2Original.toLowerCase().trim();
          if ( tMatch == t2Match) {
            elemArr[0].classList.add('vip');
            obj.isHighlighted = scbt.o.settingsFromSync.sz2c5 + scbt.v.serviceid;
            elemArr[0].style.setProperty('font-size', '3.5rem', 'important');
            elemArr[0].style.setProperty('line-height', '1', 'important');
            
            elem2Arr = scbt.f.get_arr_message_elems_from_parent_element(elem);
            if (elem2Arr[0]) {
              elem2Arr[0].style.setProperty('font-size', '3.5rem', 'important');
              elem2Arr[0].style.setProperty('line-height', '1', 'important');
            }
          } // end if ( tMatch == t2Match)
        }
      }
    }
  }
  elem = arr = arr2 = tOriginal = tMatch = t2Original = t2Match = elemArr = fullArr = arrl = elem2Arr = i = null; return obj;
}


scbt.f.get_str_channelid = async function(){
  if (scbt.v.channelid) { return scbt.v.channelid; }
  var str = null;
  if (location.pathname == '' || location.pathname == '/') {
    scbt.v.channelid = null; return scbt.v.channelid;
  }

  if (scbt.v.serviceid == 'arena') {
    // TODO
  }
  if (scbt.v.serviceid == 'instagram') {
    // TODO
  }
  if (scbt.v.serviceid == 'kick') {
    str = location.pathname.substr(1); // streamer or video/b0e6cfc7-cf0a-4ec3-b0dc-91a3a8b701e1 or streamer/chatroom
    if (str.indexOf('video/') > -1) {
      scbt.b.vod = true;
      var elemArr2 = document.body.getElementsByClassName('stream-username');
      if (elemArr2[0]) {
        str = elemArr2[0].innerText;
        scbt.v.channelid = scbt.f.get_formatted_channelid(str);
      }
      str = null; return scbt.v.channelid;
    }
    if (str.indexOf('/chatroom') > -1) {
      scbt.b.popout = true;
      str = str.split('/chatroom')[0];
      scbt.v.channelid = scbt.f.get_formatted_channelid(str);
      str = null; return scbt.v.channelid;
    }
    scbt.b.channelPage = true;
    scbt.v.channelid = scbt.f.get_formatted_channelid(str);
    str = null; return scbt.v.channelid;
  }

  if (scbt.v.serviceid == 'noice') {
    // TODO
  }

  // location.pathname.split('/@');
  // https://odysee.com/@streamer:2 -- ['', 'streamer:2']
  // https://odysee.com/@streamer:2/Live:206 -- ['', 'streamer:2/Live:206']
  // https://odysee.com/@streamer:6/2023Review-x2:6 -- ['', 'streamer:6/2023Review-x2:6']
  // location.pathname.split('/@')[1].split(':')[0].toLowerCase();
  // https://odysee.com/$/popout/@streamer:a/streamer:b
  if (scbt.v.serviceid == 'odysee') {
    var str = location.pathname.split('/@')[1].split(':')[0];
    scbt.v.channelid = scbt.f.get_formatted_channelid(str);
    str = location.pathname.substr(1);
    if (str.indexOf('/popout/') > -1) {
      scbt.b.popout = true;
    }
    str = null; return scbt.v.channelid;
  }

  if (scbt.v.serviceid == 'shareplay') {
    // TODO
  }

  // location.pathname
  // '/v45eqph-the-legend-of-dragoon-full-play-through-psz5-pt.5.html'
  // document.body.getElementsByClassName('media-heading-name')[0].innerText
  // https://rumble.com/chat/popup/246991668
  if (scbt.v.serviceid == 'rumble') {
    str = location.pathname.substr(1);
    if (str.indexOf('/popup/') > -1) {
      scbt.b.popout = true;
      var elemArr = document.body.getElementsByClassName('chat--subscribe-header--title');
      if (elemArr[0]) {
        str = elemArr[0].innerText;
        scbt.v.channelid = scbt.f.get_formatted_channelid(str);
        str = elemArr = null; return scbt.v.channelid;
      }
    }
    var elemArr = document.body.getElementsByClassName('media-heading-name');
    var elem2Arr = document.body.getElementsByClassName('channel-header--title');
    if (elem2Arr[0]) {
      elem2Arr[0] = elem2Arr[0].getElementsByTagName('h1');
    }
    var elem = '';
    if (elemArr[0]) {
      elem = elemArr[0];
    } 
    if (elem2Arr[0]) {
      elem = elem2Arr[0];
    }
    if (elem) {
      str = elem.innerText;
      if (str) {
        scbt.v.channelid = scbt.f.get_formatted_channelid(str);
        str = elemArr = null; return scbt.v.channelid;
      }
    }
  }

  // https://www.tiktok.com/@streamer/live?enter_from_merge=others_homepage&enter_method=top_live_cell
  if (scbt.v.serviceid == 'tiktok') {
    var str = location.pathname.split('@')[1].split('/')[0];
    scbt.v.channelid = scbt.f.get_formatted_channelid(str);
    return scbt.v.channelid;
  }

  if (scbt.v.serviceid == 'twitch') {
    str = location.pathname.substr(1); // https://www.twitch.tv/popout/streamer/chat?popout=
    if (str.indexOf('videos/') > -1) {
      scbt.b.vod = true;
      str = document.getElementsByTagName('h1')[0].innerText;
      scbt.v.channelid = scbt.f.get_formatted_channelid(str);
      str = null; return scbt.v.channelid;
    }
    if (str.indexOf('/chatroom') > -1) {
      scbt.b.popout = true;
      str = str.split('popout/')[1].split('/chat')[0];
      scbt.v.channelid = scbt.f.get_formatted_channelid(str);
      str = null; return scbt.v.channelid;
    }
    scbt.b.channelPage = true;
    scbt.v.channelid = scbt.f.get_formatted_channelid(str);
    str = null; return scbt.v.channelid;
  }

  if (scbt.v.serviceid == 'twitter') {
    // TODO
  }

  // https://www.youtube.com/live_chat?is_popout=1&v=abc
  // https://www.youtube.com/watch?v=abc
  if (scbt.v.serviceid == 'youtube') {
    var elemArr = [];
    if (scbt.b.mobile === true) {
      elemArr = document.body.querySelectorAll('.slim-owner-channel-name');
    } else {
      elemArr = document.body.querySelectorAll('.ytd-video-owner-renderer .ytd-channel-name');
    }
    if (elemArr[0]) {
      scbt.v.channelid = scbt.f.get_formatted_channelid(elemArr[0].innerText);
      elemArr = null; return scbt.v.channelid;
    }
  }
  
}


scbt.f.get_str_for_chat_classes_from_arr = function(arr){
  var str = '';
  if (arr) {
    if (arr[5] == 1) {
      str = str + ' sub ';
    }
    if (arr[6] == 1) {
      str = str + ' moderator ';
    }
    if (arr[7] == 1) {
      str = str + ' owner ';
    }
    if (arr[8] == 1) {
      str = str + ' donation ';
    }
    if (arr[9] == 1) {
      str = str + ' newSub ';
    }
    if (arr[10] == 1) {
      str = str + ' verified vip ';
    }
    if (arr[11] == 1) {
      str = str + ' gifter ';
    }
    if (arr[12] == 1) {
      str = str + ' founder ';
    }
    if (arr[13] == 1) {
      str = str + ' og ';
    }
    if (arr[14] == 1) {
      str = str + ' staff ';
    }
    if (arr[15] == 1) {
      str = str + ' anevent ';
    }
  }
  arr = null; return str;
}


scbt.f.get_str_for_chat_classes_from_obj = function(obj){
  var str = '';
  if (obj.sub === 1) {
    str = str + ' sub ';
  }
  if (obj.moderator === 1) {
    str = str + ' moderator ';
  }
  if (obj.owner === 1) {
    str = str + ' owner ';
  }
  if (obj.donation === 1) {
    str = str + ' donation ';
  }
  if (obj.newSub === 1) {
    str = str + ' newSub ';
  }
  if (obj.verified === 1) {
    str = str + ' verified vip ';
  }
  if (obj.gifter === 1) {
    str = str + ' gifter ';
  }
  if (obj.founder === 1) {
    str = str + ' founder ';
  }
  if (obj.og === 1) {
    str = str + ' og ';
  }
  if (obj.staff === 1) {
    str = str + ' staff ';
  }
  if (obj.anevent === 1) {
    str = str + ' anevent ';
  }
  obj = null; return str;
}


scbt.f.get_str_for_search = function(){
  var str = '';
  str = scbt.e.scbtChatSearchInputText.value;
  if (str) {
    str = str.replace(/[^a-zA-Z0-9_\-@\s]/g, '');
    str = str.trim();
  }
  return str;
}


scbt.f.get_str_military_hours_minutes_from_timestamp = function(timestamp){
  timestamp = timestamp.trim(); // 10:01AM
  if (timestamp) {
    if (timestamp.indexOf('AM') > -1) {
      timestamp = timestamp.replace('AM', '');
    } else {
      timestamp = timestamp.replace('PM', '');
      var arr = timestamp.split(':');
      var hours = parseInt(arr[0]);
      hours = hours + 12;
      timestamp = hours + ':' + arr[1];
    }
  }
  arr = hours = null; return timestamp;
}


scbt.f.get_str_seconds_from_hour_minute_time = function(str){
  str = str.replace('@', '');
  str = str.replace('.', '');
  str = str.trim();
  var p = str.split(':');
  var seconds = 0;
  var m = 1;
  while (p.length > 0) {
      seconds += m * parseInt(p.pop(), 10);
      m *= 60;
  }
  str = p = m = null;
  return seconds;
}


scbt.f.get_str_serviceid = function(){
  var str = location.hostname.toLowerCase();
  if (str === 'kick.com' || str === 'm.kick.com' || str === 'www.kick.com') {
    scbt.v.serviceid = 'kick';
    str = null; return scbt.v.serviceid;
  }
  if (str === 'noice.com' || str === 'm.noice.com' || str === 'www.noice.com') {
    scbt.v.serviceid = 'noice';
    str = null; return scbt.v.serviceid;
  }
  if (str === 'odysee.com' || str === 'm.odysee.com' || str === 'www.odysee.com') {
    scbt.v.serviceid = 'odysee';
    str = null; return scbt.v.serviceid;
  }
  if (str === 'rumble.com' || str === 'm.rumble.com' || str === 'www.rumble.com') {
    scbt.v.serviceid = 'rumble';
    str = null; return scbt.v.serviceid;
  }
  if (str === 'tiktok.com' || str === 'm.tiktok.com' || str === 'www.tiktok.com') {
    scbt.v.serviceid = 'tiktok';
    str = null; return scbt.v.serviceid;
  }
  if (str === 'twitch.tv' || str === 'm.twitch.tv' || str === 'www.twitch.tv') {
    scbt.v.serviceid = 'twitch';
    str = null; return scbt.v.serviceid;
  }
  if (str === 'youtube.com' || str === 'm.youtube.com' || str === 'www.youtube.com') {
    scbt.v.serviceid = 'youtube';
    str = null; return scbt.v.serviceid;
  }
  if (str === 'archyved.com' || str === 'www.archyved.com') {
    scbt.v.serviceid = 'archyved';
    str = null; return scbt.v.serviceid;
  }
}


scbt.f.get_str_videoid = function(){
  if (scbt.v.videoid) { return scbt.v.videoid; }
  scbt.v.videoid = null;

  if (scbt.v.serviceid == 'arena') {
    // TODO
  }
  if (scbt.v.serviceid == 'instagram') {
    // TODO
  }
  if (scbt.v.serviceid == 'kick') {
    if (scbt.v.channelid) {
      scbt.v.videoid = new Date().toISOString().slice(0, 10) + '-' + scbt.v.channelid;
      return scbt.v.videoid;
    }
  }
  if (scbt.v.serviceid == 'noice') {
    // TODO
  }
  if (scbt.v.serviceid == 'odysee') {
    // TODO
    var arr = location.pathname.split(':');
    if (arr[0]) {
      scbt.v.videoid = ':' + arr[1] + ':' + arr[2];
      arr = null; return scbt.v.videoid;
    }
  }
  if (scbt.v.serviceid == 'rumble') {
    // TODO
    var arr = location.pathname.split('/');
    if (arr[1]) {
      str = arr[1].trim();
      str = str.replace('.html', '');
      str = null;
      scbt.v.videoid = new Date().toISOString().slice(0, 10) + '-' + scbt.v.channelid;
      str = arr = null; return scbt.v.videoid;
    }
  }
  if (scbt.v.serviceid == 'shareplay') {
    // TODO
  }
  if (scbt.v.serviceid == 'tiktok') {
    scbt.v.videoid = new Date().toISOString().slice(0, 10) + '-' + scbt.v.channelid;
    return scbt.v.videoid;
  }
  if (scbt.v.serviceid == 'twitch') {
    scbt.v.videoid = new Date().toISOString().slice(0, 10) + '-' + scbt.v.channelid;
    return scbt.v.videoid;
  }
  if (scbt.v.serviceid == 'twitter') {
    // TODO
  }
  if (scbt.v.serviceid == 'youtube') {
    // TODO
    if (location.search) {
      var arr = location.search.split('v='); // ['?', 'abc']
      if (arr[1]) {
        scbt.b.vod = true;
        var arr2 = arr[1].split('&');
        if (arr2[0]) {
          str = arr2[0];
          setTimeout(function() { scbt.f.get_vod_length(); }, 2000);
          arr = arr2 = str = null; return scbt.v.videoid;
        }
      }
    } else {
      if (location.pathname.indexOf('live') > -1) {
        var arr = location.pathname.split('/');
        str = new Date().toISOString().slice(0, 10) + '&' + arr[2];
        arr = str = null;
        return scbt.v.videoid;
      }
    }
  }

  arr = null; return scbt.v.videoid;
}


scbt.f.set_page_values_to_reset = function(){
  scbt.v.channelid = null;
  scbt.v.videoid = null;
  scbt.v.dbName = null;
  scbt.v.dbNameToSearch = null;

  scbt.b.vod = false;
  scbt.b.vodLoaded = false;
  scbt.b.vodLoadedComments = false;
  scbt.b.channelPage = false;
  scbt.b.popout = false;

  scbt.a.followedChannels = [];
  scbt.a.savedStreams = [];
  scbt.a.savingChatIds = [];
  scbt.a.searchingMessageIds = [];
  scbt.a.usernames = [];

  scbt.o.categoryLoading = {};
  scbt.o.livestreamsLoading = {};
  return false;
}


// non video or channel pages to ignore
scbt.f.get_binary_if_page_valid = async function(U){
  if (!U) { U = location.href; }
  var arr = [];
  var isValid = true;

  if (scbt.v.serviceid == 'arena') {
    // TODO
  }
  if (scbt.v.serviceid == 'instagram') {
    // TODO
  }
  if (scbt.v.serviceid == 'kick') {
    arr = [
      'https://kick.com/community-guidelines',
      'https://kick.com/dmca-policy',
      'https://kick.com/privacy-policy',
      'https://kick.com/terms-of-service',
      'https://kick.com/dashboard/',
      'https://shop.kick.com/',
      'https://help.kick.com/'
    ];
    if (U == 'https://kick.com/') { U = 'https://kick.com/community-guidelines'; }
  }
  if (scbt.v.serviceid == 'noice') {
    // TODO https://noice.com/
  }
  if (scbt.v.serviceid == 'odysee') {
    arr = [
      'https://odysee.com/@taran:1/subscriptions:6',
      'https://odysee.com/subscriptions/',
      'https://odysee.com/$/signin',
      'https://odysee.com/$/signup',
      'https://odysee.com/$/tos',
      'https://odysee.com/$/help',
      'https://odysee.com/$/careers',
      'https://odysee.com/$/privacypolicy',
      'https://odysee.com/$/homepage_customization',
      'https://help.odysee.tv/communityguidelines/',
      'https://chat.odysee.com/',
      'https://help.odysee.tv/'
    ];
    if (U == 'https://odysee.com/') { U = 'https://odysee.com/@taran:1/subscriptions:6'; }
  }
  if (scbt.v.serviceid == 'rumble') {
    arr = [
      'https://rumble.com/upload.php',
      'https://rumble.com/live?',
      'https://rumble.com/account/',
      'https://rumblefaq.groovehq.com/help',
      'https://rumble.com/our-apps/',
      'https://rumble.com/s/terms',
      'https://rumble.com/s/privacy',
      'https://rumble.com/s/dmca',
      'https://corp.rumble.com/',
      'https://help.rumble.com/',
      'https://ads.rumble.com/',
      'https://rumble.store/',
      'https://www.rumble.cloud/'
    ];
    if (U == 'https://rumble.com/') { U = 'https://rumble.com/upload.php'; }
  }
  if (scbt.v.serviceid == 'shareplay') {
    // TODO
  }
  if (scbt.v.serviceid == 'tiktok') {
    arr = [
      'https://effecthouse.tiktok.com',
      'https://www.tiktok.com/about',
      'https://newsroom.tiktok.com',
      'https://www.tiktok.com/about/contact',
      'https://careers.tiktok.com',
      'https://www.tiktok.com/forgood',
      'https://www.tiktok.com/business',
      'https://developers.tiktok.com',
      'https://www.tiktok.com/transparency',
      'https://www.tiktok.com/tiktok-rewards',
      'https://www.tiktok.com/embed',
      'https://support.tiktok.com',
      'https://www.tiktok.com/safety?',
      'https://www.tiktok.com/legal',
      'https://www.tiktok.com/creators',
      'https://www.tiktok.com/signup',
      'https://www.tiktok.com/404',
      'https://www.tiktok.com/login',
      'https://www.tiktok.com/community-guidelines',
      'https://www.tiktok.com/feedback'
    ];
    if (U == 'https://www.tiktok.com/') { U = 'https://effecthouse.tiktok.com'; }
  }
  if (scbt.v.serviceid == 'twitch') {
    arr = [
      'https://www.twitch.tv/settings',
      'https://www.twitch.tv/privacy',
      'https://www.twitch.tv/inventory',
      'https://www.twitch.tv/wallet',
      'https://www.twitch.tv/subscriptions',
      'https://www.twitch.tv/p/',
      'https://www.twitch.tv/downloads',
      'https://www.twitch.tv/store',
      'https://www.twitch.tv/jobs',
      'https://www.twitch.tv/turbo',
      'https://www.twitch.tv/legal',
      'https://help.twitch.tv/',
      'https://safety.twitch.tv/',
      'https://blog.twitch.tv/',
      'https://appeals.twitch.tv/',
      'https://dashboard.twitch.tv/',
      'https://dev.twitch.tv/',
      'https://www.twitch.tv/community-guidelines',
      'https://www.twitch.tv/dmca-policy',
      'https://www.twitch.tv/privacy-policy',
      'https://www.twitch.tv/terms-of-service',
      'https://www.twitch.tv/team/',
      'https://www.twitch.tv/dashboard',
      '/categories',
      '/directory',
      '/search',
      '/subscriptions'
    ];
    if (U == 'https://www.twitch.tv/') { U = 'https://www.twitch.tv/settings'; }
  }
  if (scbt.v.serviceid == 'twitter') {
    // TODO
  }
  if (scbt.v.serviceid == 'youtube') {
    arr = [
      'https://www.youtube.com/improvedtube',
      'https://www.youtube.com/tv',
      'https://www.youtube.com/audiolibrary',
      'https://www.youtube.com/embed/',
      'https://www.youtube.com/premium',
      'https://www.youtube.com/account',
      'https://www.youtube.com/reporthistory',
      'https://www.youtube.com/about/',
      'https://www.youtube.com/new/',
      'https://www.youtube.com/t/',
      'https://www.youtube.com/creators/',
      'https://www.youtube.com/ads/',
      'https://www.youtube.com/howyoutubeworks',
      'https://developers.google.com/youtube',
      'http://studio.youtube.com',
      'https://tv.youtube.com/',
      'https://music.youtube.com/',
      'https://www.youtubekids.com/',
      'https://www.youtube.com/?bp='
    ];
    if (U == 'https://www.youtube.com/') { U = 'https://www.youtube.com/improvedtube'; }
  }
  
  [].forEach.call(arr, function(item) {
    if (U.indexOf(item) !== -1) {
      console.error('*********** in get_binary_if_page_valid BAD URL 1 ***********', U);
      scbt.f.set_page_values_to_reset();
      isValid = false;
    }
  });
  
  arr = item = null;
  if (isValid === false) {
    console.info("in get_binary_if_page_valid - bad failing url", U);
    return false;
  } else {
    console.info("in get_binary_if_page_valid - good passing url", U);
    return true;
  }
}


scbt.f.set_page_values_from_url = async function(U){
  if (scbt.v.serviceid == 'arena') {
    // TODO
  }
  if (scbt.v.serviceid == 'instagram') {
    // TODO
  }
  if (scbt.v.serviceid == 'kick') {
    if (U.indexOf('/video/') !== -1) {
      scbt.b.vod = true;
      scbt.v.videoid = null;
      scbt.b.vodLoaded = true;
      console.info('*********** NOW KICK VOD ***********');

      if (window.location.search) {
        var arr = window.location.search.split('?'); // 0: "" 1: "t=300" scbt_helper_go_to_timestamp_in_video
        if (arr[1]) {
          var arr2 = arr[1].split('='); // 0: "t" 1: "300" go_to_timestamp_in_video
          if (arr2[1]) {
            if (arr2[0] == 't') {
              var e = {};
              e.target = {};
              e.target.dataset = {};
              e.target.dataset.seconds = Number(arr2[1]);
              setTimeout(function(){ scbt.f.go_to_timestamp_in_video(e); }, 5000);
            }
          }
        }
      }

      U = null; return false;
    } else {
      
      if (U.indexOf('clip=') !== -1) {
        scbt.v.videoid = null;
        console.info('*********** NOW KICK CLIP ***********');
        U = null; return false;
      } else {
        if (U.indexOf('/chatroom') !== -1) {
          scbt.b.popout = true;
          var str = location.href.split('.com/')[1].split('/chatroom')[0];
          scbt.v.channelid = scbt.f.get_formatted_channelid(str); // HERE
          scbt.v.videoid = new Date().toISOString().slice(0, 10) + '-' + scbt.v.channelid;
          console.info('*********** NOW KICK POPOUT ' + scbt.v.channelid + ' ' + scbt.v.videoid);
          scbt.v.dbNameToSearch = 'savedchat' + '&' + scbt.v.serviceid + '&' + scbt.v.channelid + '&' + scbt.v.videoid;
          U = null; return false;
        } else {
          scbt.b.channelPage = true;
          U = U.replace('https://kick.com/', '');
          if (U.indexOf('/') !== -1) {
            U = U.split('/')[0];
            scbt.v.channelid = scbt.f.get_formatted_channelid(U);
            console.info('*********** NOW KICK CHANNEL PAGE ' + scbt.v.channelid);
            U = null; return false;
          } else {
            U = U.split('/')[0];
            scbt.v.channelid = scbt.f.get_formatted_channelid(U);
            scbt.v.videoid = new Date().toISOString().slice(0, 10) + '-' + scbt.v.channelid;
            console.info('*********** NOW KICK LIVE PAGE ' + scbt.v.channelid + ' ' + scbt.v.videoid);
            scbt.v.dbNameToSearch = 'savedchat' + '&' + scbt.v.serviceid + '&' + scbt.v.channelid + '&' + scbt.v.videoid;
            U = null; return false;
          }
        }
      }
    }
  }

  if (scbt.v.serviceid == 'noice') {
    // TODO https://noice.com/
  }

  if (scbt.v.serviceid == 'odysee') {
    if (U.indexOf('$/popout') !== -1) {
      scbt.b.popout = true;
      U = U.replace('https://odysee.com/$/popout/', '');
      var str = U.split(':')[0];
      scbt.v.channelid = scbt.f.get_formatted_channelid(str);
      scbt.v.videoid = ':' + U.split(':')[1] + ':' + U.split(':')[2];
      console.info('*********** NOW ODYSEE POPOUT ' + scbt.v.channelid + ' ' + scbt.v.videoid);
      str = U = null; return false;
    }
    // non video or channel pages to ignore
    if (U.indexOf('$') !== -1) {
      console.error('BAD ODYSEE URL ***********');
      U = null; return false;
    }
    if (U.indexOf('@') !== -1) {
      U = U.replace('https://odysee.com/', '');
      var str = U.split(':')[0];
      scbt.v.channelid = scbt.f.get_formatted_channelid(str);
      console.info('CHANNEL ODYSEE URL ***********' + scbt.v.channelid);
      if (U.indexOf('?') == -1) {
        scbt.b.channelPage = true;
        console.info('CHANNEL ODYSEE PAGE ***********' + scbt.v.channelid);
      }
      if (U.indexOf('/') !== -1) {
        scbt.b.vod = true;
        scbt.v.videoid = ':' + U.split(':')[1] + ':' + U.split(':')[2];
        console.info('ODYSEE VOD URL ***********' + scbt.v.videoid);
        str = U = null; return false;
      } 
    }
    U = null; return false;
  }

  if (scbt.v.serviceid == 'shareplay') {
    // TODO
  }
  if (scbt.v.serviceid == 'rumble') {
    if (U.indexOf('/popup/') !== -1) {
      scbt.b.popout = true;
      U = null; return false;
    }
    if ( (U.indexOf('/c/') !== -1) || (U.indexOf('/user/') !== -1) ) {
      U = U.replace('https://rumble.com/c/', '');
      U = U.replace('https://rumble.com/user/', '');
      var str = U.split('/')[0];
      scbt.v.channelid = scbt.f.get_formatted_channelid(str);
      if ( (U.slice(-5) === 'live/') || (U.slice(-5) === '/live') ) {
        scbt.v.videoid = new Date().toISOString().slice(0, 10) + '-' + scbt.v.channelid;
        console.info('this is a live RUMBLE URL: ' + scbt.v.channelid + ' ' + scbt.v.videoid);
        str = U = null; return false;
      }
      scbt.b.channelPage = true;
      console.info('RUMBLE CHANNEL PAGE ***********' + scbt.v.channelid);
      str = U = null; return false;
    } else {
      if (U.indexOf('.html') !== -1) {
        U = U.replace('https://rumble.com/', '');
        U = U.replace('.html', '');
        scbt.v.videoid = U;
        console.info('RUMBLE VOD URL ***********' + scbt.v.videoid);
        U = null; return false;
      }
    }
    U = null; return false;
  }

  if (scbt.v.serviceid == 'tiktok') {
    U = U.split('/')[3].split('@')[1];
    scbt.v.channelid = scbt.f.get_formatted_channelid(U);
    scbt.v.videoid = new Date().toISOString().slice(0, 10) + '-' + scbt.v.channelid;
    console.info('*********** NOW TIKTOK LIVE PAGE ' + scbt.v.videoid + ' ' + scbt.v.channelid);
    // console.log('savedchat' + '&' + scbt.v.serviceid + '&' + scbt.v.channelid + '&' + scbt.v.videoid);
    // scbt.v.dbNameToSearch = 'savedchat' + '&' + scbt.v.serviceid + '&' + scbt.v.channelid + '&' + scbt.v.videoid;
    // savedchat&tiktok&streamer&2024-01-30-streamer
    if (scbt.v.serviceid && scbt.v.channelid && scbt.v.videoid) {
      scbt.v.dbNameToSearch = 'savedchat' + '&' + scbt.v.serviceid + '&' + scbt.v.channelid + '&' + scbt.v.videoid;
    }
    U = null; return false;
  }

  if (scbt.v.serviceid == 'youtube') {
    // console.log('set_page_values_from_url YOUTUBE', U);

    if (U.indexOf('/user/') !== -1) {
      var str = U.split('/user/')[1].split('/')[0];
      scbt.v.channelid = scbt.f.get_formatted_channelid(str);
      scbt.b.channelPage = true;
      console.info('USER user PAGE FOR ' + scbt.v.channelid);
      str = U = null; return false;
    }
    if (U.indexOf('/@') !== -1) {
      var str = U.split('/@')[1].split('/')[0];
      scbt.v.channelid = scbt.f.get_formatted_channelid(str);
      scbt.b.channelPage = true;
      console.info('USER @ PAGE FOR ' + scbt.v.channelid);
      str = U = null; return false;
    }
    if (U.indexOf('/channel/') !== -1) {
      var str = U.split('/channel/')[1].split('/')[0];
      scbt.v.channelid = scbt.f.get_formatted_channelid(str);
      scbt.b.channelPage = true;
      console.info('USER channel PAGE FOR ' + scbt.v.channelid);
      str = U = null; return false;
    }
    if (U.indexOf('/live/') !== -1) {
      U = U.replace('https://www.youtube.com/', '');
      var str = U.split('/live')[0].replace('@', '');
      scbt.v.channelid = scbt.f.get_formatted_channelid(str);
      scbt.b.channelPage = false;
      scbt.v.videoid = new Date().toISOString().slice(0, 10) + '-' + scbt.v.channelid;
      scbt.v.dbNameToSearch = 'savedchat' + '&' + scbt.v.serviceid + '&' + scbt.v.channelid + '&' + scbt.v.videoid;
      console.info('LIVE PAGE 1 FOR ' + scbt.v.channelid + ' ' + scbt.v.videoid);
      str = U = null; return false;
    }
    if (U.indexOf('watch?v=') !== -1) {
      scbt.v.videoid = U.split('watch?v=')[1].split('&')[0];
      scbt.b.vod = true;
      scbt.b.channelPage = false;
      var x = document.body.querySelectorAll('.ytd-channel-name a');
      if (x[0]) {
        var str = x[0].innerText;
        scbt.v.channelid = scbt.f.get_formatted_channelid(str);
      }
      scbt.v.dbNameToSearch = 'savedchat' + '&' + scbt.v.serviceid + '&' + scbt.v.channelid + '&' + scbt.v.videoid;
      console.info('LIVE PAGE 2 FOR ' + scbt.v.channelid + ' ' + scbt.v.videoid);
      str = x = U = null; return false;
    }
    return false;
  }

  if (scbt.v.serviceid == 'twitch') {
    if (U.indexOf('/videos/') !== -1) {
      scbt.b.vod = true;
      console.info('*********** NOW TWITCH VOD ***********');
      U = null; return false;
    } else {
      if (U.indexOf('clip=') !== -1) {
        scbt.v.videoid = null;
        console.info('*********** NOW TWITCH CLIP ***********');
        U = null; return false;
      } else {
        if (U.indexOf('popout') !== -1) {
          scbt.b.popout = true;
          U = U.replace('https://www.twitch.tv/popout/', '');
          var str = U.split('/')[0];
          scbt.v.channelid = scbt.f.get_formatted_channelid(str);
          scbt.v.videoid = new Date().toISOString().slice(0, 10) + '-' + scbt.v.channelid;
          console.info('*********** NOW TWITCH POPOUT ' + scbt.v.channelid + ' ' + scbt.v.videoid);
          scbt.v.dbNameToSearch = 'savedchat' + '&' + scbt.v.serviceid + '&' + scbt.v.channelid + '&' + scbt.v.videoid;
          str = U = null; return false;
        } else {
          U = U.replace('https://www.twitch.tv/', '');
          U = U.split('/')[0];
          scbt.v.channelid = scbt.f.get_formatted_channelid(U);
          
          setTimeout(function() {
            var elemArr = document.getElementsByClassName('root');
            var elem = {};
            if ( elemArr[0]) {
              var elem = elemArr[0];
              var str = elem.getAttribute('data-a-page-loaded-name');
              if ( typeof str == 'string' ) {
                scbt.v.videoid = new Date().toISOString().slice(0, 10) + '-' + scbt.v.channelid;
                console.info('*********** NOW TWITCH LIVE PAGE ' + scbt.v.channelid + ' ' + scbt.v.videoid);
                scbt.v.dbNameToSearch = 'savedchat' + '&' + scbt.v.serviceid + '&' + scbt.v.channelid + '&' + scbt.v.videoid;
                str = U = null; return false;
              }
            }
            scbt.b.channelPage = true;
            console.info('*********** NOW TWITCH CHANNEL PAGE ' + scbt.v.channelid);
            U = null; return false;
          }, 1500);

        }
      }
    }
  }
  if (scbt.v.serviceid == 'twitter') {
    // TODO
  }
  U = null; return false;
}


scbt.f.set_chat_parameter = function(parameter, visibility){
  var str = '';
  
  if (parameter == 'owner') {
    if (visibility == 1) {
      str = ' - broadcaster chats -';
    }
    if (visibility == 2) {
      str = ' - non broadcaster chats -';
    }
  }

  if (parameter == 'moderator') {
    if (visibility == 1) {
      str = ' - mod chats -';
    }
    if (visibility == 2) {
      str = ' - non mod chats -';
    }
  }

  if (parameter == 'sub') {
    if (visibility == 1) {
      str = ' - sub chats -';
    }
    if (visibility == 2) {
      str = ' - non sub chats -';
    }
  }

  if (parameter == 'newSub') {
    if (visibility == 1) {
      str = ' - new sub chats -';
    }
    if (visibility == 2) {
      str = ' - non new sub events -';
    }
  }

  if (parameter == 'mod_sub') {
    if (visibility == 1) {
      str = ' - sub+mod chats -';
    }
    if (visibility == 2) {
      str = ' - non sub+mod chats -';
    }
  }

  if ( (parameter == 'vip') || (parameter == 'verified') ) {
    if (visibility == 1) {
      str = ' - vip/verified chats -';
    }
    if (visibility == 2) {
      str = ' - non vip/verified chats -';
    }
  }

  if (parameter == 'donation') {
    if (visibility == 1) {
      str = ' - donation chats -';
    }
    if (visibility == 2) {
      str = ' - non donation chats -';
    }
  }
  
  if (parameter == 'mention') {
    if (visibility == 1) {
      str = ' - mention chats -';
    }
    if (visibility == 2) {
      str = ' - non mention chats -';
    }
  }

  if (parameter == 'hashtag') {
    if (visibility == 1) {
      str = ' - hashtag chats -';
    }
    if (visibility == 2) {
      str = ' - non hashtag chats -';
    }
  }

  parameter = visibility = null; return str;
}


scbt.f.set_db_error_message = function(error){
  console.error('Error: getting and displaying saved chat failed or blocked for db: ' + scbt.v.dbName);
  if (error) {
    console.error(error);
    if (typeof error === 'string') {
      // console.error(error);
    }
    if (error.target) {
      if (error.target.transaction) {
        if (error.target.transaction.error) {
          // console.error('typeof error.target.transaction.error');
          // console.error(typeof error.target.transaction.error);
          // console.error(error.target.transaction.error);
          if (error.target.transaction.error.message) {
            // console.error('typeof error.target.transaction.error.message');
            // console.error(typeof error.target.transaction.error.message);
            console.error(error.target.transaction.error.message);
          }
          if (error.target.transaction.error.message.indexOf('uniqueness requirements') > -1) {
            console.error('no unique id chat cannot be saved');
          }
        } 
      } 
    }
  }
  error = null;
}


scbt.f.set_vod_length = function(){
  // duration is 30449.461 SECONDS, minutes is: 507,  seconds is: 29.46099999999933
  var elemArr = scbt.f.get_arr_video_elem();
  if (elemArr[0]) {
    if (elemArr[0].readyState > 0) {
      scbt.n.vodSecondsTotal = parseInt(elemArr[0].duration);
      scbt.n.vodMinutesLong = parseInt(elemArr[0].duration / 60, 10);
      scbt.n.vodSecondsLong = parseInt(elemArr[0].duration % 60);
    }
  }
  elemArr = null; return false;
}


// BUILD FUNCTIONS
scbt.f.toast = function(str){
  scbt.e.scbtSnackbar.textContent = str;
  scbt.e.scbtSnackbar.className = 'show';
  setTimeout(function(){ scbt.e.scbtSnackbar.className = scbt.e.scbtSnackbar.className.replace('show', ''); }, 2500);
  str = null; return false;
}


scbt.f.make_toast = function(){
  var elemArr = document.body.getElementsByClassName('scbtSnackbar');
  var body = document.getElementsByTagName('body');
  if (body[0]) {
    body = body[0];
  }
  if (elemArr[0]) { } else {
    var theHTML = '<div id="scbtSnackbar" class="scbtSnackbar"></div>';
    body.insertAdjacentHTML('afterbegin', theHTML);
    scbt.e.scbtSnackbar = document.body.getElementsByClassName('scbtSnackbar')[0];
    setTimeout(function(){ scbt.f.toast('Status: building menus'); }, 500);
  }
  elemArr = theHTML = null; return body;
}


scbt.f.build_chat_line_from_obj = function(chatObj, chatObjFirst){
  var classString = scbt.f.get_str_for_chat_classes_from_obj(chatObj);
  if (chatObjFirst) {
    var theHTML = "<p class='scbt-chat-line " + classString + "'><span><s>" + chatObjFirst.username + ' stream on ' + chatObjFirst.message + "</s><br><span class='scbt-chat-timestamp'>" + chatObj.timestamp + "</span> : <span class='scbt-chat-username'>" + chatObj.username + "</span> <b class='scbt-chat-message'>" +  chatObj.message + "</b></p>";
  } else {
    var theHTML = "<p class='scbt-chat-line " + classString + "'><span class='scbt-chat-timestamp'>" + chatObj.timestamp + "</span> : <span class='scbt-chat-username'>" + chatObj.username + "</span> <b class='scbt-chat-message'>" +  chatObj.message + "</b></p>";
  }
  chatObj = chatObjFirst = classString = null; return theHTML;
}


scbt.f.build_chat_line_from_arr = function(chatArr, chatArrFirst){
  var classString = scbt.f.get_str_for_chat_classes_from_arr(chatArr);
  if (chatArrFirst) {
    var theHTML = "<p class='scbt-chat-line " + classString + "'><span>" + chatArrFirst[3] + ' stream on ' + chatArrFirst[3] + "<br><span class='scbt-chat-timestamp'>" + chatArr[3] + "</span> : <span class='scbt-chat-username'>" + chatArr[4] + "</span> <b class='scbt-chat-message'>" +  chatArr[1] + "</b></p>";
  } else {
    var theHTML = "<p class='scbt-chat-line " + classString + "'><span class='scbt-chat-timestamp'>" + chatArr[3] + "</span> : <span class='scbt-chat-username'>" + chatArr[4] + "</span> <b class='scbt-chat-message'>" +  chatArr[1] + "</b></p>";
  }
  chatArr = chatArrFirst = classString = null; return theHTML;
}


scbt.f.build_top_x_menu = async function(){
  if (window.location.href.indexOf('live_chat') !== -1) {
    return false;
  }

  var elemArr = document.body.getElementsByClassName('scbtX');
  if (elemArr[0]) { return true; }
  var body = scbt.f.make_toast();
  scbt.e.scbtX = body;
  body.classList.add('scbt-' + scbt.v.serviceid);
  if (window.matchMedia('only screen and (max-width: 760px)').matches) {
    scbt.b.mobile = true;
    body.classList.add('scbt-mobile');
  } else {
    scbt.b.mobile = false;
    body.classList.add('scbt-desktop');
  }
  
var theHTML = '<div id="scbtX" class="scbtX">';
theHTML += '<svg id="scbtLoading" class="scbtLoading" version="1.1" id="L2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve"> <circle fill="none" stroke="#fff" stroke-width="4" stroke-miterlimit="10" cx="50" cy="50" r="48"/> <line fill="none" stroke-linecap="round" stroke="#fff" stroke-width="4" stroke-miterlimit="10" x1="50" y1="50" x2="85" y2="50.5"> <animateTransform attributeName="transform" dur="2s" type="rotate" from="0 50 50" to="360 50 50" repeatCount="indefinite" /> </line> <line fill="none" stroke-linecap="round" stroke="#fff" stroke-width="4" stroke-miterlimit="10" x1="50" y1="50" x2="49.5" y2="74"> <animateTransform attributeName="transform" dur="15s" type="rotate" from="0 50 50" to="360 50 50" repeatCount="indefinite" /> </line> </svg>';
theHTML += '<iframe id="scbtVideo" class="scbtVideo" allow="fullscreen" src=""></iframe>';
theHTML += '<div id="scbtClipsToggleMenu" class="scbtClipsToggleMenu"></div>';
theHTML += '<div id="scbtClipsMenuWrapper" class="scbtClipsMenuWrapper"><div id="scbtClipsMenuContent" class="scbtClipsMenuContent"></div><div id="scbtStreamsMenuContent" class="scbtStreamsMenuContent"></div></div>';
theHTML += '<dialog id="scbtSettingsMenu" class="scbtSettingsMenu">';
theHTML += '<h4>Parleystar Settings Menu</h4>';
theHTML += '<form name="scbtSettingsMenuForm" id="scbtSettingsMenuForm" class="scbtSettingsMenuForm" method="dialog" autocapitalize="off" autocomplete="off" spellcheck=="false" >';


// section
theHTML += '<details><summary>Theme</summary>';
theHTML += '<p><fieldset><legend>Theme settings</legend>';

theHTML += '<div class="scbt-' + scbt.v.serviceid + '-theme">';
theHTML += '<input type="color" name="sz1c1' + scbt.v.serviceid + '" id="sz1c1' + scbt.v.serviceid + '" class="scbt-tooltip sz1c1' + scbt.v.serviceid + '" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz1c1' + scbt.v.serviceid + '" class="">Background</label>';

theHTML += '<input type="color" name="sz1c2' + scbt.v.serviceid + '" id="sz1c2' + scbt.v.serviceid + '" class="scbt-tooltip sz1c2" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz1c2' + scbt.v.serviceid + '" class="">Username</label>';

theHTML += '<input type="color" name="sz1c3' + scbt.v.serviceid + '" id="sz1c3' + scbt.v.serviceid + '" class="scbt-tooltip sz1c3" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz1c3' + scbt.v.serviceid + '" class="">Highlighted</label>';

theHTML += '<input type="color" name="sz1c4' + scbt.v.serviceid + '" id="sz1c4' + scbt.v.serviceid + '" class="scbt-tooltip sz1c4" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz1c4' + scbt.v.serviceid + '" class="">Message</label>';

theHTML += '<input type="number" name="sz1n1' + scbt.v.serviceid + '" id="sz1n1' + scbt.v.serviceid + '" class="scbt-tooltip sz1n1" aria-label="" data-tooltip="" title="" min="0" max="9" placeholder="1.0" step="0.5" pattern="\d*">';
theHTML += '<label for="sz1n1' + scbt.v.serviceid + '" class="">Font Size</label>';

theHTML += '<input type="checkbox" name="sz1b1' + scbt.v.serviceid + '" id="sz1b1' + scbt.v.serviceid + '" class="scbt-tooltip sz1b1" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz1b1' + scbt.v.serviceid + '" class="">Text Only</label>';
theHTML += '</div>';
theHTML += '</fieldset></p></details>';



// section
theHTML += '<details><summary>Highlight</summary>';
theHTML += '<p><fieldset><legend>Highlight settings</legend>';

theHTML += '<div class="scbt-' + scbt.v.serviceid + '-highlight">';
theHTML += '<input type="color" name="sz2c1' + scbt.v.serviceid + '" id="sz2c1' + scbt.v.serviceid + '" class="scbt-tooltip sz2c1" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz2c1' + scbt.v.serviceid + '" class="">Sub color</label>';

theHTML += '<input type="color" name="sz2c2' + scbt.v.serviceid + '" id="sz2c2' + scbt.v.serviceid + '" class="scbt-tooltip sz2c2" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz2c2' + scbt.v.serviceid + '" class="">Mod color</label>';

theHTML += '<input type="color" name="sz2c3' + scbt.v.serviceid + '" id="sz2c3' + scbt.v.serviceid + '" class="scbt-tooltip sz2c3" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz2c3' + scbt.v.serviceid + '" class="">Mention color</label>';

theHTML += '<input type="color" name="sz2c4' + scbt.v.serviceid + '" id="sz2c4' + scbt.v.serviceid + '" class="scbt-tooltip sz2c4" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz2c4' + scbt.v.serviceid + '" class="">Hashtag color</label>';

theHTML += '<input type="color" name="sz2c5' + scbt.v.serviceid + '" id="sz2c5' + scbt.v.serviceid + '" class="scbt-tooltip sz2c5" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz2c5' + scbt.v.serviceid + '" class="">Verified/VIP color</label>';

theHTML += '<input type="color" name="sz2c6' + scbt.v.serviceid + '" id="sz2c6' + scbt.v.serviceid + '" class="scbt-tooltip sz2c6" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz2c6' + scbt.v.serviceid + '" class="">Founder color</label>';

theHTML += '<input type="color" name="sz2c7' + scbt.v.serviceid + '" id="sz2c7' + scbt.v.serviceid + '" class="scbt-tooltip sz2c7" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz2c7' + scbt.v.serviceid + '" class="">OG color</label>';

theHTML += '<input type="color" name="sz2c8' + scbt.v.serviceid + '" id="sz2c8' + scbt.v.serviceid + '" class="scbt-tooltip sz2c8" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz2c8' + scbt.v.serviceid + '" class="">Streamer color</label>';

theHTML += '<input type="color" name="sz2c9' + scbt.v.serviceid + '" id="sz2c9' + scbt.v.serviceid + '" class="scbt-tooltip sz2c9" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz2c9' + scbt.v.serviceid + '" class="">Gifter color</label>';
theHTML += '</div>';

theHTML += '<input type="text" name="sz2v1" id="sz2v1" class="scbt-tooltip sz2v1" aria-label="" data-tooltip="" title="" maxlength="100" size="45" placeholder="mario,peach,toad" >';
theHTML += '<label for="sz2v1" class="">VIP List</label>';

theHTML += '<input type="text" name="sz2v2" id="sz2v2" class="scbt-tooltip sz2v2" aria-label="" data-tooltip="" title="" maxlength="100" size="45" placeholder="word1,word2,word3">';
theHTML += '<label for="sz2v2" class="">Highlight words</label>';

theHTML += '</fieldset></p></details>';



// section
theHTML += '<details><summary>Mute</summary>';
theHTML += '<p><fieldset><legend>Mute settings</legend>';

theHTML += '<input type="checkbox" name="sz3b1" id="sz3b1" class="scbt-tooltip sz3b1" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz3b1" class="">Non Mod/Subs</label>';

theHTML += '<input type="checkbox" name="sz3b2" id="sz3b2" class="scbt-tooltip sz3b2" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz3b2" class="">Streamer</label>';

theHTML += '<input type="checkbox" name="sz3b3" id="sz3b3" class="scbt-tooltip sz3b3" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz3b3" class="">Mentions</label>';

theHTML += '<input type="checkbox" name="sz3b4" id="sz3b4" class="scbt-tooltip sz3b4" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz3b4" class="">Bots</label>';

theHTML += '<input type="checkbox" name="sz3b5" id="sz3b5" class="scbt-tooltip sz3b5" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz3b5" class="">Follows</label>';

theHTML += '<input type="checkbox" name="sz3b6" id="sz3b6" class="scbt-tooltip sz3b6" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz3b6" class="">Subscribes</label>';
theHTML += '</fieldset></p></details>';




// section
theHTML += '<details><summary>Hide</summary>';
theHTML += '<p><fieldset><legend>Hide settings</legend>';

theHTML += '<input type="checkbox" name="sz4b1" id="sz4b1" class="scbt-tooltip sz4b1" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b1" class="">Non Mod/Subs</label>';

theHTML += '<input type="checkbox" name="sz4b2" id="sz4b2" class="scbt-tooltip sz4b2" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b2" class="">Streamers</label>';

theHTML += '<input type="checkbox" name="sz4b3" id="sz4b3" class="scbt-tooltip sz4b3" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b3" class="">Mentions</label>';

theHTML += '<input type="checkbox" name="sz4b4" id="sz4b4" class="scbt-tooltip sz4b4" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b4" class="">Bots</label>';

theHTML += '<input type="checkbox" name="sz4b5" id="sz4b5" class="scbt-tooltip sz4b5" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b5" class="">Follows</label>';

theHTML += '<input type="checkbox" name="sz4b6" id="sz4b6" class="scbt-tooltip sz4b6" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b6" class="">Subscribes</label>';

theHTML += '<input type="text" name="sz4v1" id="sz4v1" class="scbt-tooltip sz4v1" aria-label="" data-tooltip="" title="" maxlength="100" size="45" placeholder="word1,word2,word3">';
theHTML += '<label for="sz4v1" class="">Words in Chat</label>';

theHTML += '<input type="text" name="sz4v2" id="sz4v2" class="scbt-tooltip sz4v2" aria-label="" data-tooltip="" title="" maxlength="100" size="45" placeholder="jack,jill,joe">';
theHTML += '<label for="sz4v2" class="">Chat from Usernames</label>';

theHTML += '<input type="checkbox" name="sz4b7" id="sz4b7" class="scbt-tooltip sz4b7" aria-label="" data-tooltip="" title="" maxlength="100" size="45">';
theHTML += '<label for="sz4b7" class="">Usernames in Chat</label>';

theHTML += '<input type="checkbox" name="sz4b8" id="sz4b8" class="scbt-tooltip sz4b8" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b8" class="">Usernames on screen</label>';

theHTML += '<input type="checkbox" name="sz4b22" id="sz4b22" class="scbt-tooltip sz4b22" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b22" class="">Links in chat</label>';
theHTML += '</fieldset><hr>';

theHTML += '<fieldset><legend>Custom Chat Default Filters</legend>';
theHTML += '<input type="checkbox" name="sz4b9" id="sz4b9" class="scbt-tooltip sz4b9" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b9" class="">sexual phrases</label>';

theHTML += '<input type="checkbox" name="sz4b10" id="sz4b10" class="scbt-tooltip sz4b10" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b10" class="">profanity phrases</label>';

theHTML += '<input type="checkbox" name="sz4b11" id="sz4b11" class="scbt-tooltip sz4b11" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b11" class="">political phrases</label>';

theHTML += '<input type="checkbox" name="sz4b12" id="sz4b12" class="scbt-tooltip sz4b12" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b12" class="">negative phrases</label>';

theHTML += '<input type="checkbox" name="sz4b13" id="sz4b13" class="scbt-tooltip sz4b13" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b13" class="">custom phrases</label>';
theHTML += '</fieldset><hr>';

theHTML += '<fieldset class="kick-content-filters"><legend>Kick Content Filters</legend><p>Please hide streams from me:</p>';
theHTML += '<input type="checkbox" name="sz4b14" id="sz4b14" class="scbt-tooltip sz4b14" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b14" class="">18+</label>';

theHTML += '<input type="checkbox" name="sz4b15" id="sz4b15" class="scbt-tooltip sz4b15" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b15" class="">other-tv-shows-movies</label>';

theHTML += '<input type="checkbox" name="sz4b16" id="sz4b16" class="scbt-tooltip sz4b16" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b16" class="">just-sleeping</label>';

theHTML += '<input type="checkbox" name="sz4b17" id="sz4b17" class="scbt-tooltip sz4b17" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b17" class="">asmr</label>';

theHTML += '<input type="checkbox" name="sz4b18" id="sz4b18" class="scbt-tooltip sz4b18" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b18" class="">body-art</label>';

theHTML += '<input type="checkbox" name="sz4b19" id="sz4b19" class="scbt-tooltip sz4b19" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b19" class="">gambling</label>';

theHTML += '<input type="checkbox" name="sz4b20" id="sz4b20" class="scbt-tooltip sz4b20" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b20" class="">pools-hot-tubs</label>';

theHTML += '<input type="checkbox" name="sz4b21" id="sz4b21" class="scbt-tooltip sz4b21" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz4b21" class="">just-chatting</label>';
theHTML += '</fieldset>';
theHTML += '</p></details>';


// section
theHTML += '<details><summary>Features</summary>';
theHTML += '<p>';

theHTML += '<fieldset>';
theHTML += '<legend>Load Abilities or Not</legend>';
theHTML += '<input type="checkbox" name="sz5b1" id="sz5b1" class="scbt-tooltip sz5b1" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b1" class="">Save/Search/Import</label>';

theHTML += '<input type="checkbox" name="sz5b2" id="sz5b2" class="scbt-tooltip sz5b2" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b2" class="">Simulcast Chat</label>';

theHTML += '<input type="checkbox" name="sz5b3" id="sz5b3" class="scbt-tooltip sz5b3" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b3" class="">Chat Filters</label>';

theHTML += '<input type="checkbox" name="sz5b4" id="sz5b4" class="scbt-tooltip sz5b4" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b4" class="">Keybind Commands</label>';

theHTML += '<input type="checkbox" name="sz5b5" id="sz5b5" class="scbt-tooltip sz5b5" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b5" class="">Voice Control</label>';
theHTML += '</fieldset><hr>';

theHTML += '<fieldset>';
theHTML += '<legend>Show or Hide Menus</legend>';
// theHTML += '<input type="checkbox" name="sz5b6" id="sz5b6" class="scbt-tooltip sz5b6" aria-label="" data-tooltip="" title="">';
// theHTML += '<label for="sz5b6" class="">Show Side Menu</label>';

// theHTML += '<input type="checkbox" name="sz5b7" id="sz5b7" class="scbt-tooltip sz5b7" aria-label="" data-tooltip="" title="">';
// theHTML += '<label for="sz5b7" class="">Show Chat Menu</label>';

theHTML += '<input type="checkbox" name="sz5b8" id="sz5b8" class="scbt-tooltip sz5b8" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b8" class="">Show Chat Menu</label>';

theHTML += '<input type="checkbox" name="sz5b9" id="sz5b9" class="scbt-tooltip sz5b9" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b9" class="">Show Mention Menu</label>';

theHTML += '<input type="checkbox" name="sz5b10" id="sz5b10" class="scbt-tooltip sz5b10" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b10" class="">Show Simulcast Chat</label>';

theHTML += '<input type="checkbox" name="sz5b11" id="sz5b11" class="scbt-tooltip sz5b11" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b11" class="">Show Social Media Menu</label>';
theHTML += '</fieldset><hr>';

theHTML += '<fieldset><legend>Features</legend>';
theHTML += '<input type="checkbox" name="sz5b21" id="sz5b21" class="scbt-tooltip sz5b21" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b21" class="">auto save chat locally on livestreams</label>';

theHTML += '<input type="checkbox" name="sz5b12" id="sz5b12" class="scbt-tooltip sz5b12" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b12" class="">basic keybinds</label>';

theHTML += '<input type="checkbox" name="sz5b13" id="sz5b13" class="scbt-tooltip sz5b13" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b13" class="">full keybinds</label>';

theHTML += '<input type="checkbox" name="sz5b14" id="sz5b14" class="scbt-tooltip sz5b14" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b14" class="">voice commands</label>';

theHTML += '<input type="checkbox" name="sz5b15" id="sz5b15" class="scbt-tooltip sz5b15" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b15" class="">chat on screen</label>';

theHTML += '<input type="checkbox" name="sz5b16" id="sz5b16" class="scbt-tooltip sz5b16" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b16" class="">left hand chat</label>';

theHTML += '<input type="checkbox" name="sz5b17" id="sz5b17" class="scbt-tooltip sz5b17" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b17" class="">upside down chat</label>';

theHTML += '<input type="checkbox" name="sz5b18" id="sz5b18" class="scbt-tooltip sz5b18" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b18" class="">hover enlarge</label>';

theHTML += '<input type="checkbox" name="sz5b19" id="sz5b19" class="scbt-tooltip sz5b19" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b19" class="">press to mention</label>';

theHTML += '<input type="checkbox" name="sz5b20" id="sz5b20" class="scbt-tooltip sz5b20" aria-label="" data-tooltip="" title="">';
theHTML += '<label for="sz5b20" class="">auto load saved chat on VOD</label>';

theHTML += '<input type="text" name="sz5v1" id="sz5v1" class="scbt-tooltip sz5v1" aria-label="" data-tooltip="" title="" maxlength="100" size="45" placeholder="word1,word2,word3">';
theHTML += '<label for="sz5v1" class="">speak words</label>';
theHTML += '</fieldset><hr>';

if (scbt.o.settingsFromSync.sz5b2 === true) {
  // Show Simulcast Menu
  theHTML += '<fieldset><legend>Simulcast Chat Settings</legend>';

  theHTML += '<label for="sz5b30" class="scbt-no-hover">This tab is the Receiver Tab which is an active livestream to view simulcast chat.</label>';
  theHTML += '<input type="checkbox" name="sz5b30" id="sz5b30" class="scbt-tooltip sz5b30" aria-label="" data-tooltip="" title="">';
  theHTML += '<input type="text" name="sz5v30" id="sz5v30" class="scbt-tooltip sz5v30" aria-label="" data-tooltip="" title="" readonly disabled>';
  theHTML += '<br clear="all"><hr>';

  theHTML += '<label for="sz5b31" class="scbt-no-hover">This tab is a Sender Tab which is an active livestream to send simulcast chat to the Receiver Tab.</label>';
  theHTML += '<input type="checkbox" name="sz5b31" id="sz5b31" class="scbt-tooltip sz5b31" aria-label="" data-tooltip="" title="">';
  theHTML += '<input type="text" name="sz5v31" id="sz5v31" class="scbt-tooltip sz5v31" aria-label="" data-tooltip="" title="" readonly disabled>';
  theHTML += '<br clear="all"><hr>';

  theHTML += '<label for="sz5b32" class="scbt-no-hover">This tab is a Sender Tab which is an active livestream to send simulcast chat to the Receiver Tab.</label>';
  theHTML += '<input type="checkbox" name="sz5b32" id="sz5b32" class="scbt-tooltip sz5b32" aria-label="" data-tooltip="" title="">';
  theHTML += '<input type="text" name="sz5v32" id="sz5v32" class="scbt-tooltip sz5v32" aria-label="" data-tooltip="" title="" readonly disabled>';
  theHTML += '<br clear="all"><hr>';

  theHTML += '<label for="sz5b33" class="scbt-no-hover">This tab is a Sender Tab which is an active livestream to send simulcast chat to the Receiver Tab.</label>';
  theHTML += '<input type="checkbox" name="sz5b33" id="sz5b33" class="scbt-tooltip sz5b33" aria-label="" data-tooltip="" title="">';
  theHTML += '<input type="text" name="sz5v33" id="sz5v33" class="scbt-tooltip sz5v33" aria-label="" data-tooltip="" title="" readonly disabled>';
  theHTML += '<br clear="all"><hr>';

  theHTML += '<h5> How to use<br />';
  theHTML += '1) Check the box above labeled Simulcast Chat in the Load Abilities or Not menu to load this module.<br />';
  theHTML += '2) Check the box above labeled Show Simulcast Chat in the Turn On/Off menu to turn it on or off at any time.<br />';
  theHTML += '2) Check the box above labeled Receiver Tab on the channel page you want as your base to watch. Example: https://www.youtube.com/@mychannel/live<br />';
  theHTML += '3) Check the box above labeled Sender Tab(s) on the channel(s) page(s) you want to post to your receiver to view there. Example: https://kick.com/mychannel <br />';
  theHTML += '4) When you stream to both Youtube and Kick and have both tabs open on your browser, you will see stream chat messages from Kick tab in the Youtube tab.<br />';
  theHTML += '5) Supported services as of 7/2024: Arena, Instagram, Kick, Noice, Odysee, Rumble, Shareplay, Tiktok, Twitch, Twitter, Youtube.<br />';
  theHTML += '</h5>';

  theHTML += '</fieldset>';
}

theHTML += '</p></details>';


// section
theHTML += '<details><summary>Saved</summary>';
theHTML += '<p><fieldset><legend>Save/Export settings</legend>View previous stream chat replays,<br> import or export chats or this extension settings to/from devices. <br />';
theHTML += '<div class="scbt-accordion"><div><input type="radio" name="scbtSavedAccordion" id="scbtSavedAccordion1" class="accordion__input"> <label for="scbtSavedAccordion1" class="accordion__label" title="view chat replays from previous livestreams saved to this device">Saved Chats</label> <div class="accordion__content"><p><small>(these chats are saved to this device)</small> <br /><button id="scbtSortStreamsByServiceID" class="scbtSortStreamsByServiceID" title="Sort Saved Chats by Stream Platform">Srv ⬇️</button> <button id="scbtSortStreamsByChannelID" class="scbtSortStreamsByChannelID" title="Sort Saved Chats by Alphabetical">Strmr ⬇️</button> <button id="scbtSortStreamsByVideoID" class="scbtSortStreamsByVideoID" title="Sort Saved Chats by Stream Title">Title ⬇️</button> <button id="scbtSortStreamsByChan" class="scbtSortStreamsByChan" title="Sort Saved Chats by This Channel First">Chan ⬇️</button> <div id="scbtPreviousStreams" class="scbtPreviousStreams"></div> </p></div></div><div><input type="radio" name="scbtSavedAccordion" id="scbtSavedAccordion2" class="accordion__input"> <label for="scbtSavedAccordion2" class="accordion__label" title="import chat replays from previous liveStreams to this device">Import Chat</label> <div class="accordion__content"><p><small>(view or search chat from a different livestream)</small> <br /><label for="scbtChatImportButton">upload chat-log from stream from .csv file</label> <input type="file" id="scbtChatImportButton" class="scbtChatImportButton" accept=".csv" /> </p></div></div><div><input type="radio" name="scbtSavedAccordion" id="scbtSavedAccordion4" class="accordion__input"> <label for="scbtSavedAccordion4" class="accordion__label" title="import extension settings from a different device or browser to this device">Import Settings</label> <div class="accordion__content"><p><small>(use extension settings from a different device or browser)</small> <br /><label for="scbtSettingsImportButton">import settings from .json file</label> <input type="file" id="scbtSettingsImportButton" class="scbtSettingsImportButton" value="Import Settings" accept=".json" /> </p></div></div><div><input type="radio" name="scbtSavedAccordion" id="scbtSavedAccordion5" class="accordion__input"> <label for="scbtSavedAccordion5" class="accordion__label" title="use these extension settings on a different device or browser">Export Settings</label> <div class="accordion__content"><p><small>(use these extension settings on a different device or browser)</small> <br /><label for="scbtSettingsExportButton">export settings to .json file</label> <input type="button" id="scbtSettingsExportButton" class="scbtSettingsExportButton" value="Export Settings" /> </p></div></div><div><input type="radio" name="scbtSavedAccordion" id="scbtSavedAccordion6" class="accordion__input"> <label for="scbtSavedAccordion6" class="accordion__label" title="delete these extension settings from this browser">Delete Settings</label> <div class="accordion__content"><p><small>(if this extension is messed up, delete extension settings and start from scratch)</small> <br /><label for="scbtSettingsDeleteButton">delete settings</label> <input type="button" id="scbtSettingsDeleteButton" class="scbtSettingsDeleteButton" value="Delete Settings" /> </p></div></div></div>';
theHTML += '</fieldset></p></details>';


theHTML += '<button>Close</button><button id="scbtLoadLiveViewButton" class="scbtLoadLiveViewButton">🛜</button><button id="scbtLoadClipsButton" class="scbtLoadClipsButton">📅</button>';
theHTML += '</form>';
theHTML += '<small><i>Disclaimer: This is not an official part of the streaming service. Any information, data, or exporting done from this extension are not legal representations of what has been said by people or how the service performs but to assist the streamer and viewer in using the service, not take the place of the service and what is on the service.</i></small>';
theHTML += '</dialog>';

theHTML += '<h5 id="scbtChatTitle" class="scbtChatTitle scbt-bl" aria-label="Search Chat Label" title="Search Chat Label"><span id="scbtChatA" class="scbtChatA"></span><span id="scbtChatB" class="scbtChatB"></span></h5>';

theHTML += '<div id="scbtDragSpot1" class="scbtDragSpot1"><svg id="scbtDragSpot1CloseButton" class="scbtDragSpot1CloseButton" viewPort="0 0 12 12" version="1.1" xmlns="http://www.w3.org/2000/svg"><line x1="1" y1="33" x2="33" y2="1" stroke="black" stroke-width="2"/><line x1="1" y1="1" x2="33" y2="33" stroke="black" stroke-width="2"/></svg><div id="scbtSimulcastChat1" class="scbtSimulcastChat1 scbt-chat" draggable="true"></div></div><div id="scbtDragSpot2"  class="scbtDragSpot2"><svg id="scbtDragSpot2CloseButton" class="scbtDragSpot2CloseButton" viewPort="0 0 12 12" version="1.1" xmlns="http://www.w3.org/2000/svg"><line x1="1" y1="33" x2="33" y2="1" stroke="black" stroke-width="2"/><line x1="1" y1="1" x2="33" y2="33" stroke="black" stroke-width="2"/></svg><div id="scbtSimulcastChat2" class="scbtSimulcastChat2 scbt-chat" draggable="true"></div></div><div id="scbtDragSpot3"  class="scbtDragSpot3"><svg id="scbtDragSpot3CloseButton" class="scbtDragSpot3CloseButton" viewPort="0 0 12 12" version="1.1" xmlns="http://www.w3.org/2000/svg"><line x1="1" y1="33" x2="33" y2="1" stroke="black" stroke-width="2"/><line x1="1" y1="1" x2="33" y2="33" stroke="black" stroke-width="2"/></svg><div id="scbtSimulcastChat3" class="scbtSimulcastChat3 scbt-chat" draggable="true"></div></div><div id="scbtDragSpot4"  class="scbtDragSpot4"><svg id="scbtDragSpot4CloseButton" class="scbtDragSpot4CloseButton" viewPort="0 0 12 12" version="1.1" xmlns="http://www.w3.org/2000/svg"><line x1="1" y1="33" x2="33" y2="1" stroke="black" stroke-width="2"/><line x1="1" y1="1" x2="33" y2="33" stroke="black" stroke-width="2"/></svg><div id="scbtSavedChat1" class="scbtSavedChat1 scbt-chat" draggable="true"></div></div>';

if (scbt.o.settingsFromSync.sz5b8 === true) {
  // Show Chat Menu
  theHTML += '<div id="scbtChatMenu" class="scbtChatMenu"><button id="scbtChatMenuToggleButton" class="scbtChatMenuToggleButton">⚙️</button><div id="scbtChatMenuInner" class="scbtChatMenuInner"><search><label for="scbtChatSearchInputText" class="sr-only">Enter Your Text To Search Chat For:</label><input id="scbtChatSearchInputText" class="scbtChatSearchInputText" name="scbtChatSearchInputText" type="search" autocapitalize="off" autocorrect="off" spellcheck="false" minlength="1" maxlength="100" placeholder="Search for..." aria-label="Enter Your Text To Search Chat For" dir="auto" title="Enter Your Text To Search Chat For" enterkeyhint="search"><span class="validity"></span></search><label for="scbtChatSearchStartsWithButton" class="sr-only">Search for chat starting with...</label><button id="scbtChatSearchStartsWithButton" class="scbtChatSearchStartsWithButton" aria-label="Search for chat starting with..." title="Search for chat starting with..." type="button">✍️</button><label for="scbtChatSearchUserButton" class="sr-only">Search for user saying...</label><button id="scbtChatSearchUserButton" class="scbtChatSearchUserButton" aria-label="Search for user saying..." title="Search for user saying..." type="button">🧑</button><label for="scbtChatSearchKeywordButton" class="sr-only">Search for keyword...</label><button id="scbtChatSearchKeywordButton" class="scbtChatSearchKeywordButton" aria-label="Search for keyword..." title="Search for keyword..." type="button">📝</button><label for="scbtChatSearchEventsButton" class="sr-only">Search for events...</label><button id="scbtChatSearchEventsButton" class="scbtChatSearchEventsButton" aria-label="Search for events..." title="Search for events..." type="button">🗓️</button><label for="scbtChatSearchFullButton" class="sr-only">Show the full chat from the beginning of recording</label><button id="scbtChatSearchFullButton" class="scbtChatSearchFullButton" aria-label="Show the full chat from the beginning of recording" title="Show the full chat from the beginning of recording" type="button">full</button><label for="scbtChatSearchMultiButton" class="sr-only">Search all saved chats rather than current stream</label><button id="scbtChatSearchMultiButton" class="scbtChatSearchMultiButton" aria-label="Search all saved chats rather than current stream" title="Search all saved chats rather than current stream" type="button"> + </button>    <button id="scbt14" class="scbt14" title="View Top of Chat" aria-label="View Top of Chat">⬆️</button><button id="scbt15" class="scbt15" title="View Bottom of Chat" aria-label="View Bottom of Chat">⬇️</button><button id="scbt20" class="scbt20" title="Change Chat Font Size" aria-label="Change Chat Font Size">aA</button><button id="scbt16" class="scbt16" title="Full Width Mode" aria-label="Full Width Mode"> &larr; </button><button id="scbt17" class="scbt17" title="Full Height Mode" aria-label="Full Height Mode"> &uarr; </button><button id="scbt4" class="scbt4" title="Toggle Broadcaster Messages Only" aria-label="Toggle Broadcaster Messages Only">👑</button><button id="scbt5" class="scbt5" title="Toggle Moderator Messages Only" aria-label="Toggle Moderator Messages Only">🔧</button><button id="scbt6" class="scbt6" title="Toggle Sub Messages Only" aria-label="Toggle Sub Messages Only">💰</button><button id="scbt7" class="scbt7" title="Toggle Sub + Moderator Messages Only" aria-label="Toggle Sub + Moderator Messages Only">💰🔧</button><button id="scbt8" class="scbt8" title="Toggle VIP + Verified Messages Only" aria-label="Toggle VIP + Verified Messages Only">🤵</button><button id="scbt9" class="scbt9" title="Toggle Donation Chats Only" aria-label="Toggle Donation Chats Only">$</button><button id="scbt11" class="scbt11" title="Toggle Mention Messages Only" aria-label="Toggle Mention Messages Only">@</button><button id="scbt12" class="scbt12" title="Toggle Hashtag Messages Only" aria-label="Toggle Hashtag Messages Only">#</button><button id="scbt10" class="scbt10" title="Toggle Non-Bot Messages Only" aria-label="Toggle Non-Bot Messages Only">🤖</button><button id="scbt13" class="scbt13" title="Toggle Text Messages Only" aria-label="Toggle Text Messages Only">ab</button></div></div>';
}
if (scbt.o.settingsFromSync.sz5b9 === true) {
  // Show Mention Menu
}
if (scbt.o.settingsFromSync.sz5b10 === true) {
  // Show Simulcast Chat
}
if (scbt.o.settingsFromSync.sz5b11 === true) {
  // Show Social Media Menu
}

theHTML += '</div>';
  
body.insertAdjacentHTML('afterbegin', theHTML);
// setTimeout(function() {
  var x = document.body.getElementsByClassName('scbtX');
  if (x[0]) {
    scbt.e.scbtX = x[0];
    var b = document.body;
    x = null;
    scbt.e.scbtLoading = b.getElementsByClassName('scbtLoading')[0];
    scbt.e.scbtClipsMenuWrapper = b.getElementsByClassName('scbtClipsMenuWrapper')[0];
    scbt.e.scbtClipsMenuContent = b.getElementsByClassName('scbtClipsMenuContent')[0];
    scbt.e.scbtStreamsMenuContent = b.getElementsByClassName('scbtStreamsMenuContent')[0];

    scbt.e.scbtSettingsMenu = b.getElementsByClassName('scbtSettingsMenu')[0];
    scbt.e.scbtSettingsMenuForm = b.getElementsByClassName('scbtSettingsMenuForm')[0];
    
    scbt.e.scbtChatImportButton = b.getElementsByClassName('scbtChatImportButton')[0];
    scbt.e.scbtSortStreamsByServiceID = b.getElementsByClassName('scbtSortStreamsByServiceID')[0];
    scbt.e.scbtSortStreamsByChannelID = b.getElementsByClassName('scbtSortStreamsByChannelID')[0];
    scbt.e.scbtSortStreamsByVideoID = b.getElementsByClassName('scbtSortStreamsByVideoID')[0];
    scbt.e.scbtSortStreamsByChan = b.getElementsByClassName('scbtSortStreamsByChan')[0];
    
    scbt.e.scbtPreviousStreams = b.getElementsByClassName('scbtPreviousStreams')[0];
    scbt.e.scbtChatInput = b.getElementsByClassName('scbtChatInput')[0];
    scbt.e.scbtChatTitle = b.getElementsByClassName('scbtChatTitle')[0];
    scbt.e.scbtChatA = b.getElementsByClassName('scbtChatA')[0];
    scbt.e.scbtChatB = b.getElementsByClassName('scbtChatB')[0];
    scbt.e.scbtCloseButton = b.getElementsByClassName('scbtCloseButton')[0];

    var elemArr = b.getElementsByClassName('scbtChatMenuToggleButton');
    if (elemArr[0]) {
      scbt.e.scbtChatMenuToggleButton = b.getElementsByClassName('scbtChatMenuToggleButton')[0];  
    }
    
    scbt.e.scbtSavedChat1 =     b.getElementsByClassName('scbtSavedChat1')[0];
    scbt.e.scbtSimulcastChat1 = b.getElementsByClassName('scbtSimulcastChat1')[0];
    scbt.e.scbtSimulcastChat2 = b.getElementsByClassName('scbtSimulcastChat2')[0];
    scbt.e.scbtSimulcastChat3 = b.getElementsByClassName('scbtSimulcastChat3')[0];

    scbt.e.scbtDragSpot1 = b.getElementsByClassName('scbtDragSpot1')[0];
    scbt.e.scbtDragSpot2 = b.getElementsByClassName('scbtDragSpot2')[0];
    scbt.e.scbtDragSpot3 = b.getElementsByClassName('scbtDragSpot3')[0];
    scbt.e.scbtDragSpot4 = b.getElementsByClassName('scbtDragSpot4')[0];

    scbt.e.scbtDragSpot1CloseButton = b.getElementsByClassName('scbtDragSpot1CloseButton')[0];
    scbt.e.scbtDragSpot2CloseButton = b.getElementsByClassName('scbtDragSpot2CloseButton')[0];
    scbt.e.scbtDragSpot3CloseButton = b.getElementsByClassName('scbtDragSpot3CloseButton')[0];
    scbt.e.scbtDragSpot4CloseButton = b.getElementsByClassName('scbtDragSpot4CloseButton')[0];

    scbt.e.scbtSettingsImportButton = b.getElementsByClassName('scbtSettingsImportButton')[0];
    scbt.e.scbtSettingsExportButton = b.getElementsByClassName('scbtSettingsExportButton')[0];
    scbt.e.scbtSettingsDeleteButton = b.getElementsByClassName('scbtSettingsDeleteButton')[0];

    // if we load database script
    if (scbt.o.settingsFromSync.sz5b1 === true) {
      scbt.f.add_listener_for_uploading_chatlog();
      scbt.f.add_listener_for_importing_settings();
      scbt.f.add_listener_for_exporting_settings();
      scbt.f.add_listener_for_deleting_settings();
      scbt.f.add_listener_for_chat_close();
    }

    if (scbt.o.settingsFromSync.sz5b31 === true) {
      scbt.e.scbtDragSpot1.classList.add('scbt-forefront');
      var arr = scbt.o.settingsFromSync.sz5v31.split('?p=');
      var arr2 = arr[1].split('-');
      scbt.o.settingsFromSync.scbtservice1 = arr2[0];
      scbt.o.settingsFromSync.scbtchannelid1 = arr2[1];
    }
    if (scbt.o.settingsFromSync.sz5b32 === true) {
      scbt.e.scbtDragSpot2.classList.add('scbt-forefront');
      var arr = scbt.o.settingsFromSync.sz5v32.split('?p=');
      var arr2 = arr[1].split('-');
      scbt.o.settingsFromSync.scbtservice2 = arr2[0];
      scbt.o.settingsFromSync.scbtchannelid2 = arr2[1];
    }
    if (scbt.o.settingsFromSync.sz5b33 === true) {
      scbt.e.scbtDragSpot3.classList.add('scbt-forefront');
      var arr = scbt.o.settingsFromSync.sz5v33.split('?p=');
      var arr2 = arr[1].split('-');
      scbt.o.settingsFromSync.scbtservice3 = arr2[0];
      scbt.o.settingsFromSync.scbtchannelid3 = arr2[1];
    }

    if (scbt.o.settingsFromSync.sz5b2 === true) {
      var x = b.getElementsByClassName('sz5b30');
      if (x[0]) {
        x[0].checked = scbt.o.settingsFromSync.sz5b30;
      }
      var x = b.getElementsByClassName('sz5v30');
      if (x[0]) {
        x[0].value = scbt.o.settingsFromSync.sz5v30;
      }

      var x = b.getElementsByClassName('sz5b31');
      if (x[0]) {
        x[0].checked = scbt.o.settingsFromSync.sz5b31;
      }
      var x = b.getElementsByClassName('sz5v31');
      if (x[0]) {
        x[0].value = scbt.o.settingsFromSync.sz5v31;
      }

      var x = b.getElementsByClassName('sz5b32');
      if (x[0]) {
        x[0].checked = scbt.o.settingsFromSync.sz5b32;
      }
      var x = b.getElementsByClassName('sz5v32');
      if (x[0]) {
        x[0].value = scbt.o.settingsFromSync.sz5v32;
      }

      var x = b.getElementsByClassName('sz5b33');
      if (x[0]) {
        x[0].checked = scbt.o.settingsFromSync.sz5b33;
      }
      var x = b.getElementsByClassName('sz5v33');
      if (x[0]) {
        x[0].value = scbt.o.settingsFromSync.sz5v33;
      }
    }    

    scbt.e.scbtLoadLiveViewButton = b.getElementsByClassName('scbtLoadLiveViewButton')[0];
    scbt.e.scbtLoadLiveViewButton.addEventListener('click', scbt.f.load_live_vod);

    scbt.e.scbtLoadClipsButton = b.getElementsByClassName('scbtLoadClipsButton')[0];
    scbt.e.scbtLoadClipsButton.addEventListener('click', scbt.f.toggle_clips_menu);

/*
    if (scbt.o.settingsFromSync.sz5b6 === true) {
      scbt.e.scbt14 = b.getElementsByClassName('scbt14')[0];
      scbt.e.scbt14.addEventListener('click', scbt.f.chat_up_to_top);

      scbt.e.scbt15 = b.getElementsByClassName('scbt15')[0];
      scbt.e.scbt15.addEventListener('click', scbt.f.chat_down_to_bottom);

      scbt.e.scbt20 = b.getElementsByClassName('scbt20')[0];
      scbt.e.scbt20.addEventListener('click', scbt.f.chat_font_size);

      scbt.e.scbt16 = b.getElementsByClassName('scbt16')[0];
      scbt.e.scbt16.addEventListener('click', scbt.f.chat_full_screen_width);

      scbt.e.scbt17 = b.getElementsByClassName('scbt17')[0];
      scbt.e.scbt17.addEventListener('click', scbt.f.chat_full_screen_height);
    }

    if (scbt.o.settingsFromSync.sz5b7 === true) {
      scbt.e.scbt4 = b.getElementsByClassName('scbt4')[0];
      scbt.e.scbt4.addEventListener('click', scbt.f.toggle_chats);

      scbt.e.scbt5 = b.getElementsByClassName('scbt5')[0];
      scbt.e.scbt5.addEventListener('click', scbt.f.toggle_chats);

      scbt.e.scbt6 = b.getElementsByClassName('scbt6')[0];
      scbt.e.scbt6.addEventListener('click', scbt.f.toggle_chats);

      scbt.e.scbt7 = b.getElementsByClassName('scbt7')[0];
      scbt.e.scbt7.addEventListener('click', scbt.f.toggle_chats);

      scbt.e.scbt8 = b.getElementsByClassName('scbt8')[0];
      scbt.e.scbt8.addEventListener('click', scbt.f.toggle_chats);

      scbt.e.scbt9 = b.getElementsByClassName('scbt9')[0];
      scbt.e.scbt9.addEventListener('click', scbt.f.toggle_chats);

      scbt.e.scbt11 = b.getElementsByClassName('scbt11')[0];
      scbt.e.scbt11.addEventListener('click', scbt.f.toggle_chats);

      scbt.e.scbt12 = b.getElementsByClassName('scbt12')[0];
      scbt.e.scbt12.addEventListener('click', scbt.f.toggle_chats);

      scbt.e.scbt10 = b.getElementsByClassName('scbt10')[0];
      scbt.e.scbt10.addEventListener('click', scbt.f.chat_non_bot);

      scbt.e.scbt13 = b.getElementsByClassName('scbt13')[0];
      scbt.e.scbt13.addEventListener('click', scbt.f.chat_text_only);
    }
*/

    if (scbt.o.settingsFromSync.sz5b8 === true) {
      scbt.e.scbt14 = b.getElementsByClassName('scbt14')[0];
      scbt.e.scbt14.addEventListener('click', scbt.f.chat_up_to_top);

      scbt.e.scbt15 = b.getElementsByClassName('scbt15')[0];
      scbt.e.scbt15.addEventListener('click', scbt.f.chat_down_to_bottom);

      scbt.e.scbt20 = b.getElementsByClassName('scbt20')[0];
      scbt.e.scbt20.addEventListener('click', scbt.f.chat_font_size);

      scbt.e.scbt16 = b.getElementsByClassName('scbt16')[0];
      scbt.e.scbt16.addEventListener('click', scbt.f.chat_full_screen_width);

      scbt.e.scbt17 = b.getElementsByClassName('scbt17')[0];
      scbt.e.scbt17.addEventListener('click', scbt.f.chat_full_screen_height);

      scbt.e.scbt4 = b.getElementsByClassName('scbt4')[0];
      scbt.e.scbt4.addEventListener('click', scbt.f.toggle_chats);

      scbt.e.scbt5 = b.getElementsByClassName('scbt5')[0];
      scbt.e.scbt5.addEventListener('click', scbt.f.toggle_chats);

      scbt.e.scbt6 = b.getElementsByClassName('scbt6')[0];
      scbt.e.scbt6.addEventListener('click', scbt.f.toggle_chats);

      scbt.e.scbt7 = b.getElementsByClassName('scbt7')[0];
      scbt.e.scbt7.addEventListener('click', scbt.f.toggle_chats);

      scbt.e.scbt8 = b.getElementsByClassName('scbt8')[0];
      scbt.e.scbt8.addEventListener('click', scbt.f.toggle_chats);

      scbt.e.scbt9 = b.getElementsByClassName('scbt9')[0];
      scbt.e.scbt9.addEventListener('click', scbt.f.toggle_chats);

      scbt.e.scbt11 = b.getElementsByClassName('scbt11')[0];
      scbt.e.scbt11.addEventListener('click', scbt.f.toggle_chats);

      scbt.e.scbt12 = b.getElementsByClassName('scbt12')[0];
      scbt.e.scbt12.addEventListener('click', scbt.f.toggle_chats);

      scbt.e.scbt10 = b.getElementsByClassName('scbt10')[0];
      scbt.e.scbt10.addEventListener('click', scbt.f.chat_non_bot);

      scbt.e.scbt13 = b.getElementsByClassName('scbt13')[0];
      scbt.e.scbt13.addEventListener('click', scbt.f.chat_text_only);

      scbt.e.scbtChatMenu = b.getElementsByClassName('scbtChatMenu')[0];
      scbt.e.scbtChatSearchInputText = b.getElementsByClassName('scbtChatSearchInputText')[0];
      scbt.e.scbtChatSearchInputText.addEventListener('input', scbt.f.handler_for_chat_mention_menu);

      scbt.e.scbtChatSearchStartsWithButton = b.getElementsByClassName('scbtChatSearchStartsWithButton')[0];
      scbt.e.scbtChatSearchStartsWithButton.addEventListener('click', scbt.f.search_saved_chat);

      scbt.e.scbtChatSearchUserButton = b.getElementsByClassName('scbtChatSearchUserButton')[0];
      scbt.e.scbtChatSearchUserButton.addEventListener('click', scbt.f.search_saved_chat);      

      scbt.e.scbtChatSearchKeywordButton = b.getElementsByClassName('scbtChatSearchKeywordButton')[0];
      scbt.e.scbtChatSearchKeywordButton.addEventListener('click', scbt.f.search_saved_chat);

      scbt.e.scbtChatSearchEventsButton = b.getElementsByClassName('scbtChatSearchEventsButton')[0];
      scbt.e.scbtChatSearchEventsButton.addEventListener('click', scbt.f.search_saved_chat);

      scbt.e.scbtChatSearchFullButton = b.getElementsByClassName('scbtChatSearchFullButton')[0];
      scbt.e.scbtChatSearchFullButton.addEventListener('click', scbt.f.search_saved_chat);

      scbt.e.scbtChatSearchMultiButton = b.getElementsByClassName('scbtChatSearchMultiButton')[0];
      scbt.e.scbtChatSearchMultiButton.addEventListener('click', scbt.f.toggle_search_multiple_chat);
    }

    scbt.e.scbtSortStreamsByServiceID.addEventListener('click', scbt.f.handler_sort_saved_streams_by_serviceid);
    scbt.e.scbtSortStreamsByChannelID.addEventListener('click', scbt.f.handler_sort_saved_streams_by_channelid);
    scbt.e.scbtSortStreamsByVideoID.addEventListener('click', scbt.f.handler_sort_saved_streams_by_videoid);
    scbt.e.scbtSortStreamsByChannelID.addEventListener('click', scbt.f.handler_sort_saved_streams_by_current);
    scbt.e.scbtChatMenuToggleButton.addEventListener('click', scbt.f.toggle_chat_menu);

    document.getElementById('scbtSavedAccordion1').addEventListener('click', scbt.f.get_arr_of_all_dbNames);
    if (scbt.b.popout === true) {
      console.log('we have a popout');
      document.body.classList.add('scbt-popout');
      var elemArr = document.querySelectorAll('#chatroom-top .items-center.text-center');
      console.log(elemArr);
      if (elemArr[0]) {
        elemArr[0].addEventListener('click', scbt.f.open_settings);
      }
    }

    scbt.e.scbtSavedChat1.addEventListener('dragstart', scbt.f.drag);
    scbt.e.scbtSimulcastChat1.addEventListener('dragstart', scbt.f.drag);
    scbt.e.scbtSimulcastChat2.addEventListener('dragstart', scbt.f.drag);
    scbt.e.scbtSimulcastChat3.addEventListener('dragstart', scbt.f.drag);

    scbt.e.scbtDragSpot1.addEventListener('drop', scbt.f.drop);
    scbt.e.scbtDragSpot1.addEventListener('dragover', scbt.f.allowDrop);
    scbt.e.scbtDragSpot2.addEventListener('drop', scbt.f.drop);
    scbt.e.scbtDragSpot2.addEventListener('dragover', scbt.f.allowDrop);
    scbt.e.scbtDragSpot3.addEventListener('drop', scbt.f.drop);
    scbt.e.scbtDragSpot3.addEventListener('dragover', scbt.f.allowDrop);
    scbt.e.scbtDragSpot4.addEventListener('drop', scbt.f.drop);
    scbt.e.scbtDragSpot4.addEventListener('dragover', scbt.f.allowDrop);

    scbt.f.do_settings();
    elemArr = body = theHTML = x = b = null; return true;
  }
// }, 1000);
}


scbt.f.open_settings = function(){
  console.log('open_settings');
  var x = document.querySelectorAll('#scbtSettingsMenu');
  console.log(x);
  if (x[0]) {
    x[0].showModal();  
  }
  x = null; return false;
}


scbt.f.get_info_from_url_string = function(str){
  var obj = {};
  obj.channelid = '';
  obj.serviceid = location.href.split('/')[3];
  if ( str.indexOf('arena.tv') > -1) {
    obj.channelid = 'arena';
  }
  if ( str.indexOf('instagram.com') > -1) {
    obj.channelid = 'instagram';
  }
  if ( str.indexOf('kick.com') > -1) {
    obj.channelid = 'kick';
  }
  if ( str.indexOf('noice.com') > -1) {
    obj.channelid = 'noice';
  }
  if ( str.indexOf('odysee.com') > -1) {
    obj.channelid = 'odysee';
  }
  if ( str.indexOf('rumble.com') > -1) {
    obj.channelid = 'rumble';
  }
  if ( str.indexOf('shareplay.com') > -1) {
    obj.channelid = 'shareplay';
  }
  if ( str.indexOf('tiktok.com') > -1) {
    obj.channelid = 'tiktok';
  }
  if ( str.indexOf('twitch.tv') > -1) {
    obj.channelid = 'twitch';
  }
  if ( str.indexOf('twitter.com') > -1) {
    obj.channelid = 'twitter';
  }
  if ( str.indexOf('youtube.com') > -1) {
    obj.channelid = 'youtube';
  }
  return obj;
}


scbt.f.add_listener_for_saved_chat_from_streams = function(e){
  setTimeout(function() {
    var elemArr = document.body.getElementsByClassName('az2');
    [].forEach.call(elemArr, function(elem) {
      if (elem) { elem.addEventListener('click', scbt.f.go_to_stream); }
    });
    elemArr = document.body.getElementsByClassName('az3');
    [].forEach.call(elemArr, function(elem) {
      if (elem) { elem.addEventListener('click', scbt.f.chat_load_by_videoid); }
    });
    elemArr = document.body.getElementsByClassName('az4');
    [].forEach.call(elemArr, function(elem) {
      if (elem) { elem.addEventListener('click', scbt.f.chat_export_by_videoid); }
    });
    elemArr = document.body.getElementsByClassName('az5');
    [].forEach.call(elemArr, function(elem) {
      if (elem) { elem.addEventListener('click', scbt.f.chat_delete_by_videoid); }
    });
    elemArr = document.body.getElementsByClassName('az6');
    [].forEach.call(elemArr, function(elem) {
      if (elem) { elem.addEventListener('click', scbt.f.chat_mark_by_videoid); }
    });
    elemArr = document.body.getElementsByClassName('az7');
    [].forEach.call(elemArr, function(elem) {
      if (elem) { elem.addEventListener('click', scbt.f.chat_export_to_offline); }
    });
    e = elem = elemArr = arrl = null; return false;
  }, 2000);
}


// FUNCTIONAL functions TODO
scbt.f.chat_auto_show = function(){
  scbt.f.chat_full_screen_height();
  scbt.f.chat_full_screen_width();
  var elemArr = [];

  // hide usernames on screen
  if (scbt.o.settingsFromSync.sz4b8 === true) {
    var css = ' ';
    if (scbt.v.serviceid == 'arena') {
      // TODO
    }
    if (scbt.v.serviceid == 'instagram') {
      // TODO
    }
    if (scbt.v.serviceid == 'kick') {
      css = ' .chat-entry-username, .inline-block.text-xs.font-semibold.text-gray-400 { display: none; } ';
    }
    if (scbt.v.serviceid == 'noice') {

    }
    if (scbt.v.serviceid == 'odysee') {
      css = ' .livestream-comment__meta-information { display: none; } ';
    }
    if (scbt.v.serviceid == 'rumble') {
      css = ' .chat-history--message { display: none; } ';
    }
    if (scbt.v.serviceid == 'shareplay') {
      // TODO
    }
    if (scbt.v.serviceid == 'tiktok') {
    
    }
    if (scbt.v.serviceid == 'twitch') {
      css = ' .video-chat__message-author, .chatter-name, .chat-author__display-name, .chat-line__username, .user-notice-line { display: none; } ';
    }
    if (scbt.v.serviceid == 'twitter') {
      // TODO
    }
    if (scbt.v.serviceid == 'youtube') {
      css = ' .yt-live-chat-item-list-renderer #author-name { display: none; } ';
    }
      
    var head = document.getElementsByTagName('head')[0];
    var style = document.createElement('style');
    head.appendChild(style);
    style.type = 'text/css';
    if (style.styleSheet){
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }
    if (scbt.e.scbtYTChatIframe) {
      style = document.createElement('style');
      style = 'text/css';
      head = scbt.e.scbtYTChatIframe.head;
      head.appendChild(style);
      if (style.styleSheet){
        style.styleSheet.cssText = css;
      } else {
        style.appendChild(document.createTextNode(css));
      }
    }
  }

  if (scbt.b.mobile == false) {
    if (scbt.v.serviceid == 'arena') {
      // TODO
    }
    if (scbt.v.serviceid == 'instagram') {
      // TODO
    }
    if (scbt.v.serviceid == 'kick') {
      elemArr = document.body.querySelectorAll('.channel.relative.h-full > div > div:last-of-type');
      if (elemArr[0]) {
        elemArr[0].style.position = 'absolute';
      }
      elemArr = document.body.getElementsByClassName('chat-container');
      if (elemArr[0]) {
        elemArr[0].style.position = 'absolute';
        elemArr[0].style.top = '1px';
        elemArr[0].style.marginTop = '0 auto';
        elemArr[0].style.zIndex = '100000000';
        elemArr[0].style.display = 'block';
        elemArr[0].style.height = (window.screen.height - 100) + 'px';
        elemArr[0].style.background = '#111';
        elemArr[0].style.opacity = '.66';
      }
      elemArr = document.body.getElementsByClassName('chat-content');
      if (elemArr[0]) {
        elemArr[0].style.background = 'transparent';
      }
    }
    if (scbt.v.serviceid == 'noice') {
      // TODO
    }
    if (scbt.v.serviceid == 'odysee') {
      elemArr = document.body.getElementsByClassName('chat__wrapper');
      if (elemArr[0]) {
        elemArr[0].style.position = 'absolute';
      }
      elemArr = document.getElementsByClassName('chat__wrapper');
      if (elemArr[0]) {
        // elemArr[0].style.position = 'absolute';
        // elemArr[0].style.top = '1px';
        // elemArr[0].style.marginTop = '0 auto';
        elemArr[0].style.zIndex = '100000000';
        elemArr[0].style.display = 'block';
        elemArr[0].style.height = (window.screen.height - 100) + 'px';
        elemArr[0].setAttribute('style', 'background-color:transparent !important');
        // elemArr[0].style.opacity = '.66';
        elemArr[0].style.textShadow = '1px 1px 4px #000';
      }
      elemArr = document.body.getElementsByClassName('chat');
      if (elemArr[0]) {
        elemArr[0].style.background = 'transparent';
      }
    }
    if (scbt.v.serviceid == 'rumble') {
      elemArr = document.body.getElementsByClassName('media-page-chat-aside-chat-wrapper-fixed');
      if (elemArr[0]) {
        // position: absolute;
        // top: 0px;
        elemArr[0].style.position = 'absolute';
        elemArr[0].style.top = '1px';
        elemArr[0].style.height = '104vh';
      }
      elemArr = document.body.getElementsByClassName('media-page-chat-aside-chat');
      if (elemArr[0]) {
        elemArr[0].style.position = 'absolute';
        elemArr[0].style.top = '0px';
        elemArr[0].style.left = '0px';
        elemArr[0].style.right = 'initial';
      }
      elemArr = document.body.getElementsByClassName('chat');
      if (elemArr[0]) {
        elemArr[0].style.backgroundColor = 'transparent';
      }
      elemArr = document.body.getElementsByClassName('chat-form-overflow-wrapper');
      if (elemArr[0]) {
        elemArr[0].style.backgroundColor = '#000';
      }
    }
    if (scbt.v.serviceid == 'shareplay') {
      // TODO
    }
    if (scbt.v.serviceid == 'tiktok') {
      // TODO
    }
    if (scbt.v.serviceid == 'twitch') {
      elemArr = document.querySelectorAll('.channel.relative.h-full > div > div:last-of-type');
      if (elemArr[0]) {
        elemArr[0].style.position = 'absolute';
      }
      elemArr = document.body.getElementsByClassName('chat-container');
      if (elemArr[0]) {
        elemArr[0].style.position = 'absolute';
        elemArr[0].style.top = '1px';
        elemArr[0].style.marginTop = '0 auto';
        elemArr[0].style.zIndex = '100000000';
        elemArr[0].style.display = 'block';
        elemArr[0].style.height = (window.screen.height - 100) + 'px';
        elemArr[0].style.background = '#111';
        elemArr[0].style.opacity = '.66';
      }
      elemArr = document.body.getElementsByClassName('chat-content');
      if (elemArr[0]) {
        elemArr[0].style.background = 'transparent';
      }
    }
    if (scbt.v.serviceid == 'twitter') {
      // TODO
    }
    if (scbt.v.serviceid == 'youtube') {
      // TODO 
    }

  }
  
  css = head = style = elemArr = null; return false;
}


scbt.f.chat_blur = function(elem){
  elem.style.paddingLeft = '.5rem';
  elem.style.paddingRight = '.5rem';
  elem.style.borderBottom = '1px dotted ' + scbt.o.scbtBorderColor;
  elem = null; return false;
}


scbt.f.chat_clean = function(obj, elem){
  if (scbt.v.serviceid == 'arena') {
    // TODO
  }
  if (scbt.v.serviceid == 'instagram') {
    // TODO
  }
  if (scbt.v.serviceid == 'kick') {
    obj = scbt.f.chat_clean_kick(obj, elem);
  }
  if (scbt.v.serviceid == 'noice') {
    obj = scbt.f.chat_clean_noice(obj, elem);
  }
  if (scbt.v.serviceid == 'odysee') {
    obj = scbt.f.chat_clean_odysee(obj, elem);
  }
  if (scbt.v.serviceid == 'rumble') {
    obj = scbt.f.chat_clean_rumble(obj, elem);
  }
  if (scbt.v.serviceid == 'shareplay') {
    // TODO
  }
  if (scbt.v.serviceid == 'tiktok') {
    obj = scbt.f.chat_clean_tiktok(obj, elem);
  }
  if (scbt.v.serviceid == 'twitch') {
    obj = scbt.f.chat_clean_twitch(obj, elem);
  }
  if (scbt.v.serviceid == 'twitter') {
    // TODO
  }
  if (scbt.v.serviceid == 'youtube') {
    obj = scbt.f.chat_clean_youtube(obj, elem);
  }
  elem = null; return obj;
}


scbt.f.chat_down_to_bottom = function(){
  if (!scbt.v.dbNameToSearch) { scbt.f.toast(' No active stream or VOD chat '); return false; } 
  if (scbt.e.scbtSavedChat1.classList.contains('scbt-active') ) {
    scbt.e.scbtDragSpot1.scrollTop = scbt.e.scbtDragSpot1.scrollHeight - scbt.e.scbtDragSpot1.clientHeight;
    scbt.e.scbtDragSpot2.scrollTop = scbt.e.scbtDragSpot2.scrollHeight - scbt.e.scbtDragSpot2.clientHeight;
    scbt.e.scbtDragSpot3.scrollTop = scbt.e.scbtDragSpot3.scrollHeight - scbt.e.scbtDragSpot3.clientHeight;
    scbt.e.scbtDragSpot4.scrollTop = scbt.e.scbtDragSpot4.scrollHeight - scbt.e.scbtDragSpot4.clientHeight;
    return false;
  } else {
    var elemArr = [];
    if (scbt.v.serviceid == 'arena') {
      // TODO
    }
    if (scbt.v.serviceid == 'instagram') {
      // TODO
    }
    if (scbt.v.serviceid == 'kick') {
      elemArr = scbt.f.get_arr_chatbox_elem();
      if (elemArr[0]) {
        elemArr[0].scrollTop = elemArr[0].scrollHeight - elemArr[0].clientHeight;
      }
    }
    if (scbt.v.serviceid == 'noice') {

    }
    if (scbt.v.serviceid == 'odysee') {
      elemArr = scbt.f.get_arr_chatbox_elem();
      if (elemArr[0]) {
        elemArr[0].scrollTop = elemArr[0].scrollHeight - elemArr[0].clientHeight;
      }
    }
    if (scbt.v.serviceid == 'rumble') {
      elemArr = scbt.f.get_arr_chatbox_elem();
      if (elemArr[0]) {
        elemArr[0].scrollTop = elemArr[0].scrollHeight - elemArr[0].clientHeight;
      }
    }
    if (scbt.v.serviceid == 'shareplay') {
      // TODO
    }
    if (scbt.v.serviceid == 'tiktok') {
      var elemArr = scbt.f.get_arr_chatbox_elem();
      if (elemArr[0]) {
        elemArr[0].scrollTop = elemArr[0].scrollHeight - elemArr[0].clientHeight;
      }
    }
    if (scbt.v.serviceid == 'twitch') {
      elemArr = document.body.getElementsByClassName('simplebar-scroll-content');
      if (elemArr[4]) {
        elemArr[4].scrollTop = elemArr[4].scrollHeight - elemArr[4].clientHeight;
      }
      if (elemArr[2]) {
        elemArr[2].scrollTop = elemArr[2].scrollHeight - elemArr[2].clientHeight;
      }
    }
    if (scbt.v.serviceid == 'twitter') {
      // TODO
    }
    if (scbt.v.serviceid == 'youtube') {
      if (scbt.e.scbtYTChatIframe && scbt.e.scbtYTChatIframe.body) {
        elemArr = scbt.e.scbtYTChatIframe.body.querySelectorAll('#contents.yt-live-chat-app');
        if (elemArr[0]) {
          elemArr[0].scrollTop = elemArr[0].scrollHeight - elemArr[0].clientHeight;
        }
        elemArr = scbt.e.scbtYTChatIframe.body.querySelectorAll('#content-pages.yt-live-chat-renderer');
        if (elemArr[0]) {
          elemArr[0].scrollTop = elemArr[0].scrollHeight - elemArr[0].clientHeight;
        }
        elemArr = scbt.e.scbtYTChatIframe.body.querySelectorAll('#chat-messages.yt-live-chat-renderer');
        if (elemArr[0]) {
          elemArr[0].scrollTop = elemArr[0].scrollHeight - elemArr[0].clientHeight;
        }
        elemArr = scbt.e.scbtYTChatIframe.body.querySelectorAll('.style-scope.yt-live-chat-renderer');
        if (elemArr[0]) {
          elemArr[0].scrollTop = elemArr[0].scrollHeight - elemArr[0].clientHeight;
        }
        elemArr = scbt.e.scbtYTChatIframe.body.querySelectorAll('#primary-content.yt-live-chat-header-renderer');
        if (elemArr[0]) {
          elemArr[0].scrollTop = elemArr[0].scrollHeight - elemArr[0].clientHeight;
        }
        elemArr = scbt.e.scbtYTChatIframe.body.querySelectorAll('#contents.yt-live-chat-renderer');
        if (elemArr[0]) {
          elemArr[0].scrollTop = elemArr[0].scrollHeight - elemArr[0].clientHeight;
        }
        elemArr = scbt.e.scbtYTChatIframe.body.querySelectorAll('#chat.yt-live-chat-renderer');
        if (elemArr[0]) {
          elemArr[0].scrollTop = elemArr[0].scrollHeight - elemArr[0].clientHeight;
        }
        elemArr = scbt.e.scbtYTChatIframe.body.querySelectorAll('#item-scroller');
        if (elemArr[0]) {
          elemArr[0].scrollTop = elemArr[0].scrollHeight - elemArr[0].clientHeight;
        }
      }
      // console.log('in chat_down_to_bottom elemarr is ', elemArr);
    }
  }
  elemArr = null; return false;
}  


scbt.f.chat_focus = function(){
  scbt.e.scbtChatInput.focus();
  return false;
}


scbt.f.chat_font_size = function(){
  if (!scbt.v.dbNameToSearch) { scbt.f.toast(' No active stream or VOD chat ');  } 
  if (scbt.e.scbtSavedChat1.classList.contains('scbt-active') ) {
    var elemArr = scbt.e.scbtSavedChat1.getElementsByTagName('p');
  } else {
    // console.log('doing scbt.f.chat_font_size 1');

    if ( (scbt.v.serviceid == 'twitch') || (scbt.v.serviceid == 'youtube') ) {
      var elemArr = scbt.f.get_arr_chats();
    } else {
      var elemArr = scbt.f.get_arr_message_elems_from_parent_element(document.body);
      // console.log('doing scbt.f.chat_font_size 3');
    }
    if (scbt.v.fontUp < 6) {
      if (scbt.v.fontSize == 'initial') {
        scbt.v.fontSize = 1;
      }
      if (scbt.v.heightSize == 'initial') {
        scbt.v.heightSize = 'auto'; 
      }
      scbt.v.fontSize = scbt.v.fontSize - -.25;
      var str = scbt.v.fontSize + 'em';
      scbt.v.fontUp = scbt.v.fontUp - -1;
    } else {
      scbt.v.fontSize = 'initial';
      var str = 'initial';
      scbt.v.heightSize = 'initial';
      var str2 = 'initial';
      scbt.v.fontUp = 1;
    }
    [].forEach.call(elemArr, function(elem) {
      if (scbt.v.serviceid == 'youtube') {
        var els = elem.querySelectorAll('#message');
        if (els[0]) {
          els[0].style.setProperty('font-size', str, 'important');
          els[0].style.height = str2;
        }
      } else {
        elem.style.setProperty('font-size', str, 'important');
        elem.style.height = str2;  
      }
    });
  }
  str = str2 = elem = elemArr = els = null; return false;
}


scbt.f.chat_full_screen_height = function(){
  if (scbt.v.serviceid == 'arena') {
    // TODO
  }
  if (scbt.v.serviceid == 'instagram') {
    // TODO
  }
  if (scbt.v.serviceid == 'kick') {
      if (scbt.b.fullScreenHeight === false) {
        var elemArr = document.body.querySelectorAll('.flex.h-full.w-full.flex-col.overflow-hidden > .flex.grow.overflow-hidden');
        if (elemArr[0]) {
          elemArr[0].style.height = '100vh';
          elemArr[0].style.overflow = 'visible';
          elemArr[0].style.zIndex = '100';
          elemArr[0].style.marginTop = '-3rem';
        }
        scbt.b.fullScreenHeight = true;
        scbt.f.chat_down_to_bottom();
        elemArr = null; return false;
      }

    if (scbt.b.fullScreenHeight === true) {
      var elemArr = document.body.querySelectorAll('.flex.h-full.w-full.flex-col.overflow-hidden > .flex.grow.overflow-hidden');
      if (elemArr[0]) {
        elemArr[0].style.height = 'initial';
        elemArr[0].style.overflow = 'hidden';
        elemArr[0].style.zIndex = '1';
        elemArr[0].style.marginTop = 'initial';
      }
      scbt.b.fullScreenHeight = false;
      scbt.f.chat_down_to_bottom();
      elemArr = null; return false;
    }
  }
  if (scbt.v.serviceid == 'noice') {
    // TODO
  }
  if (scbt.v.serviceid == 'odysee') {
    // TODO
  }
  if (scbt.v.serviceid == 'rumble') {
    if (scbt.b.fullScreenHeight === false) {
      var elemArr = document.body.getElementsByClassName('media-page-chat-aside-chat');
      if (elemArr[0]) {
        elemArr[0].style.position = 'absolute';
        elemArr[0].style.top = '0px';
        elemArr[0].style.right = '20px';
        elemArr[0].style.zIndex = '10000';
        elemArr[0].style.height = '99vh';
        elemArr[0].style.marginTop = '-3rem';
      }
      elemArr = document.body.getElementsByClassName('media-page-chat-aside-chat-wrapper-fixed');
      if (elemArr[0]) {
        elemArr[0].style.top = '0px';
      }
      scbt.b.fullScreenHeight = true;
      scbt.f.chat_down_to_bottom();
      elemArr = null; return false;
    }

    if (scbt.b.fullScreenHeight === true) {
      var elemArr = document.body.getElementsByClassName('media-page-chat-aside-chat');
      if (elemArr[0]) {
        elemArr[0].style.position = 'relative';
        elemArr[0].style.right = 'initial';
        elemArr[0].style.top = 'initial';
        elemArr[0].style.zIndex = 'initial';
        elemArr[0].style.height = 'initial';
        elemArr[0].style.marginTop = 'initial';
      }
      elemArr = document.body.getElementsByClassName('media-page-chat-aside-chat-wrapper-fixed');
      if (elemArr[0]) {
        elemArr[0].style.top = '70px';
      }

      scbt.b.fullScreenHeight = false;
      scbt.f.chat_down_to_bottom();
      elemArr = null; return false;
    }
  }
  if (scbt.v.serviceid == 'shareplay') {
    // TODO
  }
  if (scbt.v.serviceid == 'tiktok') {
    if (scbt.b.fullScreenHeight === false) {
      var elemArr = document.body.querySelectorAll("div[class*='DivHeaderContainer']");
      if (elemArr[0]) {
        elemArr[0].style.display = 'none';
      }
      elemArr = document.body.querySelectorAll("div[class*='DivSideNavContainer']");
      if (elemArr[0]) {
        elemArr[0].style.display = 'none';
      }
      elemArr = document.body.querySelectorAll("div[class*='DivChatRoomHeader']");
      if (elemArr[0]) {
        elemArr[0].style.display = 'none';
      }
      elemArr = document.body.querySelectorAll("div[class*='DivTopViewersContainer']");
      if (elemArr[0]) {
        elemArr[0].style.display = 'none';
      }
      elemArr = document.body.querySelectorAll("div[class*='DivRoomMessage']");
      if (elemArr[0]) {
        elemArr[0].style.display = 'none';
      }
      elemArr = document.body.querySelectorAll("div[class*='DivEnterMessage']");
      if (elemArr[0]) {
        elemArr[0].style.display = 'none';
      }
      elemArr = document.body.querySelectorAll("div[class*='DivChatRoomAnimationContainer']");
      if (elemArr[0]) {
        elemArr[0].style.height = '105vh';
      }
      elemArr = document.body.querySelectorAll("div[class*='DivLiveContentContainer']");
      if (elemArr[0]) {
        elemArr[0].style.marginTop = '-50px';
      }
      scbt.b.fullScreenHeight = true;
      scbt.f.chat_down_to_bottom();
      elemArr = null; return false;
    }
    if (scbt.b.fullScreenHeight === true) {
      var elemArr = document.body.querySelectorAll("div[class*='DivHeaderContainer']");
      if (elemArr[0]) {
        elemArr[0].style.display = 'flex';
      }
      elemArr = document.body.querySelectorAll("div[class*='DivSideNavContainer']");
      if (elemArr[0]) {
        elemArr[0].style.display = 'block';
      }
      elemArr = document.body.querySelectorAll("div[class*='DivChatRoomHeader']");
      if (elemArr[0]) {
        elemArr[0].style.display = 'flex';
      }
      elemArr = document.body.querySelectorAll("div[class*='DivTopViewersContainer']");
      if (elemArr[0]) {
        elemArr[0].style.display = 'flex';
      }
      elemArr = document.body.querySelectorAll("div[class*='DivRoomMessage']");
      if (elemArr[0]) {
        elemArr[0].style.display = 'flex';
      }
      elemArr = document.body.querySelectorAll("div[class*='DivEnterMessage']");
      if (elemArr[0]) {
        elemArr[0].style.display = 'flex';
      }
      elemArr = document.body.querySelectorAll("div[class*='DivChatRoomAnimationContainer']");
      if (elemArr[0]) {
        elemArr[0].style.height = 'calc(100vh - 60px)';
      }
      elemArr = document.body.querySelectorAll("div[class*='DivLiveContentContainer']");
      if (elemArr[0]) {
        elemArr[0].style.marginTop = '0px';
      }
      scbt.b.fullScreenHeight = false;
      scbt.f.chat_down_to_bottom();
      elemArr = null; return false;
    }
  }
  if (scbt.v.serviceid == 'twitch') {
    // TODO
  }
  if (scbt.v.serviceid == 'twitter') {
    // TODO
  }
  if (scbt.v.serviceid == 'youtube') {
    if (scbt.b.fullScreenHeight === false) {
       var elemArr = document.body.querySelectorAll('#chat');
       if (elemArr[0]) {
        elemArr[0].style.height = '96vh'; 
       }
       var elemArr = document.body.querySelectorAll('#secondary');
       if (elemArr[0]) {
        elemArr[0].style.paddingTop = '0px';
       }
       scbt.b.fullScreenHeight = true;
       scbt.f.chat_down_to_bottom();
       elemArr = null; return false;
    }

    if (scbt.b.fullScreenHeight === true) {
      var elemArr = document.body.querySelectorAll('#chat');
      if (elemArr[0]) {
        elemArr[0].style.height = 'initial';
      }
      var elemArr = document.body.querySelectorAll('#secondary');
      if (elemArr[0]) {
        elemArr[0].style.paddingTop = 'initial';
      }
      scbt.b.fullScreenHeight = false;
      scbt.f.chat_down_to_bottom();
      elemArr = null; return false;
    }
  }

}


scbt.f.chat_full_screen_width = function(){
  if (scbt.v.serviceid == 'arena') {
    // TODO
  }
  if (scbt.v.serviceid == 'instagram') {
    // TODO
  }
  if (scbt.v.serviceid == 'kick') {
    if (scbt.b.fullScreenWidth === false) {
      var elemArr = document.body.querySelectorAll('div.cursor-pointer.opacity-100');
      if (elemArr[0]) {
        elemArr[0].click();
      }
      var elemArr = document.body.querySelectorAll('.main-html.h-screen.w-screen .flex.h-full.w-full > .flex.grow.overflow-hidden:first-of-type > div.relative:first-of-type');
      if (elemArr[0]) {
        elemArr[0].style.display = 'none';
      }
      scbt.b.fullScreenWidth = true;
      elemArr = null; return false;
    }
    if (scbt.b.fullScreenWidth === true) {
      var elemArr = document.body.querySelectorAll('div.cursor-pointer.base-icon');
      if (elemArr[0]) {
        elemArr[0].click();
      }
      var elemArr = document.body.querySelectorAll('.main-html.h-screen.w-screen .flex.h-full.w-full > .flex.grow.overflow-hidden:first-of-type > div.relative:first-of-type');
      if (elemArr[0]) {
        elemArr[0].style.display = 'flex';
      }
      scbt.b.fullScreenWidth = false;
      elemArr = null; return false;
    }
  }
  if (scbt.v.serviceid == 'noice') {
    // TODO
  }
  if (scbt.v.serviceid == 'odysee') {
    if (scbt.b.fullScreenWidth === false) {
      var elemArr = document.body.getElementsByClassName('file-page__video');
      if (elemArr[0]) {
        elemArr[0].style.paddingRight = '0.1rem';
        elemArr[0].style.paddingLeft = '0.1rem';
        elemArr[0].style.maxWidth = 'none';
      }
      elemArr = document.body.getElementsByClassName('chat__wrapper');
      if (elemArr[0]) {
        elemArr[0].style.marginLeft = '-1rem';
        elemArr[0].style.width = '350px';
      }
      scbt.b.fullScreenWidth = true;
      elemArr = null; return false;
    }

    if (scbt.b.fullScreenWidth === true) {
      var elemArr = document.body.getElementsByClassName('file-page__video');
      if (elemArr[0]) {
        elemArr[0].style.paddingRight = '2rem';
        elemArr[0].style.paddingLeft = '2rem';
        elemArr[0].style.maxWidth = '1700px';
      }
      elemArr = document.body.getElementsByClassName('chat__wrapper');
      if (elemArr[0]) {
        elemArr[0].style.marginLeft = '0';
        elemArr[0].style.width = '350px';
      }
      scbt.b.fullScreenWidth = false;
      elemArr = null; return false;
    }
  }
  if (scbt.v.serviceid == 'rumble') {
    if (scbt.b.fullScreenWidth === false) {
      var elemArr = document.body.getElementsByClassName('nonconstrained');
      if (elemArr[0]) {
        elemArr[0].style.marginLeft = '0px';
      }
      elemArr = document.body.getElementsByClassName('navs');
      if (elemArr[0]) {
        elemArr[0].style.display = 'none';
      }
      scbt.b.fullScreenWidth = true;
      elemArr = null; return false;
    }
    if (scbt.b.fullScreenWidth === true) {
      var elemArr = document.body.getElementsByClassName('nonconstrained');
      if (elemArr[0]) {
        elemArr[0].style.marginLeft = '80px';
      }
      elemArr = document.body.getElementsByClassName('navs');
      if (elemArr[0]) {
        elemArr[0].style.display = 'block';
      }
      scbt.b.fullScreenWidth = false;
      elemArr = null; return false;
      }
  }
  if (scbt.v.serviceid == 'shareplay') {
    // TODO
  }
  if (scbt.v.serviceid == 'tiktok') {
    if (scbt.b.fullScreenWidth === false) {
      var elemArr = document.body.querySelectorAll('[data-e2e="chat-close-button"]');
      if (elemArr[0]) {
        elemArr[0].click();  
      } 
      elemArr = document.body.querySelectorAll("div[class*='DivSideNavContainer']");
      if (elemArr[0]) {
        elemArr[0].style.display = 'none';
      }
      scbt.b.fullScreenWidth = true;
      elemArr = null; return false;
    }
    if (scbt.b.fullScreenWidth === true) {
      var elemArr = document.body.querySelectorAll('[data-e2e="open-chat-button"]');
      if (elemArr[0]) {
        elemArr[0].click();  
      }
      elemArr = document.body.querySelectorAll("div[class*='DivSideNavContainer']");
      if (elemArr[0]) {
         elemArr[0].style.display = 'block';
      }
      scbt.b.fullScreenWidth = false;
      elemArr = null; return false;
    }
  }
  if (scbt.v.serviceid == 'twitch') {
    if (scbt.b.fullScreenWidth === false) {
      var elemArr = document.body.querySelectorAll("[data-a-target='right-column__toggle-collapse-btn']");
      if (elemArr[0]) {
        elemArr[0].click();  
      }
      var elemArr = document.body.getElementsByClassName('side-nav');
      if (elemArr[0]) {
        elemArr[0].style.display = 'none';
      }
      scbt.b.fullScreenWidth = true;
      elemArr = null; return false;
    }
    if (scbt.b.fullScreenWidth === true) {
      var elemArr = document.body.querySelectorAll("[data-a-target='right-column__toggle-collapse-btn']");
      if (elemArr[0]) {
        elemArr[0].click();  
      }
      var elemArr = document.body.getElementsByClassName('side-nav');
      if (elemArr[0]) {
         elemArr[0].style.display = 'block';
      }
      scbt.b.fullScreenWidth = false;
      elemArr = null; return false;
    }
  }
  if (scbt.v.serviceid == 'twitter') {
    // TODO
  }
  if (scbt.v.serviceid == 'youtube') {
    if (scbt.b.fullScreenWidth === false) {
    var elemArr = document.body.querySelectorAll('#chat');
    if (elemArr[0]) {
      elemArr[0].style.width = '34vw';
    }
    var elemArr = document.body.querySelectorAll('#primary');
    if (elemArr[0]) {
      elemArr[0].style.paddingRight = '0px';
    }
    scbt.b.fullScreenWidth = true;
    elemArr = null; return false;
  }

  if (scbt.b.fullScreenWidth === true) {
    var elemArr = document.body.querySelectorAll('#chat');
    if (elemArr[0]) {
      elemArr[0].style.width = 'initial';
    }
    var elemArr = document.body.querySelectorAll('#primary');
    if (elemArr[0]) {
      elemArr[0].style.paddingRight = 'initial';
    }
    scbt.b.fullScreenWidth = false;
    elemArr = null; return false;
    }
  }

}


scbt.f.chat_listen = function(isSaving){
  // console.log('doing scbt.f.chat_listen with isSaving', isSaving);
  if (scbt.o.observer) { return false; }
  targetNode = scbt.f.get_arr_chatbox_elem();
  // console.log('doing scbt.f.chat_listen with targetnode', targetNode);
  if (targetNode[0]) {
    var config = { childList: true,
                 attributes: false,
                 characterData: false,
                 subtree: false,
                 attributeOldValue: false,
                 characterDataOldValue: false 
               };
    var callback = function(mutationsList, observer) {
      for(const mutation of mutationsList) {
          if (mutation.type === 'childList') {
              if (mutation.addedNodes) {
                var arrl = mutation.addedNodes.length;
                for (var i=0; i < arrl; i++) { 
                  scbt.f.process_chat_line(mutation.addedNodes[i], isSaving); // save locally
                  if (scbt.o.settingsFromSync.sz5b17 === true) {
                    var elemArr = scbt.f.get_arr_chatbox_elem();
                    if (elemArr[0]) {
                      var str = elemArr[0].scrollHeight;
                      elemArr[0].scrollTop = "-" + str;  
                    }
                  }
                }
              }  // if (mutation.addedNodes) {
            }   // end if (mutation.type === 'childList') {      
       } // end for(const mutation of mutationsList) {
    };  // end var callback = function(mutationsList, observer) {

    if (scbt.o) {
      scbt.o.observer = new MutationObserver(callback);
      scbt.o.observer.observe(targetNode[0], config);
    }
  }
  return false;
}


scbt.f.chat_load_by_videoid = function(e){
  if (!e) { return false; }
  if (!e.srcElement) { return false; }
  if (!e.srcElement.dataset) { return false; }
  if (!e.srcElement.dataset.dbname) { return false; }
  if (e.preventDefault) { e.preventDefault(); }
  scbt.f.build_chat_by_dbName_string(e.srcElement.dataset.dbname);
  scbt.e.scbtSettingsMenu.close("");
  
  // scbt.e.scbtSettingsMenu.classList.remove('scbt-bl'); dbName
  // scbt.e.scbtChatTitle.classList.add('scbt-bl');
  // scbt.e.scbtChatContent.classList.add('scbt-bl');
  // scbt.e.scbtChatMenu.classList.add('scbt-fl');  
  // scbt.b.searchBarActive = true;
  e = null; return false;
}


scbt.f.chat_make_decisions = function(obj, elem){
  if (scbt.v.serviceid == 'arena') {
    // TODO
  }
  if (scbt.v.serviceid == 'instagram') {
    // TODO
  }
  if (scbt.v.serviceid == 'kick') {
    obj = scbt.f.chat_make_decisions_kick(obj, elem, scbt.o.settingsFromSync);
  }
  if (scbt.v.serviceid == 'noice') {
    obj = scbt.f.chat_make_decisions_noice(obj, elem, scbt.o.settingsFromSync);
  }
  if (scbt.v.serviceid == 'rumble') {
    obj = scbt.f.chat_make_decisions_rumble(obj, elem, scbt.o.settingsFromSync);
  }
  if (scbt.v.serviceid == 'shareplay') {
    // TODO
  }
  if (scbt.v.serviceid == 'odysee') {
    obj = scbt.f.chat_make_decisions_odysee(obj, elem, scbt.o.settingsFromSync);
  }
  if (scbt.v.serviceid == 'tiktok') {
    obj = scbt.f.chat_make_decisions_tiktok(obj, elem, scbt.o.settingsFromSync);
  }
  if (scbt.v.serviceid == 'twitch') {
    obj = scbt.f.chat_make_decisions_twitch(obj, elem, scbt.o.settingsFromSync);
  }
  if (scbt.v.serviceid == 'twitter') {
    // TODO
  }
  if (scbt.v.serviceid == 'youtube') {
    obj = scbt.f.chat_make_decisions_youtube(obj, elem, scbt.o.settingsFromSync);
  }
  return obj;
} // end scbt_helper_chat_make_decisions


scbt.f.chat_non_bot = function(){
  if (scbt.e.scbtSavedChat1.classList.contains('scbt-active') ) {
  } else {
    if (scbt.v.serviceid != 'twitch') {
      var elemArr = scbt.f.get_arr_chats();
      if (scbt.n.nonBotChatShow === 1) {
          scbt.e.scbtChatB.textContent = ' - non bot chats -';
          [].forEach.call(elemArr, function(elem) {
            elem.style.opacity = 1;
            var elem2Arr = scbt.f.get_arr_message_elems_from_parent_element(elem);
            if (elem2Arr[0]) {
              var str = elem2Arr[0].textContent;
              if (str) {
                if (str.indexOf('!') == '0') {
                  elem.style.opacity = '.25';
                }
              }
            }
            var elem2Arr = scbt.f.get_arr_username_elems_from_parent_element(elem);
            if (elem2Arr[0]) {
              var str = elem2Arr[0].textContent;
              if (str) {
                str = str.toLowerCase();
                if ( ( str.indexOf('bot') > -1 ) || ( str == 'streamelements') || ( str == 'streamlabs') ) {
                  elem.style.opacity = '.25';
                }
              }
            }
        });
        scbt.n.nonBotChatShow = 2;
        elemArr = elem2Arr = str = elem = null; return false;
      }
    }

    if (scbt.v.serviceid == 'twitch') {
      var elemArr = document.body.getElementsByClassName('va-vod-chat');
      if (elemArr[0]) {
        var elemArr = document.body.getElementsByClassName('video-chat__message');
      } else {
        var elemArr = document.body.querySelectorAll('.chat-line-message-body, .chat-line__message');  
      }
      var toSearchFor = '.text-fragment';
      var toSearchFor2 = '.chat-author__display-name';
      if (scbt.n.nonBotChatShow === 1) {
          scbt.e.scbtChatB.textContent = ' - non bot chats -';
          [].forEach.call(elemArr, function(elem) {
            elem.style.opacity = 1;
            var m = elem.querySelector(toSearchFor);
            if (m) {
              var um = m.textContent;
              if (um) {
                if (um.indexOf('!') == '0') {
                  elem.style.opacity = '.25';
                }
              }
            }
            var arr = elem.querySelectorAll(toSearchFor2);
            [].forEach.call(arr, function(username) {
              if (username) {
                var u = username.textContent;
                if (u) {
                  u = u.toLowerCase();
                  if ( ( u.indexOf('bot') > -1 ) || ( u == 'streamelements') || ( u == 'streamlabs') ) {
                    elem.style.opacity = '.25';
                  }
                }
              }
            });
          });
      scbt.n.nonBotChatShow = 2;
      elemArr = elem2Arr = str = elem = null; return false;
    }
  }

  if (scbt.n.nonBotChatShow === 2) {
    scbt.e.scbtChatB.textContent = '';
    [].forEach.call(elemArr, function(elem) {
      elem.style.opacity = 1;
      elem.parentElement.style.opacity = 1;
    });
    scbt.n.nonBotChatShow = 1;
    elemArr = elem2Arr = str = elem = null; return false;
  }

  }
}


scbt.f.chat_off = function(elem){
  elem.style.paddingLeft = 'initial';
  elem.style.paddingRight = 'initial';
  elem.style.borderBottom = '0px';
}

scbt.f.chat_on = function(elem){
  elem.style.paddingLeft = '.5rem';
  elem.style.paddingRight = '.5rem';
  elem.style.borderBottom = '2px solid ' + scbt.c.scbtBorderColor;
}

scbt.f.chat_set_to_hide = function(elem){
  elem.style.opacity = 0;
}

scbt.f.chat_set_to_highlight = function(elem, color){
  elem.style.setProperty('border-left', '2px solid ' + color, 'important');
  elem.style.setProperty('border-right', '2px solid ' + color, 'important');
}

scbt.f.chat_set_to_mute = function(elem){
  elem.style.opacity = '.3';
}

scbt.f.chat_set_to_show = function(elem){
  elem.style.opacity = '1';
}


scbt.f.chat_text_only = function(){
  if (scbt.e.scbtSavedChat1.classList.contains('scbt-active') ) {
    var elemArr = scbt.e.scbtSavedChat1.getElementsByTagName('span');

    if (scbt.n.textOnlyChatShow === 1) {
      scbt.e.scbtChatB.textContent = ' - text only chats -';
      [].forEach.call(elemArr, function(elem) {
        elem.style.display = 'none';
      });
      scbt.n.textOnlyChatShow = 2;
      elemArr = elem = null; return false;
    }

    if (scbt.n.textOnlyChatShow === 2) { 
      scbt.e.scbtChatB.textContent = ' - non text only chats -';
      [].forEach.call(elemArr, function(elem) {
          elem.style.display = 'inline-block';
          scbt.f.chat_blur(elem);
      });
      scbt.n.textOnlyChatShow = 3;
      elemArr = elem = null; return false;
    }

    if (scbt.n.textOnlyChatShow === 3) {
      scbt.e.scbtChatB.textContent = '';
      [].forEach.call(elemArr, function(elem) {
        elem.style.display = 'inline-block';
        scbt.f.chat_off(elem);
      });
      scbt.n.textOnlyChatShow = 1;
      elemArr = elem = null; return false;
    }

  } else {
    
    var elemArr = scbt.f.get_arr_chats();

    if (scbt.n.textOnlyChatShow === 1) {
      scbt.e.scbtChatB.textContent = ' - text only chats -';

      [].forEach.call(elemArr, function(elem) {
        var elem2Arr = scbt.f.get_arr_message_elems_from_parent_element(elem);
        if (elem2Arr[0]) {
          elem2Arr[0].style.visibility = 'hidden';
        }
        var imgElemArr = elem.getElementsByTagName('img');
        [].forEach.call(imgElemArr, function(imgElm) {
          imgElm.style.visibility = 'hidden';
        });
        var iElemArr = elem.getElementsByTagName('i');
        [].forEach.call(iElemArr, function(iElem) {
          iElem.style.visibility = 'hidden';
        });
        var svgElemArr = elem.getElementsByTagName('svg');
        [].forEach.call(svgElemArr, function(svgElem) {
          svgElem.style.visibility = 'hidden';
        });
        var canvasElemArr = elem.getElementsByTagName('canvas');
        [].forEach.call(canvasElemArr, function(canvasElem) {
          canvasElem.style.visibility = 'hidden';
        });
      });

      scbt.n.textOnlyChatShow = 2;
      elemArr = elem = elem2Arr = imgElemArr = imgElm = iElemArr = iElem = svgElemArr = svgElem = canvasElemArr = canvasElem = null; return false;
    }

    if (scbt.n.textOnlyChatShow === 2) {
      scbt.e.scbtChatB.textContent = '';
      
      [].forEach.call(elemArr, function(elem) {
        var elem2Arr = scbt.f.get_arr_message_elems_from_parent_element(elem);
        if (elem2Arr[0]) {
          elem2Arr[0].style.visibility = 'visible';
        }
        var imgElemArr = elem.getElementsByTagName('img');
        [].forEach.call(imgElemArr, function(imgElm) {
          imgElm.style.visibility = 'visible';
        });
        var iElemArr = elem.getElementsByTagName('i');
        [].forEach.call(iElemArr, function(iElem) {
          iElem.style.visibility = 'visible';
        });
        var svgElemArr = elem.getElementsByTagName('svg');
        [].forEach.call(svgElemArr, function(svgElem) {
          svgElem.style.visibility = 'visible';
        });
        var canvasElemArr = elem.getElementsByTagName('canvas');
        [].forEach.call(canvasElemArr, function(canvasElem) {
          canvasElem.style.visibility = 'visible';
        });
      });

      scbt.n.textOnlyChatShow = 1;
      elemArr = elem = elem2Arr = imgElemArr = imgElm = iElemArr = iElem = svgElemArr = svgElem = canvasElemArr = canvasElem = null; return false;
    } //if (scbt.n.textOnlyChatShow === 2)
  } // if livechat
}


scbt.f.chat_up_to_top = function(){
  if (!scbt.v.dbNameToSearch) { scbt.f.toast(' No active stream or VOD chat '); return false; } 
  if (scbt.e.scbtSavedChat1.classList.contains('scbt-active') ) {
    scbt.e.scbtDragSpot1.scrollTop = 0;
    scbt.e.scbtDragSpot2.scrollTop = 0;
    scbt.e.scbtDragSpot3.scrollTop = 0;
    scbt.e.scbtDragSpot4.scrollTop = 0;
  } else {
    
    if (scbt.v.serviceid == 'kick') {
      var elemArr = scbt.f.get_arr_chatbox_elem();
      if (elemArr[0]) {
        elemArr[0].scroll({top:0,behavior:'smooth'});
        elemArr[0].animate({ scrollTop: 0 }, 100);
        elemArr[0].scrollTop = 0;
      }
    }
    if (scbt.v.serviceid == 'noice') {
    }
    if (scbt.v.serviceid == 'odysee') {
    }
    if (scbt.v.serviceid == 'rumble') {
      elemArr = scbt.f.get_arr_chatbox_elem();
      if (elemArr[0]) {
        elemArr[0].scroll({top:0,behavior:'smooth'});
        elemArr[0].animate({ scrollTop: 0 }, 100);
        elemArr[0].scrollTop = 0;
      }
    }
    if (scbt.v.serviceid == 'tiktok') {
      var elemArr = scbt.f.get_arr_chatbox_elem();
      if (elemArr[0]) {
        elemArr[0].scroll({top:0,behavior:'smooth'});
        elemArr[0].animate({ scrollTop: 0 }, 100);
        elemArr[0].scrollTop = 0;
      }
    }
    if (scbt.v.serviceid == 'twitch') {
      var elemArr = document.body.getElementsByClassName('simplebar-scroll-content');
      if (elemArr[4]) {
        elemArr[4].scroll({top:0,behavior:'smooth'});
        elemArr[4].animate({ scrollTop: 0 }, 100);
        elemArr[4].scrollTop = 0;
      }
      if (elemArr[2]) {
        elemArr[2].scroll({top:0,behavior:'smooth'});
        elemArr[2].animate({ scrollTop: 0 }, 100);
        elemArr[2].scrollTop = 0;
      }
    }
    if (scbt.v.serviceid == 'youtube') {
      // console.log('in chat_up_to_top elemarr is ', scbt.e.scbtYTChatIframe);
      if (scbt.e.scbtYTChatIframe && scbt.e.scbtYTChatIframe.body) {
        scbt.e.scbtYTChatIframe.body.querySelectorAll('#contents.yt-live-chat-app')[0].scrollTop = 0;
        scbt.e.scbtYTChatIframe.body.querySelectorAll('#content-pages.yt-live-chat-renderer')[0].scrollTop = 0;
        scbt.e.scbtYTChatIframe.body.querySelectorAll('#chat-messages.yt-live-chat-renderer')[0].scrollTop = 0;
        scbt.e.scbtYTChatIframe.body.querySelectorAll('.style-scope.yt-live-chat-renderer')[0].scrollTop = 0;
        scbt.e.scbtYTChatIframe.body.querySelectorAll('#primary-content.yt-live-chat-header-renderer')[0].scrollTop = 0;
        scbt.e.scbtYTChatIframe.body.querySelectorAll('#contents.yt-live-chat-renderer')[0].scrollTop = 0;
        scbt.e.scbtYTChatIframe.body.querySelectorAll('#chat.yt-live-chat-renderer')[0].scrollTop = 0;
        scbt.e.scbtYTChatIframe.body.querySelectorAll('#item-scroller')[0].scrollTop = 0;
      }
    }
  }
  elemArr = null; return false;
}



scbt.f.copy_text_to_clipboard = function(str){
  var toShare = location.href;
  if (typeof str == 'string') {
    toShare = str;
  }
  navigator.clipboard.writeText(toShare).then(function() {
    setTimeout(function(){ scbt.f.toast('copied to clipboard'); str = toShare = null; return false; }, 500);
  }, function(err) {
    alert(err);
    console.error(err);
    err = str = toShare = null; return false;
  });
}


scbt.f.do_coming_soon_message = function(){
  scbt.f.toast('Status: Coming soon in a later version in 2024');
  return false;
}


scbt.f.full_screen_video = function(){
  var videoEl = scbt.f.get_arr_video_elem();
  if (videoEl[0]) {
    videoEl[0].requestFullscreen();
  }
  videoEl = null; return false;
}


scbt.f.go_to_stream = function(e){
  if (!e) { return false; }
  if (!e.srcElement) { return false; }
  if (!e.srcElement.dataset) { return false; }
  if (!e.srcElement.dataset.channelid) { return false; }
  if (!e.srcElement.dataset.serviceid) { return false; }

  var str = e.target.dataset.channelid;
  var str2 = e.target.dataset.serviceid;
  if (str2 == 'kick') {
    window.open('https://kick.com/' + str,'_blank');
  }
  if (str2 == 'noice') {
    window.open('https://noice.com/' + str,'_blank');
  }
  if (str2 == 'odysee') {
    window.open('https://odysee.com/$/search?q=' + str,'_blank');
  }
  if (str2 == 'rumble') {
    str = str.replace(/ /g, '');
    str = str.replace(/_+/g, '');
    window.open('https://rumble.com/c/' + str,'_blank');
  }
  if (str2 == 'tiktok') {
    window.open('https://www.tiktok.com/@' + str, '_blank');
  }
  if (str2 == 'twitch') {
    window.open('https://twitch.tv/' + str, '_blank');
  }
  if (str2 == 'youtube') {
    str = str.replace(/ /g, '+');
    window.open('https://www.youtube.com/results?search_query=' + str,'_blank');
  }
  e = str = str2 = null; return false;
}


scbt.f.go_to_timestamp_in_video = function(e){
  if (scbt.b.vodLoaded === true) {
    var seconds = e;
    if (e) {
      if (e.target) {
        if (e.target.dataset) {
          if (e.target.dataset.seconds) {
           seconds = e.target.dataset.seconds;
          }
        }
      }
    }
    var videoEl = document.body.getElementsByTagName('video');
    if (videoEl[0]) {
      if (videoEl[0].readyState > 0) {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        videoEl[0].currentTime = Number(seconds);
        videoEl[0].play();
        videoEl = null;
      }
    }
  }
  return false;
}


scbt.f.is_element_visible = function(el){
  var rect     = el.getBoundingClientRect();
  var vWidth   = window.innerWidth || document.documentElement.clientWidth;
  var vHeight  = window.innerHeight || document.documentElement.clientHeight;
  var efp      = function(x, y) { return document.elementFromPoint(x, y) };     
  if (rect.right < 0 || rect.bottom < 0 || rect.left > vWidth || rect.top > vHeight) {
    return false;
  }
  return (
        el.contains(efp(rect.left,  rect.top))
    ||  el.contains(efp(rect.right, rect.top))
    ||  el.contains(efp(rect.right, rect.bottom))
    ||  el.contains(efp(rect.left,  rect.bottom))
  );
}


scbt.f.notify_background_page = function(obj){
  chrome.runtime.sendMessage({ chatmessage: obj });
  return false;
}


scbt.f.process_chat_line = function(elem, currentChatLine){
  var obj = {};
  obj.isHighlighted = 0;
  obj.isMuted = 0;
  obj.isHidden = 0;
  obj.isBot = 0;
  obj.hashtag = 0;
  obj.mention = 0;

  obj.anevent = 0;
  obj.donation = 0;
  obj.founder = 0;
  obj.gifter = 0;
  obj.moderator = 0;
  obj.newSub = 0;
  obj.og = 0;
  obj.owner = 0;
  obj.staff = 0;
  obj.sub = 0;
  obj.timestamp = '00:02AM';
  obj.verified = 0;

  obj = scbt.f.chat_clean(obj, elem);
  // console.log(' doing process_chat_line obj is: ', obj);

  if (scbt.o.settingsFromSync.sz4b22) {
    obj = scbt.f.get_obj_filter_blocked_links_from_obj(obj, elem);
  }
  if (scbt.o.settingsFromSync.sz4v2) {
    obj = scbt.f.get_obj_filter_blocked_users_from_obj(obj, elem);
  }
  if (scbt.o.settingsFromSync.sz4v1 || scbt.b.sz5b3Loaded === true) {
    obj = scbt.f.get_obj_filter_blocked_words_from_obj(obj, elem);
  }
  if (scbt.o.settingsFromSync.sz2v1) {
    obj = scbt.f.get_obj_filter_vip_users_from_obj(obj, elem);
  }
  if (scbt.o.settingsFromSync.sz2v2) {
    obj = scbt.f.get_obj_filter_highlighted_words_from_obj(obj, elem);
  }

  obj = scbt.f.chat_make_decisions(obj, elem);


  if (obj.isHighlighted != 0) {
    scbt.f.chat_set_to_highlight(elem, obj.isHighlighted);
  }
  if (obj.isMuted === 1) {
    scbt.f.chat_set_to_mute(elem);
  }
  if (obj.isHidden === 1) {
    scbt.f.chat_set_to_hide(elem);
  }

  // click on message to mention in chat (for mobile)
  if (scbt.o.settingsFromSync.sz5b19 === true) {
    var elemArr = scbt.f.get_arr_message_elems_from_parent_element(elem);
    if (elemArr[0]) {
      elemArr[0].addEventListener('click', scbt.f.handler_for_chat_mention_menu, false);  
      elemArr = null;
    }
  }

  // speak out loud chat with keywords
  if (scbt.o.settingsFromSync.sz5v1 && (scbt.o.settingsFromSync.sz5v1 != '') && (currentChatLine === true) ) {
    scbt.f.chat_speak(obj.message);
  }

  // console.log('because we are saving? ' + currentChatLine + ' , going to save this chat line USER: ' + obj.username + ' MES: ' + obj.message + ' BOT: ' + obj.isBot + ' to dbName ' + scbt.v.dbName);
  // console.log('simulcastSender is ' + scbt.b.simulcastSender + ' simulcastReceiverTabId is ' + scbt.v.simulcastReceiverTabId);

  if ( 
    (scbt.o.settingsFromSync.sz5b1 === true) && 
    scbt.v.dbName && 
    (currentChatLine === true) &&
    obj &&
    (obj.isBot == 0) &&
    obj.itemid &&
    obj.username &&
    obj.message
    ) {

    // delete obj.isHighlighted;
    // delete obj.isMuted;
    // delete obj.isHidden;
    // delete obj.isBot;
    // delete obj.hashtag;
    // delete obj.mention;
    try {
      if ( (scbt.b.simulcastSender === true) && scbt.v.simulcastReceiverTabId) {
        console.log(location.href + ' is sending a message over to: ' + scbt.v.simulcastReceiverTabId);
        obj.service = scbt.v.serviceid;
        obj.channelid = scbt.v.channelid;
        obj.simulcastReceiverTabId = scbt.v.simulcastReceiverTabId;
        chrome.runtime.sendMessage({ chatmessage: obj });
      } else {
        scbt.f.set_db_save_chat_obj(obj);
      }
      elem = currentChatLine = obj = null; return false;
    } catch (errorx) {
      console.error('chat save error ', errorx);
      elem = currentChatLine = obj = errorx = null; return false;
    }
  
  }
}




scbt.f.share_native_disabled = function(e){
  if (e) {
    if (navigator.share) { } else {
      e.classList.add('disabled');
    }
  }
  e = null; return false;
}


scbt.f.theatre_mode = function(){
  if (scbt.v.serviceid == 'kick') {
    var elemArr = document.body.getElementsByClassName('chat-container');
   if (elemArr[0]) {
    if (elemArr[0].style.display == 'block') {
      elemArr[0].style.display = 'none';
    } else {
      elemArr[0].style.display = 'block';
    }
   }
   var elemArr = document.body.getElementsByClassName('sidebar');
   if (elemArr[0]) {
    if (elemArr[0].style.display == 'block') {
      elemArr[0].style.display = 'none';
    } else {
      elemArr[0].style.display = 'block';
    }
   }
  }
  
  if (scbt.v.serviceid == 'noice') {

  }

  if (scbt.v.serviceid == 'odysee') {
  
  }
  
  if (scbt.v.serviceid == 'rumble') {
    var elemArr = document.body.getElementsByClassName('media-page-chat-container-toggle-btn');
    if (elemArr[0]) {
      elemArr[0].click();
    }
    scbt.f.chat_full_screen_width();
  }
  
  if (scbt.v.serviceid == 'tiktok') {
    scbt.f.chat_full_screen_width();
  }
  
  if (scbt.v.serviceid == 'twitch') {
    var elemArr = document.body.querySelectorAll('.toggle-visibility__right-column button');
    if (elemArr[0]) {
      elemArr[0].click();
    }
  }
  
  if (scbt.v.serviceid == 'youtube') {
    var elemArr = document.body.querySelectorAll('#secondary');
    if (elemArr[0]) {
      if (elemArr[0].style.display == 'none') {
        elemArr[0].style.display = 'block';
      } else {
        elemArr[0].style.display = 'none';
      }
    }
  }

  elemArr = null; return false;
}


scbt.f.toggle_chat_menu = function(e){
  var elemArr = document.body.getElementsByClassName('scbtChatMenuInner');
  if (elemArr[0]) {
    if (elemArr[0].classList.contains('scbt-bl') ) {
      elemArr[0].classList.remove('scbt-bl');
    } else {
      elemArr[0].classList.add('scbt-bl');
    }
  }
  return false;
}


scbt.f.toggle_simulcast_chat = function(e){
  if (scbt.o.settingsFromSync.sz5b10 === true) {
    scbt.e.scbtDragSpot1.classList.add('scbt-active');
    scbt.e.scbtDragSpot2.classList.add('scbt-active');
    scbt.e.scbtDragSpot3.classList.add('scbt-active');
    scbt.e.scbtDragSpot4.classList.add('scbt-active');
  }
  if (scbt.o.settingsFromSync.sz5b10 === false) {
    scbt.e.scbtDragSpot1.classList.remove('scbt-active');
    scbt.e.scbtDragSpot2.classList.remove('scbt-active');
    scbt.e.scbtDragSpot3.classList.remove('scbt-active');
    scbt.e.scbtDragSpot4.classList.remove('scbt-active'); 
  }
  return false;
}


scbt.f.toggle_chats = function(e){
  var parameter = '';
  if (e) {
    if (typeof e == 'string') {
      parameter = e;
    } else {
      if (e.preventDefault) { e.preventDefault(); }
      if (e.target) {
        if (e.target.id) {
          switch ( e.target.id ) {
            case 'scbt4':
              parameter = 'owner';
              break;
            case 'scbt5':
              parameter = 'moderator';
              break;
            case 'scbt6':
               parameter = 'sub';
              break;
            case 'scbt7':
              parameter = 'mod_sub';
              break;
            case 'scbt8':
              parameter = 'vip';
              break;
            case 'scbt9':
              parameter = 'donation';
              break;
            case 'scbt11':
              parameter = 'mention';
              break;
            case 'scbt12':
              parameter = 'hashtag';
              break;
            case 'scbt29':
              parameter = 'newSub';
              break;
          } // end switch
        } // if e target id
      }   // e target
    }     // if e is not a string 

  }       // if e
console.log(' toggle_chats ' + parameter);
  if (parameter) {
    
    if (parameter == 'vip') {
      parameter = 'verified';
    }
    // saved chat
    if (scbt.e.scbtSavedChat1.classList.contains('scbt-active') ) {
      var elemArr = scbt.e.scbtSavedChat1.getElementsByTagName('p');
      
      if (scbt.n.visibilityChatShow === 1) {
        scbt.e.scbtChatB.textContent = scbt.f.set_chat_parameter(parameter, scbt.n.visibilityChatShow);
        [].forEach.call(elemArr, function(elem) {
          if (parameter == 'mod_sub') {
            if ( (elem.classList.contains('sub')) || (elem.classList.contains('moderator')) ) {
              scbt.f.chat_on(elem);
              elem.style.display = 'block';
            } else {
              elem.style.display = 'none';
            }
          } else if ( (parameter == 'mention') || (parameter == 'hashtag') ) {
            var elem2Arr = elem.getElementsByTagName('b');
            if (elem2Arr[0]) { var str = elem2Arr[0].textContent; } else { var str = elem.textContent; }
            if ( ((parameter == 'mention') && (str.indexOf('@') > -1) ) || ((parameter == 'hashtag') && ( str.indexOf('#') > -1) ) ) {  
              scbt.f.chat_on(elem);
              elem.style.display = 'block';
            } else {
              elem.style.display = 'none';
            }
          } else {
            if (elem.classList.contains(parameter) ) {
              scbt.f.chat_on(elem);
              elem.style.display = 'block';
            } else {
              elem.style.display = 'none';
            }
          }
        });
        scbt.n.visibilityChatShow= 2; 
        parameter = str = a = alt = elemArr = elem = elem2Arr = elem2 = toSearchFor = arr = null; return false;
      }

      if (scbt.n.visibilityChatShow === 2) {
        scbt.e.scbtChatB.textContent = scbt.f.set_chat_parameter(parameter, scbt.n.visibilityChatShow);
        [].forEach.call(elemArr, function(elem) {
          if (parameter == 'mod_sub') {
            if ( (elem.classList.contains('sub')) || (elem.classList.contains('moderator')) ) {
              elem.style.display = 'none';
            } else {
              elem.style.display = 'block';
              scbt.f.chat_blur(elem);
            }
          } else if ( (parameter == 'mention') || (parameter == 'hashtag') ) {
            var elem2Arr = elem.getElementsByTagName('b');
            if (elem2Arr[0]) { var str = elem2Arr[0].textContent; } else { var str = elem.textContent; }
            if ( ((parameter == 'mention') && (str.indexOf('@') > -1) ) || ((parameter == 'hashtag') && ( str.indexOf('#') > -1) ) ) {  
              elem.style.display = 'none';
            } else {
              elem.style.display = 'block';
              scbt.f.chat_blur(elem);
            }
          } else {
            if (elem.classList.contains(parameter) ) {
              elem.style.display = 'none';
            } else {
              elem.style.display = 'block';
              scbt.f.chat_blur(elem);
            }
          }
        });
        scbt.n.visibilityChatShow = 3;
        parameter = str = a = alt = elemArr = elem = elem2Arr = elem2 = toSearchFor = arr = null; return false;
      }

      if (scbt.n.visibilityChatShow === 3) {
        scbt.e.scbtChatB.textContent = '';
        [].forEach.call(elemArr, function(elem) {
          elem.style.display = 'block';
          scbt.f.chat_off(elem);
        });
        scbt.n.visibilityChatShow = 1;
        parameter = str = a = alt = elemArr = elem = elem2Arr = elem2 = toSearchFor = arr = null; return false;
      }

      // live chat
    } else {
      if (scbt.v.serviceid == 'kick') {
        scbt.f.toggle_chats_kick(parameter); return false;
      }
      if (scbt.v.serviceid == 'noice') {
        scbt.f.toggle_chats_noice(parameter); return false;
      }
      if (scbt.v.serviceid == 'odysee') {
        scbt.f.toggle_chats_odysee(parameter); return false;
      }
      if (scbt.v.serviceid == 'rumble') {
        scbt.f.toggle_chats_rumble(parameter); return false;
      }
      if (scbt.v.serviceid == 'tiktok') {
        scbt.f.toggle_chats_tiktok(parameter); return false;
      }
      if (scbt.v.serviceid == 'twitch') {
        scbt.f.toggle_chats_twitch(parameter); return false;
      }
      if (scbt.v.serviceid == 'youtube') {
        scbt.f.toggle_chats_youtube(parameter); return false;
      }
  }
  return false;
  }
}

 
scbt.f.allowDrop = function(ev){
  ev.preventDefault();
}

scbt.f.drag = function(ev){
  scbt.e.scbtDragSpot1.classList.add('scbt-forefront');
  scbt.e.scbtDragSpot2.classList.add('scbt-forefront');
  scbt.e.scbtDragSpot3.classList.add('scbt-forefront');
  scbt.e.scbtDragSpot4.classList.add('scbt-forefront');
  ev.dataTransfer.setData("text", ev.target.id);
}

scbt.f.drop = function(ev){
  ev.preventDefault();
  scbt.e.scbtDragSpot1.classList.remove('scbt-forefront');
  scbt.e.scbtDragSpot2.classList.remove('scbt-forefront');
  scbt.e.scbtDragSpot3.classList.remove('scbt-forefront');
  scbt.e.scbtDragSpot4.classList.remove('scbt-forefront');
  var data = ev.dataTransfer.getData("text");
  ev.target.classList.add('scbt-forefront');
  ev.target.appendChild(document.getElementById(data));
}

// {sz1c1kick: '#f2c797'} is obj {sz1c1youtube: '#ee291b'}
scbt.f.style_with_obj_of_changes = function(obj){
  console.log('doing style_with_obj_of_changes with', obj);
  var css = '';
  if (obj) {
    
    // Background of chat in this hex colour.
    var isTrue = Object.keys(obj).some(i => { return i.startsWith('sz1c1') })
    var propStr = Object.keys(obj)[0];
    var valStr = obj[propStr];
    if (isTrue && propStr) {
      if (valStr == '#000000' || valStr == '') {
        css = css + ' body.scbt-desktop .scbt-chat { background-color: asdf !important; } ';
      }
      if (valStr == '#ffffff') {
        css = css + ' body.scbt-desktop .scbt-chat { background-color: transparent !important; } ';
      }
      if ( (valStr != '#ffffff') && (valStr != '#000000') && (valStr != '') ) {
        css = css + ' body.scbt-desktop .scbt-chat { background-color: ' + valStr + ' !important; } ';
      }
    }

    // User names in chat in this hex colour
    var isTrue = Object.keys(obj).some(i => { return i.startsWith('sz1c2') })
    var propStr = Object.keys(obj)[0];
    var valStr = obj[propStr];
    if (isTrue && propStr) {
      if (valStr == '#000000' || valStr == '') {
        css = css + ' body.scbt-desktop .scbt-chat { color: asdf !important; text-shadow: 0px; } ';
      }
      if (valStr == '#ffffff') {
        css = css + ' body.scbt-desktop .scbt-chat { color: transparent !important; text-shadow: 0.3px 0.3px dimgray; } ';
      }
      if ( (valStr != '#ffffff') && (valStr != '#000000') && (valStr != '') ) {
        css = css + ' body.scbt-desktop .scbt-chat { color:' + valStr + ' !important; text-shadow: 0.5px 0.5px darkslategrey; } ';
      }
    }

    // Highlights of chat in this colour
    var isTrue = Object.keys(obj).some(i => { return i.startsWith('sz1c3') })
    var propStr = Object.keys(obj)[0];
    // propStr = propStr + scbt.v.serviceid;
    var valStr = obj[propStr];
    if (isTrue && propStr) {
      if (valStr != '#000000') {
        scbt.c.scbtBorderColor = valStr;
      } else {
        scbt.c.scbtBorderColor = '#ff0000';
      }
    }

    // Chat messages in this colour
    var isTrue = Object.keys(obj).some(i => { return i.startsWith('sz1c4') })
    var propStr = Object.keys(obj)[0];
    // propStr = propStr + scbt.v.serviceid;
    var valStr = obj[propStr];
    if (isTrue && propStr) {
      if (valStr == '#000000' || valStr == '') {
        css = css + ' body.scbt-desktop .scbt-chat-message { color: asdf !important; text-shadow: 0px; } ';
      }
      if (valStr == '#ffffff') {
        css = css + ' body.scbt-desktop .scbt-chat-message { color: transparent !important; text-shadow: 0.3px 0.3px dimgray; } ';
      }  
      if ( (valStr != '#ffffff') && (valStr != '#000000') && (valStr != '') ) {
        css = css + ' body.scbt-desktop .scbt-chat-message { color: ' + valStr + ' !important; text-shadow: 0.5px 0.5px darkslategrey; } ';
      }
    }

    // Chat font sizes
    var isTrue = Object.keys(obj).some(i => { return i.startsWith('sz1n1') })
    var propStr = Object.keys(obj)[0];
    // propStr = propStr + scbt.v.serviceid;
    var valStr = obj[propStr];
    if (isTrue && propStr) {
      if ( Number(valStr) > 0) {
        var str = Number(valStr) + 'rem';
        css = css + ' body.scbt-desktop .scbt-chat-message { font-size: ' + str + '; line-height: ' + Number(valStr)  + '; } ';
      } else {
        css = css + ' body.scbt-desktop .scbt-chat-message { font-size: initial; line-height: unset; } ';
      }
    }

    // text only chat 
    var isTrue = Object.keys(obj).some(i => { return i.startsWith('sz1b1') })
    var propStr = Object.keys(obj)[0];
    // propStr = propStr + scbt.v.serviceid;
    var valStr = obj[propStr];
    if (isTrue && propStr && (valStr === true) ) {
      css = css + ' body.scbt-desktop .scbt-chat-username, body.scbt-desktop .scbt-chat-timestamp { visibility: hidden !important; } ';
    }
    if (isTrue && propStr && (valStr === false) ) {
      css = css + ' body.scbt-desktop .scbt-chat-username, body.scbt-desktop .scbt-chat-timestamp { visibility: visible !important; } ';      
    }

    // Upside down chat screen. Newest chat is on the top, oldest chat is on the bottom 
    var isTrue = Object.keys(obj).some(i => { return i.startsWith('sz5b17') })
    var propStr = Object.keys(obj)[0];
    // propStr = propStr + scbt.v.serviceid;
    var valStr = obj[propStr];
    if (isTrue && propStr && (valStr === true) ) {
     css = css + ' body.scbt-desktop .scbt-chat { display: flex; flex-direction: column-reverse; } ';
    }
    if (isTrue && propStr && (valStr === false) ) {
      css = css + ' body.scbt-desktop .scbt-chat { display: block; flex-direction: initial; } ';
    }

    // mouseover hover enlarge
    var isTrue = Object.keys(obj).some(i => { return i.startsWith('sz5b18') })
    var propStr = Object.keys(obj)[0];
    // propStr = propStr + scbt.v.serviceid;
    var valStr = obj[propStr];
    if (isTrue && propStr && (valStr === true) ) {
      css = css + ' body.scbt-desktop .scbt-chat { font-size: 175% !important; } ';
    }
    if (isTrue && propStr && (valStr === false) ) {
      css = css + ' body.scbt-desktop .scbt-chat { font-size: 100% !important; } ';
    }

  if (css) {
    var head = document.getElementsByTagName('head')[0];
    var style = document.createElement('style');
    head.appendChild(style);
    style.type = 'text/css';
    if (style.styleSheet){
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }
  }
  if (scbt.v.serviceid == 'arena') {
    // TODO
  }
  if (scbt.v.serviceid == 'instagram') {
    // TODO
  }
  if (scbt.v.serviceid == 'kick') {
    scbt.f.style_with_obj_of_changes_kick(obj);
  }
  if (scbt.v.serviceid == 'noice') {
    scbt.f.style_with_obj_of_changes_noice(obj);
  }
  if (scbt.v.serviceid == 'odysee') {
    scbt.f.style_with_obj_of_changes_odysee(obj);
  }
  if (scbt.v.serviceid == 'rumble') {
    scbt.f.style_with_obj_of_changes_rumble(obj);
  }
  if (scbt.v.serviceid == 'shareplay') {
    // TODO
  }
  if (scbt.v.serviceid == 'tiktok') {
    scbt.f.style_with_obj_of_changes_tiktok(obj);
  }
  if (scbt.v.serviceid == 'twitch') {
    scbt.f.style_with_obj_of_changes_twitch(obj);
  }
  if (scbt.v.serviceid == 'twitter') {
    // TODO
  }
  if (scbt.v.serviceid == 'youtube') {
    scbt.f.style_with_obj_of_changes_youtube(obj);
  }
 }
  obj = css = isTrue = propStr = valStr = head = style = null; return false;
}


scbt.f.sz5b30_off = function(){
  document.body.getElementsByClassName('sz5b30')[0].checked = false;
  document.body.getElementsByClassName('sz5v30')[0].value = '';
  scbt.o.settingsFromSync.sz5b30 = false;
  scbt.o.settingsFromSync.sz5v30 = '';
  scbt.b.simulcastSender = false;
  scbt.b.simulcastReceiver = false;
  chrome.storage.sync.set({'sz5b30': false}, function(error) { scbt.f.show_settings_error(error); })
  chrome.storage.sync.set({'sz5v30': ''}, function(error) { scbt.f.show_settings_error(error); })
  setTimeout(function(){ scbt.f.toast('Error: Not a valid livestream'); }, 2700);
  return false;
}
scbt.f.sz5b30_on = function(){
  document.body.getElementsByClassName('sz5b30')[0].checked = true;
  document.body.getElementsByClassName('sz5v30')[0].value = location.href;
  scbt.o.settingsFromSync.sz5b30 = true;
  scbt.o.settingsFromSync.sz5v30 = location.href;
  scbt.b.simulcastSender = false;
  scbt.b.simulcastReceiver = true;
  chrome.storage.sync.set({'sz5b30': true}, function(error) { scbt.f.show_settings_error(error); })
  chrome.storage.sync.set({'sz5v30': location.href}, function(error) { scbt.f.show_settings_error(error); })
  console.log('- this tab is the receiver: ' + location.href);
  chrome.runtime.sendMessage({ simulcastReceiver: "simulcastReceiver" });
  return false;
}

scbt.f.sz5b31_off = function(){
  document.body.getElementsByClassName('sz5b31')[0].checked = false;
  document.body.getElementsByClassName('sz5v31')[0].value = '';
  scbt.o.settingsFromSync.sz5b31 = false;
  scbt.o.settingsFromSync.sz5v31 = '';
  scbt.b.simulcastSender = false;
  scbt.b.simulcastReceiver = false;
  chrome.storage.sync.set({'sz5b31': false}, function(error) { scbt.f.show_settings_error(error); })
  chrome.storage.sync.set({'sz5v31': ''}, function(error) { scbt.f.show_settings_error(error); })
  setTimeout(function(){ scbt.f.toast('Error: Not a valid livestream'); }, 2700);
  return false;
}
scbt.f.sz5b31_on = function(){
  var url = '';
  if ( location.href.indexOf('?') > -1) {
    url = location.href + '&p=' + scbt.v.serviceid + '-' + scbt.v.channelid;
  } else {
    url = location.href + '/?p=' + scbt.v.serviceid + '-' + scbt.v.channelid;
  }
  document.body.getElementsByClassName('sz5b31')[0].checked = true;
  document.body.getElementsByClassName('sz5v31')[0].value = url;
  scbt.o.settingsFromSync.sz5b31 = true;
  scbt.o.settingsFromSync.sz5v31 = url;
  scbt.b.simulcastSender = true;
  scbt.b.simulcastReceiver = false;
  chrome.storage.sync.set({'sz5b31': true}, function(error) { scbt.f.show_settings_error(error); })
  chrome.storage.sync.set({'sz5v31': url}, function(error) { scbt.f.show_settings_error(error); })
  console.log('- this tab is the sender1: ' + url);
  return false;
}

scbt.f.sz5b32_off = function(){
  document.body.getElementsByClassName('sz5b32')[0].checked = false;
  document.body.getElementsByClassName('sz5v32')[0].value = '';
  scbt.o.settingsFromSync.sz5b32 = false;
  scbt.o.settingsFromSync.sz5v32 = '';
  scbt.b.simulcastSender = false;
  scbt.b.simulcastReceiver = false;
  chrome.storage.sync.set({'sz5b32': false}, function(error) { scbt.f.show_settings_error(error); })
  chrome.storage.sync.set({'sz5v32': ''}, function(error) { scbt.f.show_settings_error(error); })
  setTimeout(function(){ scbt.f.toast('Error: Not a valid livestream'); }, 2700);
  return false;
}
scbt.f.sz5b32_on = function(){
  var url = '';
  if ( location.href.indexOf('?') > -1) {
    url = location.href + '&p=' + scbt.v.serviceid + '-' + scbt.v.channelid;
  } else {
    url = location.href + '/?p=' + scbt.v.serviceid + '-' + scbt.v.channelid;
  }
  document.body.getElementsByClassName('sz5b32')[0].checked = true;
  document.body.getElementsByClassName('sz5v32')[0].value = url;
  scbt.o.settingsFromSync.sz5b32 = true;
  scbt.o.settingsFromSync.sz5v32 = url;
  scbt.b.simulcastSender = true;
  scbt.b.simulcastReceiver = false;
  chrome.storage.sync.set({'sz5b32': true}, function(error) { scbt.f.show_settings_error(error); })
  chrome.storage.sync.set({'sz5v32': url}, function(error) { scbt.f.show_settings_error(error); })
  console.log('- this tab is the sender2: ' + url);
  return false;
}

scbt.f.sz5b33_off = function(){
  document.body.getElementsByClassName('sz5b33')[0].checked = false;
  document.body.getElementsByClassName('sz5v33')[0].value = '';
  scbt.o.settingsFromSync.sz5b33 = false;
  scbt.o.settingsFromSync.sz5v33 = '';
  scbt.b.simulcastSender = false;
  scbt.b.simulcastReceiver = false;
  chrome.storage.sync.set({'sz5b33': false}, function(error) { scbt.f.show_settings_error(error); })
  chrome.storage.sync.set({'sz5v33': ''}, function(error) { scbt.f.show_settings_error(error); })
  setTimeout(function(){ scbt.f.toast('Error: Not a valid livestream'); }, 2700);
  return false;
}
scbt.f.sz5b33_on = function(){
  var url = '';
  if ( location.href.indexOf('?') > -1) {
    url = location.href + '&p=' + scbt.v.serviceid + '-' + scbt.v.channelid;
  } else {
    url = location.href + '/?p=' + scbt.v.serviceid + '-' + scbt.v.channelid;
  }
  document.body.getElementsByClassName('sz5b33')[0].checked = true;
  document.body.getElementsByClassName('sz5v33')[0].value = url;
  scbt.o.settingsFromSync.sz5b33 = true;
  scbt.o.settingsFromSync.sz5v33 = url;
  scbt.b.simulcastSender = true;
  scbt.b.simulcastReceiver = false;
  chrome.storage.sync.set({'sz5b33': true}, function(error) { scbt.f.show_settings_error(error); })
  chrome.storage.sync.set({'sz5v33': url}, function(error) { scbt.f.show_settings_error(error); })
  console.log('- this tab is the sender3: ' + url);
  return false;
}


scbt.f.set_settings_changes = async function(request){
  console.log("LOOK AT THESE CHANGES", request.scbtChanged.changes);
    var obj = {};
    if (
      request.scbtChanged.changes.sz5b8 || 
      request.scbtChanged.changes.sz5b9 || 
      request.scbtChanged.changes.sz5b11 
    ) {
      location.reload();
    }

  // receiver
  if (request.scbtChanged.changes.sz5b30) {
    if (request.scbtChanged.changes.sz5b30.newValue === true) {
      var isValid = await scbt.f.get_binary_if_page_valid(scbt.v.lastPage);
      console.log('get_binary_if_page_valid is ', isValid);
      if (!isValid) {
        console.log('not valid 1');
        scbt.f.sz5b30_off();
        return false;
      }
      if (location.href.indexOf('/video') > -1) {
        console.log('not valid 2');
        scbt.f.sz5b30_off();
        return false;
      }
      var url = '';
      if ( location.href.indexOf('?') > -1) {
        url = location.href + '&p=' + scbt.v.serviceid + '-' + scbt.v.channelid;
      } else {
        url = location.href + '/?p=' + scbt.v.serviceid + '-' + scbt.v.channelid;
      }
      if ( 
        (url == scbt.o.settingsFromSync.sz5v31) || 
        (url == scbt.o.settingsFromSync.sz5v32) || 
        (url == scbt.o.settingsFromSync.sz5v33) 
      ) {
        console.log('not valid 3');
        scbt.f.sz5b30_off();
        return false;
      }
      scbt.f.sz5b30_on();
    }
    if (request.scbtChanged.changes.sz5b30.newValue === false) {
      console.log('not valid 4');
      scbt.f.sz5b30_off();
      return false;
    }
  }

  // sender sz5v31
  if (request.scbtChanged.changes.sz5b31) {
    if (request.scbtChanged.changes.sz5b31.newValue === true) {
      var isValid = await scbt.f.get_binary_if_page_valid(scbt.v.lastPage);
      console.log('get_binary_if_page_valid is ', isValid);
      if (!isValid) {
        console.log('not valid 1');
        scbt.f.sz5b31_off();
        return false;
      }
      if (location.href.indexOf('/video') > -1) {
        console.log('not valid 2');
        scbt.f.sz5b31_off();
        return false;
      }
      var url = '';
      if ( location.href.indexOf('?') > -1) {
        url = location.href + '&p=' + scbt.v.serviceid + '-' + scbt.v.channelid;
      } else {
        url = location.href + '/?p=' + scbt.v.serviceid + '-' + scbt.v.channelid;
      }
      if ( 
        (url == scbt.o.settingsFromSync.sz5v30) || 
        (url == scbt.o.settingsFromSync.sz5v32) || 
        (url == scbt.o.settingsFromSync.sz5v33) 
      ) {
        console.log('not valid 3');
        scbt.f.sz5b31_off();
        return false;
      }
      scbt.f.sz5b31_on();
    }
    if (request.scbtChanged.changes.sz5b31.newValue === false) {
      console.log('not valid 4');
      scbt.f.sz5b31_off();
      return false;
    }
  }

  // sender sz5v32
  if (request.scbtChanged.changes.sz5b32) {
    if (request.scbtChanged.changes.sz5b32.newValue === true) {
      var isValid = await scbt.f.get_binary_if_page_valid(scbt.v.lastPage);
      console.log('get_binary_if_page_valid is ', isValid);
      if (!isValid) {
        console.log('not valid 1');
        scbt.f.sz5b32_off();
        return false;
      }
      if (location.href.indexOf('/video') > -1) {
        console.log('not valid 2');
        scbt.f.sz5b32_off();
        return false;
      }
      var url = '';
      if ( location.href.indexOf('?') > -1) {
        url = location.href + '&p=' + scbt.v.serviceid + '-' + scbt.v.channelid;
      } else {
        url = location.href + '/?p=' + scbt.v.serviceid + '-' + scbt.v.channelid;
      }
      if ( 
        (url == scbt.o.settingsFromSync.sz5v31) || 
        (url == scbt.o.settingsFromSync.sz5v30) || 
        (url == scbt.o.settingsFromSync.sz5v33) 
      ) {
        console.log('not valid 3');
        scbt.f.sz5b32_off();
        return false;
      }
      scbt.f.sz5b32_on();
    }
    if (request.scbtChanged.changes.sz5b32.newValue === false) {
      console.log('not valid 4');
      scbt.f.sz5b32_off();
      return false;
    }
  }

  // sender sz5v33
  if (request.scbtChanged.changes.sz5b33) {
    if (request.scbtChanged.changes.sz5b33.newValue === true) {
      var isValid = await scbt.f.get_binary_if_page_valid(scbt.v.lastPage);
      console.log('get_binary_if_page_valid is ', isValid);
      if (!isValid) {
        console.log('not valid 1');
        scbt.f.sz5b33_off();
        return false;
      }
      if (location.href.indexOf('/video') > -1) {
        console.log('not valid 2');
        scbt.f.sz5b33_off();
        return false;
      }
      var url = '';
      if ( location.href.indexOf('?') > -1) {
        url = location.href + '&p=' + scbt.v.serviceid + '-' + scbt.v.channelid;
      } else {
        url = location.href + '/?p=' + scbt.v.serviceid + '-' + scbt.v.channelid;
      }
      if ( 
        (url == scbt.o.settingsFromSync.sz5v31) || 
        (url == scbt.o.settingsFromSync.sz5v32) || 
        (url == scbt.o.settingsFromSync.sz5v30) 
      ) {
        console.log('not valid 3');
        scbt.f.sz5b33_off();
        return false;
      }
      scbt.f.sz5b33_on();
    }
    if (request.scbtChanged.changes.sz5b33.newValue === false) {
      console.log('not valid 4');
      scbt.f.sz5b33_off();
      return false;
    }
  }



  // chat auto show on video
  if (request.scbtChanged.changes.sz5b15) {
    if (request.scbtChanged.changes.sz5b15.newValue === true) {
      chrome.storage.sync.set({'sz5b15': true }, function(error) { scbt.f.chat_auto_show(); })
    }
    if (request.scbtChanged.changes.sz5b15.newValue === false) {
      chrome.storage.sync.set({'sz5b15': false }, function(error) { window.location.reload(); })
    }
  }

  // simulcast chat
  if (request.scbtChanged.changes.sz5b10) {
    if (request.scbtChanged.changes.sz5b10.newValue === true) {
      scbt.o.settingsFromSync.sz5b10 = true;
      chrome.storage.sync.set({'sz5b10': true }, function(error) { scbt.f.toggle_simulcast_chat(error); })
    }
    if (request.scbtChanged.changes.sz5b10.newValue === false) {
      scbt.o.settingsFromSync.sz5b10 = false;
      chrome.storage.sync.set({'sz5b10': false }, function(error) { scbt.f.toggle_simulcast_chat(error); })
    }
  }

  for (var [key, value] of Object.entries(request.scbtChanged.changes)) {
    if (typeof value === 'object') {
      // if (value.newValue) {
        scbt.o.settingsFromSync[key] = value.newValue;
        obj[key] = value.newValue;
        scbt.f.style_with_obj_of_changes(obj);
      // }
    }
  }
  return false;
}


// START ENDING 
scbt.f.script_js_loaded = function(){
 return false;
}


scbt.f.handle_js_loading_error = function(e){
  console.error('Parleystar script loading error');
  console.error(e);
  return false;
}


scbt.f.set_page_redirect_to_home = function(U){
 if ( 
  (scbt.o.settingsFromSync.sz4b15 > 0 && U == 'https://kick.com/categories/alternative/other-tv-shows-movies') ||  
  (scbt.o.settingsFromSync.sz4b16 > 0 && U == 'https://kick.com/categories/alternative/just-sleeping') || 
  (scbt.o.settingsFromSync.sz4b17 > 0 && U == 'https://kick.com/categories/irl/asmr') || 
  (scbt.o.settingsFromSync.sz4b18 > 0 && U == 'https://kick.com/categories/games/body-art') || 
  (scbt.o.settingsFromSync.sz4b19 > 0 && U == 'https://kick.com/categories/gambling/slots') || 
  (scbt.o.settingsFromSync.sz4b20 > 0 && U == 'https://kick.com/categories/irl/pools-hot-tubs-bikinis') || 
  (scbt.o.settingsFromSync.sz4b21 > 0 && U == 'https://kick.com/categories/irl/just-chatting') 
  ) {
  window.location.replace('https://kick.com/');
 }
 return false;
}


scbt.f.do_loading_event_2 = function(){
  setTimeout(function() { scbt.f.loading_event_2(); return false; }, 3000);
}


scbt.f.loading_event_1 = async function(eType, U){
  console.log('in function loading_event_1 the lastpage is: ' + scbt.v.lastPage + ' and current URL is: ' + U);
  console.log("------------ start loading_event_1 -------------- with lastPage, etype, currentPage");
  console.log(scbt.v.lastPage);
  console.log(eType);
  console.log(U);
  console.log('------------ end loading_event_1 --------------');

  if ( (scbt.v.lastPage != U) && (U.indexOf('live_chat') == -1) ) {
    scbt.v.lastPage = U;
    scbt.f.get_str_serviceid();
    scbt.f.set_page_values_to_reset();

    var isValid = await scbt.f.get_binary_if_page_valid(scbt.v.lastPage);
    if (isValid === true) {
      await scbt.f.set_page_values_from_url(scbt.v.lastPage);  
    }
    var sending = null;
    console.info('loading_event_1 last page is not current page, eType: ' + eType + ' serviceid: ' + scbt.v.serviceid + ' readyState: ' + document.readyState + ' URL: ' + U);

    if (scbt.o.settingsFromSync === false) {
      scbt.o.settingsFromSync = await chrome.storage.sync.get();
      // console.log(' $$$$ scbt.o.settingsFromSync: ', scbt.o.settingsFromSync);
      if (scbt.o.settingsFromSync.simulcastReceiverTabId) {
        scbt.v.simulcastReceiverTabId = scbt.o.settingsFromSync.simulcastReceiverTabId;
      }

      // load javascript files
      if (scbt.o.settingsFromSync.sz5b3 === true) {
        if (!scbt.b.sz5b3Loaded) {
          sending = chrome.runtime.sendMessage({loadServiceJS: 'filters'});
          scbt.b.sz5b3Loaded = true;
          sending.then(scbt.f.script_js_loaded, scbt.f.handle_js_loading_error);
        }
      }
      if (scbt.o.settingsFromSync.sz5b4 === true) {
        if (!scbt.b.sz5b4Loaded) {
          sending = chrome.runtime.sendMessage({loadServiceJS: 'commands'});
          scbt.b.sz5b4Loaded = true;
          scbt.b.keybindOn = true;
          sending.then(scbt.f.script_js_loaded, scbt.f.handle_js_loading_error);
        }
      }
      if (scbt.o.settingsFromSync.sz5b1 === true) {
        if (!scbt.b.sz5b1Loaded) {
          sending = chrome.runtime.sendMessage({loadServiceJS: 'db'});
          scbt.b.sz5b1Loaded = true;
          sending.then(scbt.f.script_js_loaded, scbt.f.handle_js_loading_error);
        }
      }
      if (scbt.o.settingsFromSync.sz5b2 === true) {
        if (!scbt.b.sz5b2Loaded) {
          sending = chrome.runtime.sendMessage({loadServiceJS: 'simulcast'});
          scbt.b.sz5b2Loaded = true;
          sending.then(scbt.f.script_js_loaded, scbt.f.handle_js_loading_error);
        }
      }
      if (scbt.o.settingsFromSync.sz5b5 === true) {
        if (!scbt.b.sz5b5Loaded) {
          sending = chrome.runtime.sendMessage({loadServiceJS: 'voice'});
          scbt.b.sz5b5Loaded = true;
          sending.then(scbt.f.script_js_loaded, scbt.f.handle_js_loading_error);
        }
      }
      scbt.f.set_page_redirect_to_home(U);
    } else {
      if (scbt.v.serviceid == 'kick') {
        scbt.f.set_page_redirect_to_home(U);
        setTimeout(function() {
          scbt.f.hide_mature_elements_on_page_load();
        }, 1000);
      }
    }

    

    if (scbt.v.serviceid === 'arena') {
      if (!scbt.b.arenaLoaded) {
        var sending = chrome.runtime.sendMessage({loadServiceJS: 'arena'});
        scbt.b.arenaLoaded = true;
        scbt.f.do_loading_event_2();
      } else {
        // do arena functions
        scbt.f.do_loading_event_2();
      }
    }
    if (scbt.v.serviceid === 'instagram') {
      if (!scbt.b.instagramLoaded) {
        var sending = chrome.runtime.sendMessage({loadServiceJS: 'instagram'});
        scbt.b.instagramLoaded = true;
        scbt.f.do_loading_event_2();
      } else {
        // do instagram functions
        scbt.f.do_loading_event_2();
      }
    }
    if (scbt.v.serviceid === 'kick') {
      if (!scbt.b.kickLoaded) {
        var sending = chrome.runtime.sendMessage({loadServiceJS: 'kick'});
        scbt.b.kickLoaded = true;
        setTimeout(function() {
          scbt.f.loading_event_2();
          // initUI();
        }, 2000);
      } else {
        // do kick functions
        scbt.f.do_loading_event_2();
      }
    }
    if (scbt.v.serviceid === 'noice') {
      if (!scbt.b.noiceLoaded) {
        var sending = chrome.runtime.sendMessage({loadServiceJS: 'noice'});
        scbt.b.noiceLoaded = true;
        scbt.f.do_loading_event_2();
      } else {
        // do noice functions
        scbt.f.do_loading_event_2();
      }
    }
    if (scbt.v.serviceid === 'odysee') {
      if (!scbt.b.odyseeLoaded) {
        var sending = chrome.runtime.sendMessage({loadServiceJS: 'odysee'});
        scbt.b.odyseeLoaded = true;
        scbt.f.do_loading_event_2();
      } else {
        // do odysee functions
        scbt.f.do_loading_event_2();
      }
    }
    if (scbt.v.serviceid === 'rumble') {
      if (!scbt.b.rumbleLoaded) {
        var sending = chrome.runtime.sendMessage({loadServiceJS: 'rumble'});
        scbt.b.rumbleLoaded = true;
        scbt.f.do_loading_event_2();
      } else {
        // do rumble functions
        scbt.f.do_loading_event_2();
      }
    }
    if (scbt.v.serviceid === 'shareplay') {
      if (!scbt.b.shareplayLoaded) {
        var sending = chrome.runtime.sendMessage({loadServiceJS: 'shareplay'});
        scbt.b.shareplayLoaded = true;
        scbt.f.do_loading_event_2();
      } else {
        // do shareplay functions
        scbt.f.do_loading_event_2();
      }
    }
    if (scbt.v.serviceid === 'tiktok') {
      if (!scbt.b.tiktokLoaded) {
        var sending = chrome.runtime.sendMessage({loadServiceJS: 'tiktok'});
        scbt.b.tiktokLoaded = true;
        scbt.f.do_loading_event_2();
      } else {
        // do tiktok functions
        scbt.f.do_loading_event_2();
      }
    }
    if (scbt.v.serviceid === 'twitch') {
      if (!scbt.b.twitchLoaded) {
        var sending = chrome.runtime.sendMessage({loadServiceJS: 'twitch'});
        scbt.b.twitchLoaded = true;
        scbt.f.do_loading_event_2();
      } else {
        // do twitch functions
        scbt.f.do_loading_event_2();
      }
    }
    if (scbt.v.serviceid === 'twitter') {
      if (!scbt.b.twitterLoaded) {
        var sending = chrome.runtime.sendMessage({loadServiceJS: 'twitter'});
        scbt.b.twitterLoaded = true;
        scbt.f.do_loading_event_2();
      } else {
        // do twitter functions
        scbt.f.do_loading_event_2();
      }
    }
    if (scbt.v.serviceid === 'youtube') {
      if (!scbt.b.youtubeLoaded) {
        var sending = chrome.runtime.sendMessage({loadServiceJS: 'youtube'});
        scbt.b.youtubeLoaded = true;
        scbt.f.do_loading_event_2();
      } else {
        // do youtube functions .none
        scbt.f.do_loading_event_2();
      }
    }

    if (scbt.v.serviceid === 'archyved') {
      var elemArr = document.getElementsByTagName('main');
      if (elemArr[0]) {
        elemArr[0].classList.add('none');
      }
      scbt.f.do_loading_event_2();
    }
  }
  eType = e = sending = U = elemArr = null; return false;
}


scbt.f.loading_event_2 = async function(){
  if (location.href.indexOf('live_chat') > -1) { return false; }
  console.log(' * doing loading_event_2 A ');
  var menuExists = await scbt.f.build_top_x_menu();
  if (menuExists === true) {
    scbt.v.channelid = await scbt.f.get_str_channelid();
console.log(' * doing loading_event_2 B * ' + ' serviceid: ' + scbt.v.serviceid + ' channelid: ' + scbt.v.channelid + ' videoid: ' + scbt.v.videoid + ' isVOD: ' + scbt.b.vod + ' mobile: ' + scbt.b.mobile + ' channelPage: ' + scbt.b.channelPage);
    if (scbt.v.serviceid === 'youtube' && scbt.b.mobile === false) {
      var myInterval = setInterval(function () {
        var elemArr = document.body.querySelectorAll('iframe#chatframe');
        if (elemArr[0]) {
          if (elemArr[0].contentWindow && elemArr[0].contentWindow.document && elemArr[0].contentWindow.document.body) {
            clearInterval(myInterval);
            scbt.b.vod = false;
            scbt.e.scbtYTChatIframe = elemArr[0].contentWindow.document;
            console.log('in loading_event_2 elemarr is ', scbt.e.scbtYTChatIframe);
            elemArr = null;
            scbt.f.loading_event_3();
          }
        }
      }, 1000);
    } else {
      scbt.f.loading_event_3();
    }
  }
  menuExists = null;
}


scbt.f.loading_event_3 = async function(){
  if (location.href.indexOf('live_chat') > -1) { return false; }
  console.log('doing loading_event_3 1');
  var isSaving = false;
  scbt.f.get_arr_of_all_dbNames();
  var elemArr = scbt.f.get_arr_chats();
  console.log('doing loading_event_3 chats', elemArr);

  [].forEach.call(elemArr, function(elm) {
    scbt.f.process_chat_line(elm, isSaving);
  });

  var elemArr2 = scbt.f.get_arr_video_elem(); // if there is a video element
  console.log('doing loading_event_3 video', elemArr2);
  var elemArr3 = scbt.f.get_arr_chatbox_elem(); // if there is a chatbox element
  console.log('doing loading_event_3 chatbox', elemArr3);
  console.log('doing loading_event_3 serviceid: ' + scbt.v.serviceid + ' channelid: ' + scbt.v.channelid + ' videoid: ' + scbt.v.videoid + ' Load: ' + scbt.o.settingsFromSync.sz5b1 + ' auto: ' + scbt.o.settingsFromSync.sz5b21 );

  if ( (elemArr2[0] && elemArr3[0]) || (scbt.b.popout === true) ) {
    console.log('doing loading_event_3 2');
    if (
      scbt.v.serviceid && 
      scbt.v.channelid && 
      scbt.v.videoid && 
      scbt.o.settingsFromSync.sz5b1 === true && 
      scbt.o.settingsFromSync.sz5b21 === true
    ) {
      console.log('doing loading_event_3 3');
      isSaving = true;
      scbt.f.get_str_dbName(isSaving);
      } else {
        console.log('doing loading_event_3 4');
        scbt.f.chat_listen(isSaving);
    }
  }  
  elemArr = elm = elemArr2 = elemArr3 = isSaving = null; return false;
}


// ENDING
console.info('CHANGE EVENT 1 script running ' + document.readyState + ' ' + location.href);
window.addEventListener('unhandledrejection', e => {
  console.log('Browser error: unhandled rejection: ');
  var request = e.target; 
  var error = e.reason; 
  console.error({ request, error });
  console.error(e);
  e = request = error = null;
});

// kick on first page load, not clicking around
// odysee on first page load, not clicking around
// rumble every click/page
// twitch on first page load, not clicking around
// youtube on first page load, not clicking around
window.addEventListener('load', function(e) {
  if (location.href.indexOf('live_chat') == -1) {
    console.info('CHANGE EVENT -- 2 window on load ' + document.readyState + ' ' + location.href, e);
    scbt.f.loading_event_1('load', location.href);
  }
  e = null; return false;
});

// back button all except rumble
window.addEventListener('popstate', function (e) {
  if (location.href.indexOf('live_chat') == -1) {
    console.info('CHANGE EVENT -- 3 window on popstate ' + document.readyState + ' ' + location.href, e);
    scbt.f.loading_event_1('popstate', location.href);
  }
  e = null; return false;
});

// youtube
document.addEventListener('transitionend', function(e) {
  if (event.target.id === 'progress') {
    if (location.href.indexOf('live_chat') == -1) {
      console.info('CHANGE EVENT -- 4 document on transitionend ' + document.readyState + ' ' + location.href, e);
      scbt.f.loading_event_1('transitionend', location.href);
    }
    e = null; return false;
  }
});

// kick on first page load, not clicking around
// odysee on first page load, not clicking around
// rumble every click/page
// twitch on first page load, not clicking around
// youtube on first page load, not clicking around
document.addEventListener('readystatechange', function(e) {
  if (location.href.indexOf('live_chat') == -1) {
    console.info('CHANGE EVENT -- 4 document on readystatechange ' + document.readyState + ' ' + location.href, e);
    scbt.f.loading_event_1('readystatechange', location.href);
  }
  e = null; return false;
});


chrome.runtime.onMessage.addListener( function(request, sender, goCapture) {
  console.log("request is: ", request);

  if (typeof request === 'object') {
    if (request.tabUpdated) {
      console.log('tabUpdated', request.tabUpdated);
    }
    if (request.clickedTab) {
      console.log('clickedTab', request.clickedTab);
    }
    if (request.simulcastReceiverTabId) {
      scbt.v.simulcastReceiverTabId = request.simulcastReceiverTabId;
      chrome.storage.sync.set({'simulcastReceiverTabId': request.simulcastReceiverTabId}, function(error) { scbt.f.show_settings_error(error); })
      return false;
    }
    if ( (location.href.indexOf('live_chat') == -1) && 
      request.scbtChanged &&
      request.scbtChanged.area && 
      request.scbtChanged.area == "sync" && 
      request.scbtChanged.changes && 
      typeof request.scbtChanged.changes === 'object'
    ) {
      scbt.f.set_settings_changes(request);
    }

    if ( request.tabUpdated && (typeof request.tabUpdated === 'object') ) {
      if (!request.tabUpdated.url) { return false; }
      if ( (scbt.v.lastPage == request.tabUpdated.url) || (request.tabUpdated.status != 'complete') || ( request.tabUpdated.url.indexOf('live_chat') !== -1) ) {
        // page didnt change, just updated
        console.warn('NO NO NO 0 0 0 0 X X X PAGE DIDNT CHANGE ' + request.tabUpdated.status + ' ' + request.tabUpdated.title + ' ' + request.tabUpdated.url);
        return false;
      }
      console.info('REQUEST PAGE CHANGE ' + request.tabUpdated.status + ' $ ' + request.tabUpdated.title + ' $ ' + request.tabUpdated.url + ' $ ' + scbt.v.serviceid);
      scbt.f.loading_event_1('tabudated', request.tabUpdated.url);
    } else {
  // not tabupdated, probably 'loaded' message
  console.log(request, location.href);
  if (scbt.b.keybindOn === true) {
    if (request.message === 'streamcommand1') {
      scbt.f.user_command1();
    }
    if (request.message === 'streamcommand2') {
      console.log('streamcommand2 from foreground');
      scbt.f.user_command2();
    }
    if (request.message === 'streamcommand3') {
      console.log('streamcommand3 from foreground');
      scbt.f.user_command3();
    }
    if (request.message === 'streamcommand4') {
      console.log('streamcommand4 from foreground');
      scbt.f.user_command4();
    }
  }

  if (request.message) {
    if (request.message.message) {
      console.log('service', request.message.service);
      console.log('channelid', request.message.channelid);
      if ( (request.message.service == scbt.o.settingsFromSync.scbtservice1) && (request.message.channelid == scbt.o.settingsFromSync.scbtchannelid1) ) {
        var theHTML = scbt.f.build_chat_line_from_obj(request.message);
        scbt.e.scbtSimulcastChat1.insertAdjacentHTML('beforeend', theHTML);
        scbt.e.scbtDragSpot1.scrollTop = scbt.e.scbtDragSpot1.scrollHeight - scbt.e.scbtDragSpot1.clientHeight;
      }
      if ( (request.message.service == scbt.o.settingsFromSync.scbtservice2) && (request.message.channelid == scbt.o.settingsFromSync.scbtchannelid2) ) {
        var theHTML = scbt.f.build_chat_line_from_obj(request.message);
        scbt.e.scbtSimulcastChat2.insertAdjacentHTML('beforeend', theHTML);
        scbt.e.scbtDragSpot2.scrollTop = scbt.e.scbtDragSpot2.scrollHeight - scbt.e.scbtDragSpot2.clientHeight;
      }
      if ( (request.message.service == scbt.o.settingsFromSync.scbtservice3) && (request.message.channelid == scbt.o.settingsFromSync.scbtchannelid3) ) {
        var theHTML = scbt.f.build_chat_line_from_obj(request.message);
        scbt.e.scbtSimulcastChat3.insertAdjacentHTML('beforeend', theHTML);
        scbt.e.scbtDragSpot3.scrollTop = scbt.e.scbtDragSpot3.scrollHeight - scbt.e.scbtDragSpot3.clientHeight;
      }

    }
  }

  if (request.streamChatToArchive && request.streamChatToArchive.streamForOffline) {
    scbt.f.set_chat_arr_to_database(request.streamChatToArchive.streamForOffline);
    return false;
  } // request.streamChatToArchive.streamForOffline
} // if not request.tabUpdated

}
});